﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class SelectZumenBan : BaseView
    {
        public static List<String> zuBanList;
        public static string zumenBan { get; set; }
        public static bool selZubanFlg { get; set; }

        public SelectZumenBan()
        {
            InitializeComponent();
        }

        // ロード時処理
        private void SelectZumenBan_Load(object sender, EventArgs e)
        {
            try
            {
                selZubanFlg = false;
                // 図面番号一覧作成
                foreach (var zuban in zuBanList)
                {
                    zumenBanList.Items.Add(zuban);
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        //図面番号リストダブルクリック時処理
        private void zumenBanList_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (zumenBanList.SelectedItem == null)
                {
                    return;
                }
                // 選択項目を取得する
                zumenBan = zumenBanList.SelectedItem.ToString();
                if (zumenBan != null && zumenBan != "")
                {
                    selZubanFlg = true;
                }
                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
