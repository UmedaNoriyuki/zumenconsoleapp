﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class SelectOrderNo : BaseView
    {
        public static List<String> ordList;
        public static string orderNo { get; set; }
        public static bool selOrdFlg { get; set; }

        public SelectOrderNo()
        {
            InitializeComponent();
        }

        // ロード時処理
        private void SelectOrderNo_Load(object sender, EventArgs e)
        {
            try
            {
                selOrdFlg = false;
                // オーダーNO一覧作成
                foreach (var orderNo in ordList)
                {
                    orderNoList.Items.Add(orderNo);
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        //オーダーNOリストダブルクリック時処理
        private void orderNoList_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (orderNoList.SelectedItem == null)
                {
                    return;
                }
                // 選択項目を取得する
                orderNo = orderNoList.SelectedItem.ToString();
                if (orderNo != null && orderNo != "")
                {
                    selOrdFlg = true;
                }
                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }
    }
}
