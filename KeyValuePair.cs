﻿namespace ZumenConsoleApp
{
    public class KeyValuePairDto
    {
        int key;
        string val;

        /// <summary>
        /// キー
        /// </summary>
        public int Key
        {
            get { return key; }
            set { key = value; }
        }

        /// <summary>
        /// 値
        /// </summary>
        public string Value
        {
            get { return val; }
            set { val = value; }
        }

        /// <summary>
        /// 表示される値
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return val;
        }
    }
}
