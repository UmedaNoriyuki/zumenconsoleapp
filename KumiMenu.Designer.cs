﻿namespace ZumenConsoleApp.View
{
    partial class KumiMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalcWorkTime = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCalcWorkTime
            // 
            this.btnCalcWorkTime.BackColor = System.Drawing.Color.GreenYellow;
            this.btnCalcWorkTime.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCalcWorkTime.Location = new System.Drawing.Point(66, 35);
            this.btnCalcWorkTime.Name = "btnCalcWorkTime";
            this.btnCalcWorkTime.Size = new System.Drawing.Size(140, 70);
            this.btnCalcWorkTime.TabIndex = 50;
            this.btnCalcWorkTime.Text = "作業時間計算";
            this.btnCalcWorkTime.UseVisualStyleBackColor = false;
            this.btnCalcWorkTime.Click += new System.EventHandler(this.btnCalcWorkTime_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Red;
            this.btnStop.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnStop.Location = new System.Drawing.Point(238, 35);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(140, 70);
            this.btnStop.TabIndex = 51;
            this.btnStop.Text = "パーツリスト";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnPartsListDisp_Click);
            // 
            // KumiMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 395);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnCalcWorkTime);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "KumiMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "組立メニュー";
            this.Controls.SetChildIndex(this.btnCalcWorkTime, 0);
            this.Controls.SetChildIndex(this.btnStop, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCalcWorkTime;
        private System.Windows.Forms.Button btnStop;
    }
}