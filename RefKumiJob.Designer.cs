﻿namespace ZumenConsoleApp.View
{
    partial class RefKumiJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kumiJobList = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // kumiJobList
            // 
            this.kumiJobList.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.kumiJobList.HideSelection = false;
            this.kumiJobList.LabelWrap = false;
            this.kumiJobList.Location = new System.Drawing.Point(37, 33);
            this.kumiJobList.MultiSelect = false;
            this.kumiJobList.Name = "kumiJobList";
            this.kumiJobList.Size = new System.Drawing.Size(944, 542);
            this.kumiJobList.TabIndex = 0;
            this.kumiJobList.TabStop = false;
            this.kumiJobList.UseCompatibleStateImageBehavior = false;
            this.kumiJobList.View = System.Windows.Forms.View.List;
            this.kumiJobList.DoubleClick += new System.EventHandler(this.kumiJobList_DoubleClick);
            // 
            // RefKumiJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 618);
            this.Controls.Add(this.kumiJobList);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RefKumiJob";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "工数参照";
            this.Load += new System.EventHandler(this.RefKumiJob_Load);
            this.Controls.SetChildIndex(this.kumiJobList, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView kumiJobList;
    }
}