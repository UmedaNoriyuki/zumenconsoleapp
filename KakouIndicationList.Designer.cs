﻿namespace ZumenConsoleApp
{
    partial class KakouIndicationList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvKakouList = new System.Windows.Forms.DataGridView();
            this.工程表番号 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.オーダーNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.図面番号 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.機種名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.現在工程 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.工程1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbKakou1 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSearchNowKoutei = new System.Windows.Forms.Button();
            this.cmbKoutei1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtZumenNo = new System.Windows.Forms.TextBox();
            this.txtOdrNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSearchZentai = new System.Windows.Forms.Button();
            this.cmbKoutei2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbKakou2 = new System.Windows.Forms.ComboBox();
            this.countLabel = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCalendarColumn1 = new ZumenConsoleApp.DataGridViewCalendarColumn();
            this.dataGridViewCalendarColumn2 = new ZumenConsoleApp.DataGridViewCalendarColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSearchZanKoutei = new System.Windows.Forms.Button();
            this.cmbKoutei3 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbKakou3 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKakouList)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvKakouList
            // 
            this.dgvKakouList.AllowUserToAddRows = false;
            this.dgvKakouList.AllowUserToDeleteRows = false;
            this.dgvKakouList.AllowUserToResizeColumns = false;
            this.dgvKakouList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgvKakouList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvKakouList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvKakouList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvKakouList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKakouList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvKakouList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKakouList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.工程表番号,
            this.オーダーNO,
            this.図面番号,
            this.機種名,
            this.現在工程,
            this.工程1});
            this.dgvKakouList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvKakouList.Location = new System.Drawing.Point(12, 250);
            this.dgvKakouList.MultiSelect = false;
            this.dgvKakouList.Name = "dgvKakouList";
            this.dgvKakouList.ReadOnly = true;
            this.dgvKakouList.RowHeadersVisible = false;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgvKakouList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvKakouList.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgvKakouList.RowTemplate.Height = 28;
            this.dgvKakouList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvKakouList.Size = new System.Drawing.Size(1376, 374);
            this.dgvKakouList.TabIndex = 14;
            this.dgvKakouList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellContentClickDgvKakouList);
            // 
            // 工程表番号
            // 
            this.工程表番号.DataPropertyName = "工程表番号";
            this.工程表番号.HeaderText = "工程表番号";
            this.工程表番号.Name = "工程表番号";
            this.工程表番号.ReadOnly = true;
            this.工程表番号.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.工程表番号.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.工程表番号.Width = 129;
            // 
            // オーダーNO
            // 
            this.オーダーNO.DataPropertyName = "オーダーNO";
            this.オーダーNO.HeaderText = "オーダーNO";
            this.オーダーNO.Name = "オーダーNO";
            this.オーダーNO.ReadOnly = true;
            this.オーダーNO.Width = 120;
            // 
            // 図面番号
            // 
            this.図面番号.DataPropertyName = "図面番号";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.図面番号.DefaultCellStyle = dataGridViewCellStyle3;
            this.図面番号.HeaderText = "図面番号";
            this.図面番号.Name = "図面番号";
            this.図面番号.ReadOnly = true;
            this.図面番号.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.図面番号.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.図面番号.Width = 110;
            // 
            // 機種名
            // 
            this.機種名.DataPropertyName = "機種名";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.機種名.DefaultCellStyle = dataGridViewCellStyle4;
            this.機種名.HeaderText = "機種名";
            this.機種名.Name = "機種名";
            this.機種名.ReadOnly = true;
            this.機種名.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.機種名.Width = 91;
            // 
            // 現在工程
            // 
            this.現在工程.DataPropertyName = "現在工程";
            this.現在工程.HeaderText = "現在工程";
            this.現在工程.Name = "現在工程";
            this.現在工程.ReadOnly = true;
            this.現在工程.Width = 110;
            // 
            // 工程1
            // 
            this.工程1.DataPropertyName = "工程1";
            this.工程1.HeaderText = "工程1";
            this.工程1.Name = "工程1";
            this.工程1.ReadOnly = true;
            this.工程1.Width = 82;
            // 
            // cmbKakou1
            // 
            this.cmbKakou1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKakou1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            this.cmbKakou1.FormattingEnabled = true;
            this.cmbKakou1.Location = new System.Drawing.Point(101, 30);
            this.cmbKakou1.Name = "cmbKakou1";
            this.cmbKakou1.Size = new System.Drawing.Size(218, 27);
            this.cmbKakou1.TabIndex = 17;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSearchNowKoutei);
            this.groupBox1.Controls.Add(this.cmbKoutei1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbKakou1);
            this.groupBox1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(332, 168);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "現在の工程で検索";
            // 
            // btnSearchNowKoutei
            // 
            this.btnSearchNowKoutei.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnSearchNowKoutei.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearchNowKoutei.Location = new System.Drawing.Point(227, 114);
            this.btnSearchNowKoutei.Name = "btnSearchNowKoutei";
            this.btnSearchNowKoutei.Size = new System.Drawing.Size(92, 44);
            this.btnSearchNowKoutei.TabIndex = 21;
            this.btnSearchNowKoutei.Text = "検索";
            this.btnSearchNowKoutei.UseVisualStyleBackColor = false;
            this.btnSearchNowKoutei.Click += new System.EventHandler(this.ClickBtnSearchNowKoutei);
            // 
            // cmbKoutei1
            // 
            this.cmbKoutei1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKoutei1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            this.cmbKoutei1.FormattingEnabled = true;
            this.cmbKoutei1.Location = new System.Drawing.Point(101, 72);
            this.cmbKoutei1.Name = "cmbKoutei1";
            this.cmbKoutei1.Size = new System.Drawing.Size(218, 27);
            this.cmbKoutei1.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 19);
            this.label2.TabIndex = 19;
            this.label2.Text = "作業工程";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 19);
            this.label1.TabIndex = 18;
            this.label1.Text = "依頼先";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtZumenNo);
            this.groupBox2.Controls.Add(this.txtOdrNo);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.btnSearchZentai);
            this.groupBox2.Controls.Add(this.cmbKoutei2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cmbKakou2);
            this.groupBox2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.Location = new System.Drawing.Point(713, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(698, 168);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "加工指示書全体で検索";
            // 
            // txtZumenNo
            // 
            this.txtZumenNo.Location = new System.Drawing.Point(438, 77);
            this.txtZumenNo.Name = "txtZumenNo";
            this.txtZumenNo.Size = new System.Drawing.Size(219, 26);
            this.txtZumenNo.TabIndex = 29;
            // 
            // txtOdrNo
            // 
            this.txtOdrNo.Location = new System.Drawing.Point(438, 30);
            this.txtOdrNo.Name = "txtOdrNo";
            this.txtOdrNo.Size = new System.Drawing.Size(219, 26);
            this.txtOdrNo.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(336, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 19);
            this.label6.TabIndex = 27;
            this.label6.Text = "図面番号";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(336, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 19);
            this.label5.TabIndex = 26;
            this.label5.Text = "オーダーNO";
            // 
            // btnSearchZentai
            // 
            this.btnSearchZentai.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnSearchZentai.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearchZentai.Location = new System.Drawing.Point(565, 114);
            this.btnSearchZentai.Name = "btnSearchZentai";
            this.btnSearchZentai.Size = new System.Drawing.Size(92, 44);
            this.btnSearchZentai.TabIndex = 25;
            this.btnSearchZentai.Text = "検索";
            this.btnSearchZentai.UseVisualStyleBackColor = false;
            this.btnSearchZentai.Click += new System.EventHandler(this.ClickBtnSearchAll);
            // 
            // cmbKoutei2
            // 
            this.cmbKoutei2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKoutei2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            this.cmbKoutei2.FormattingEnabled = true;
            this.cmbKoutei2.Location = new System.Drawing.Point(110, 72);
            this.cmbKoutei2.Name = "cmbKoutei2";
            this.cmbKoutei2.Size = new System.Drawing.Size(218, 27);
            this.cmbKoutei2.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 19);
            this.label3.TabIndex = 23;
            this.label3.Text = "作業工程";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 19);
            this.label4.TabIndex = 22;
            this.label4.Text = "依頼先";
            // 
            // cmbKakou2
            // 
            this.cmbKakou2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKakou2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            this.cmbKakou2.FormattingEnabled = true;
            this.cmbKakou2.Location = new System.Drawing.Point(110, 30);
            this.cmbKakou2.Name = "cmbKakou2";
            this.cmbKakou2.Size = new System.Drawing.Size(218, 27);
            this.cmbKakou2.TabIndex = 21;
            // 
            // countLabel
            // 
            this.countLabel.AutoSize = true;
            this.countLabel.Font = new System.Drawing.Font("メイリオ", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.countLabel.Location = new System.Drawing.Point(16, 193);
            this.countLabel.Name = "countLabel";
            this.countLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.countLabel.Size = new System.Drawing.Size(281, 44);
            this.countLabel.TabIndex = 28;
            this.countLabel.Text = "検索してください。";
            this.countLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "工程順";
            this.dataGridViewTextBoxColumn1.HeaderText = "工程順";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 120;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "工程表番号";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn2.HeaderText = "オーダーNO";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.Width = 87;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "機種名";
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn3.HeaderText = "機種名";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.Width = 84;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "機種名";
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn4.HeaderText = "機種名";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.Width = 84;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "工程1";
            this.dataGridViewTextBoxColumn5.HeaderText = "工程1";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 400;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "工程2";
            this.dataGridViewTextBoxColumn6.HeaderText = "工程2";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 400;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "工程3";
            this.dataGridViewTextBoxColumn7.HeaderText = "工程3";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 400;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "工程4";
            this.dataGridViewTextBoxColumn8.HeaderText = "工程4";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 400;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "工程5";
            this.dataGridViewTextBoxColumn9.HeaderText = "工程5";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 400;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "工程6";
            this.dataGridViewTextBoxColumn10.HeaderText = "工程6";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 400;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "工程7";
            this.dataGridViewTextBoxColumn11.HeaderText = "工程7";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 400;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "工程8";
            this.dataGridViewTextBoxColumn12.HeaderText = "工程8";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 400;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "工程9";
            this.dataGridViewTextBoxColumn13.HeaderText = "工程9";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 400;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "工程10";
            this.dataGridViewTextBoxColumn14.HeaderText = "工程10";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 400;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "工程11";
            this.dataGridViewTextBoxColumn15.HeaderText = "工程11";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 400;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "工程12";
            this.dataGridViewTextBoxColumn16.HeaderText = "工程12";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 400;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "工程13";
            this.dataGridViewTextBoxColumn17.HeaderText = "工程13";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 400;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "工程14";
            this.dataGridViewTextBoxColumn18.HeaderText = "工程14";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 400;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "工程15";
            this.dataGridViewTextBoxColumn19.HeaderText = "工程15";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 400;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "工程16";
            this.dataGridViewTextBoxColumn20.HeaderText = "工程16";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 400;
            // 
            // dataGridViewCalendarColumn1
            // 
            this.dataGridViewCalendarColumn1.DataPropertyName = "完了予定日";
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            this.dataGridViewCalendarColumn1.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewCalendarColumn1.HeaderText = "完了予定日";
            this.dataGridViewCalendarColumn1.Name = "dataGridViewCalendarColumn1";
            this.dataGridViewCalendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCalendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCalendarColumn1.Width = 160;
            // 
            // dataGridViewCalendarColumn2
            // 
            this.dataGridViewCalendarColumn2.DataPropertyName = "作業完了日";
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            this.dataGridViewCalendarColumn2.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewCalendarColumn2.HeaderText = "作業完了日";
            this.dataGridViewCalendarColumn2.Name = "dataGridViewCalendarColumn2";
            this.dataGridViewCalendarColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCalendarColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCalendarColumn2.Width = 160;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSearchZanKoutei);
            this.groupBox3.Controls.Add(this.cmbKoutei3);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.cmbKakou3);
            this.groupBox3.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox3.Location = new System.Drawing.Point(363, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(332, 168);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "残工程で検索";
            // 
            // btnSearchZanKoutei
            // 
            this.btnSearchZanKoutei.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnSearchZanKoutei.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearchZanKoutei.Location = new System.Drawing.Point(227, 114);
            this.btnSearchZanKoutei.Name = "btnSearchZanKoutei";
            this.btnSearchZanKoutei.Size = new System.Drawing.Size(92, 44);
            this.btnSearchZanKoutei.TabIndex = 21;
            this.btnSearchZanKoutei.Text = "検索";
            this.btnSearchZanKoutei.UseVisualStyleBackColor = false;
            this.btnSearchZanKoutei.Click += new System.EventHandler(this.ClickBtnSearchZanKoutei);
            // 
            // cmbKoutei3
            // 
            this.cmbKoutei3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKoutei3.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            this.cmbKoutei3.FormattingEnabled = true;
            this.cmbKoutei3.Location = new System.Drawing.Point(101, 72);
            this.cmbKoutei3.Name = "cmbKoutei3";
            this.cmbKoutei3.Size = new System.Drawing.Size(218, 27);
            this.cmbKoutei3.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 19);
            this.label7.TabIndex = 19;
            this.label7.Text = "作業工程";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 19);
            this.label8.TabIndex = 18;
            this.label8.Text = "依頼先";
            // 
            // cmbKakou3
            // 
            this.cmbKakou3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKakou3.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            this.cmbKakou3.FormattingEnabled = true;
            this.cmbKakou3.Location = new System.Drawing.Point(101, 30);
            this.cmbKakou3.Name = "cmbKakou3";
            this.cmbKakou3.Size = new System.Drawing.Size(218, 27);
            this.cmbKakou3.TabIndex = 17;
            // 
            // KakouIndicationList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1423, 647);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.countLabel);
            this.Controls.Add(this.dgvKakouList);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "KakouIndicationList";
            this.Text = "加工指示書一覧";
            this.Load += new System.EventHandler(this.LoadKakouIndicationList);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKakouList)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvKakouList;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewCalendarColumn dataGridViewCalendarColumn1;
        private DataGridViewCalendarColumn dataGridViewCalendarColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.ComboBox cmbKakou1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbKoutei1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbKoutei2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbKakou2;
        private System.Windows.Forms.Button btnSearchNowKoutei;
        private System.Windows.Forms.Button btnSearchZentai;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.Label countLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtZumenNo;
        private System.Windows.Forms.TextBox txtOdrNo;
        private System.Windows.Forms.DataGridViewButtonColumn 工程表番号;
        private System.Windows.Forms.DataGridViewTextBoxColumn オーダーNO;
        private System.Windows.Forms.DataGridViewButtonColumn 図面番号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 機種名;
        private System.Windows.Forms.DataGridViewTextBoxColumn 現在工程;
        private System.Windows.Forms.DataGridViewTextBoxColumn 工程1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSearchZanKoutei;
        private System.Windows.Forms.ComboBox cmbKoutei3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbKakou3;
    }
}