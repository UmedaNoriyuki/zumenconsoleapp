﻿using System.Runtime.InteropServices;
using System.Text;

namespace ZumenConsoleApp
{
    public class ClassINI
    {
        [DllImport("kernel32.dll")]
        private static extern int GetPrivateProfileString(
                                    string lpApplicationName,
                                    string lpKeyName,
                                    string lpDefault,
                                    StringBuilder lpReturnedstring,
                                    int nSize,
                                    string lpFileName);

        public static string GetIniValue(string path, string section, string key)
        {
            StringBuilder sb = new StringBuilder(512);
            GetPrivateProfileString(section, key, string.Empty, sb, sb.Capacity, path);
            return sb.ToString();
        }

    }
}
