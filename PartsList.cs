﻿
namespace ZumenConsoleApp
{
    public class PartsList
    {
        public string Plid { get; set; }
        public string Rownum { get; set; }
        public string OrderNo { get; set; }
        public string BlockNo { get; set; }
        public string ZumenBan { get; set; }
        public string Qty { get; set; }
        public string ProductName { get; set; }
        public string ZumenPath { get; set; }
        public string FileName { get; set; }
        public string kakoSaki { get; set; }
        public string Mitumori { get; set; }
        public string Mokuhyo { get; set; }
    }
}
