﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZumenConsoleApp
{
    class WorkingRegister
    {
        // No
        public int No { get; set; }

        // 図面番号
        public string DrawingNumber { get; set; }

        // 作業者
        public string Worker { get; set; }

        // 作業登録日
        public string WorkRegistrationDate { get; set; }

        // 工程表番号
        public string KouteihyouNo { get; set; }
    }
}
