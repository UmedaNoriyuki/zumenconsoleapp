﻿
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ZumenConsoleApp
{
    public partial class Attention : Form
    {
        #region クラス変数

        // 選択内容
        public string SelectValue = string.Empty;
        public string SelectKey = string.Empty;

        #endregion

        public Attention()
        {
            InitializeComponent();
        }

        #region イベント

        /// <summary>
        /// 画面起動時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadListBox(object sender, EventArgs e)
        {
            try
            {
                SetDataGridView();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 画面起動時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetDataGridView()
        {
            try
            {
                this.AttentionList.Items.Clear();
                List<KeyValuePairDto> list = new List<KeyValuePairDto>();
                DataTable dt = Get注意事項データ();

                foreach (DataRow dr in dt.Rows)
                {
                    KeyValuePairDto dto = new KeyValuePairDto
                    {
                        Key = int.Parse(dr["注意事項コード"].ToString()),
                        Value = dr["注意事項"].ToString()
                    };
                    list.Add(dto);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    this.AttentionList.Items.Add(list[i]);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 選択ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickSelectBtn(object sender, EventArgs e)
        {
            try
            {
                if (AttentionList.SelectedIndex >= 0)
                {
                    //アプリケーション終了後もクリップボードにデータを残しておく
                    Clipboard.SetDataObject(((KeyValuePairDto)AttentionList.SelectedItem).Value.ToString(), true);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("選択されていません。");
                    return;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        /// <summary>
        /// リストボックスダブルクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoubleClickListBox(object sender, EventArgs e)
        {
            try
            {
                if (AttentionList.SelectedIndex >= 0)
                {
                    //アプリケーション終了後もクリップボードにデータを残しておく
                    Clipboard.SetDataObject(((KeyValuePairDto)AttentionList.SelectedItem).Value.ToString(), true);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("選択されていません。");
                    return;
                }
            }
            catch
            {
                throw;
            }
        }

        private void ClickDelBtn(object sender, EventArgs e)
        {
            try
            {
                if (AttentionList.SelectedIndex >= 0)
                {
                    // 確認を行う
                    var result = MessageBox.Show(((KeyValuePairDto)AttentionList.SelectedItem).Value.ToString() + "の削除を行います。よろしいですか？", "確認",
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        // 削除処理
                        DeleteAttention(Convert.ToInt32(((KeyValuePairDto)AttentionList.SelectedItem).Key));
                        // データを取得
                        SetDataGridView();
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        private void DeleteAttention(int 注意事項コード)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                // トランザクション開始
                using (var tran = con.BeginTransaction())
                {
                    try
                    {
                        StringBuilder query = new StringBuilder();

                        query.AppendLine("DELETE");
                        query.AppendLine("  FROM 注意事項データ");
                        query.AppendLine(" WHERE 注意事項コード = :注意事項コード ");

                        NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                        cmd.Parameters.Add(new NpgsqlParameter("注意事項コード", NpgsqlDbType.Integer));
                        cmd.Parameters["注意事項コード"].Value = 注意事項コード;

                        cmd.ExecuteNonQuery();

                        tran.Commit();
                    }
                    catch (Exception e)
                    {
                        // ロールバック
                        tran.Rollback();
                        throw e;
                    }
                }
            }
        }

        private void ClickRegstBtn(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(attentionTxt.Text.Trim()))
                {
                    // 存在チェック
                    DataTable dt = Get注意事項データ(attentionTxt.Text);
                    if (dt.Rows.Count > 0)
                    {
                        MessageBox.Show("存在します。");
                        return;
                    }

                    // 確認を行う
                    var result = MessageBox.Show(attentionTxt.Text + "の登録を行います。よろしいですか？", "確認",
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        // 登録処理
                        RegistAttention(attentionTxt.Text);
                        // データを取得
                        SetDataGridView();
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        private DataTable Get注意事項データ()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("   注意事項コード ");
                    query.AppendLine("  ,注意事項 ");
                    query.AppendLine("  FROM 注意事項データ ");
                    query.AppendLine(" ORDER BY ");
                    query.AppendLine("   注意事項コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        private DataTable Get注意事項データ(string 注意事項)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("   注意事項コード ");
                    query.AppendLine("  ,注意事項 ");
                    query.AppendLine("  FROM 注意事項データ ");
                    query.AppendLine(" WHERE 注意事項 = :注意事項 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("注意事項", NpgsqlDbType.Varchar));
                    cmd.Parameters["注意事項"].Value = 注意事項;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }
        private void RegistAttention(string 注意事項)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                using (var trn = con.BeginTransaction())
                {
                    try
                    {
                        StringBuilder query = new StringBuilder();

                        query.AppendLine("INSERT ");
                        query.AppendLine("    INTO 注意事項データ (  ");
                        query.AppendLine("    注意事項コード ");
                        query.AppendLine("   ,注意事項 ");
                        query.AppendLine("   ,更新時刻 ");
                        query.AppendLine("   ,更新者) ");
                        query.AppendLine("  ");
                        query.AppendLine("  SELECT ");
                        query.AppendLine("     COUNT(注意事項コード) +1 ");
                        query.AppendLine("   ,:注意事項 ");
                        query.AppendLine("   ,:更新日時 ");
                        query.AppendLine("   ,:更新者 ");
                        query.AppendLine("   FROM 注意事項データ ");

                        NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                        cmd.Parameters.Add(new NpgsqlParameter("注意事項", NpgsqlDbType.Varchar));
                        cmd.Parameters["注意事項"].Value = 注意事項;

                        cmd.Parameters.Add(new NpgsqlParameter("更新日時", NpgsqlDbType.Timestamp));
                        cmd.Parameters["更新日時"].Value = DateTime.Now;

                        cmd.Parameters.Add(new NpgsqlParameter("更新者", NpgsqlDbType.Varchar));
                        cmd.Parameters["更新者"].Value = LoginInfo.社員番号;

                        cmd.ExecuteNonQuery();
                        trn.Commit();
                    }
                    catch (Exception e)
                    {
                        trn.Rollback();
                    }
                    finally
                    {
                        // 接続を閉じる
                        con.Close();
                        con.Dispose();
                    }
                }
            }
        }
    }
}
