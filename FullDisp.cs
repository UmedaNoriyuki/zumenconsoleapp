﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class FullDisp : BaseView
    {
        public static string ZumenUrlLeft { get; set; }
        public static string ZumenUrlRight { get; set; }
        public static List<PartsList> FullDispLeftList { get; set; }
        public static List<PartsList> FullDispRightList { get; set; }
        public static List<PartsList> SubDispLeftList { get; set; }
        public static List<PartsList> SubDispRightList { get; set; }
        public static List<JissekiKakou> FullDispList { get; set; }
        public static int leftIndex { get; set; }
        public static int rightIndex { get; set; }
        public static String selLeftBlockNo { get; set; }
        public static String selRightBlockNo { get; set; }
        public static bool isLeftZumen { get; set; }
        public static bool isRightZumen { get; set; }
        // 単図面かどうか
        public static bool isSingleZumen { get; set; }

        private int screen_width = Screen.PrimaryScreen.Bounds.Width;
        private int screen_height = Screen.PrimaryScreen.Bounds.Height;
        private int i = 0;
        private int j = 0;

        public FullDisp()
        {
            InitializeComponent();
        }

        private void FullDisp_Load(object sender, EventArgs e)
        {
            try
            {
                if (ZumenUrlLeft == "")
                {
                    menuStrip1.Visible = false;
                    leftBkBtn.Visible = false;
                    leftNtBtn.Visible = false;
                    rightBkBtn.Visible = false;
                    rightNtBtn.Visible = false;
                    rightBkBtn.SetBounds(0, screen_height / 2, 50, 50);
                    rightNtBtn.SetBounds(screen_width - 60, screen_height / 2, 50, 50);
                    axDeskCtrlLeft.Visible = false;

                    //インストールされているDocuWorksのバージョンでViewer Controlの種類を変更
                    if (!MainDisp.isDw9)
                    {
                        axDwCtrlEdRight.Visible = false;
                        axDwCtrlRight.SetBounds(0, 0, screen_width, screen_height);
                        axDwCtrlRight.LoadFile(ZumenUrlRight.Replace("#", "%23"));
                        axDwCtrlRight.SetCharDrawAdvance(true);
                        axDwCtrlRight.SetZoomWithPage();
                    }
                    else
                    {
                        axDwCtrlRight.Visible = false;
                        axDwCtrlEdRight.SetBounds(0, 0, screen_width, screen_height);
                        axDwCtrlEdRight.LoadFile(ZumenUrlRight.Replace("#", "%23"));
                        axDwCtrlEdRight.SetCharDrawAdvance(true);
                        axDwCtrlEdRight.SetZoomWithPage();
                    }

                    i = rightIndex;
                    // リストになっている場合のみ矢印表示
                    if (!isSingleZumen)
                    {
                        setBtn();
                    }
                }
                else
                {
                    // 文字なめらか表示設定
                    axDeskCtrlLeft.SetCharDrawAdvance(true);
                    axDwCtrlRight.Visible = false;
                    axDwCtrlEdRight.Dock = System.Windows.Forms.DockStyle.Right;
                    leftBkBtn.SetBounds(0, screen_height / 2, 50, 50);
                    leftNtBtn.SetBounds((screen_width / 2) - 60, screen_height / 2, 50, 50);
                    rightBkBtn.SetBounds(screen_width / 2, screen_height / 2, 50, 50);
                    rightNtBtn.SetBounds(screen_width - 60, screen_height / 2, 50, 50);
                    axDeskCtrlLeft.SetBounds(0, 0, screen_width / 2, screen_height);
                    axDwCtrlEdRight.SetBounds(screen_width / 2, 0, screen_width / 2, screen_height);
                    j = leftIndex;
                    // 組立図表示（ロード時は左側に組立図を表示する）
                    axDeskCtrlLeft.LoadFile(ZumenUrlLeft.Replace("#", "%23"));
                    i = rightIndex;
                    // 部品図表示（ロード時は右側に部品図を表示する）
                    axDwCtrlEdRight.LoadFile(ZumenUrlRight.Replace("#", "%23"));
                    axDwCtrlEdRight.fullscreen = "no";
                    // 左側矢印表示
                    setLeftBtn();
                    // 右側矢印表示
                    setRightBtn();
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 図面一覧（左）選択
        private void LeftZumenMenuClick(object sender, EventArgs e)
        {
            try
            {
                // ダイアログ表示
                var a = new SetLeftZumen();
                a.ShowDialog();
                if (selLeftBlockNo != null && selLeftBlockNo != "" && isLeftZumen)
                {
                    isLeftZumen = false;
                    j = 0;
                    SubDispLeftList = new List<PartsList>();
                    var tag = CommonModule.ConvertToString(selLeftBlockNo).Split('_');
                    if (tag[1] == "k")
                    {
                        SubDispLeftList = FullDispLeftList.FindAll(x => x.BlockNo == tag[0]);
                    }
                    else
                    {
                        SubDispLeftList = FullDispRightList.FindAll(x => x.BlockNo == tag[0]);
                    }
                    setLeftBtn();
                    loadLeftFile();
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 図面一覧（右）選択
        private void RightZumenMenuClick(object sender, EventArgs e)
        {
            try
            {
                // ダイアログ表示
                var a = new SetRightZumen();
                a.ShowDialog();
                if (selRightBlockNo != null && selRightBlockNo != "" && isRightZumen)
                {
                    isRightZumen = false;
                    i = 0;
                    SubDispRightList = new List<PartsList>();
                    var tag = CommonModule.ConvertToString(selRightBlockNo).Split('_');
                    if (tag[1] == "k")
                    {
                        SubDispRightList = FullDispLeftList.FindAll(x => x.BlockNo == tag[0]);
                    }
                    else
                    {
                        SubDispRightList = FullDispRightList.FindAll(x => x.BlockNo == tag[0]);
                    }
                    setRightBtn();
                    loadRightFile();
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 左側のみ表示
        private void LeftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // 左側矢印表示
                setLeftBtn();
                leftBkBtn.SetBounds(0, screen_height / 2, 50, 50);
                leftNtBtn.SetBounds(screen_width - 60, screen_height / 2, 50, 50);
                rightBkBtn.Visible = false;
                rightNtBtn.Visible = false;
                axDeskCtrlLeft.Visible = true;
                axDeskCtrlLeft.SetBounds(0, 0, screen_width, screen_height);
                loadLeftFile();
                axDwCtrlEdRight.Visible = false;
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 右側のみ表示
        private void RightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // 右側矢印表示
                setRightBtn();
                leftBkBtn.Visible = false;
                leftNtBtn.Visible = false;
                rightBkBtn.SetBounds(0, screen_height / 2, 50, 50);
                rightNtBtn.SetBounds(screen_width - 60, screen_height / 2, 50, 50);
                axDwCtrlEdRight.Visible = true;
                axDwCtrlEdRight.SetBounds(0, 0, screen_width, screen_height);
                loadRightFile();
                axDeskCtrlLeft.Visible = false;
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 両側図面一覧表示
        private void DoubleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // 左側矢印表示
                setLeftBtn();
                // 右側矢印表示
                setRightBtn();
                leftBkBtn.SetBounds(0, screen_height / 2, 50, 50);
                leftNtBtn.SetBounds((screen_width / 2) - 60, screen_height / 2, 50, 50);
                rightBkBtn.SetBounds(screen_width / 2, screen_height / 2, 50, 50);
                rightNtBtn.SetBounds(screen_width - 60, screen_height / 2, 50, 50);
                axDeskCtrlLeft.Visible = true;
                axDwCtrlEdRight.Visible = true;
                axDeskCtrlLeft.SetBounds(0, 0, screen_width / 2, screen_height);
                axDwCtrlEdRight.SetBounds(screen_width / 2, 0, screen_width / 2, screen_height);
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 次の右側図面へ
        private void rightNtBtn_Click(object sender, EventArgs e)
        {
            try
            {
                i++;
                rightBkBtn.Visible = true;
                if (FullDispList == null || FullDispList.Count == 0)
                {
                    if (i > SubDispRightList.Count - 1)
                    {
                        i--;
                        return;
                    }
                    loadRightFile();
                    if (i == SubDispRightList.Count - 1)
                    {
                        rightNtBtn.Visible = false;
                    }
                }
                else
                {
                    if (i > FullDispList.Count - 1)
                    {
                        i--;
                        return;
                    }
                    loadFile();
                    if (i == FullDispList.Count - 1)
                    {
                        rightNtBtn.Visible = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 前の右側図面へ
        private void rightBkBtn_Click(object sender, EventArgs e)
        {
            try
            {
                i--;
                rightNtBtn.Visible = true;
                if (i < 0)
                {
                    i++;
                    return;
                }
                if (FullDispList == null || FullDispList.Count == 0)
                {
                    loadRightFile();
                }
                else
                {
                    loadFile();
                }
                if (i == 0)
                {
                    rightBkBtn.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 次の左側図面へ
        private void leftNtBtn_Click(object sender, EventArgs e)
        {
            try
            {
                j++;
                leftBkBtn.Visible = true;
                if (j > SubDispLeftList.Count - 1)
                {
                    j--;
                    return;
                }
                loadLeftFile();
                if (j == SubDispLeftList.Count - 1)
                {
                    leftNtBtn.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 前の左側図面へ
        private void leftBkBtn_Click(object sender, EventArgs e)
        {
            try
            {
                j--;
                leftNtBtn.Visible = true;
                if (j < 0)
                {
                    j++;
                    return;
                }
                loadLeftFile();
                if (j == 0)
                {
                    leftBkBtn.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 左側図面表示
        private void loadLeftFile()
        {
            if (SubDispLeftList.Count > 0 && SubDispLeftList[j].ZumenPath != null && SubDispLeftList[j].ZumenPath.Length > 0)
            {
                axDeskCtrlLeft.LoadFile(MainDisp.ZUMEN_URL + SubDispLeftList[j].FileName.Replace("#", "%23") + ".xdw");
            }
            else
            {
                axDeskCtrlLeft.LoadFile(MainDisp.ERROR_FILE);
            }
        }

        // 右側図面表示
        private void loadRightFile()
        {
            if (SubDispRightList.Count > 0 && SubDispRightList[i].ZumenPath != null && SubDispRightList[i].ZumenPath.Length > 0)
            {
                axDwCtrlEdRight.LoadFile(MainDisp.ZUMEN_URL + SubDispRightList[i].FileName.Replace("#", "%23") + ".xdw");
            }
            else
            {
                axDwCtrlEdRight.LoadFile(MainDisp.ERROR_FILE);
            }
        }

        // 右側図面表示(図面検索時)
        private void loadFile()
        {
            if (FullDispList.Count > 0 && FullDispList[i].ZumenPath != null && FullDispList[i].ZumenPath.Length > 0)
            {
                if (MainDisp.isDw9)
                {
                    axDwCtrlEdRight.LoadFile(MainDisp.ZUMEN_URL + FullDispList[i].FileName.Replace("#", "%23") + ".xdw");
                    axDwCtrlEdRight.SetZoomWithPage();
                }
                else
                {
                    axDwCtrlRight.LoadFile(MainDisp.ZUMEN_URL + FullDispList[i].FileName.Replace("#", "%23") + ".xdw");
                    axDwCtrlRight.SetZoomWithPage();
                }
            }
            else
            {
                if (MainDisp.isDw9)
                {
                    axDwCtrlEdRight.LoadFile(MainDisp.ERROR_FILE);
                }
                else
                {
                    axDwCtrlRight.LoadFile(MainDisp.ERROR_FILE);
                }
            }
        }

        // 左側矢印表示
        private void setLeftBtn()
        {
            leftBkBtn.Visible = true;
            leftNtBtn.Visible = true;
            if (SubDispLeftList.Count <= 1)
            {
                leftBkBtn.Visible = false;
                leftNtBtn.Visible = false;
            }
            else
            {
                if (j == 0)
                {
                    leftBkBtn.Visible = false;
                }
                if (j == SubDispLeftList.Count - 1)
                {
                    leftNtBtn.Visible = false;
                }
            }
        }

        // 右側矢印表示
        private void setRightBtn()
        {
            rightBkBtn.Visible = true;
            rightNtBtn.Visible = true;
            if (SubDispRightList.Count <= 1)
            {
                rightBkBtn.Visible = false;
                rightNtBtn.Visible = false;
            }
            else
            {
                if (i == 0)
                {
                    rightBkBtn.Visible = false;
                }
                if (i == SubDispRightList.Count - 1)
                {
                    rightNtBtn.Visible = false;
                }
            }
        }

        // 矢印表示
        private void setBtn()
        {
            rightBkBtn.Visible = true;
            rightNtBtn.Visible = true;
            if (FullDispList == null || FullDispList.Count <= 1)
            {
                rightBkBtn.Visible = false;
                rightNtBtn.Visible = false;
            }
            else
            {
                if (i == 0)
                {
                    rightBkBtn.Visible = false;
                }
                if (i == FullDispList.Count - 1)
                {
                    rightNtBtn.Visible = false;
                }
            }
        }

        // 全画面表示を閉じるときにFullDispListを初期化する
        private void FullDisp_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (leftBkBtn.Visible || leftNtBtn.Visible || rightBkBtn.Visible || rightNtBtn.Visible)
                {
                    if (!menuStrip1.Visible)
                    {
                        FullDispList = new List<JissekiKakou>();
                    }
                    else
                    {
                        FullDispLeftList = new List<PartsList>();
                        FullDispRightList = new List<PartsList>();
                    }
                }
            }
            catch(Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 異なるディスプレイに移ったときに図面や矢印の位置を変更
        private void FullDisp_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                Control c = (Control)sender;
                screen_width = c.Bounds.Width;
                screen_height = c.Bounds.Height;

                if (axDeskCtrlLeft.Visible == false && axDwCtrlEdRight.Visible == false && axDwCtrlRight.Visible == false)
                {
                    return;
                }
                else if (axDeskCtrlLeft.Visible == true && axDwCtrlEdRight.Visible == false)
                {
                    LeftToolStripMenuItem_Click(sender, e);
                }
                else if (axDeskCtrlLeft.Visible == false && axDwCtrlEdRight.Visible == true)
                {
                    rightBkBtn.SetBounds(0, screen_height / 2, 50, 50);
                    rightNtBtn.SetBounds(screen_width - 60, screen_height / 2, 50, 50);
                    axDwCtrlEdRight.SetBounds(0, 0, screen_width, screen_height);
                }
                else if (axDeskCtrlLeft.Visible == false && axDwCtrlRight.Visible == true)
                {
                    rightBkBtn.SetBounds(0, screen_height / 2, 50, 50);
                    rightNtBtn.SetBounds(screen_width - 60, screen_height / 2, 50, 50);
                    axDwCtrlRight.SetBounds(0, 0, screen_width, screen_height);
                }
                else
                {
                    DoubleToolStripMenuItem_Click(sender, e);
                }
            }
            catch(Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
