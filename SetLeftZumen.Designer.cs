﻿namespace ZumenConsoleApp.View
{
    partial class SetLeftZumen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSet = new System.Windows.Forms.Button();
            this.leftZumenList = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(184, 365);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(104, 55);
            this.btnSet.TabIndex = 1;
            this.btnSet.Text = "設定";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.setBtn_Click);
            // 
            // leftZumenList
            // 
            this.leftZumenList.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.leftZumenList.Location = new System.Drawing.Point(13, 33);
            this.leftZumenList.Name = "leftZumenList";
            this.leftZumenList.Size = new System.Drawing.Size(275, 316);
            this.leftZumenList.TabIndex = 2;
            // 
            // SetLeftZumen
            // 
            this.AcceptButton = this.btnSet;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 431);
            this.Controls.Add(this.leftZumenList);
            this.Controls.Add(this.btnSet);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetLeftZumen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "図面（左）設定";
            this.Load += new System.EventHandler(this.SetLeftZumen_Load);
            this.Controls.SetChildIndex(this.btnSet, 0);
            this.Controls.SetChildIndex(this.leftZumenList, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.TreeView leftZumenList;
    }
}