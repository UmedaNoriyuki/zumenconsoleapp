﻿namespace ZumenConsoleApp
{
    partial class MstRegist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_登録 = new System.Windows.Forms.Button();
            this.txt_名称 = new System.Windows.Forms.TextBox();
            this.btn_更新 = new System.Windows.Forms.Button();
            this.txt_コード = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "コード";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(12, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 36);
            this.label2.TabIndex = 1;
            this.label2.Text = "名称";
            // 
            // btn_登録
            // 
            this.btn_登録.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btn_登録.Location = new System.Drawing.Point(362, 116);
            this.btn_登録.Name = "btn_登録";
            this.btn_登録.Size = new System.Drawing.Size(138, 57);
            this.btn_登録.TabIndex = 1;
            this.btn_登録.Text = "登録";
            this.btn_登録.UseVisualStyleBackColor = true;
            this.btn_登録.Click += new System.EventHandler(this.ClickBtn_登録);
            // 
            // txt_名称
            // 
            this.txt_名称.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt_名称.Location = new System.Drawing.Point(178, 64);
            this.txt_名称.Name = "txt_名称";
            this.txt_名称.Size = new System.Drawing.Size(322, 43);
            this.txt_名称.TabIndex = 4;
            // 
            // btn_更新
            // 
            this.btn_更新.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btn_更新.Location = new System.Drawing.Point(178, 116);
            this.btn_更新.Name = "btn_更新";
            this.btn_更新.Size = new System.Drawing.Size(138, 57);
            this.btn_更新.TabIndex = 2;
            this.btn_更新.Text = "更新";
            this.btn_更新.UseVisualStyleBackColor = true;
            this.btn_更新.Click += new System.EventHandler(this.ClickBtn_更新);
            // 
            // txt_コード
            // 
            this.txt_コード.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt_コード.Location = new System.Drawing.Point(178, 12);
            this.txt_コード.Name = "txt_コード";
            this.txt_コード.ReadOnly = true;
            this.txt_コード.Size = new System.Drawing.Size(322, 43);
            this.txt_コード.TabIndex = 5;
            // 
            // MstRegist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 178);
            this.Controls.Add(this.txt_コード);
            this.Controls.Add(this.txt_名称);
            this.Controls.Add(this.btn_更新);
            this.Controls.Add(this.btn_登録);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MstRegist";
            this.Text = "MstRegist";
            this.Load += new System.EventHandler(this.LoadViewMstRegist);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_登録;
        private System.Windows.Forms.TextBox txt_名称;
        private System.Windows.Forms.Button btn_更新;
        private System.Windows.Forms.TextBox txt_コード;
    }
}