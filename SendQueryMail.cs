﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Npgsql;
using System.Data;
using System.IO;
using System.Collections.Generic;

namespace ZumenConsoleApp.View
{
    public partial class SendQueryMail : BaseView
    {
        public static string orderNo { get; set; }
        public static string zumenBan { get; set; }
        public static string zumenPath { get; set; }
        private int selIndex = -1;
        private const string RECIPIENTINI_FILE = "\\\\PORTAL-SERVER\\app\\ZumenConsoleAppPublish\\recipient.ini";
        public SendQueryMail()
        {
            InitializeComponent();
        }

        // ロード時処理
        private void SendQueryMail_Load(object sender, EventArgs e)
        {
            try
            {
                txtMailOrderNo.Text = orderNo;
                txtMailZumenBan.Text = zumenBan;
                axDeskCtrlQuery.SetCharDrawAdvance(true);
                axDeskCtrlQuery.LoadFile(zumenPath);
                axDeskCtrlQuery.fullscreen = "no";

                List<ZumenWorker> zumenWorkerList = new List<ZumenWorker>();
                zumenWorkerList = CommonModule.GetZumenWorkers(Properties.Settings.Default.connectionString);
                if (zumenWorkerList.Count == 0)
                {
                    MessageBox.Show("作業者マスタよりデータを正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                // 送信者一覧作成
                foreach (var zw in zumenWorkerList)
                {
                    cmbFrom.Items.Add(zw);
                }
                // INIファイルより宛先の名前とメールアドレスの一覧を取得する
                string recipientNameStr = ClassINI.GetIniValue(RECIPIENTINI_FILE, "NAME", "RecipientNameStr");
                string recipientAddressStr = ClassINI.GetIniValue(RECIPIENTINI_FILE, "ADDRESS", "RecipientAddressStr");
                string recipientTantouCdStr = ClassINI.GetIniValue(RECIPIENTINI_FILE, "TANTOUCD", "RecipientTantouCdStr");
                // 分割する。
                char[] separator = new char[] { ',' };
                string[] recipientNames = recipientNameStr.Split(separator);
                string[] recipientAddresses = recipientAddressStr.Split(separator);
                string[] recipientTantouCd = recipientTantouCdStr.Split(separator);
                // 宛先一覧作成
                for (int i=0; i< recipientNames.Length; i++)
                {
                    // 宛先リスト
                    cmbTo.Items.Add(
                                    new Recipient
                                    {
                                        Name = recipientNames[i],
                                        MailAddress = recipientAddresses[i]
                                    }
                                );
                    if (recipientTantouCd[i] == MainDisp.selTantouCd)
                    {
                        selIndex = i;
                    }
                }
                cmbTo.SelectedIndex = selIndex;
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        // メール送信ボタン押下時処理
        private void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                ZumenWorker sd = (ZumenWorker)cmbFrom.SelectedItem;
                Recipient rc = (Recipient)cmbTo.SelectedItem;
                if (sd == null)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("送信者を選択してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                else if (rc == null)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("宛先を選択してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                //MailMessageの作成
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                //送信メールアドレス（メールアドレスはkakou-tab001@nakamurakikai.co.jpに統一する）
                msg.From = new System.Net.Mail.MailAddress("kakou-tab001@nakamurakikai.co.jp");
                //宛先
                msg.To.Add(new System.Net.Mail.MailAddress(rc.MailAddress));

                var conn = Properties.Settings.Default.connectionString;
                var maxRenno = GetMaxRenno(conn, zumenBan);
                if (maxRenno < 0)
                {
                    MessageBox.Show("図面問い合わせデータよりデータを正しく取得できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }
                else if (maxRenno > 0)
                {
                    MessageBox.Show("この図面問い合わせは" + (maxRenno+1).ToString() + "回目です。",
                    "注意",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                }

                DialogResult dr = MessageBox.Show(rc.Name + "様にメール送信してもよろしいですか？", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                if (dr == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }

                //件名
                msg.Subject = txtSubject.Text;
                //本文
                msg.Body = "送信者：" + sd.HyoujiMei + "\n" + "オーダーNO：" + txtMailOrderNo.Text + "\n\n" + txtMailBody.Text;

                msg.Priority = System.Net.Mail.MailPriority.Normal;
                //メールの配達が遅れたとき、失敗したとき、正常に配達されたときに通知する
                msg.DeliveryNotificationOptions =
                    System.Net.Mail.DeliveryNotificationOptions.Delay |
                    System.Net.Mail.DeliveryNotificationOptions.OnFailure |
                    System.Net.Mail.DeliveryNotificationOptions.OnSuccess;

                // 編集した図面を添付する
                System.Net.Mail.Attachment attach1 =
                    new System.Net.Mail.Attachment(zumenPath);
                msg.Attachments.Add(attach1);

                // メールのアカウント、パスワードを設定
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                sc.Credentials = new System.Net.NetworkCredential("kakou-tab001@nakamurakikai.co.jp", "0KZ0j3oLOE");
                //SMTPサーバーなどを設定する
                sc.Host = "smtp.nakamurakikai.co.jp";
                sc.Port = 587;
                sc.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //メッセージを送信する
                sc.Send(msg);

                //後始末
                msg.Dispose();
                //後始末（.NET Framework 4.0以降）
                sc.Dispose();

                // 図面問い合わせデータに登録
                if (!DbInsert(conn, zumenBan, maxRenno + 1, sd.TantouCode, rc.Name, zumenPath))
                {
                    MessageBox.Show("図面問い合わせデータに登録できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    this.Enabled = true;
                    this.Activate();
                    return;
                }

                //メッセージボックスを表示する
                MessageBox.Show("メールを送信しました。",
                    "メール送信完了",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.None);

                Close();

            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught: ", e);
            }
        }

        // キャンセルボタン押下時処理
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// 図面問い合わせデータの問い合わせ回数の最大を抽出する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <returns></returns>
        private int GetMaxRenno(string connectionString, string zumenBan)
        {
            var maxRenno = 0;

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var cmd = new NpgsqlCommand("select" +
                                            "  max(問い合わせ回数) as 最大問い合わせ回数" +
                                            " from 図面問い合わせデータ" +
                                            " where 図面番号 = :zumenBan", con);
                cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = zumenBan });

                try
                {
                    var result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        maxRenno = CommonModule.ConvertToInt(result["最大問い合わせ回数"]);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return -1;
                }
            }
            return maxRenno;
        }

        /// <summary>
        /// 図面問い合わせの履歴を図面問い合わせデータ(DB)に登録する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <param name="newRenno"></param>
        /// <param name="sender"></param>
        /// <param name="recipient"></param>
        /// <param name="zumenPath"></param>
        /// <returns></returns>
        private bool DbInsert(string connectionString, string zumenBan, int newRenno, int sender, string recipient, string zumenPath)
        {
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    var cmd = new NpgsqlCommand("insert into 図面問い合わせデータ " +
                                                "  (図面番号" +
                                                "  ,問い合わせ回数" +
                                                "  ,メール送信者 " +
                                                "  ,メール宛先 " +
                                                "  ,更新者 " +
                                                "  ,メール送信日時 " +
                                                "  ,図面パス)" +
                                                "  values " +
                                                "  (:zumenBan " +
                                                "  ,:newRenno " +
                                                "  ,:sender " +
                                                "  ,:recipient " +
                                                "  ,:updater " +
                                                "  ,now() " +
                                                "  ,:zumenPath) ", con);
                    cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = zumenBan });
                    cmd.Parameters.Add(new NpgsqlParameter("newRenno", DbType.Int32) { Value = newRenno });
                    cmd.Parameters.Add(new NpgsqlParameter("sender", DbType.Int32) { Value = sender });
                    cmd.Parameters.Add(new NpgsqlParameter("recipient", DbType.String) { Value = recipient });
                    cmd.Parameters.Add(new NpgsqlParameter("updater", DbType.String) { Value = Environment.MachineName });
                    cmd.Parameters.Add(new NpgsqlParameter("zumenPath", DbType.String) { Value = zumenPath });

                    try
                    {
                        var result = cmd.ExecuteNonQuery();
                        Debug.WriteLine($"[result]{result}");
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine($"[err]{e.Message}");
                        tran.Rollback();
                        return false;
                    }
                    tran.Commit();
                }
            }
            return true;
        }
    }
}
