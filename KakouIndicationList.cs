﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZumenConsoleApp
{
    public partial class KakouIndicationList : Form
    {

        #region クラス変数  

        /// <summary>
        /// 工程表番号
        /// </summary>
        public string KouteiNo = string.Empty;

        #endregion

        #region コンストラクタ 

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KakouIndicationList()
        {
            InitializeComponent();
        }

        #endregion

        #region イベント

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadKakouIndicationList(object sender, EventArgs e)
        {
            try
            {
                // DataGirdViewのTypeを取得
                Type dgvtype = typeof(DataGridView);
                // プロパティ設定の取得
                System.Reflection.PropertyInfo dgvPropertyInfo = dgvtype.GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                // 対象のDataGridViewにtrueをセットする
                dgvPropertyInfo.SetValue(dgvKakouList, true, null);

                //// プログレスバー表示
                //ProgressBar pb = new ProgressBar
                //{
                //    StartPosition = FormStartPosition.CenterScreen
                //};
                //pb.Show();

                ////非同期処理開始
                //Task<DataTable> task = Task.Run(() => Async());
                //DataTable dt = task.Result;
                //// データグリッドの設定
                //SetDataGrid(dt);
                // 加工先コンボボックス作成
                GetCmbKakou();
                // 工程コンボボックス作成
                GetCmbKoutei();

                //task.Wait();
                //pb.Close();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 検索ボタン(現在工程)押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnSearchNowKoutei(object sender, EventArgs e)
        {
            try
            {
                // プログレスバー表示
                ProgressBar pb = new ProgressBar
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                pb.Show();

                // 加工先
                int resultKakouCd = -1;
                int kakouCd = -1;

                if (int.TryParse(this.cmbKakou1.SelectedValue.ToString(), out resultKakouCd))
                {
                    kakouCd = resultKakouCd;
                }

                // 工程
                int resultkouteiCd = 0;
                int kouteiCd = 0;
                if (int.TryParse(this.cmbKoutei1.SelectedValue.ToString(), out resultkouteiCd))
                {
                    kouteiCd = resultkouteiCd;
                }

                // 非同期処理開始
                Task<DataTable> task = Task.Run(() => AsyncClickBtnSearchNowKoutei(kakouCd, kouteiCd));
                DataTable dt = task.Result;
                SetDataGrid(dt);

                task.Wait();
                pb.Close();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 検索ボタン(全体工程)押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnSearchAll(object sender, EventArgs e)
        {
            try
            {
                // プログレスバー表示
                ProgressBar pb = new ProgressBar
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                pb.Show();

                // 加工先
                int resultKakouCd = -1;
                int kakouCd = -1;

                if (int.TryParse(this.cmbKakou2.SelectedValue.ToString(), out resultKakouCd))
                {
                    kakouCd = resultKakouCd;
                }
                // 工程
                int resultkouteiCd = 0;
                int kouteiCd = 0;
                if (int.TryParse(this.cmbKoutei2.SelectedValue.ToString(), out resultkouteiCd))
                {
                    kouteiCd = resultkouteiCd;
                }
                // オーダーNO
                string odrNo = this.txtOdrNo.Text;
                // 図面番号
                string zumenNo = this.txtZumenNo.Text;

                // 非同期処理開始
                Task<DataTable> task = Task.Run(() => AsyncClickBtnSearchAllKoutei(kakouCd, kouteiCd, odrNo, zumenNo));
                DataTable dt = task.Result;
                SetDataGrid(dt);

                task.Wait();
                pb.Close();

            }
            catch
            {
                throw;
            }
        }

        private void ClickBtnSearchZanKoutei(object sender, EventArgs e)
        {
            try
            {
                // プログレスバー表示
                ProgressBar pb = new ProgressBar
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                pb.Show();

                // 加工先
                int resultKakouCd = -1;
                int kakouCd = -1;

                if (int.TryParse(this.cmbKakou3.SelectedValue.ToString(), out resultKakouCd))
                {
                    kakouCd = resultKakouCd;
                }
                // 工程
                int resultkouteiCd = 0;
                int kouteiCd = 0;
                if (int.TryParse(this.cmbKoutei3.SelectedValue.ToString(), out resultkouteiCd))
                {
                    kouteiCd = resultkouteiCd;
                }

                // 非同期処理開始
                Task<DataTable> task = Task.Run(() => AsyncClickBtnSearchZanKoutei(kakouCd, kouteiCd));
                DataTable dt = task.Result;
                SetDataGrid(dt);

                task.Wait();
                pb.Close();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// DgvCellContentClick処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellContentClickDgvKakouList(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //列ヘッダーをクリックした場合などにキャンセルする。	
                if (e.RowIndex < 0) { return; }
                string kouteiNo = string.Empty;

                //クリックした列が対象列かチェックする。			
                DataGridView dgv = (DataGridView)sender;
                // 作業印ボタン押下時に終了時刻を設定
                if (dgv.Columns[e.ColumnIndex].Name == "工程表番号")
                {
                    kouteiNo = this.dgvKakouList[e.ColumnIndex, e.RowIndex].Value.ToString();
                    KakouIndication form = new KakouIndication();
                    form.StartPosition = FormStartPosition.CenterScreen;
                    form.KouteiNo = kouteiNo;
                    form.ShowDialog();

                    //if (this.Mode == 1)
                    //{
                    //    ClickBtnSearchAll(sender, e);
                    //}
                    //else if (this.Mode == 2)
                    //{
                    //    ClickBtnSearchNowKoutei(sender, e);
                    //}
                }
                else if (dgv.Columns[e.ColumnIndex].Name == "図面番号")
                {
                    kouteiNo = this.dgvKakouList[e.ColumnIndex - 2, e.RowIndex].Value.ToString();
                    DispZumen form = new DispZumen();
                    form.KouteiNo = kouteiNo;
                    form.Show();
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region private関数

        /// <summary>
        /// 非同期処理
        /// </summary>
        /// <returns></returns>
        private async Task<DataTable> Async()
        {
            try
            {
                return await Task.Run(() => GetLoadDate());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 非同期処理
        /// </summary>
        /// <returns></returns>
        private async Task<DataTable> AsyncClickBtnSearchNowKoutei(int kakouCd, int kouteiCd)
        {
            try
            {
                return await Task.Run(() => GetKakouIndicationNowData(kakouCd, kouteiCd));
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 非同期処理
        /// </summary>
        /// <returns></returns>
        private async Task<DataTable> AsyncClickBtnSearchAllKoutei(int kakouCd, int kouteiCd, string odrNo, string zumenNo)
        {
            try
            {
                return await Task.Run(() => GetKakouIndicationAllData(kakouCd, kouteiCd, odrNo, zumenNo));
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 非同期処理
        /// </summary>
        /// <returns></returns>
        private async Task<DataTable> AsyncClickBtnSearchZanKoutei(int kakouCd, int kouteiCd)
        {
            try
            {
                return await Task.Run(() => GetKakouIndicationZanData(kakouCd, kouteiCd));
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// GetLoadDate
        /// </summary>
        /// <returns></returns>
        private DataTable GetLoadDate()
        {
            try
            {
                // データ取得
                DataTable dt = new DataTable();
                int maxKoutei = GetMaxKoutei();
                dt = GetKakouIndicationList(maxKoutei);

                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表リストを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndicationNowData(int kakouCd, int kouteiCd)
        {
            try
            {
                // 検索対象の加工指示書コードを取得
                List<int> list = GetKakouIndicationCdANowList(kakouCd, kouteiCd);
                int maxKoutei = 0;

                // データ取得
                DataTable dt = new DataTable();
                maxKoutei = GetMaxKoutei(list);
                dt = GetKakouIndicationList(list, maxKoutei);

                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表リストを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndicationAllData(int kakouCd, int kouteiCd, string odrNo, string zumenNo)
        {
            try
            {
                // 検索対象の加工指示書コードを取得
                List<int> list = GetKakouIndicationCdAllList(kakouCd, kouteiCd, odrNo, zumenNo);
                int maxKoutei = 0;

                // データ取得
                DataTable dt = new DataTable();
                maxKoutei = GetMaxKoutei(list);
                dt = GetKakouIndicationList(list, maxKoutei);

                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表リストを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndicationZanData(int kakouCd, int kouteiCd)
        {
            try
            {
                // 検索対象の加工指示書コードを取得
                List<int> list = GetKakouIndicationCdZanList(kakouCd, kouteiCd);
                int maxKoutei = 0;

                // データ取得
                DataTable dt = new DataTable();
                maxKoutei = GetMaxKoutei(list);
                dt = GetKakouIndicationList(list, maxKoutei);

                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表リストを取得します
        /// </summary>
        /// <returns></returns>
        private List<int> GetKakouIndicationCdAllList(int kakouCd, int kouteiCd, string odrNo, string zumenNo)
        {
            try
            {
                List<int> list = new List<int>();

                DataTable dt = GetKakouIndicationCdAll(kakouCd, kouteiCd, odrNo, zumenNo);

                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(int.Parse(dr["加工指示書コード"].ToString()));
                }

                return list;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表リストを取得します
        /// </summary>
        /// <returns></returns>
        private List<int> GetKakouIndicationCdZanList(int kakouCd, int kouteiCd)
        {
            try
            {
                List<int> list = new List<int>();

                DataTable dt = GetKakouIndicationCdZan(kakouCd, kouteiCd);

                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(int.Parse(dr["加工指示書コード"].ToString()));
                }

                return list;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表リスト(現在工程)を取得します
        /// </summary>
        /// <returns></returns>
        private List<int> GetKakouIndicationCdANowList(int kakouCd, int kouteiCd)
        {
            try
            {
                List<int> list = new List<int>();

                DataTable dt = GetKakouIndicationCdNow(kakouCd, kouteiCd);

                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(int.Parse(dr["加工指示書コード"].ToString()));
                }

                return list;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書コードを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndicationCdNow(int kakouCd, int kouteiCd)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("   A.加工指示書コード ");
                    query.AppendLine("  FROM ");
                    query.AppendLine("   加工指示書データ A ");
                    query.AppendLine(" INNER JOIN 実績加工データ B ");
                    query.AppendLine("    ON A.工程表番号 = B.工程表番号  ");
                    query.AppendLine("  LEFT JOIN 加工指示書工程データ C ");
                    query.AppendLine("    ON A.加工指示書コード = C.加工指示書コード  ");
                    query.AppendLine("   AND A.現在工程 = C.工程順 ");
                    query.AppendLine("   LEFT JOIN 加工指示書作業データ D ");
                    query.AppendLine("    ON C.加工指示書コード = D.加工指示書コード ");
                    query.AppendLine("   AND A.現在工程 = D.工程順 ");
                    query.AppendLine("   AND A.現在作業工程 = D.作業工程順 ");
                    query.AppendLine("WHERE  ");
                    query.AppendLine("   1 = 1 ");

                    if (kakouCd != -1)
                    {
                        query.AppendLine(" AND   C.加工先 = :加工先 ");
                    }

                    if (kouteiCd != 0)
                    {
                        query.AppendLine(" AND   D.作業工程 = :作業工程 ");
                    }

                    query.AppendLine("   GROUP BY ");
                    query.AppendLine("      A.加工指示書コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("加工先", NpgsqlDbType.Integer));
                    cmd.Parameters["加工先"].Value = kakouCd;

                    cmd.Parameters.Add(new NpgsqlParameter("作業工程", NpgsqlDbType.Integer));
                    cmd.Parameters["作業工程"].Value = kouteiCd;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 最大工程数を取得します
        /// </summary>
        /// <returns></returns>
        private int GetMaxKoutei()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    int maxKoutei = 0;
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("   MAX(B.工程順) AS  工程順 ");
                    query.AppendLine("  FROM ");
                    query.AppendLine("   加工指示書データ A ");
                    query.AppendLine("  LEFT JOIN 加工指示書工程データ B ");
                    query.AppendLine("    ON A.加工指示書コード = B.加工指示書コード  ");
                    query.AppendLine("  LEFT JOIN 加工指示書作業データ C ");
                    query.AppendLine("    ON B.加工指示書コード = C.加工指示書コード ");
                    query.AppendLine("   AND A.現在工程 = C.工程順 ");
                    query.AppendLine("WHERE  ");
                    query.AppendLine("   A.現在工程 <> 0 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    if (!string.IsNullOrWhiteSpace(dt.Rows[0]["工程順"].ToString()))
                    {
                        maxKoutei = int.Parse(dt.Rows[0]["工程順"].ToString());
                    }

                    return maxKoutei;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 最大工程数を取得します
        /// </summary>
        /// <returns></returns>
        private int GetMaxKoutei(List<int> list)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    int maxKoutei = 0;
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("   MAX(B.工程順) AS  工程順 ");
                    query.AppendLine("  FROM ");
                    query.AppendLine("   加工指示書データ A ");
                    query.AppendLine("   LEFT JOIN 加工指示書工程データ B ");
                    query.AppendLine("     ON A.加工指示書コード = B.加工指示書コード  ");
                    query.AppendLine("   LEFT JOIN 加工指示書作業データ C ");
                    query.AppendLine("     ON B.加工指示書コード = C.加工指示書コード ");
                    query.AppendLine("    AND A.現在工程 = C.工程順 ");
                    query.AppendLine("WHERE  ");
                    query.AppendLine("   1 = 1 ");

                    query.AppendLine("  AND A.加工指示書コード IN(");

                    if (list.Count != 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            if (i < (list.Count - 1))
                            {
                                query.AppendLine(":param" + i + ", ");
                            }
                            else
                            {
                                query.AppendLine(":param" + i);
                            }
                        }
                    }
                    else
                    {
                        query.AppendLine(" 0 ");
                    }

                    query.AppendLine("   )");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    for (int i = 0; i < list.Count; i++)
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("param" + i, NpgsqlDbType.Integer));
                        cmd.Parameters["param" + i].Value = list[i];
                    }

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    if (!string.IsNullOrWhiteSpace(dt.Rows[0]["工程順"].ToString()))
                    {
                        maxKoutei = int.Parse(dt.Rows[0]["工程順"].ToString());
                    }

                    return maxKoutei;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書コードを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndicationCdAll(int kakouCd, int kouteiCd, string odrNo, string zumenNo)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("   A.加工指示書コード ");
                    query.AppendLine("  FROM ");
                    query.AppendLine("   加工指示書データ A ");
                    query.AppendLine("   INNER JOIN 実績加工データ D ");
                    query.AppendLine("     ON A.工程表番号 = D.工程表番号 ");
                    query.AppendLine("   LEFT JOIN 加工指示書工程データ B ");
                    query.AppendLine("    ON A.加工指示書コード = B.加工指示書コード  ");
                    query.AppendLine("   LEFT JOIN 加工指示書作業データ C ");
                    query.AppendLine("    ON B.加工指示書コード = C.加工指示書コード ");
                    query.AppendLine("   AND B.工程順 = C.工程順 ");
                    query.AppendLine("WHERE  ");
                    query.AppendLine("   1 = 1 ");

                    if (kakouCd != -1)
                    {
                        query.AppendLine(" AND   B.加工先 = :加工先 ");
                    }

                    if (kouteiCd != 0)
                    {
                        query.AppendLine(" AND   C.作業工程 = :作業工程 ");
                    }

                    // オーダーNO
                    if (!string.IsNullOrWhiteSpace(odrNo))
                    {
                        query.AppendLine(" AND   A.\"オーダーNO\" LIKE '%'||UPPER(:オーダーNO)|| '%' ");
                    }

                    // 図面番号
                    if (!string.IsNullOrWhiteSpace(zumenNo))
                    {
                        query.AppendLine(" AND   D.図面番号 LIKE '%'||UPPER(:図面番号)|| '%' ");
                    }

                    query.AppendLine("   GROUP BY ");
                    query.AppendLine("      A.加工指示書コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("加工先", NpgsqlDbType.Integer));
                    cmd.Parameters["加工先"].Value = kakouCd;

                    cmd.Parameters.Add(new NpgsqlParameter("作業工程", NpgsqlDbType.Integer));
                    cmd.Parameters["作業工程"].Value = kouteiCd;

                    // オーダーNO
                    if (!string.IsNullOrWhiteSpace(odrNo))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("オーダーNO", NpgsqlDbType.Varchar));
                        cmd.Parameters["オーダーNO"].Value = odrNo;
                    }

                    // 図面番号
                    if (!string.IsNullOrWhiteSpace(zumenNo))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("図面番号", NpgsqlDbType.Varchar));
                        cmd.Parameters["図面番号"].Value = zumenNo;
                    }

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書コードを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndicationCdZan(int kakouCd, int kouteiCd)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("   A.加工指示書コード ");
                    query.AppendLine("  FROM ");
                    query.AppendLine("   加工指示書データ A ");
                    query.AppendLine("   INNER JOIN 実績加工データ D ");
                    query.AppendLine("     ON A.工程表番号 = D.工程表番号 ");
                    query.AppendLine("   LEFT JOIN 加工指示書工程データ B ");
                    query.AppendLine("    ON A.加工指示書コード = B.加工指示書コード  ");
                    query.AppendLine("   LEFT JOIN 加工指示書作業データ C ");
                    query.AppendLine("    ON B.加工指示書コード = C.加工指示書コード ");
                    query.AppendLine("   AND B.工程順 = C.工程順 ");
                    query.AppendLine("WHERE  ");
                    query.AppendLine("   1 = 1 ");

                    if (kakouCd != -1)
                    {
                        query.AppendLine(" AND   B.加工先 = :加工先 ");
                    }

                    if (kouteiCd != 0)
                    {
                        query.AppendLine(" AND   C.作業工程 = :作業工程 ");
                    }

                    query.AppendLine(" AND C.作業者印 = '' ");

                    query.AppendLine("   GROUP BY ");
                    query.AppendLine("      A.加工指示書コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("加工先", NpgsqlDbType.Integer));
                    cmd.Parameters["加工先"].Value = kakouCd;

                    cmd.Parameters.Add(new NpgsqlParameter("作業工程", NpgsqlDbType.Integer));
                    cmd.Parameters["作業工程"].Value = kouteiCd;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// データgridの設定を行います
        /// </summary>
        private void SetDataGrid(DataTable dt)
        {
            try
            {
                DateTime date;
                int count = 0;

                this.dgvKakouList.DataSource = dt;
                foreach (DataRow dr in dt.Rows)
                {
                    if (DateTime.TryParse(dr["完了予定日"].ToString(), out date))
                    {
                        if (DateTime.Now.AddDays(-1) > date)
                        {
                            this.dgvKakouList.Rows[count].DefaultCellStyle.BackColor = Color.Tomato;
                        }
                    }
                    count++;
                }

                this.dgvKakouList.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                this.dgvKakouList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

                // 列の並び順指定
                this.dgvKakouList.Columns["工程表番号"].DisplayIndex = 0;
                this.dgvKakouList.Columns["オーダーNO"].DisplayIndex = 1;
                this.dgvKakouList.Columns["図面番号"].DisplayIndex = 2;
                this.dgvKakouList.Columns["機種名"].DisplayIndex = 3;
                this.dgvKakouList.Columns["現在工程"].DisplayIndex = 4;

                this.dgvKakouList.Columns["完了予定日"].Visible = false;

                this.countLabel.Text = "検索結果:" + this.dgvKakouList.Rows.Count + "件";
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工先コンボボックスを設定します
        /// </summary>
        private void GetCmbKakou()
        {
            try
            {
                DataTable dt = GetShiireMst();
                DataRow dr;
                dr = dt.NewRow();
                dr["仕入先略名"] = string.Empty;
                dr["仕入先コード"] = -1;
                dt.Rows.InsertAt(dr, 0);

                // コンボボックスの設定
                this.cmbKakou1.DataSource = dt;
                // 表示用の列を設定
                this.cmbKakou1.DisplayMember = "仕入先略名";
                // データ用の列を設定
                this.cmbKakou1.ValueMember = "仕入先コード";

                DataTable dt2 = GetShiireMst();
                dr = dt2.NewRow();
                dt2.Rows.InsertAt(dr, 0);

                // コンボボックスの設定
                this.cmbKakou2.DataSource = dt2;
                // 表示用の列を設定
                this.cmbKakou2.DisplayMember = "仕入先略名";
                // データ用の列を設定
                this.cmbKakou2.ValueMember = "仕入先コード";

                DataTable dt3 = GetShiireMst();
                dr = dt3.NewRow();
                dt3.Rows.InsertAt(dr, 0);
                // コンボボックスの設定
                this.cmbKakou3.DataSource = dt3;
                // 表示用の列を設定
                this.cmbKakou3.DisplayMember = "仕入先略名";
                // データ用の列を設定
                this.cmbKakou3.ValueMember = "仕入先コード";
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程コンボボックスを設定します
        /// </summary>
        private void GetCmbKoutei()
        {
            try
            {
                DataTable dt = GetKouteiMst();
                // コンボボックスの設定
                this.cmbKoutei1.DataSource = dt;
                // 表示用の列を設定
                this.cmbKoutei1.DisplayMember = "工程名";
                // データ用の列を設定
                this.cmbKoutei1.ValueMember = "工程コード";

                DataTable dt2 = GetKouteiMst();
                // コンボボックスの設定
                this.cmbKoutei2.DataSource = dt2;
                // 表示用の列を設定
                this.cmbKoutei2.DisplayMember = "工程名";
                // データ用の列を設定
                this.cmbKoutei2.ValueMember = "工程コード";

                DataTable dt3 = GetKouteiMst();
                // コンボボックスの設定
                this.cmbKoutei3.DataSource = dt3;
                // 表示用の列を設定
                this.cmbKoutei3.DisplayMember = "工程名";
                // データ用の列を設定
                this.cmbKoutei3.ValueMember = "工程コード";
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程マスタを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKouteiMst()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   工程コード ");
                    query.Append("  ,工程名 ");
                    query.Append("  FROM 工程マスタ ");
                    query.Append("  WHERE ");
                    query.Append("   作業区分 IN(0,2)");
                    query.Append(" ORDER BY ");
                    query.Append("   並び順 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 仕入先マスタを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetShiireMst()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("   仕入先コード ");
                    query.AppendLine("  ,仕入先略名 ");
                    query.AppendLine("  FROM ");
                    query.AppendLine("   仕入先マスタ ");
                    query.AppendLine("WHERE  ");
                    query.AppendLine("   (仕入先コード IN (0,1) ");
                    query.AppendLine("   OR 業種コード = :業種コード) ");
                    query.AppendLine(" AND 取引有無区分 = 0 ");
                    query.AppendLine(" ORDER BY 仕入先コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("業種コード", NpgsqlDbType.Integer));
                    cmd.Parameters["業種コード"].Value = 4;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表一覧を取得します
        /// </summary>
        /// <param name="list"></param>
        /// <param name="maxKoutei"></param>
        /// <returns></returns>
        private DataTable GetKakouIndicationList(List<int> list, int maxKoutei)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("    A.\"オーダーNO\" ");
                    query.AppendLine("   ,A.工程表番号 ");
                    query.AppendLine("   ,L.機種名 ");
                    query.AppendLine("   ,K.図面番号 ");

                    query.AppendLine("    , '工程:' || A.現在工程 || chr(10) || I.仕入先略名  ");
                    query.AppendLine("       || chr(10) || J.工程名  || chr(10) || '完了予定日:'|| ");
                    query.AppendLine("       COALESCE(to_char(H.完了予定日, 'YYYY/MM/DD'),'未設定') AS 現在工程 ");

                    for (int i = 1; i <= maxKoutei; i++)
                    {
                        query.AppendLine("   ,( ");
                        query.AppendLine("       SELECT ");
                        query.AppendLine("           E.仕入先略名 || chr(10) || array_to_string(  ");
                        query.AppendLine("               array_agg(  ");
                        query.AppendLine("                    F.工程名 || ' ' || COALESCE(  ");
                        query.AppendLine("                         '完了予定日' || ':' || to_char(D.完了予定日, 'YYYY/MM/DD')|| ':' || D.出荷 ");
                        query.AppendLine("                     , '') || chr(10) || D.作業者印 ||  COALESCE(':' ||  ");
                        query.AppendLine("                       '完了日' || ':' || to_char(D.完了日, 'YYYY/MM/DD'), '' ");
                        query.AppendLine("                     ) ");
                        query.AppendLine("                 ORDER BY D.作業工程順)  ");
                        query.AppendLine("                , chr(10) ");
                        query.AppendLine("             )  ");
                        query.AppendLine("   FROM ");
                        query.AppendLine("      加工指示書データ B  ");
                        query.AppendLine("      LEFT JOIN 加工指示書工程データ C  ");
                        query.AppendLine("         ON B.加工指示書コード = C.加工指示書コード  ");
                        query.AppendLine("      LEFT JOIN 加工指示書作業データ D  ");
                        query.AppendLine("         ON C.加工指示書コード = D.加工指示書コード ");
                        query.AppendLine("         AND C.工程順 = D.工程順  ");
                        query.AppendLine("      LEFT JOIN 仕入先マスタ E  ");
                        query.AppendLine("         ON C.加工先 = E.仕入先コード ");
                        query.AppendLine("      LEFT JOIN 工程マスタ F ");
                        query.AppendLine("         ON D.作業工程 = F.工程コード ");
                        query.AppendLine("   WHERE ");
                        query.AppendLine("       B.加工指示書コード = A.加工指示書コード  ");
                        query.AppendLine("       AND C.工程順 = " + i);
                        query.AppendLine("  GROUP BY ");
                        query.AppendLine("     E.仕入先略名 ");
                        query.AppendLine("    ) 工程" + i);
                    }
                    query.AppendLine("    ,H.完了予定日 ");

                    query.AppendLine("  FROM ");
                    query.AppendLine("     加工指示書データ A ");
                    query.AppendLine("      LEFT JOIN 加工指示書工程データ G  ");
                    query.AppendLine("         ON A.加工指示書コード = G.加工指示書コード  ");
                    query.AppendLine("         AND A.現在工程 = G.工程順  ");
                    query.AppendLine("      LEFT JOIN 加工指示書作業データ H  ");
                    query.AppendLine("         ON A.加工指示書コード = H.加工指示書コード ");
                    query.AppendLine("         AND A.現在工程 = H.工程順  ");
                    query.AppendLine("         AND A.現在作業工程 = H.作業工程順  ");
                    query.AppendLine("      LEFT JOIN 仕入先マスタ I  ");
                    query.AppendLine("         ON G.加工先 = I.仕入先コード ");
                    query.AppendLine("      LEFT JOIN 工程マスタ J ");
                    query.AppendLine("         ON H.作業工程 = J.工程コード ");
                    query.AppendLine("      LEFT JOIN 実績加工データ K ");
                    query.AppendLine("         ON A.工程表番号 = K.工程表番号 ");
                    query.AppendLine("  LEFT JOIN オーダー管理部材テーブル L ");
                    query.AppendLine("         ON K.\"オーダーNO\" = L.\"オーダーNO\" ");
                    query.AppendLine("        AND L.部材番号 = 1 ");
                    query.AppendLine("  WHERE ");
                    query.AppendLine("     A.加工指示書コード IN(");

                    if (list.Count != 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            if (i < (list.Count - 1))
                            {
                                query.AppendLine(":param" + i + ", ");
                            }
                            else
                            {
                                query.AppendLine(":param" + i);
                            }
                        }
                    }
                    else
                    {
                        query.AppendLine(" 0 ");
                    }

                    query.AppendLine("   )");

                    query.AppendLine(" AND A.現在工程 <> 0 ");
                    query.AppendLine(" ORDER BY ");
                    query.AppendLine("    H.完了予定日 ");
                    query.AppendLine("   ,A.\"オーダーNO\" ");
                    query.AppendLine("   ,K.図面番号 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    for (int i = 0; i < list.Count; i++)
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("param" + i, NpgsqlDbType.Integer));
                        cmd.Parameters["param" + i].Value = list[i];
                    }

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書一覧を取得します
        /// </summary>
        /// <param name="maxKoutei"></param>
        /// <returns></returns>
        private DataTable GetKakouIndicationList(int maxKoutei)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("    A.\"オーダーNO\" ");
                    query.AppendLine("   ,A.工程表番号 ");
                    query.AppendLine("   ,L.機種名 ");
                    query.AppendLine("   ,K.図面番号 ");
                    query.AppendLine("    , '工程:' || A.現在工程 || chr(10) || I.仕入先略名  ");
                    query.AppendLine("       || chr(10) || J.工程名  || chr(10) || '完了予定日:'|| ");
                    query.AppendLine("       COALESCE(to_char(H.完了予定日, 'YYYY/MM/DD'),'未設定') AS 現在工程 ");

                    for (int i = 1; i <= maxKoutei; i++)
                    {
                        query.AppendLine("   ,( ");
                        query.AppendLine("       SELECT ");
                        query.AppendLine("           E.仕入先略名 || chr(10) || array_to_string(  ");
                        query.AppendLine("               array_agg(  ");
                        query.AppendLine("                    F.工程名 || ' ' || COALESCE(  ");
                        query.AppendLine("                         '完了予定日' || ':' || to_char(D.完了予定日, 'YYYY/MM/DD')|| ':' || D.出荷 ");
                        query.AppendLine("                     , '') || chr(10) || D.作業者印 ||  COALESCE(':' ||  ");
                        query.AppendLine("                       '完了日' || ':' || to_char(D.完了日, 'YYYY/MM/DD'), '' ");
                        query.AppendLine("                     ) ");
                        query.AppendLine("                 ORDER BY D.作業工程順)  ");
                        query.AppendLine("                , chr(10) ");
                        query.AppendLine("             )  ");
                        query.AppendLine("   FROM ");
                        query.AppendLine("      加工指示書データ B  ");
                        query.AppendLine("      LEFT JOIN 加工指示書工程データ C  ");
                        query.AppendLine("         ON B.加工指示書コード = C.加工指示書コード  ");
                        query.AppendLine("      LEFT JOIN 加工指示書作業データ D  ");
                        query.AppendLine("         ON C.加工指示書コード = D.加工指示書コード ");
                        query.AppendLine("         AND C.工程順 = D.工程順  ");
                        query.AppendLine("      LEFT JOIN 仕入先マスタ E  ");
                        query.AppendLine("         ON C.加工先 = E.仕入先コード ");
                        query.AppendLine("      LEFT JOIN 工程マスタ F ");
                        query.AppendLine("         ON D.作業工程 = F.工程コード ");
                        query.AppendLine("   WHERE ");
                        query.AppendLine("       B.加工指示書コード = A.加工指示書コード  ");
                        query.AppendLine("       AND C.工程順 = " + i);
                        query.AppendLine("  GROUP BY ");
                        query.AppendLine("     E.仕入先略名 ");
                        query.AppendLine("    ) 工程" + i);
                    }
                    query.AppendLine("   ,H.完了予定日 ");

                    query.AppendLine("  FROM ");
                    query.AppendLine("     加工指示書データ A ");
                    query.AppendLine("      LEFT JOIN 加工指示書工程データ G  ");
                    query.AppendLine("         ON A.加工指示書コード = G.加工指示書コード  ");
                    query.AppendLine("         AND A.現在工程 = G.工程順  ");
                    query.AppendLine("      LEFT JOIN 加工指示書作業データ H  ");
                    query.AppendLine("         ON A.加工指示書コード = H.加工指示書コード ");
                    query.AppendLine("         AND A.現在工程 = H.工程順  ");
                    query.AppendLine("         AND A.現在作業工程 = H.作業工程順  ");
                    query.AppendLine("      LEFT JOIN 仕入先マスタ I  ");
                    query.AppendLine("         ON G.加工先 = I.仕入先コード ");
                    query.AppendLine("      LEFT JOIN 工程マスタ J ");
                    query.AppendLine("         ON H.作業工程 = J.工程コード ");
                    query.AppendLine("      LEFT JOIN 実績加工データ K ");
                    query.AppendLine("         ON A.工程表番号 = K.工程表番号 ");
                    query.AppendLine("      LEFT JOIN オーダー管理部材テーブル L ");
                    query.AppendLine("         ON K.\"オーダーNO\" = L.\"オーダーNO\" ");
                    query.AppendLine("       AND L.部材番号 = 1 ");
                    query.AppendLine(" WHERE ");
                    query.AppendLine(" A.現在工程 <> 0 ");
                    query.AppendLine(" ORDER BY ");
                    query.AppendLine("    H.完了予定日 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
