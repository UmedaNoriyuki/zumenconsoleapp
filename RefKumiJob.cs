﻿using System;
using System.IO;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class RefKumiJob : BaseView
    {
        public static string[] files;

        public RefKumiJob()
        {
            InitializeComponent();
        }

        // ロード時処理
        private void RefKumiJob_Load(object sender, EventArgs e)
        {
            try
            {
                foreach (var file in files)
                {
                    if (file.Contains("$"))
                    {
                        continue;
                    }
                    ListViewItem item = new ListViewItem();
                    item.Tag = file;
                    item.Text = Path.GetFileName(file);
                    kumiJobList.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        //工数リストダブルクリック時処理
        private void kumiJobList_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                // 選択項目を取得する
                ListViewItem itemx = kumiJobList.SelectedItems[0];
                //パーツリストを起動する
                System.Diagnostics.Process p =
                    System.Diagnostics.Process.Start(CommonModule.ConvertToString(itemx.Tag));

            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            
        }
    }
}
