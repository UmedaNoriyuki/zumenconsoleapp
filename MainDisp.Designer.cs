﻿namespace ZumenConsoleApp.View
{
    partial class MainDisp
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDisp));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpSearch = new System.Windows.Forms.TabPage();
            this.btnKakouIndicationList = new System.Windows.Forms.Button();
            this.btnKakouIndication = new System.Windows.Forms.Button();
            this.btnWiring = new System.Windows.Forms.Button();
            this.btnRefKumiJob = new System.Windows.Forms.Button();
            this.btnTransmission = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnKumiMenu = new System.Windows.Forms.Button();
            this.KumiZumen = new System.Windows.Forms.Label();
            this.pdfBtn = new System.Windows.Forms.Button();
            this.KumiSearchBtn = new System.Windows.Forms.Button();
            this.zumenList = new System.Windows.Forms.TreeView();
            this.fullDispBtn = new System.Windows.Forms.Button();
            this.zumenDocuBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtKouteihyouNo = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.txtKakouTantou = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTantou = new System.Windows.Forms.TextBox();
            this.txtBlockNo = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.txtZaiSize = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtNyukaDate = new System.Windows.Forms.TextBox();
            this.txtZaiQty = new System.Windows.Forms.TextBox();
            this.txtZairyo = new System.Windows.Forms.TextBox();
            this.txtZaiNyuka = new System.Windows.Forms.TextBox();
            this.txtSinchoku = new System.Windows.Forms.TextBox();
            this.txtKyakuNouki = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtKakosaki = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNouki = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.announceList = new System.Windows.Forms.ListView();
            this.txtKakoQty = new System.Windows.Forms.TextBox();
            this.txtZumenBan = new System.Windows.Forms.TextBox();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.zumenSearchBtn = new System.Windows.Forms.Button();
            this.scanBtn = new System.Windows.Forms.Button();
            this.axDeskCtrlZumen = new AxDESKCTRLLib.AxDeskCtrl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoKamera = new System.Windows.Forms.RadioButton();
            this.rdoBarcodeReader = new System.Windows.Forms.RadioButton();
            this.tpZumen = new System.Windows.Forms.TabPage();
            this.chkIncOtherOrder = new System.Windows.Forms.CheckBox();
            this.rirekiSearchBtn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.txtUpdateDatetime = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtZumenBan2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtProductName2 = new System.Windows.Forms.TextBox();
            this.txtOrderNo2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rirekiList = new System.Windows.Forms.ListBox();
            this.rirekiDocuBtn = new System.Windows.Forms.Button();
            this.axDeskCtrlRireki = new AxDESKCTRLLib.AxDeskCtrl();
            this.tpZumenInfo = new System.Windows.Forms.TabPage();
            this.relatedZumenInfoList = new System.Windows.Forms.DataGridView();
            this.オーダーNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.品名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.図面番号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.納期 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.加工先 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.作業状況 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label49 = new System.Windows.Forms.Label();
            this.txtBlockNo2 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtTantou2 = new System.Windows.Forms.TextBox();
            this.txtZaiSize2 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtNyukaDate2 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtZaiQty2 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtZairyo2 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtZaiNyuka2 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtKyakuNouki2 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtSinchoku2 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtKakosaki2 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtNouki2 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtKakoQty2 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtZumenBan3 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtProductName3 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtOrderNo3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tpKouteiInfo = new System.Windows.Forms.TabPage();
            this.kouteiInfoList = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpSameZumenList = new System.Windows.Forms.TabPage();
            this.sameZumenList = new System.Windows.Forms.DataGridView();
            this.オーダーNO2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.品名2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.図面番号2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.数量2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.納期2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.前回作業者2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.加工先2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.作業状況2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpQueryMail = new System.Windows.Forms.TabPage();
            this.btnQueryMail = new System.Windows.Forms.Button();
            this.queryDocuBtn = new System.Windows.Forms.Button();
            this.queryList = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtUpdater2 = new System.Windows.Forms.TextBox();
            this.txtUpdateDatetime2 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txtZumenBan4 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtProductName4 = new System.Windows.Forms.TextBox();
            this.txtOrderNo4 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.axDeskCtrlQuery = new AxDESKCTRLLib.AxDeskCtrl();
            this.tpWorkingRegister = new System.Windows.Forms.TabPage();
            this.messageLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.drawingNumber = new System.Windows.Forms.DataGridViewButtonColumn();
            this.worker = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkRegistrationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteihyouNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label52 = new System.Windows.Forms.Label();
            this.wokerComboBox = new System.Windows.Forms.ComboBox();
            this.NameScanBtn = new System.Windows.Forms.Button();
            this.BarLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.kouteiScanBtn = new System.Windows.Forms.Button();
            this.registerBtn = new System.Windows.Forms.Button();
            this.BarTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.userName = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.searchBtn = new System.Windows.Forms.Button();
            this.userCd = new System.Windows.Forms.TextBox();
            this.zumenn_no = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lbl名前 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tpSearch.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrlZumen)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tpZumen.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrlRireki)).BeginInit();
            this.tpZumenInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.relatedZumenInfoList)).BeginInit();
            this.panel3.SuspendLayout();
            this.tpKouteiInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kouteiInfoList)).BeginInit();
            this.tpSameZumenList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sameZumenList)).BeginInit();
            this.tpQueryMail.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrlQuery)).BeginInit();
            this.tpWorkingRegister.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tpSearch);
            this.tabControl.Controls.Add(this.tpZumen);
            this.tabControl.Controls.Add(this.tpZumenInfo);
            this.tabControl.Controls.Add(this.tpKouteiInfo);
            this.tabControl.Controls.Add(this.tpSameZumenList);
            this.tabControl.Controls.Add(this.tpQueryMail);
            this.tabControl.Controls.Add(this.tpWorkingRegister);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tabControl.Location = new System.Drawing.Point(0, 24);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1370, 673);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpSearch
            // 
            this.tpSearch.AutoScroll = true;
            this.tpSearch.BackColor = System.Drawing.Color.White;
            this.tpSearch.Controls.Add(this.btnKakouIndicationList);
            this.tpSearch.Controls.Add(this.btnKakouIndication);
            this.tpSearch.Controls.Add(this.btnWiring);
            this.tpSearch.Controls.Add(this.btnRefKumiJob);
            this.tpSearch.Controls.Add(this.btnTransmission);
            this.tpSearch.Controls.Add(this.btnDel);
            this.tpSearch.Controls.Add(this.btnKumiMenu);
            this.tpSearch.Controls.Add(this.KumiZumen);
            this.tpSearch.Controls.Add(this.pdfBtn);
            this.tpSearch.Controls.Add(this.KumiSearchBtn);
            this.tpSearch.Controls.Add(this.zumenList);
            this.tpSearch.Controls.Add(this.fullDispBtn);
            this.tpSearch.Controls.Add(this.zumenDocuBtn);
            this.tpSearch.Controls.Add(this.panel1);
            this.tpSearch.Controls.Add(this.zumenSearchBtn);
            this.tpSearch.Controls.Add(this.scanBtn);
            this.tpSearch.Controls.Add(this.axDeskCtrlZumen);
            this.tpSearch.Controls.Add(this.groupBox1);
            this.tpSearch.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tpSearch.Location = new System.Drawing.Point(4, 34);
            this.tpSearch.Name = "tpSearch";
            this.tpSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tpSearch.Size = new System.Drawing.Size(1362, 635);
            this.tpSearch.TabIndex = 0;
            this.tpSearch.Text = "図面・オーダー検索";
            // 
            // btnKakouIndicationList
            // 
            this.btnKakouIndicationList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKakouIndicationList.BackColor = System.Drawing.Color.Thistle;
            this.btnKakouIndicationList.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnKakouIndicationList.Location = new System.Drawing.Point(722, 6);
            this.btnKakouIndicationList.Name = "btnKakouIndicationList";
            this.btnKakouIndicationList.Size = new System.Drawing.Size(72, 55);
            this.btnKakouIndicationList.TabIndex = 81;
            this.btnKakouIndicationList.Text = "加工指 　示書一覧";
            this.btnKakouIndicationList.UseVisualStyleBackColor = false;
            this.btnKakouIndicationList.Click += new System.EventHandler(this.ClickBtnKakouIndicationList);
            // 
            // btnKakouIndication
            // 
            this.btnKakouIndication.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKakouIndication.BackColor = System.Drawing.Color.Pink;
            this.btnKakouIndication.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnKakouIndication.Location = new System.Drawing.Point(802, 6);
            this.btnKakouIndication.Name = "btnKakouIndication";
            this.btnKakouIndication.Size = new System.Drawing.Size(72, 55);
            this.btnKakouIndication.TabIndex = 80;
            this.btnKakouIndication.Text = "加工指 示書";
            this.btnKakouIndication.UseVisualStyleBackColor = false;
            this.btnKakouIndication.Click += new System.EventHandler(this.ClickBtnKakouIndication);
            // 
            // btnWiring
            // 
            this.btnWiring.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWiring.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnWiring.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnWiring.Location = new System.Drawing.Point(882, 6);
            this.btnWiring.Name = "btnWiring";
            this.btnWiring.Size = new System.Drawing.Size(72, 55);
            this.btnWiring.TabIndex = 79;
            this.btnWiring.Text = "配線図面";
            this.btnWiring.UseVisualStyleBackColor = false;
            this.btnWiring.Click += new System.EventHandler(this.OnClickBtnWiring);
            // 
            // btnRefKumiJob
            // 
            this.btnRefKumiJob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefKumiJob.BackColor = System.Drawing.Color.MediumPurple;
            this.btnRefKumiJob.Enabled = false;
            this.btnRefKumiJob.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRefKumiJob.Location = new System.Drawing.Point(962, 6);
            this.btnRefKumiJob.Name = "btnRefKumiJob";
            this.btnRefKumiJob.Size = new System.Drawing.Size(72, 55);
            this.btnRefKumiJob.TabIndex = 78;
            this.btnRefKumiJob.Text = "組立工数参照";
            this.btnRefKumiJob.UseVisualStyleBackColor = false;
            this.btnRefKumiJob.Click += new System.EventHandler(this.btnRefKumiJob_Click);
            // 
            // btnTransmission
            // 
            this.btnTransmission.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTransmission.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnTransmission.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnTransmission.Location = new System.Drawing.Point(1042, 6);
            this.btnTransmission.Name = "btnTransmission";
            this.btnTransmission.Size = new System.Drawing.Size(72, 55);
            this.btnTransmission.TabIndex = 74;
            this.btnTransmission.Text = "伝達事項";
            this.btnTransmission.UseVisualStyleBackColor = false;
            this.btnTransmission.Click += new System.EventHandler(this.btnTransmission_Click);
            // 
            // btnDel
            // 
            this.btnDel.BackColor = System.Drawing.Color.White;
            this.btnDel.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnDel.Location = new System.Drawing.Point(252, 6);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(72, 55);
            this.btnDel.TabIndex = 26;
            this.btnDel.Text = "リストから削除";
            this.btnDel.UseVisualStyleBackColor = false;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnKumiMenu
            // 
            this.btnKumiMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKumiMenu.BackColor = System.Drawing.Color.Aquamarine;
            this.btnKumiMenu.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnKumiMenu.Location = new System.Drawing.Point(1122, 6);
            this.btnKumiMenu.Name = "btnKumiMenu";
            this.btnKumiMenu.Size = new System.Drawing.Size(72, 55);
            this.btnKumiMenu.TabIndex = 25;
            this.btnKumiMenu.Text = "組立   メニュー";
            this.btnKumiMenu.UseVisualStyleBackColor = false;
            this.btnKumiMenu.Click += new System.EventHandler(this.btnKumiMenu_Click);
            // 
            // KumiZumen
            // 
            this.KumiZumen.AutoSize = true;
            this.KumiZumen.Location = new System.Drawing.Point(705, 552);
            this.KumiZumen.Name = "KumiZumen";
            this.KumiZumen.Size = new System.Drawing.Size(0, 24);
            this.KumiZumen.TabIndex = 24;
            this.KumiZumen.Visible = false;
            // 
            // pdfBtn
            // 
            this.pdfBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pdfBtn.BackColor = System.Drawing.Color.Red;
            this.pdfBtn.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pdfBtn.Location = new System.Drawing.Point(1202, 6);
            this.pdfBtn.Name = "pdfBtn";
            this.pdfBtn.Size = new System.Drawing.Size(72, 55);
            this.pdfBtn.TabIndex = 23;
            this.pdfBtn.Text = "PDF化";
            this.pdfBtn.UseVisualStyleBackColor = false;
            this.pdfBtn.Click += new System.EventHandler(this.pdfBtn_Click);
            // 
            // KumiSearchBtn
            // 
            this.KumiSearchBtn.BackColor = System.Drawing.Color.Violet;
            this.KumiSearchBtn.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.KumiSearchBtn.Location = new System.Drawing.Point(172, 6);
            this.KumiSearchBtn.Name = "KumiSearchBtn";
            this.KumiSearchBtn.Size = new System.Drawing.Size(72, 55);
            this.KumiSearchBtn.TabIndex = 21;
            this.KumiSearchBtn.Text = "組立図 検索";
            this.KumiSearchBtn.UseVisualStyleBackColor = false;
            this.KumiSearchBtn.Click += new System.EventHandler(this.KumiSearchBtn_Click);
            // 
            // zumenList
            // 
            this.zumenList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.zumenList.BackColor = System.Drawing.Color.White;
            this.zumenList.Location = new System.Drawing.Point(15, 287);
            this.zumenList.Name = "zumenList";
            this.zumenList.Size = new System.Drawing.Size(312, 183);
            this.zumenList.TabIndex = 20;
            this.zumenList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.zumenList_AfterSelect);
            // 
            // fullDispBtn
            // 
            this.fullDispBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fullDispBtn.BackColor = System.Drawing.Color.Orange;
            this.fullDispBtn.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.fullDispBtn.Location = new System.Drawing.Point(1282, 6);
            this.fullDispBtn.Name = "fullDispBtn";
            this.fullDispBtn.Size = new System.Drawing.Size(72, 55);
            this.fullDispBtn.TabIndex = 19;
            this.fullDispBtn.Text = "全画面 表示";
            this.fullDispBtn.UseVisualStyleBackColor = false;
            this.fullDispBtn.Click += new System.EventHandler(this.fullDispBtn_Click);
            // 
            // zumenDocuBtn
            // 
            this.zumenDocuBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.zumenDocuBtn.BackColor = System.Drawing.Color.Tomato;
            this.zumenDocuBtn.Location = new System.Drawing.Point(15, 476);
            this.zumenDocuBtn.Name = "zumenDocuBtn";
            this.zumenDocuBtn.Size = new System.Drawing.Size(312, 49);
            this.zumenDocuBtn.TabIndex = 18;
            this.zumenDocuBtn.Text = "編集する";
            this.zumenDocuBtn.UseVisualStyleBackColor = false;
            this.zumenDocuBtn.Click += new System.EventHandler(this.zumenDocuBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.txtKouteihyouNo);
            this.panel1.Controls.Add(this.label51);
            this.panel1.Controls.Add(this.txtKakouTantou);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.txtTantou);
            this.panel1.Controls.Add(this.txtBlockNo);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.label46);
            this.panel1.Controls.Add(this.txtZaiSize);
            this.panel1.Controls.Add(this.label39);
            this.panel1.Controls.Add(this.txtNyukaDate);
            this.panel1.Controls.Add(this.txtZaiQty);
            this.panel1.Controls.Add(this.txtZairyo);
            this.panel1.Controls.Add(this.txtZaiNyuka);
            this.panel1.Controls.Add(this.txtSinchoku);
            this.panel1.Controls.Add(this.txtKyakuNouki);
            this.panel1.Controls.Add(this.label38);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.txtKakosaki);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtNouki);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.announceList);
            this.panel1.Controls.Add(this.txtKakoQty);
            this.panel1.Controls.Add(this.txtZumenBan);
            this.panel1.Controls.Add(this.txtProductName);
            this.panel1.Controls.Add(this.txtOrderNo);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(15, 70);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1339, 211);
            this.panel1.TabIndex = 17;
            // 
            // txtKouteihyouNo
            // 
            this.txtKouteihyouNo.BackColor = System.Drawing.Color.Aquamarine;
            this.txtKouteihyouNo.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKouteihyouNo.ForeColor = System.Drawing.Color.Gray;
            this.txtKouteihyouNo.Location = new System.Drawing.Point(715, 37);
            this.txtKouteihyouNo.Name = "txtKouteihyouNo";
            this.txtKouteihyouNo.ReadOnly = true;
            this.txtKouteihyouNo.Size = new System.Drawing.Size(130, 26);
            this.txtKouteihyouNo.TabIndex = 82;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label51.Location = new System.Drawing.Point(560, 40);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(109, 19);
            this.label51.TabIndex = 81;
            this.label51.Text = "工程表番号";
            // 
            // txtKakouTantou
            // 
            this.txtKakouTantou.BackColor = System.Drawing.Color.Aquamarine;
            this.txtKakouTantou.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKakouTantou.ForeColor = System.Drawing.Color.Gray;
            this.txtKakouTantou.Location = new System.Drawing.Point(715, 75);
            this.txtKakouTantou.Name = "txtKakouTantou";
            this.txtKakouTantou.ReadOnly = true;
            this.txtKakouTantou.Size = new System.Drawing.Size(130, 26);
            this.txtKakouTantou.TabIndex = 80;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label19.Location = new System.Drawing.Point(560, 75);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(109, 19);
            this.label19.TabIndex = 79;
            this.label19.Text = "加工担当者";
            // 
            // txtTantou
            // 
            this.txtTantou.BackColor = System.Drawing.Color.Aquamarine;
            this.txtTantou.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantou.ForeColor = System.Drawing.Color.Gray;
            this.txtTantou.Location = new System.Drawing.Point(715, 110);
            this.txtTantou.Name = "txtTantou";
            this.txtTantou.ReadOnly = true;
            this.txtTantou.Size = new System.Drawing.Size(130, 26);
            this.txtTantou.TabIndex = 77;
            // 
            // txtBlockNo
            // 
            this.txtBlockNo.BackColor = System.Drawing.Color.Aquamarine;
            this.txtBlockNo.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBlockNo.ForeColor = System.Drawing.Color.Gray;
            this.txtBlockNo.Location = new System.Drawing.Point(715, 145);
            this.txtBlockNo.Name = "txtBlockNo";
            this.txtBlockNo.ReadOnly = true;
            this.txtBlockNo.Size = new System.Drawing.Size(130, 26);
            this.txtBlockNo.TabIndex = 76;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label47.Location = new System.Drawing.Point(560, 110);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(149, 19);
            this.label47.TabIndex = 75;
            this.label47.Text = "図面作成担当者";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label46.Location = new System.Drawing.Point(560, 145);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(89, 19);
            this.label46.TabIndex = 74;
            this.label46.Text = "ブロックNo";
            // 
            // txtZaiSize
            // 
            this.txtZaiSize.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZaiSize.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZaiSize.ForeColor = System.Drawing.Color.Gray;
            this.txtZaiSize.Location = new System.Drawing.Point(715, 180);
            this.txtZaiSize.Name = "txtZaiSize";
            this.txtZaiSize.ReadOnly = true;
            this.txtZaiSize.Size = new System.Drawing.Size(130, 26);
            this.txtZaiSize.TabIndex = 73;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label39.Location = new System.Drawing.Point(560, 180);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(98, 19);
            this.label39.TabIndex = 72;
            this.label39.Text = "材料サイズ";
            // 
            // txtNyukaDate
            // 
            this.txtNyukaDate.BackColor = System.Drawing.Color.Aquamarine;
            this.txtNyukaDate.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNyukaDate.ForeColor = System.Drawing.Color.Gray;
            this.txtNyukaDate.Location = new System.Drawing.Point(390, 145);
            this.txtNyukaDate.Name = "txtNyukaDate";
            this.txtNyukaDate.ReadOnly = true;
            this.txtNyukaDate.Size = new System.Drawing.Size(170, 26);
            this.txtNyukaDate.TabIndex = 71;
            // 
            // txtZaiQty
            // 
            this.txtZaiQty.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZaiQty.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZaiQty.ForeColor = System.Drawing.Color.Gray;
            this.txtZaiQty.Location = new System.Drawing.Point(390, 180);
            this.txtZaiQty.Name = "txtZaiQty";
            this.txtZaiQty.ReadOnly = true;
            this.txtZaiQty.Size = new System.Drawing.Size(170, 26);
            this.txtZaiQty.TabIndex = 70;
            this.txtZaiQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtZairyo
            // 
            this.txtZairyo.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZairyo.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZairyo.ForeColor = System.Drawing.Color.Gray;
            this.txtZairyo.Location = new System.Drawing.Point(390, 110);
            this.txtZairyo.Name = "txtZairyo";
            this.txtZairyo.ReadOnly = true;
            this.txtZairyo.Size = new System.Drawing.Size(170, 26);
            this.txtZairyo.TabIndex = 69;
            // 
            // txtZaiNyuka
            // 
            this.txtZaiNyuka.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZaiNyuka.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZaiNyuka.ForeColor = System.Drawing.Color.Gray;
            this.txtZaiNyuka.Location = new System.Drawing.Point(390, 75);
            this.txtZaiNyuka.Name = "txtZaiNyuka";
            this.txtZaiNyuka.ReadOnly = true;
            this.txtZaiNyuka.Size = new System.Drawing.Size(170, 26);
            this.txtZaiNyuka.TabIndex = 68;
            // 
            // txtSinchoku
            // 
            this.txtSinchoku.BackColor = System.Drawing.Color.Aquamarine;
            this.txtSinchoku.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSinchoku.ForeColor = System.Drawing.Color.Gray;
            this.txtSinchoku.Location = new System.Drawing.Point(110, 110);
            this.txtSinchoku.Name = "txtSinchoku";
            this.txtSinchoku.ReadOnly = true;
            this.txtSinchoku.Size = new System.Drawing.Size(170, 26);
            this.txtSinchoku.TabIndex = 67;
            // 
            // txtKyakuNouki
            // 
            this.txtKyakuNouki.BackColor = System.Drawing.Color.Aquamarine;
            this.txtKyakuNouki.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyakuNouki.ForeColor = System.Drawing.Color.Gray;
            this.txtKyakuNouki.Location = new System.Drawing.Point(110, 180);
            this.txtKyakuNouki.Name = "txtKyakuNouki";
            this.txtKyakuNouki.ReadOnly = true;
            this.txtKyakuNouki.Size = new System.Drawing.Size(170, 26);
            this.txtKyakuNouki.TabIndex = 66;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label38.Location = new System.Drawing.Point(280, 145);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(109, 19);
            this.label38.TabIndex = 65;
            this.label38.Text = "材料入荷日";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label37.Location = new System.Drawing.Point(280, 180);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(89, 19);
            this.label37.TabIndex = 64;
            this.label37.Text = "材料数量";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label36.Location = new System.Drawing.Point(280, 110);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(49, 19);
            this.label36.TabIndex = 63;
            this.label36.Text = "材料";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label35.Location = new System.Drawing.Point(280, 75);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(109, 19);
            this.label35.TabIndex = 62;
            this.label35.Text = "材料入荷先";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label34.Location = new System.Drawing.Point(10, 110);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(89, 19);
            this.label34.TabIndex = 61;
            this.label34.Text = "作業状況";
            // 
            // txtKakosaki
            // 
            this.txtKakosaki.BackColor = System.Drawing.Color.Aquamarine;
            this.txtKakosaki.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKakosaki.ForeColor = System.Drawing.Color.Gray;
            this.txtKakosaki.Location = new System.Drawing.Point(110, 75);
            this.txtKakosaki.Name = "txtKakosaki";
            this.txtKakosaki.ReadOnly = true;
            this.txtKakosaki.Size = new System.Drawing.Size(170, 26);
            this.txtKakosaki.TabIndex = 38;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label33.Location = new System.Drawing.Point(10, 180);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(89, 19);
            this.label33.TabIndex = 60;
            this.label33.Text = "客先納期";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.Location = new System.Drawing.Point(10, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 19);
            this.label11.TabIndex = 18;
            this.label11.Text = "加工先";
            // 
            // txtNouki
            // 
            this.txtNouki.BackColor = System.Drawing.Color.Aquamarine;
            this.txtNouki.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNouki.ForeColor = System.Drawing.Color.Gray;
            this.txtNouki.Location = new System.Drawing.Point(110, 145);
            this.txtNouki.Name = "txtNouki";
            this.txtNouki.ReadOnly = true;
            this.txtNouki.Size = new System.Drawing.Size(170, 26);
            this.txtNouki.TabIndex = 59;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label32.Location = new System.Drawing.Point(10, 145);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(49, 19);
            this.label32.TabIndex = 58;
            this.label32.Text = "納期";
            // 
            // announceList
            // 
            this.announceList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.announceList.Font = new System.Drawing.Font("MS UI Gothic", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.announceList.ForeColor = System.Drawing.Color.Red;
            this.announceList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.announceList.HideSelection = false;
            this.announceList.LabelWrap = false;
            this.announceList.Location = new System.Drawing.Point(853, 40);
            this.announceList.MultiSelect = false;
            this.announceList.Name = "announceList";
            this.announceList.Size = new System.Drawing.Size(483, 164);
            this.announceList.TabIndex = 46;
            this.announceList.UseCompatibleStateImageBehavior = false;
            this.announceList.View = System.Windows.Forms.View.List;
            // 
            // txtKakoQty
            // 
            this.txtKakoQty.BackColor = System.Drawing.Color.Aquamarine;
            this.txtKakoQty.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKakoQty.ForeColor = System.Drawing.Color.Gray;
            this.txtKakoQty.Location = new System.Drawing.Point(390, 40);
            this.txtKakoQty.Name = "txtKakoQty";
            this.txtKakoQty.ReadOnly = true;
            this.txtKakoQty.Size = new System.Drawing.Size(170, 26);
            this.txtKakoQty.TabIndex = 32;
            this.txtKakoQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtZumenBan
            // 
            this.txtZumenBan.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZumenBan.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZumenBan.ForeColor = System.Drawing.Color.Gray;
            this.txtZumenBan.Location = new System.Drawing.Point(390, 5);
            this.txtZumenBan.Name = "txtZumenBan";
            this.txtZumenBan.ReadOnly = true;
            this.txtZumenBan.Size = new System.Drawing.Size(170, 26);
            this.txtZumenBan.TabIndex = 31;
            // 
            // txtProductName
            // 
            this.txtProductName.BackColor = System.Drawing.Color.Aquamarine;
            this.txtProductName.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtProductName.ForeColor = System.Drawing.Color.Gray;
            this.txtProductName.Location = new System.Drawing.Point(110, 40);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.ReadOnly = true;
            this.txtProductName.Size = new System.Drawing.Size(170, 26);
            this.txtProductName.TabIndex = 28;
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.BackColor = System.Drawing.Color.Aquamarine;
            this.txtOrderNo.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo.ForeColor = System.Drawing.Color.Gray;
            this.txtOrderNo.Location = new System.Drawing.Point(110, 5);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.ReadOnly = true;
            this.txtOrderNo.Size = new System.Drawing.Size(170, 26);
            this.txtOrderNo.TabIndex = 27;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label16.Location = new System.Drawing.Point(280, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 19);
            this.label16.TabIndex = 23;
            this.label16.Text = "数量";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(280, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 19);
            this.label9.TabIndex = 16;
            this.label9.Text = "図面番号";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(10, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 19);
            this.label1.TabIndex = 8;
            this.label1.Text = "オーダーNo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(10, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 19);
            this.label2.TabIndex = 9;
            this.label2.Text = "品名";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(224, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 10;
            // 
            // zumenSearchBtn
            // 
            this.zumenSearchBtn.BackColor = System.Drawing.Color.Gold;
            this.zumenSearchBtn.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.zumenSearchBtn.Location = new System.Drawing.Point(92, 6);
            this.zumenSearchBtn.Name = "zumenSearchBtn";
            this.zumenSearchBtn.Size = new System.Drawing.Size(72, 55);
            this.zumenSearchBtn.TabIndex = 6;
            this.zumenSearchBtn.Text = "図面検索";
            this.zumenSearchBtn.UseVisualStyleBackColor = false;
            this.zumenSearchBtn.Click += new System.EventHandler(this.zumenSearchBtn_Click);
            // 
            // scanBtn
            // 
            this.scanBtn.BackColor = System.Drawing.Color.GreenYellow;
            this.scanBtn.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.scanBtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.scanBtn.Location = new System.Drawing.Point(12, 6);
            this.scanBtn.Name = "scanBtn";
            this.scanBtn.Size = new System.Drawing.Size(72, 55);
            this.scanBtn.TabIndex = 0;
            this.scanBtn.Text = "スキャン";
            this.scanBtn.UseVisualStyleBackColor = false;
            this.scanBtn.Click += new System.EventHandler(this.ScanBtn_Click);
            // 
            // axDeskCtrlZumen
            // 
            this.axDeskCtrlZumen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.axDeskCtrlZumen.Enabled = true;
            this.axDeskCtrlZumen.Location = new System.Drawing.Point(333, 285);
            this.axDeskCtrlZumen.Name = "axDeskCtrlZumen";
            this.axDeskCtrlZumen.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axDeskCtrlZumen.OcxState")));
            this.axDeskCtrlZumen.Size = new System.Drawing.Size(1021, 237);
            this.axDeskCtrlZumen.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoKamera);
            this.groupBox1.Controls.Add(this.rdoBarcodeReader);
            this.groupBox1.Location = new System.Drawing.Point(328, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 58);
            this.groupBox1.TabIndex = 77;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "スキャン方法";
            // 
            // rdoKamera
            // 
            this.rdoKamera.AutoSize = true;
            this.rdoKamera.Checked = true;
            this.rdoKamera.Location = new System.Drawing.Point(6, 24);
            this.rdoKamera.Name = "rdoKamera";
            this.rdoKamera.Size = new System.Drawing.Size(79, 28);
            this.rdoKamera.TabIndex = 75;
            this.rdoKamera.TabStop = true;
            this.rdoKamera.Text = "カメラ";
            this.rdoKamera.UseVisualStyleBackColor = true;
            // 
            // rdoBarcodeReader
            // 
            this.rdoBarcodeReader.AutoSize = true;
            this.rdoBarcodeReader.Location = new System.Drawing.Point(88, 24);
            this.rdoBarcodeReader.Name = "rdoBarcodeReader";
            this.rdoBarcodeReader.Size = new System.Drawing.Size(101, 28);
            this.rdoBarcodeReader.TabIndex = 76;
            this.rdoBarcodeReader.Text = "スキャナ";
            this.rdoBarcodeReader.UseVisualStyleBackColor = true;
            // 
            // tpZumen
            // 
            this.tpZumen.BackColor = System.Drawing.Color.White;
            this.tpZumen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tpZumen.Controls.Add(this.chkIncOtherOrder);
            this.tpZumen.Controls.Add(this.rirekiSearchBtn);
            this.tpZumen.Controls.Add(this.panel2);
            this.tpZumen.Controls.Add(this.rirekiList);
            this.tpZumen.Controls.Add(this.rirekiDocuBtn);
            this.tpZumen.Controls.Add(this.axDeskCtrlRireki);
            this.tpZumen.Location = new System.Drawing.Point(4, 34);
            this.tpZumen.Name = "tpZumen";
            this.tpZumen.Padding = new System.Windows.Forms.Padding(3);
            this.tpZumen.Size = new System.Drawing.Size(1362, 635);
            this.tpZumen.TabIndex = 1;
            this.tpZumen.Text = "図面履歴";
            // 
            // chkIncOtherOrder
            // 
            this.chkIncOtherOrder.AutoSize = true;
            this.chkIncOtherOrder.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkIncOtherOrder.Location = new System.Drawing.Point(123, 44);
            this.chkIncOtherOrder.Name = "chkIncOtherOrder";
            this.chkIncOtherOrder.Size = new System.Drawing.Size(193, 16);
            this.chkIncOtherOrder.TabIndex = 9;
            this.chkIncOtherOrder.Text = "他オーダーNOも含めて履歴表示";
            this.chkIncOtherOrder.UseVisualStyleBackColor = true;
            this.chkIncOtherOrder.CheckedChanged += new System.EventHandler(this.chkIncOtherOrder_CheckedChanged);
            // 
            // rirekiSearchBtn
            // 
            this.rirekiSearchBtn.BackColor = System.Drawing.Color.Gold;
            this.rirekiSearchBtn.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rirekiSearchBtn.Location = new System.Drawing.Point(9, 6);
            this.rirekiSearchBtn.Name = "rirekiSearchBtn";
            this.rirekiSearchBtn.Size = new System.Drawing.Size(108, 55);
            this.rirekiSearchBtn.TabIndex = 8;
            this.rirekiSearchBtn.Text = "履歴検索";
            this.rirekiSearchBtn.UseVisualStyleBackColor = false;
            this.rirekiSearchBtn.Click += new System.EventHandler(this.rirekiSearchBtn_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtUpdater);
            this.panel2.Controls.Add(this.txtUpdateDatetime);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtZumenBan2);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.txtProductName2);
            this.panel2.Controls.Add(this.txtOrderNo2);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(9, 67);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(312, 246);
            this.panel2.TabIndex = 7;
            // 
            // txtUpdater
            // 
            this.txtUpdater.BackColor = System.Drawing.Color.Aquamarine;
            this.txtUpdater.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUpdater.ForeColor = System.Drawing.Color.Gray;
            this.txtUpdater.Location = new System.Drawing.Point(134, 210);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(170, 26);
            this.txtUpdater.TabIndex = 43;
            // 
            // txtUpdateDatetime
            // 
            this.txtUpdateDatetime.BackColor = System.Drawing.Color.Aquamarine;
            this.txtUpdateDatetime.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUpdateDatetime.ForeColor = System.Drawing.Color.Gray;
            this.txtUpdateDatetime.Location = new System.Drawing.Point(134, 163);
            this.txtUpdateDatetime.Name = "txtUpdateDatetime";
            this.txtUpdateDatetime.ReadOnly = true;
            this.txtUpdateDatetime.Size = new System.Drawing.Size(170, 26);
            this.txtUpdateDatetime.TabIndex = 42;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.Location = new System.Drawing.Point(16, 210);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 24);
            this.label14.TabIndex = 41;
            this.label14.Text = "更新者";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(16, 161);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 24);
            this.label8.TabIndex = 40;
            this.label8.Text = "更新日時";
            // 
            // txtZumenBan2
            // 
            this.txtZumenBan2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZumenBan2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZumenBan2.ForeColor = System.Drawing.Color.Gray;
            this.txtZumenBan2.Location = new System.Drawing.Point(134, 114);
            this.txtZumenBan2.Name = "txtZumenBan2";
            this.txtZumenBan2.ReadOnly = true;
            this.txtZumenBan2.Size = new System.Drawing.Size(170, 26);
            this.txtZumenBan2.TabIndex = 39;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label17.Location = new System.Drawing.Point(16, 114);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 24);
            this.label17.TabIndex = 37;
            this.label17.Text = "図面番号";
            // 
            // txtProductName2
            // 
            this.txtProductName2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtProductName2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtProductName2.ForeColor = System.Drawing.Color.Gray;
            this.txtProductName2.Location = new System.Drawing.Point(134, 66);
            this.txtProductName2.Name = "txtProductName2";
            this.txtProductName2.ReadOnly = true;
            this.txtProductName2.Size = new System.Drawing.Size(170, 26);
            this.txtProductName2.TabIndex = 35;
            // 
            // txtOrderNo2
            // 
            this.txtOrderNo2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtOrderNo2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo2.ForeColor = System.Drawing.Color.Gray;
            this.txtOrderNo2.Location = new System.Drawing.Point(134, 18);
            this.txtOrderNo2.Name = "txtOrderNo2";
            this.txtOrderNo2.ReadOnly = true;
            this.txtOrderNo2.Size = new System.Drawing.Size(170, 26);
            this.txtOrderNo2.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.White;
            this.label13.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(16, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 24);
            this.label13.TabIndex = 10;
            this.label13.Text = "品名";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(16, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "オーダーNo";
            // 
            // rirekiList
            // 
            this.rirekiList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rirekiList.FormattingEnabled = true;
            this.rirekiList.HorizontalScrollbar = true;
            this.rirekiList.ItemHeight = 24;
            this.rirekiList.Location = new System.Drawing.Point(9, 336);
            this.rirekiList.Name = "rirekiList";
            this.rirekiList.Size = new System.Drawing.Size(312, 196);
            this.rirekiList.TabIndex = 6;
            this.rirekiList.SelectedIndexChanged += new System.EventHandler(this.rirekiList_SelectedIndexChanged);
            // 
            // rirekiDocuBtn
            // 
            this.rirekiDocuBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rirekiDocuBtn.BackColor = System.Drawing.Color.Tomato;
            this.rirekiDocuBtn.Location = new System.Drawing.Point(9, 540);
            this.rirekiDocuBtn.Name = "rirekiDocuBtn";
            this.rirekiDocuBtn.Size = new System.Drawing.Size(312, 49);
            this.rirekiDocuBtn.TabIndex = 4;
            this.rirekiDocuBtn.Text = "編集する";
            this.rirekiDocuBtn.UseVisualStyleBackColor = false;
            this.rirekiDocuBtn.Click += new System.EventHandler(this.rirekiDocuBtn_Click);
            // 
            // axDeskCtrlRireki
            // 
            this.axDeskCtrlRireki.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.axDeskCtrlRireki.Enabled = true;
            this.axDeskCtrlRireki.Location = new System.Drawing.Point(327, 6);
            this.axDeskCtrlRireki.Name = "axDeskCtrlRireki";
            this.axDeskCtrlRireki.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axDeskCtrlRireki.OcxState")));
            this.axDeskCtrlRireki.Size = new System.Drawing.Size(1089, 598);
            this.axDeskCtrlRireki.TabIndex = 0;
            // 
            // tpZumenInfo
            // 
            this.tpZumenInfo.Controls.Add(this.relatedZumenInfoList);
            this.tpZumenInfo.Controls.Add(this.panel3);
            this.tpZumenInfo.Location = new System.Drawing.Point(4, 34);
            this.tpZumenInfo.Name = "tpZumenInfo";
            this.tpZumenInfo.Size = new System.Drawing.Size(1362, 635);
            this.tpZumenInfo.TabIndex = 2;
            this.tpZumenInfo.Text = "図面情報";
            this.tpZumenInfo.UseVisualStyleBackColor = true;
            // 
            // relatedZumenInfoList
            // 
            this.relatedZumenInfoList.AllowUserToAddRows = false;
            this.relatedZumenInfoList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.relatedZumenInfoList.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.relatedZumenInfoList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.relatedZumenInfoList.ColumnHeadersHeight = 50;
            this.relatedZumenInfoList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.relatedZumenInfoList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.オーダーNO,
            this.品名,
            this.図面番号,
            this.数量,
            this.納期,
            this.加工先,
            this.作業状況});
            this.relatedZumenInfoList.EnableHeadersVisualStyles = false;
            this.relatedZumenInfoList.Location = new System.Drawing.Point(8, 298);
            this.relatedZumenInfoList.MultiSelect = false;
            this.relatedZumenInfoList.Name = "relatedZumenInfoList";
            this.relatedZumenInfoList.ReadOnly = true;
            this.relatedZumenInfoList.RowHeadersVisible = false;
            this.relatedZumenInfoList.RowTemplate.Height = 50;
            this.relatedZumenInfoList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.relatedZumenInfoList.Size = new System.Drawing.Size(1411, 300);
            this.relatedZumenInfoList.TabIndex = 26;
            // 
            // オーダーNO
            // 
            this.オーダーNO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.オーダーNO.FillWeight = 75F;
            this.オーダーNO.HeaderText = "オーダーNO";
            this.オーダーNO.Name = "オーダーNO";
            this.オーダーNO.ReadOnly = true;
            // 
            // 品名
            // 
            this.品名.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.品名.HeaderText = "品名";
            this.品名.Name = "品名";
            this.品名.ReadOnly = true;
            // 
            // 図面番号
            // 
            this.図面番号.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.図面番号.HeaderText = "図面番号";
            this.図面番号.Name = "図面番号";
            this.図面番号.ReadOnly = true;
            // 
            // 数量
            // 
            this.数量.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.数量.DefaultCellStyle = dataGridViewCellStyle2;
            this.数量.FillWeight = 40F;
            this.数量.HeaderText = "数量";
            this.数量.Name = "数量";
            this.数量.ReadOnly = true;
            // 
            // 納期
            // 
            this.納期.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.納期.FillWeight = 80F;
            this.納期.HeaderText = "納期";
            this.納期.Name = "納期";
            this.納期.ReadOnly = true;
            // 
            // 加工先
            // 
            this.加工先.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.加工先.HeaderText = "加工先";
            this.加工先.Name = "加工先";
            this.加工先.ReadOnly = true;
            // 
            // 作業状況
            // 
            this.作業状況.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.作業状況.HeaderText = "作業状況";
            this.作業状況.Name = "作業状況";
            this.作業状況.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label49);
            this.panel3.Controls.Add(this.txtBlockNo2);
            this.panel3.Controls.Add(this.label48);
            this.panel3.Controls.Add(this.txtTantou2);
            this.panel3.Controls.Add(this.txtZaiSize2);
            this.panel3.Controls.Add(this.label40);
            this.panel3.Controls.Add(this.txtNyukaDate2);
            this.panel3.Controls.Add(this.label31);
            this.panel3.Controls.Add(this.txtZaiQty2);
            this.panel3.Controls.Add(this.label30);
            this.panel3.Controls.Add(this.txtZairyo2);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Controls.Add(this.txtZaiNyuka2);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.txtKyakuNouki2);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.txtSinchoku2);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.txtKakosaki2);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.txtNouki2);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.txtKakoQty2);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.txtZumenBan3);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.txtProductName3);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.txtOrderNo3);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1362, 635);
            this.panel3.TabIndex = 0;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label49.Location = new System.Drawing.Point(370, 30);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(103, 24);
            this.label49.TabIndex = 28;
            this.label49.Text = "ブロックNo";
            // 
            // txtBlockNo2
            // 
            this.txtBlockNo2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtBlockNo2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBlockNo2.ForeColor = System.Drawing.Color.Gray;
            this.txtBlockNo2.Location = new System.Drawing.Point(480, 30);
            this.txtBlockNo2.Name = "txtBlockNo2";
            this.txtBlockNo2.ReadOnly = true;
            this.txtBlockNo2.Size = new System.Drawing.Size(168, 31);
            this.txtBlockNo2.TabIndex = 60;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label48.Location = new System.Drawing.Point(370, 210);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(82, 24);
            this.label48.TabIndex = 27;
            this.label48.Text = "担当者";
            // 
            // txtTantou2
            // 
            this.txtTantou2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtTantou2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantou2.ForeColor = System.Drawing.Color.Gray;
            this.txtTantou2.Location = new System.Drawing.Point(480, 210);
            this.txtTantou2.Name = "txtTantou2";
            this.txtTantou2.ReadOnly = true;
            this.txtTantou2.Size = new System.Drawing.Size(168, 31);
            this.txtTantou2.TabIndex = 59;
            // 
            // txtZaiSize2
            // 
            this.txtZaiSize2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZaiSize2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZaiSize2.ForeColor = System.Drawing.Color.Gray;
            this.txtZaiSize2.Location = new System.Drawing.Point(780, 165);
            this.txtZaiSize2.Name = "txtZaiSize2";
            this.txtZaiSize2.ReadOnly = true;
            this.txtZaiSize2.Size = new System.Drawing.Size(230, 31);
            this.txtZaiSize2.TabIndex = 60;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(670, 165);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(115, 24);
            this.label40.TabIndex = 59;
            this.label40.Text = "材料サイズ";
            // 
            // txtNyukaDate2
            // 
            this.txtNyukaDate2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtNyukaDate2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNyukaDate2.ForeColor = System.Drawing.Color.Gray;
            this.txtNyukaDate2.Location = new System.Drawing.Point(780, 210);
            this.txtNyukaDate2.Name = "txtNyukaDate2";
            this.txtNyukaDate2.ReadOnly = true;
            this.txtNyukaDate2.Size = new System.Drawing.Size(230, 31);
            this.txtNyukaDate2.TabIndex = 58;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label31.Location = new System.Drawing.Point(670, 210);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 24);
            this.label31.TabIndex = 57;
            this.label31.Text = "入荷日";
            // 
            // txtZaiQty2
            // 
            this.txtZaiQty2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZaiQty2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZaiQty2.ForeColor = System.Drawing.Color.Gray;
            this.txtZaiQty2.Location = new System.Drawing.Point(480, 165);
            this.txtZaiQty2.Name = "txtZaiQty2";
            this.txtZaiQty2.ReadOnly = true;
            this.txtZaiQty2.Size = new System.Drawing.Size(168, 31);
            this.txtZaiQty2.TabIndex = 56;
            this.txtZaiQty2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label30.Location = new System.Drawing.Point(370, 165);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(106, 24);
            this.label30.TabIndex = 55;
            this.label30.Text = "材料数量";
            // 
            // txtZairyo2
            // 
            this.txtZairyo2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZairyo2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZairyo2.ForeColor = System.Drawing.Color.Gray;
            this.txtZairyo2.Location = new System.Drawing.Point(780, 120);
            this.txtZairyo2.Name = "txtZairyo2";
            this.txtZairyo2.ReadOnly = true;
            this.txtZairyo2.Size = new System.Drawing.Size(230, 31);
            this.txtZairyo2.TabIndex = 54;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label29.Location = new System.Drawing.Point(670, 120);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(58, 24);
            this.label29.TabIndex = 53;
            this.label29.Text = "材料";
            // 
            // txtZaiNyuka2
            // 
            this.txtZaiNyuka2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZaiNyuka2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZaiNyuka2.ForeColor = System.Drawing.Color.Gray;
            this.txtZaiNyuka2.Location = new System.Drawing.Point(130, 165);
            this.txtZaiNyuka2.Name = "txtZaiNyuka2";
            this.txtZaiNyuka2.ReadOnly = true;
            this.txtZaiNyuka2.Size = new System.Drawing.Size(230, 31);
            this.txtZaiNyuka2.TabIndex = 52;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label28.Location = new System.Drawing.Point(3, 165);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(130, 24);
            this.label28.TabIndex = 51;
            this.label28.Text = "材料入荷先";
            // 
            // txtKyakuNouki2
            // 
            this.txtKyakuNouki2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtKyakuNouki2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyakuNouki2.ForeColor = System.Drawing.Color.Gray;
            this.txtKyakuNouki2.Location = new System.Drawing.Point(480, 75);
            this.txtKyakuNouki2.Name = "txtKyakuNouki2";
            this.txtKyakuNouki2.ReadOnly = true;
            this.txtKyakuNouki2.Size = new System.Drawing.Size(168, 31);
            this.txtKyakuNouki2.TabIndex = 50;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label26.Location = new System.Drawing.Point(370, 75);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(106, 24);
            this.label26.TabIndex = 49;
            this.label26.Text = "客先納期";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label27.Location = new System.Drawing.Point(6, 262);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(143, 33);
            this.label27.TabIndex = 48;
            this.label27.Text = "関連図番";
            // 
            // txtSinchoku2
            // 
            this.txtSinchoku2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtSinchoku2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSinchoku2.ForeColor = System.Drawing.Color.Gray;
            this.txtSinchoku2.Location = new System.Drawing.Point(130, 120);
            this.txtSinchoku2.Name = "txtSinchoku2";
            this.txtSinchoku2.ReadOnly = true;
            this.txtSinchoku2.Size = new System.Drawing.Size(230, 31);
            this.txtSinchoku2.TabIndex = 45;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label25.Location = new System.Drawing.Point(3, 120);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(106, 24);
            this.label25.TabIndex = 44;
            this.label25.Text = "作業状況";
            // 
            // txtKakosaki2
            // 
            this.txtKakosaki2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtKakosaki2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKakosaki2.ForeColor = System.Drawing.Color.Gray;
            this.txtKakosaki2.Location = new System.Drawing.Point(780, 75);
            this.txtKakosaki2.Name = "txtKakosaki2";
            this.txtKakosaki2.ReadOnly = true;
            this.txtKakosaki2.Size = new System.Drawing.Size(230, 31);
            this.txtKakosaki2.TabIndex = 43;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label24.Location = new System.Drawing.Point(670, 75);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 24);
            this.label24.TabIndex = 42;
            this.label24.Text = "加工先";
            // 
            // txtNouki2
            // 
            this.txtNouki2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtNouki2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNouki2.ForeColor = System.Drawing.Color.Gray;
            this.txtNouki2.Location = new System.Drawing.Point(130, 75);
            this.txtNouki2.Name = "txtNouki2";
            this.txtNouki2.ReadOnly = true;
            this.txtNouki2.Size = new System.Drawing.Size(230, 31);
            this.txtNouki2.TabIndex = 41;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label23.Location = new System.Drawing.Point(3, 75);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 24);
            this.label23.TabIndex = 40;
            this.label23.Text = "納期";
            // 
            // txtKakoQty2
            // 
            this.txtKakoQty2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtKakoQty2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKakoQty2.ForeColor = System.Drawing.Color.Gray;
            this.txtKakoQty2.Location = new System.Drawing.Point(480, 120);
            this.txtKakoQty2.Name = "txtKakoQty2";
            this.txtKakoQty2.ReadOnly = true;
            this.txtKakoQty2.Size = new System.Drawing.Size(168, 31);
            this.txtKakoQty2.TabIndex = 34;
            this.txtKakoQty2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label22.Location = new System.Drawing.Point(370, 120);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(58, 24);
            this.label22.TabIndex = 33;
            this.label22.Text = "数量";
            // 
            // txtZumenBan3
            // 
            this.txtZumenBan3.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZumenBan3.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZumenBan3.ForeColor = System.Drawing.Color.Gray;
            this.txtZumenBan3.Location = new System.Drawing.Point(780, 30);
            this.txtZumenBan3.Name = "txtZumenBan3";
            this.txtZumenBan3.ReadOnly = true;
            this.txtZumenBan3.Size = new System.Drawing.Size(230, 31);
            this.txtZumenBan3.TabIndex = 32;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label21.Location = new System.Drawing.Point(670, 30);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(106, 24);
            this.label21.TabIndex = 31;
            this.label21.Text = "図面番号";
            // 
            // txtProductName3
            // 
            this.txtProductName3.BackColor = System.Drawing.Color.Aquamarine;
            this.txtProductName3.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtProductName3.ForeColor = System.Drawing.Color.Gray;
            this.txtProductName3.Location = new System.Drawing.Point(130, 210);
            this.txtProductName3.Name = "txtProductName3";
            this.txtProductName3.ReadOnly = true;
            this.txtProductName3.Size = new System.Drawing.Size(230, 31);
            this.txtProductName3.TabIndex = 30;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label20.Location = new System.Drawing.Point(3, 210);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 24);
            this.label20.TabIndex = 29;
            this.label20.Text = "品名";
            // 
            // txtOrderNo3
            // 
            this.txtOrderNo3.BackColor = System.Drawing.Color.Aquamarine;
            this.txtOrderNo3.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo3.ForeColor = System.Drawing.Color.Gray;
            this.txtOrderNo3.Location = new System.Drawing.Point(130, 30);
            this.txtOrderNo3.Name = "txtOrderNo3";
            this.txtOrderNo3.ReadOnly = true;
            this.txtOrderNo3.Size = new System.Drawing.Size(230, 31);
            this.txtOrderNo3.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(3, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 24);
            this.label7.TabIndex = 9;
            this.label7.Text = "オーダーNo";
            // 
            // tpKouteiInfo
            // 
            this.tpKouteiInfo.Controls.Add(this.kouteiInfoList);
            this.tpKouteiInfo.Location = new System.Drawing.Point(4, 34);
            this.tpKouteiInfo.Name = "tpKouteiInfo";
            this.tpKouteiInfo.Size = new System.Drawing.Size(1362, 635);
            this.tpKouteiInfo.TabIndex = 3;
            this.tpKouteiInfo.Text = "工程情報";
            this.tpKouteiInfo.UseVisualStyleBackColor = true;
            // 
            // kouteiInfoList
            // 
            this.kouteiInfoList.AllowUserToAddRows = false;
            this.kouteiInfoList.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.kouteiInfoList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.kouteiInfoList.ColumnHeadersHeight = 50;
            this.kouteiInfoList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.kouteiInfoList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.kouteiInfoList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kouteiInfoList.EnableHeadersVisualStyles = false;
            this.kouteiInfoList.Location = new System.Drawing.Point(0, 0);
            this.kouteiInfoList.MultiSelect = false;
            this.kouteiInfoList.Name = "kouteiInfoList";
            this.kouteiInfoList.ReadOnly = true;
            this.kouteiInfoList.RowHeadersVisible = false;
            this.kouteiInfoList.RowTemplate.Height = 50;
            this.kouteiInfoList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.kouteiInfoList.Size = new System.Drawing.Size(1362, 635);
            this.kouteiInfoList.TabIndex = 27;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "工程順";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.FillWeight = 90F;
            this.dataGridViewTextBoxColumn2.HeaderText = "加工先";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.FillWeight = 90F;
            this.dataGridViewTextBoxColumn3.HeaderText = "作業工程";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.FillWeight = 70F;
            this.dataGridViewTextBoxColumn4.HeaderText = "予定日";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "備考";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // tpSameZumenList
            // 
            this.tpSameZumenList.BackColor = System.Drawing.Color.Transparent;
            this.tpSameZumenList.Controls.Add(this.sameZumenList);
            this.tpSameZumenList.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tpSameZumenList.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tpSameZumenList.Location = new System.Drawing.Point(4, 34);
            this.tpSameZumenList.Name = "tpSameZumenList";
            this.tpSameZumenList.Size = new System.Drawing.Size(1362, 635);
            this.tpSameZumenList.TabIndex = 4;
            this.tpSameZumenList.Text = "同図面一覧";
            // 
            // sameZumenList
            // 
            this.sameZumenList.AllowUserToAddRows = false;
            this.sameZumenList.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.sameZumenList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.sameZumenList.ColumnHeadersHeight = 50;
            this.sameZumenList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.sameZumenList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.オーダーNO2,
            this.品名2,
            this.図面番号2,
            this.数量2,
            this.納期2,
            this.前回作業者2,
            this.加工先2,
            this.作業状況2});
            this.sameZumenList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sameZumenList.EnableHeadersVisualStyles = false;
            this.sameZumenList.Location = new System.Drawing.Point(0, 0);
            this.sameZumenList.MultiSelect = false;
            this.sameZumenList.Name = "sameZumenList";
            this.sameZumenList.ReadOnly = true;
            this.sameZumenList.RowHeadersVisible = false;
            this.sameZumenList.RowTemplate.Height = 50;
            this.sameZumenList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sameZumenList.Size = new System.Drawing.Size(1362, 635);
            this.sameZumenList.TabIndex = 28;
            // 
            // オーダーNO2
            // 
            this.オーダーNO2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.オーダーNO2.FillWeight = 75F;
            this.オーダーNO2.HeaderText = "オーダーNO";
            this.オーダーNO2.Name = "オーダーNO2";
            this.オーダーNO2.ReadOnly = true;
            // 
            // 品名2
            // 
            this.品名2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.品名2.FillWeight = 90F;
            this.品名2.HeaderText = "品名";
            this.品名2.Name = "品名2";
            this.品名2.ReadOnly = true;
            // 
            // 図面番号2
            // 
            this.図面番号2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.図面番号2.FillWeight = 90F;
            this.図面番号2.HeaderText = "図面番号";
            this.図面番号2.Name = "図面番号2";
            this.図面番号2.ReadOnly = true;
            // 
            // 数量2
            // 
            this.数量2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.数量2.FillWeight = 40F;
            this.数量2.HeaderText = "数量";
            this.数量2.Name = "数量2";
            this.数量2.ReadOnly = true;
            // 
            // 納期2
            // 
            this.納期2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.納期2.FillWeight = 80F;
            this.納期2.HeaderText = "納期";
            this.納期2.Name = "納期2";
            this.納期2.ReadOnly = true;
            // 
            // 前回作業者2
            // 
            this.前回作業者2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.前回作業者2.HeaderText = "前回作業者";
            this.前回作業者2.Name = "前回作業者2";
            this.前回作業者2.ReadOnly = true;
            // 
            // 加工先2
            // 
            this.加工先2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.加工先2.HeaderText = "加工先";
            this.加工先2.Name = "加工先2";
            this.加工先2.ReadOnly = true;
            // 
            // 作業状況2
            // 
            this.作業状況2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.作業状況2.HeaderText = "作業状況";
            this.作業状況2.Name = "作業状況2";
            this.作業状況2.ReadOnly = true;
            // 
            // tpQueryMail
            // 
            this.tpQueryMail.Controls.Add(this.btnQueryMail);
            this.tpQueryMail.Controls.Add(this.queryDocuBtn);
            this.tpQueryMail.Controls.Add(this.queryList);
            this.tpQueryMail.Controls.Add(this.panel4);
            this.tpQueryMail.Controls.Add(this.axDeskCtrlQuery);
            this.tpQueryMail.Location = new System.Drawing.Point(4, 34);
            this.tpQueryMail.Name = "tpQueryMail";
            this.tpQueryMail.Size = new System.Drawing.Size(1362, 635);
            this.tpQueryMail.TabIndex = 5;
            this.tpQueryMail.Text = "図面問い合わせ";
            this.tpQueryMail.UseVisualStyleBackColor = true;
            // 
            // btnQueryMail
            // 
            this.btnQueryMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnQueryMail.BackColor = System.Drawing.Color.Cyan;
            this.btnQueryMail.Location = new System.Drawing.Point(8, 549);
            this.btnQueryMail.Name = "btnQueryMail";
            this.btnQueryMail.Size = new System.Drawing.Size(312, 49);
            this.btnQueryMail.TabIndex = 11;
            this.btnQueryMail.Text = "図面問い合わせ";
            this.btnQueryMail.UseVisualStyleBackColor = false;
            this.btnQueryMail.Click += new System.EventHandler(this.btnQueryMail_Click);
            // 
            // queryDocuBtn
            // 
            this.queryDocuBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.queryDocuBtn.BackColor = System.Drawing.Color.Tomato;
            this.queryDocuBtn.Location = new System.Drawing.Point(8, 494);
            this.queryDocuBtn.Name = "queryDocuBtn";
            this.queryDocuBtn.Size = new System.Drawing.Size(312, 49);
            this.queryDocuBtn.TabIndex = 10;
            this.queryDocuBtn.Text = "編集する";
            this.queryDocuBtn.UseVisualStyleBackColor = false;
            this.queryDocuBtn.Click += new System.EventHandler(this.queryDocuBtn_Click);
            // 
            // queryList
            // 
            this.queryList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.queryList.FormattingEnabled = true;
            this.queryList.HorizontalScrollbar = true;
            this.queryList.ItemHeight = 24;
            this.queryList.Location = new System.Drawing.Point(8, 264);
            this.queryList.Name = "queryList";
            this.queryList.Size = new System.Drawing.Size(312, 220);
            this.queryList.TabIndex = 9;
            this.queryList.SelectedIndexChanged += new System.EventHandler(this.queryList_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtUpdater2);
            this.panel4.Controls.Add(this.txtUpdateDatetime2);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.label42);
            this.panel4.Controls.Add(this.txtZumenBan4);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.txtProductName4);
            this.panel4.Controls.Add(this.txtOrderNo4);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Controls.Add(this.label45);
            this.panel4.Location = new System.Drawing.Point(8, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(312, 252);
            this.panel4.TabIndex = 8;
            // 
            // txtUpdater2
            // 
            this.txtUpdater2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtUpdater2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUpdater2.ForeColor = System.Drawing.Color.Gray;
            this.txtUpdater2.Location = new System.Drawing.Point(134, 210);
            this.txtUpdater2.Name = "txtUpdater2";
            this.txtUpdater2.ReadOnly = true;
            this.txtUpdater2.Size = new System.Drawing.Size(170, 26);
            this.txtUpdater2.TabIndex = 43;
            // 
            // txtUpdateDatetime2
            // 
            this.txtUpdateDatetime2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtUpdateDatetime2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUpdateDatetime2.ForeColor = System.Drawing.Color.Gray;
            this.txtUpdateDatetime2.Location = new System.Drawing.Point(134, 163);
            this.txtUpdateDatetime2.Name = "txtUpdateDatetime2";
            this.txtUpdateDatetime2.ReadOnly = true;
            this.txtUpdateDatetime2.Size = new System.Drawing.Size(170, 26);
            this.txtUpdateDatetime2.TabIndex = 42;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label41.Location = new System.Drawing.Point(16, 210);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 24);
            this.label41.TabIndex = 41;
            this.label41.Text = "更新者";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label42.Location = new System.Drawing.Point(16, 161);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(106, 24);
            this.label42.TabIndex = 40;
            this.label42.Text = "更新日時";
            // 
            // txtZumenBan4
            // 
            this.txtZumenBan4.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZumenBan4.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZumenBan4.ForeColor = System.Drawing.Color.Gray;
            this.txtZumenBan4.Location = new System.Drawing.Point(134, 114);
            this.txtZumenBan4.Name = "txtZumenBan4";
            this.txtZumenBan4.ReadOnly = true;
            this.txtZumenBan4.Size = new System.Drawing.Size(170, 26);
            this.txtZumenBan4.TabIndex = 39;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label43.Location = new System.Drawing.Point(16, 114);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(106, 24);
            this.label43.TabIndex = 37;
            this.label43.Text = "図面番号";
            // 
            // txtProductName4
            // 
            this.txtProductName4.BackColor = System.Drawing.Color.Aquamarine;
            this.txtProductName4.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtProductName4.ForeColor = System.Drawing.Color.Gray;
            this.txtProductName4.Location = new System.Drawing.Point(134, 66);
            this.txtProductName4.Name = "txtProductName4";
            this.txtProductName4.ReadOnly = true;
            this.txtProductName4.Size = new System.Drawing.Size(170, 26);
            this.txtProductName4.TabIndex = 35;
            // 
            // txtOrderNo4
            // 
            this.txtOrderNo4.BackColor = System.Drawing.Color.Aquamarine;
            this.txtOrderNo4.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo4.ForeColor = System.Drawing.Color.Gray;
            this.txtOrderNo4.Location = new System.Drawing.Point(134, 18);
            this.txtOrderNo4.Name = "txtOrderNo4";
            this.txtOrderNo4.ReadOnly = true;
            this.txtOrderNo4.Size = new System.Drawing.Size(170, 26);
            this.txtOrderNo4.TabIndex = 34;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.White;
            this.label44.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label44.Location = new System.Drawing.Point(16, 64);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 24);
            this.label44.TabIndex = 10;
            this.label44.Text = "品名";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label45.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label45.Location = new System.Drawing.Point(16, 18);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(112, 24);
            this.label45.TabIndex = 9;
            this.label45.Text = "オーダーNo";
            // 
            // axDeskCtrlQuery
            // 
            this.axDeskCtrlQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.axDeskCtrlQuery.Enabled = true;
            this.axDeskCtrlQuery.Location = new System.Drawing.Point(326, 6);
            this.axDeskCtrlQuery.Name = "axDeskCtrlQuery";
            this.axDeskCtrlQuery.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axDeskCtrlQuery.OcxState")));
            this.axDeskCtrlQuery.Size = new System.Drawing.Size(1088, 598);
            this.axDeskCtrlQuery.TabIndex = 1;
            // 
            // tpWorkingRegister
            // 
            this.tpWorkingRegister.BackColor = System.Drawing.Color.White;
            this.tpWorkingRegister.Controls.Add(this.messageLabel);
            this.tpWorkingRegister.Controls.Add(this.label18);
            this.tpWorkingRegister.Controls.Add(this.dataGridView1);
            this.tpWorkingRegister.Controls.Add(this.panel7);
            this.tpWorkingRegister.Controls.Add(this.panel5);
            this.tpWorkingRegister.Location = new System.Drawing.Point(4, 34);
            this.tpWorkingRegister.Name = "tpWorkingRegister";
            this.tpWorkingRegister.Padding = new System.Windows.Forms.Padding(3);
            this.tpWorkingRegister.Size = new System.Drawing.Size(1362, 635);
            this.tpWorkingRegister.TabIndex = 6;
            this.tpWorkingRegister.Text = "図面作業者登録";
            this.tpWorkingRegister.TextChanged += new System.EventHandler(this.BarTextBox_TextChanged);
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.messageLabel.Location = new System.Drawing.Point(440, 250);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(0, 24);
            this.messageLabel.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(19, 250);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 24);
            this.label18.TabIndex = 4;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.drawingNumber,
            this.worker,
            this.WorkRegistrationDate,
            this.KouteihyouNo});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView1.Location = new System.Drawing.Point(19, 292);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 70;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView1.RowTemplate.Height = 44;
            this.dataGridView1.RowTemplate.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1094, 294);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.TabStop = false;
            this.dataGridView1.UseWaitCursor = true;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.DataPropertyName = "No";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column1.FillWeight = 200F;
            this.Column1.HeaderText = "No";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 250;
            // 
            // drawingNumber
            // 
            this.drawingNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.drawingNumber.DataPropertyName = "DrawingNumber";
            this.drawingNumber.FillWeight = 9.3535F;
            this.drawingNumber.HeaderText = "図面番号";
            this.drawingNumber.Name = "drawingNumber";
            this.drawingNumber.ReadOnly = true;
            this.drawingNumber.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.drawingNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // worker
            // 
            this.worker.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.worker.DataPropertyName = "Worker";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.worker.DefaultCellStyle = dataGridViewCellStyle7;
            this.worker.FillWeight = 4.67675F;
            this.worker.HeaderText = "作業者";
            this.worker.Name = "worker";
            this.worker.ReadOnly = true;
            // 
            // WorkRegistrationDate
            // 
            this.WorkRegistrationDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.WorkRegistrationDate.DataPropertyName = "WorkRegistrationDate";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.WorkRegistrationDate.DefaultCellStyle = dataGridViewCellStyle8;
            this.WorkRegistrationDate.FillWeight = 4.67675F;
            this.WorkRegistrationDate.HeaderText = "作業登録日";
            this.WorkRegistrationDate.Name = "WorkRegistrationDate";
            this.WorkRegistrationDate.ReadOnly = true;
            // 
            // KouteihyouNo
            // 
            this.KouteihyouNo.DataPropertyName = "KouteihyouNo";
            this.KouteihyouNo.HeaderText = "Column2";
            this.KouteihyouNo.Name = "KouteihyouNo";
            this.KouteihyouNo.ReadOnly = true;
            this.KouteihyouNo.Visible = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label52);
            this.panel7.Controls.Add(this.wokerComboBox);
            this.panel7.Controls.Add(this.NameScanBtn);
            this.panel7.Controls.Add(this.BarLabel);
            this.panel7.Controls.Add(this.NameLabel);
            this.panel7.Controls.Add(this.NameTextBox);
            this.panel7.Controls.Add(this.label50);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.kouteiScanBtn);
            this.panel7.Controls.Add(this.registerBtn);
            this.panel7.Controls.Add(this.BarTextBox);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Location = new System.Drawing.Point(440, 21);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(673, 215);
            this.panel7.TabIndex = 2;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(194, 17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(82, 24);
            this.label52.TabIndex = 21;
            this.label52.Text = "作業者";
            // 
            // wokerComboBox
            // 
            this.wokerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.wokerComboBox.FormattingEnabled = true;
            this.wokerComboBox.Location = new System.Drawing.Point(288, 14);
            this.wokerComboBox.Name = "wokerComboBox";
            this.wokerComboBox.Size = new System.Drawing.Size(300, 32);
            this.wokerComboBox.TabIndex = 20;
            this.wokerComboBox.TextChanged += new System.EventHandler(this.wokerComboBox_TextChanged);
            // 
            // NameScanBtn
            // 
            this.NameScanBtn.Location = new System.Drawing.Point(435, 63);
            this.NameScanBtn.Name = "NameScanBtn";
            this.NameScanBtn.Size = new System.Drawing.Size(130, 36);
            this.NameScanBtn.TabIndex = 19;
            this.NameScanBtn.Text = "スキャン";
            this.NameScanBtn.UseVisualStyleBackColor = true;
            this.NameScanBtn.Click += new System.EventHandler(this.NameScanBtn_Click);
            // 
            // BarLabel
            // 
            this.BarLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BarLabel.ForeColor = System.Drawing.Color.Black;
            this.BarLabel.Location = new System.Drawing.Point(105, 162);
            this.BarLabel.Name = "BarLabel";
            this.BarLabel.Size = new System.Drawing.Size(315, 31);
            this.BarLabel.TabIndex = 18;
            // 
            // NameLabel
            // 
            this.NameLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NameLabel.ForeColor = System.Drawing.Color.Black;
            this.NameLabel.Location = new System.Drawing.Point(198, 66);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(222, 31);
            this.NameLabel.TabIndex = 17;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.NameTextBox.Location = new System.Drawing.Point(105, 66);
            this.NameTextBox.MaxLength = 3;
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(87, 31);
            this.NameTextBox.TabIndex = 16;
            this.NameTextBox.TextChanged += new System.EventHandler(this.NameTextBox_TextChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(17, 65);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(82, 24);
            this.label50.TabIndex = 14;
            this.label50.Text = "作業者";
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Gainsboro;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Font = new System.Drawing.Font("MS UI Gothic", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.Location = new System.Drawing.Point(13, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(157, 40);
            this.label12.TabIndex = 4;
            this.label12.Text = "作業登録";
            // 
            // kouteiScanBtn
            // 
            this.kouteiScanBtn.Location = new System.Drawing.Point(435, 115);
            this.kouteiScanBtn.Name = "kouteiScanBtn";
            this.kouteiScanBtn.Size = new System.Drawing.Size(130, 36);
            this.kouteiScanBtn.TabIndex = 13;
            this.kouteiScanBtn.Text = "スキャン";
            this.kouteiScanBtn.UseVisualStyleBackColor = true;
            this.kouteiScanBtn.Click += new System.EventHandler(this.KouteiScanBtn_Click);
            // 
            // registerBtn
            // 
            this.registerBtn.Enabled = false;
            this.registerBtn.Location = new System.Drawing.Point(527, 157);
            this.registerBtn.Name = "registerBtn";
            this.registerBtn.Size = new System.Drawing.Size(130, 36);
            this.registerBtn.TabIndex = 11;
            this.registerBtn.Text = "登録";
            this.registerBtn.UseVisualStyleBackColor = true;
            this.registerBtn.Click += new System.EventHandler(this.RegisterBtn_Click);
            // 
            // BarTextBox
            // 
            this.BarTextBox.Location = new System.Drawing.Point(105, 115);
            this.BarTextBox.MaxLength = 8;
            this.BarTextBox.Name = "BarTextBox";
            this.BarTextBox.Size = new System.Drawing.Size(315, 31);
            this.BarTextBox.TabIndex = 1;
            this.BarTextBox.TextChanged += new System.EventHandler(this.BarTextBox_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 115);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 24);
            this.label10.TabIndex = 8;
            this.label10.Text = "工程表";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.userName);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.searchBtn);
            this.panel5.Controls.Add(this.userCd);
            this.panel5.Controls.Add(this.zumenn_no);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label5);
            this.panel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel5.Location = new System.Drawing.Point(19, 21);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(398, 215);
            this.panel5.TabIndex = 0;
            // 
            // userName
            // 
            this.userName.AutoSize = true;
            this.userName.Location = new System.Drawing.Point(49, 157);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(79, 24);
            this.userName.TabIndex = 7;
            this.userName.Text = "label51";
            this.userName.Visible = false;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Gainsboro;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("MS UI Gothic", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label15.Location = new System.Drawing.Point(13, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(216, 40);
            this.label15.TabIndex = 5;
            this.label15.Text = "作業者検索";
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(227, 152);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(130, 36);
            this.searchBtn.TabIndex = 6;
            this.searchBtn.Text = "検索";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // userCd
            // 
            this.userCd.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.userCd.Location = new System.Drawing.Point(152, 115);
            this.userCd.MaxLength = 3;
            this.userCd.Name = "userCd";
            this.userCd.Size = new System.Drawing.Size(205, 31);
            this.userCd.TabIndex = 5;
            // 
            // zumenn_no
            // 
            this.zumenn_no.Location = new System.Drawing.Point(152, 62);
            this.zumenn_no.Name = "zumenn_no";
            this.zumenn_no.Size = new System.Drawing.Size(205, 31);
            this.zumenn_no.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 24);
            this.label6.TabIndex = 3;
            this.label6.Text = "作業者コード";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "図面番号";
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.statusStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 675);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1370, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Yu Gothic UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Red;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // lbl名前
            // 
            this.lbl名前.AutoSize = true;
            this.lbl名前.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl名前.Location = new System.Drawing.Point(84, 4);
            this.lbl名前.Name = "lbl名前";
            this.lbl名前.Size = new System.Drawing.Size(54, 16);
            this.lbl名前.TabIndex = 3;
            this.lbl名前.Text = "label56";
            // 
            // MainDisp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 697);
            this.Controls.Add(this.lbl名前);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl);
            this.MinimumSize = new System.Drawing.Size(800, 700);
            this.Name = "MainDisp";
            this.Text = "図面ビューアーアプリ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            //this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.MainDisp_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDisp_FormClosing);
            this.Load += new System.EventHandler(this.MainDisp_Load);
            this.Controls.SetChildIndex(this.tabControl, 0);
            this.Controls.SetChildIndex(this.statusStrip1, 0);
            this.Controls.SetChildIndex(this.lbl名前, 0);
            this.tabControl.ResumeLayout(false);
            this.tpSearch.ResumeLayout(false);
            this.tpSearch.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrlZumen)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpZumen.ResumeLayout(false);
            this.tpZumen.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrlRireki)).EndInit();
            this.tpZumenInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.relatedZumenInfoList)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tpKouteiInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kouteiInfoList)).EndInit();
            this.tpSameZumenList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sameZumenList)).EndInit();
            this.tpQueryMail.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrlQuery)).EndInit();
            this.tpWorkingRegister.ResumeLayout(false);
            this.tpWorkingRegister.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpZumen;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private AxDESKCTRLLib.AxDeskCtrl axDeskCtrlZumen;
        private System.Windows.Forms.Button rirekiDocuBtn;
        private System.Windows.Forms.ListBox rirekiList;
        private System.Windows.Forms.TabPage tpSearch;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button zumenSearchBtn;
        private System.Windows.Forms.Button scanBtn;
        private AxDESKCTRLLib.AxDeskCtrl axDeskCtrlRireki;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtKakosaki;
        private System.Windows.Forms.TextBox txtKakoQty;
        private System.Windows.Forms.TextBox txtZumenBan;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtZumenBan2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtProductName2;
        private System.Windows.Forms.TextBox txtOrderNo2;
        private System.Windows.Forms.Button zumenDocuBtn;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button fullDispBtn;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtUpdateDatetime;
        private System.Windows.Forms.TextBox txtUpdater;
        private System.Windows.Forms.Button KumiSearchBtn;
        public System.Windows.Forms.TreeView zumenList;
        private System.Windows.Forms.Button pdfBtn;
        private System.Windows.Forms.Label KumiZumen;
        private System.Windows.Forms.ListView announceList;
        private System.Windows.Forms.Button btnKumiMenu;
        private System.Windows.Forms.TabPage tpZumenInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtOrderNo3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtProductName3;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtZumenBan3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtKakoQty2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtNouki2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtKakosaki2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtSinchoku2;
        private System.Windows.Forms.DataGridView relatedZumenInfoList;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabPage tpKouteiInfo;
        private System.Windows.Forms.DataGridView kouteiInfoList;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.TextBox txtKyakuNouki2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtNyukaDate2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtZaiQty2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtZairyo2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtZaiNyuka2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtNouki;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtNyukaDate;
        private System.Windows.Forms.TextBox txtZaiQty;
        private System.Windows.Forms.TextBox txtZairyo;
        private System.Windows.Forms.TextBox txtZaiNyuka;
        private System.Windows.Forms.TextBox txtSinchoku;
        private System.Windows.Forms.TextBox txtKyakuNouki;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabPage tpSameZumenList;
        private System.Windows.Forms.DataGridView sameZumenList;
        private System.Windows.Forms.TextBox txtZaiSize;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtZaiSize2;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.TabPage tpQueryMail;
        private System.Windows.Forms.ListBox queryList;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtUpdater2;
        private System.Windows.Forms.TextBox txtUpdateDatetime2;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtZumenBan4;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtProductName4;
        private System.Windows.Forms.TextBox txtOrderNo4;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private AxDESKCTRLLib.AxDeskCtrl axDeskCtrlQuery;
        private System.Windows.Forms.Button btnQueryMail;
        private System.Windows.Forms.Button queryDocuBtn;
        private System.Windows.Forms.Button btnTransmission;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtTantou;
        private System.Windows.Forms.TextBox txtBlockNo;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txtBlockNo2;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtTantou2;
        private System.Windows.Forms.RadioButton rdoBarcodeReader;
        private System.Windows.Forms.RadioButton rdoKamera;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tpWorkingRegister;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TextBox userCd;
        private System.Windows.Forms.TextBox zumenn_no;
        private System.Windows.Forms.Button registerBtn;
        private System.Windows.Forms.TextBox BarTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button kouteiScanBtn;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label BarLabel;
        private System.Windows.Forms.Button NameScanBtn;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Button btnRefKumiJob;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewButtonColumn drawingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn worker;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkRegistrationDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteihyouNo;
        private System.Windows.Forms.Button rirekiSearchBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn オーダーNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn 品名;
        private System.Windows.Forms.DataGridViewTextBoxColumn 図面番号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 納期;
        private System.Windows.Forms.DataGridViewTextBoxColumn 加工先;
        private System.Windows.Forms.DataGridViewTextBoxColumn 作業状況;
        private System.Windows.Forms.DataGridViewTextBoxColumn オーダーNO2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 品名2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 図面番号2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 数量2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 納期2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 前回作業者2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 加工先2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 作業状況2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtKakouTantou;
        private System.Windows.Forms.Label userName;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txtKouteihyouNo;
        private System.Windows.Forms.Button btnWiring;
        private System.Windows.Forms.CheckBox chkIncOtherOrder;
        private System.Windows.Forms.Label lbl名前;
        private System.Windows.Forms.Button btnKakouIndication;
        private System.Windows.Forms.Button btnKakouIndicationList;
        private System.Windows.Forms.ComboBox wokerComboBox;
        private System.Windows.Forms.Label label52;
    }
}

