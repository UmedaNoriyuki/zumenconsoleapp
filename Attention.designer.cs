﻿namespace ZumenConsoleApp
{
    partial class Attention
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AttentionList = new System.Windows.Forms.ListBox();
            this.selectBtn = new System.Windows.Forms.Button();
            this.delBtn = new System.Windows.Forms.Button();
            this.attentionTxt = new System.Windows.Forms.TextBox();
            this.regstBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AttentionList
            // 
            this.AttentionList.Font = new System.Drawing.Font("メイリオ", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.AttentionList.FormattingEnabled = true;
            this.AttentionList.ItemHeight = 48;
            this.AttentionList.Location = new System.Drawing.Point(24, 16);
            this.AttentionList.Name = "AttentionList";
            this.AttentionList.ScrollAlwaysVisible = true;
            this.AttentionList.Size = new System.Drawing.Size(648, 340);
            this.AttentionList.TabIndex = 0;
            this.AttentionList.DoubleClick += new System.EventHandler(this.DoubleClickListBox);
            // 
            // selectBtn
            // 
            this.selectBtn.Font = new System.Drawing.Font("メイリオ", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.selectBtn.Location = new System.Drawing.Point(534, 364);
            this.selectBtn.Name = "selectBtn";
            this.selectBtn.Size = new System.Drawing.Size(138, 56);
            this.selectBtn.TabIndex = 1;
            this.selectBtn.Text = "コピー";
            this.selectBtn.UseVisualStyleBackColor = true;
            this.selectBtn.Click += new System.EventHandler(this.ClickSelectBtn);
            // 
            // delBtn
            // 
            this.delBtn.Font = new System.Drawing.Font("メイリオ", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.delBtn.Location = new System.Drawing.Point(24, 364);
            this.delBtn.Name = "delBtn";
            this.delBtn.Size = new System.Drawing.Size(138, 56);
            this.delBtn.TabIndex = 2;
            this.delBtn.Text = "削除";
            this.delBtn.UseVisualStyleBackColor = true;
            this.delBtn.Click += new System.EventHandler(this.ClickDelBtn);
            // 
            // attentionTxt
            // 
            this.attentionTxt.Font = new System.Drawing.Font("メイリオ", 24F);
            this.attentionTxt.Location = new System.Drawing.Point(24, 428);
            this.attentionTxt.Name = "attentionTxt";
            this.attentionTxt.Size = new System.Drawing.Size(648, 55);
            this.attentionTxt.TabIndex = 3;
            // 
            // regstBtn
            // 
            this.regstBtn.Font = new System.Drawing.Font("メイリオ", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.regstBtn.Location = new System.Drawing.Point(24, 492);
            this.regstBtn.Name = "regstBtn";
            this.regstBtn.Size = new System.Drawing.Size(138, 56);
            this.regstBtn.TabIndex = 4;
            this.regstBtn.Text = "登録";
            this.regstBtn.UseVisualStyleBackColor = true;
            this.regstBtn.Click += new System.EventHandler(this.ClickRegstBtn);
            // 
            // Attention
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 559);
            this.Controls.Add(this.regstBtn);
            this.Controls.Add(this.attentionTxt);
            this.Controls.Add(this.delBtn);
            this.Controls.Add(this.selectBtn);
            this.Controls.Add(this.AttentionList);
            this.Name = "Attention";
            this.Text = "注意事項";
            this.Load += new System.EventHandler(this.LoadListBox);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox AttentionList;
        private System.Windows.Forms.Button selectBtn;
        private System.Windows.Forms.Button delBtn;
        private System.Windows.Forms.TextBox attentionTxt;
        private System.Windows.Forms.Button regstBtn;
    }
}