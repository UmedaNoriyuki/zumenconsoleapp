﻿
using System;
using System.Collections.Generic;

namespace ZumenConsoleApp
{
    /// <summary>
    /// 加工指示書データ
    /// </summary>
    public class KakouIndicationInfo
    {
        /// <summary>
        /// 加工指示書コード
        /// </summary>
        public int KakouIndicationCd { get; set; }

        /// <summary>
        /// 工程表番号
        /// </summary>
        public string KouteihyouNo { get; set; }

        /// <summary>
        /// オーダーNO
        /// </summary>
        public string OdaNo { get; set; }

        /// <summary>
        /// 作成者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 現在工程順
        /// </summary>
        public int NowKoutei { get; set; }

        /// <summary>
        /// 現在作業工程順
        /// </summary>
        public int NowWorkKoutei { get; set; }

        /// <summary>
        /// 工程データ
        /// </summary>
        public List<KouteiInfo> KouteiInfoList { get; set; }
    }

    /// <summary>
    /// 加工指示書工程データ
    /// </summary>
    public class KouteiInfo
    {
        /// <summary>
        /// 工程順
        /// </summary>
        public int KouteiNo { get; set; }

        /// <summary>
        /// 加工先
        /// </summary>
        public int KakouSaki { get; set; }

        /// <summary>
        /// 加工指示書作業データ
        /// </summary>
        public List<SagyoInfo> SagyoInfoList { get; set; }
    }

    /// <summary>
    /// 加工指示書作業データ
    /// </summary>
    public class SagyoInfo
    {
        /// <summary>
        /// 工程順
        /// </summary>
        public int KouteiNo { get; set; }

        /// <summary>
        /// 作業工程順
        /// </summary>
        public int SagyoKouteiNo { get; set; }

        /// <summary>
        /// 作業工程
        /// </summary>
        public int SagyoKoutei { get; set; }

        /// <summary>
        /// 完了予定日
        /// </summary>
        public DateTime? CompletionDate { get; set; }

        /// <summary>
        /// 出荷
        /// </summary>
        public string Shipment { get; set; }

        /// <summary>
        /// 注意事項
        /// </summary>
        public string Precautions { get; set; }

        /// <summary>
        /// 注意
        /// </summary>
        public bool Check { get; set; }

        /// <summary>
        /// 作業者
        /// </summary>
        public string Worker { get; set; }

        /// <summary>
        /// 作業完了日
        /// </summary>
        public DateTime? CompletedDate { get; set; }

    }
}
