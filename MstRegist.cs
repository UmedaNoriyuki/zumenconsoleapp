﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ZumenConsoleApp
{
    public partial class MstRegist : Form
    {

        #region クラス変数    

        /// <summary>
        /// コード
        /// </summary>
        public int Cd;

        /// <summary>
        /// モード 1:登録(加工) 2:更新(加工) 3:登録(工程) 4:更新(工程)
        /// </summary>
        public int Mode;

        #endregion

        #region コンストラクタ

        public MstRegist()
        {
            InitializeComponent();
        }

        #endregion

        #region イベント

        /// <summary>
        /// 画面ロード処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadViewMstRegist(object sender, EventArgs e)
        {
            try
            {
                // ボタン正業
                if (Mode == 1 || Mode == 3)
                {
                    this.btn_更新.Visible = false;
                }
                else
                {
                    this.btn_登録.Visible = false;
                }

                SetForm();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 登録ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtn_登録(object sender, System.EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.txt_名称.Text))
                {
                    MessageBox.Show("入力してください。");
                    this.txt_名称.Focus();
                    return;
                }

                // 確認を行う
                var result = MessageBox.Show("登録を行います。よろしいですか？", "確認",
                         MessageBoxButtons.YesNo,
                         MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    int cd = Int32.Parse(this.txt_コード.Text);
                    string name = this.txt_名称.Text;

                    if (this.Mode == 1)
                    {
                        Insert加工指示書加工先マスタ(cd, name);
                    }
                    else if (this.Mode == 3)
                    {
                        Insert加工指示書工程マスタ(cd, name);
                    }
                    // 更新処理
                    this.Close();
                }
                else
                {
                    return;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 更新ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtn_更新(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.txt_名称.Text))
                {
                    MessageBox.Show("入力してください。");
                    this.txt_名称.Focus();
                    return;
                }

                // 確認を行う
                var result = MessageBox.Show("更新を行います。よろしいですか？", "確認",
                         MessageBoxButtons.YesNo,
                         MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    int cd = Int32.Parse(this.txt_コード.Text);
                    string name = this.txt_名称.Text;

                    if (this.Mode == 2)
                    {
                        Update加工指示書加工先マスタ(cd, name);
                    }
                    else if (this.Mode == 4)
                    {
                        Update加工指示書工程マスタ(cd, name);
                    }
                    // 更新処理
                    this.Close();
                }
                else
                {
                    return;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region private関数

        /// <summary>
        /// 画面情報を設定します。
        /// </summary>
        private void SetForm()
        {
            try
            {
                DataTable dt = new DataTable();
                if (this.Mode == 2)
                {
                    dt = Get加工先マスタ();
                }
                else if (this.Mode == 4)
                {
                    dt = Get工程マスタ();
                }
                else if (this.Mode == 1)
                {
                    dt = Get加工先マスタコード();
                }
                else if (this.Mode == 3)
                {
                    dt = Get工程マスタコード();
                }

                this.txt_コード.Text = dt.Rows[0]["コード"].ToString();
                this.txt_名称.Text = dt.Rows[0]["名称"].ToString();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get加工先マスタ
        /// </summary>
        /// <returns></returns>
        private DataTable Get加工先マスタ()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {

                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   加工先コード AS コード ");
                    query.Append("  ,加工先名 AS 名称 ");
                    query.Append("  FROM 加工指示書加工先マスタ ");
                    query.Append(" WHERE ");
                    query.Append("   加工先コード = :加工先コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("加工先コード", NpgsqlDbType.Integer));
                    cmd.Parameters["加工先コード"].Value = Cd;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get加工先マスタ
        /// </summary>
        /// <returns></returns>
        private DataTable Get工程マスタ()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   工程コード AS コード ");
                    query.Append("  ,工程名 AS 名称 ");
                    query.Append("  FROM 加工指示書工程マスタ ");
                    query.Append(" WHERE ");
                    query.Append("   工程コード = :工程コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("工程コード", NpgsqlDbType.Integer));
                    cmd.Parameters["工程コード"].Value = Cd;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get加工先マスタ
        /// </summary>
        /// <returns></returns>
        private DataTable Get加工先マスタコード()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   MAX(加工先コード) + 1 AS コード ");
                    query.Append("  ,'' AS 名称 ");
                    query.Append("  FROM 加工指示書加工先マスタ ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get加工先マスタ
        /// </summary>
        /// <returns></returns>
        private DataTable Get工程マスタコード()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {

                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   MAX(工程コード) + 1 AS コード ");
                    query.Append("  ,'' AS 名称 ");
                    query.Append("  FROM 加工指示書工程マスタ ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書加工先マスタ更新
        /// </summary>
        /// <param name="cd"></param>
        /// <param name="name"></param>
        private void Update加工指示書加工先マスタ(int cd, string name)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    try
                    {
                        StringBuilder query = new StringBuilder();

                        query.AppendLine("UPDATE 加工指示書加工先マスタ ");
                        query.AppendLine("SET  ");
                        query.AppendLine("    加工先名 = :加工先名");
                        query.AppendLine("   ,更新者 = :更新者");
                        query.AppendLine("   ,更新時刻 = :更新時刻");
                        query.AppendLine("WHERE  ");
                        query.AppendLine("    加工先コード = :加工先コード");

                        NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                        cmd.Parameters.Add(new NpgsqlParameter("加工先コード", NpgsqlDbType.Integer));
                        cmd.Parameters["加工先コード"].Value = cd;

                        cmd.Parameters.Add(new NpgsqlParameter("加工先名", NpgsqlDbType.Varchar));
                        cmd.Parameters["加工先名"].Value = name;

                        cmd.Parameters.Add(new NpgsqlParameter("更新者", NpgsqlDbType.Varchar));
                        cmd.Parameters["更新者"].Value = LoginInfo.氏名;

                        cmd.Parameters.Add(new NpgsqlParameter("更新時刻", NpgsqlDbType.Timestamp));
                        cmd.Parameters["更新時刻"].Value = DateTime.Now;

                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        tran.Rollback();
                        throw e;
                    }
                    tran.Commit();
                }
            }
        }

        /// <summary>
        /// 加工指示書加工先マスタ更新
        /// </summary>
        /// <param name="cd"></param>
        /// <param name="name"></param>
        private void Update加工指示書工程マスタ(int cd, string name)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    try
                    {
                        StringBuilder query = new StringBuilder();

                        query.AppendLine("UPDATE 加工指示書工程マスタ ");
                        query.AppendLine("SET  ");
                        query.AppendLine("    工程名 = :工程名");
                        query.AppendLine("   ,更新者 = :更新者");
                        query.AppendLine("   ,更新時刻 = :更新時刻");
                        query.AppendLine("WHERE  ");
                        query.AppendLine("    工程コード = :工程コード");

                        NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                        cmd.Parameters.Add(new NpgsqlParameter("工程コード", NpgsqlDbType.Integer));
                        cmd.Parameters["工程コード"].Value = cd;

                        cmd.Parameters.Add(new NpgsqlParameter("工程名", NpgsqlDbType.Varchar));
                        cmd.Parameters["工程名"].Value = name;

                        cmd.Parameters.Add(new NpgsqlParameter("更新者", NpgsqlDbType.Varchar));
                        cmd.Parameters["更新者"].Value = LoginInfo.氏名;

                        cmd.Parameters.Add(new NpgsqlParameter("更新時刻", NpgsqlDbType.Timestamp));
                        cmd.Parameters["更新時刻"].Value = DateTime.Now;

                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        tran.Rollback();
                        throw e;
                    }
                    tran.Commit();
                }
            }
        }

        /// <summary>
        /// 加工指示書工程マスタ登録
        /// </summary>
        /// <param name="cd"></param>
        /// <param name="name"></param>
        private void Insert加工指示書加工先マスタ(int cd, string name)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    try
                    {
                        StringBuilder query = new StringBuilder();

                        query.AppendLine("INSERT ");
                        query.AppendLine("    INTO 加工指示書加工先マスタ (  ");
                        query.AppendLine("    加工先コード ");
                        query.AppendLine("   ,加工先名 ");
                        query.AppendLine("   ,更新者 ");
                        query.AppendLine("   ,更新時刻) ");
                        query.AppendLine("VALUES (  ");
                        query.AppendLine("    :加工先コード ");
                        query.AppendLine("   ,:加工先名 ");
                        query.AppendLine("   ,:更新者 ");
                        query.AppendLine("   ,:更新時刻) ");

                        NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);
                        cmd.Parameters.Add(new NpgsqlParameter("加工先コード", NpgsqlDbType.Integer));
                        cmd.Parameters["加工先コード"].Value = cd;

                        cmd.Parameters.Add(new NpgsqlParameter("加工先名", NpgsqlDbType.Varchar));
                        cmd.Parameters["加工先名"].Value = name;

                        cmd.Parameters.Add(new NpgsqlParameter("更新者", NpgsqlDbType.Varchar));
                        cmd.Parameters["更新者"].Value = LoginInfo.氏名;

                        cmd.Parameters.Add(new NpgsqlParameter("更新時刻", NpgsqlDbType.Timestamp));
                        cmd.Parameters["更新時刻"].Value = DateTime.Now;

                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        tran.Rollback();
                        throw e;
                    }
                    tran.Commit();
                }
            }
        }

        /// <summary>
        /// 加工指示書工程マスタ更新
        /// </summary>
        /// <param name="cd"></param>
        /// <param name="name"></param>
        private void Insert加工指示書工程マスタ(int cd, string name)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    try
                    {
                        StringBuilder query = new StringBuilder();

                        query.AppendLine("INSERT ");
                        query.AppendLine("    INTO 加工指示書工程マスタ (  ");
                        query.AppendLine("    工程コード ");
                        query.AppendLine("   ,工程名 ");
                        query.AppendLine("   ,更新者 ");
                        query.AppendLine("   ,更新時刻) ");
                        query.AppendLine("VALUES (  ");
                        query.AppendLine("    :工程コード ");
                        query.AppendLine("   ,:工程名 ");
                        query.AppendLine("   ,:更新者 ");
                        query.AppendLine("   ,:更新時刻) ");

                        NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);
                        cmd.Parameters.Add(new NpgsqlParameter("工程コード", NpgsqlDbType.Integer));
                        cmd.Parameters["工程コード"].Value = cd;

                        cmd.Parameters.Add(new NpgsqlParameter("工程名", NpgsqlDbType.Varchar));
                        cmd.Parameters["工程名"].Value = name;

                        cmd.Parameters.Add(new NpgsqlParameter("更新者", NpgsqlDbType.Varchar));
                        cmd.Parameters["更新者"].Value = LoginInfo.氏名;

                        cmd.Parameters.Add(new NpgsqlParameter("更新時刻", NpgsqlDbType.Timestamp));
                        cmd.Parameters["更新時刻"].Value = DateTime.Now;

                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        tran.Rollback();
                        throw e;
                    }
                    tran.Commit();
                }
            }
        }

        #endregion

    }
}
