﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Npgsql;
using System.Diagnostics;

namespace ZumenConsoleApp.View
{
    public partial class SearchEntry : BaseView
    {
        public static JissekiKakou JisKak { get; set; }
        public SearchEntry()
        {
            InitializeComponent();
        }

        private void SearchEntry_Load(object sender, EventArgs e)
        {
            if (JisKak != null)
            {
                // 保持したデータを検索条件に表示
                txtOrderNo.Text = JisKak.OrderNo;
                txtBlockNo.Text = JisKak.BlockNo;
                txtZumenBan.Text = JisKak.ZumenBan;
                txtKouteihyouNo.Text = JisKak.KouteihyouNo;
                txtChumonsyoNo.Text = JisKak.ChumonsyoNo;
                txtKisyu.Text = JisKak.Kisyu;
            }
        }

        // 検索ボタン押下時処理
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var conn = Properties.Settings.Default.connectionString;
                var dataCnt = 0;
                dataCnt = GetZumenInfoCount(conn, txtOrderNo.Text, txtBlockNo.Text, txtZumenBan.Text, txtKouteihyouNo.Text, txtChumonsyoNo.Text, txtKisyu.Text);
                if (dataCnt == 0)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面データがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                else if (dataCnt > 500)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面データが多すぎます。条件を指定してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                var jisKakList = new List<JissekiKakou>();
                jisKakList = GetZumenInfo(conn, txtOrderNo.Text, txtBlockNo.Text, txtZumenBan.Text, txtKouteihyouNo.Text, txtChumonsyoNo.Text, txtKisyu.Text);

                if (jisKakList == null || jisKakList.Count == 0)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面ファイルがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                // 要素の削除
                for (int i = MainDisp.JkList.Count - 1; i >= 0; i--)
                {
                    if (MainDisp.JkList[i].SearchMethod == 2)
                    {
                        MainDisp.JkList.Remove(MainDisp.JkList[i]);
                    }
                }

                // リストに追加
                foreach (var a in jisKakList)
                {
                    MainDisp.JkList.Add(a);
                }

                MainDisp.isZumenSearch = true;

                JisKak = new JissekiKakou();
                JisKak.OrderNo = txtOrderNo.Text;
                JisKak.ZumenBan = txtZumenBan.Text;
                JisKak.BlockNo = txtBlockNo.Text;
                JisKak.KouteihyouNo = txtKouteihyouNo.Text;
                JisKak.ChumonsyoNo = txtChumonsyoNo.Text;
                JisKak.Kisyu = txtKisyu.Text;

                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // オーダーNoクリア
        private void btnOrderNoClr_Click(object sender, EventArgs e)
        {
            txtOrderNo.Clear();
            txtOrderNo.Focus();
        }

        // ブロックNoクリア
        private void btnBlockNoClr_Click(object sender, EventArgs e)
        {
            txtBlockNo.Clear();
            txtBlockNo.Focus();
        }

        // 工程表番号クリア
        private void btnKouteihyoClr_Click(object sender, EventArgs e)
        {
            txtKouteihyouNo.Clear();
            txtKouteihyouNo.Focus();
        }

        // 図面番号クリア
        private void btnZumenBanClr_Click(object sender, EventArgs e)
        {
            txtZumenBan.Clear();
            txtZumenBan.Focus();
        }

        // 注文書番号クリア
        private void btnChumonsyoNoClr_Click(object sender, EventArgs e)
        {
            txtChumonsyoNo.Clear();
            txtChumonsyoNo.Focus();
        }

        // 機種名クリア
        private void btnKisyuClr_Click(object sender, EventArgs e)
        {
            txtKisyu.Clear();
            txtKisyu.Focus();
        }

        /// <summary>
        /// 図面情報を検索する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="blockNo"></param>
        /// <param name="zumenBan"></param>
        /// <param name="kouteihyouNo"></param>
        /// <param name="chumonsyoNo"></param>
        /// <param name="kisyu"></param>
        /// <returns></returns>
        private List<JissekiKakou> GetZumenInfo(string connectionString
                                            , string orderNo
                                            , string blockNo
                                            , string zumenBan
                                            , string kouteihyouNo
                                            , string chumonsyoNo
            , string kisyu)
        {
            var JisKakDataList = new List<JissekiKakou>();
            var JisKakData = new JissekiKakou();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  jkkd.\"オーダーNO\"" +
                                "  ,jkkd.グループ" +
                                "  ,jkkd.連番" +
                                "  ,jkkd.工程表番号" +
                                "  ,jkkd.\"引合NO\"" +
                                "  ,jkkd.図面番号" +
                                "  ,jkkd.品名" +
                                "  ,jkkd.数量 as 加工数量" +
                                "  ,jkkd.納期" +
                                "  ,jkkd.客先指定納期" +
                                "  ,case when jkkd.図面パス is not null then jkkd.図面パス " +
                                "   when zd.パス is not null then zd.パス " +
                                "   when zd2.パス is not null then zd2.パス end as 図面パス" +
                                "  ,jkkd.担当者コード as 担当者コード" +
                                "  ,sam.作業者名 as 担当者" +
                                "  ,sam2.作業者名 as 加工担当者" +
                                "  ,jkkd.注文書番号" +
                                "  ,jkkd.注文書行番号" +
                                "  ,ssm.仕入先略名 as 加工先" +
                                "  ,sjm.作業状況名" +
                                "  ,ssm2.仕入先略名 as 材料入荷先" +
                                "  ,jzd.材質" +
                                "  ,jzd.サイズ" +
                                "  ,jzd.数量 as 材料数量" +
                                "  ,jzd.納期 as 入荷日" +
                                "  ,okj.先行手配" +
                                "  ,okku.組立有無" +
                                " from 実績加工データ jkkd" +
                                " left join 仕入先マスタ ssm" +
                                "   on jkkd.加工先コード = ssm.仕入先コード" +
                                " left join 検査実績データ kjd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 作業状況マスタ sjm" +
                                "   on sjm.作業状況コード = kjd.作業状況" +
                                " left join 実績材料データ jzd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 仕入先マスタ ssm2" +
                                "   on jzd.仕入先コード = ssm2.仕入先コード" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(jkkd.図面番号,'/','／')" +
                                " left join 図面データ zd2" +
                                "   on zd2.ファイル like replace('%jkkd.図面番号%','/','／')" +
                                " left join 作業者マスタ sam" +
                                "   on jkkd.担当者コード = sam.作業者コード" +
                                " left join オーダー管理状態テーブル okj" +
                                "   on jkkd.\"オーダーNO\" = okj.\"オーダーNO\" and jkkd.グループ = okj.グループ" +
                                " left join オーダー管理組立有無テーブル okku" +
                                "   on jkkd.\"オーダーNO\" = okku.\"オーダーNO\" and jkkd.グループ = okku.グループ" +
                                " left join オーダー管理部材テーブル okb" +
                                "   on jkkd.\"オーダーNO\" = okb.\"オーダーNO\" and jkkd.部材番号 = okb.部材番号" +
                                " left join 図面作成データ zsd" +
                                "   on jkkd.図面番号 = zsd.図面番号" +
                                " left join 作業者マスタ sam2" +
                                "   on zsd.作業者 = sam2.作業者コード" +
                " where jkkd.\"オーダーNO\" like UPPER('%" + orderNo + "%')";
                if (blockNo != "")
                {
                    sqlCmdTxt += " and jkkd.\"引合NO\" LIKE '%" + blockNo + "%'";
                }
                if (zumenBan != "")
                {
                    sqlCmdTxt += " and UPPER(jkkd.図面番号) like UPPER('%" + zumenBan + "%')";
                }
                if (kouteihyouNo != "")
                {
                    sqlCmdTxt += " and jkkd.工程表番号 like UPPER('%" + kouteihyouNo + "%')";
                }
                if (chumonsyoNo != "")
                {
                    sqlCmdTxt += " and jkkd.注文書番号 like UPPER('%" + chumonsyoNo + "%')";
                }
                if (kisyu != "")
                {
                    sqlCmdTxt += " and okb.機種名 like UPPER('%" + kisyu + "%')";
                }
                sqlCmdTxt += " order by jkkd.\"引合NO\", jkkd.図面番号" + " limit 500";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        JisKakData = new JissekiKakou();
                        JisKakData.SearchMethod = 2;
                        JisKakData.OrderNo = CommonModule.ConvertToString(result["オーダーNO"]);
                        JisKakData.Group = CommonModule.ConvertToString(result["グループ"]);
                        JisKakData.Renno = CommonModule.ConvertToString(result["連番"]);
                        JisKakData.KouteihyouNo = CommonModule.ConvertToString(result["工程表番号"]);
                        JisKakData.BlockNo = CommonModule.ConvertToString(result["引合NO"]);
                        JisKakData.ZumenBan = CommonModule.ConvertToString(result["図面番号"]);
                        JisKakData.ProductName = CommonModule.ConvertToString(result["品名"]);
                        JisKakData.KakoQty = CommonModule.ConvertToString(result["加工数量"]);
                        JisKakData.ZumenPath = CommonModule.ConvertToString(result["図面パス"]);
                        if (JisKakData.ZumenPath != "")
                        {
                            JisKakData.FileName = System.IO.Path.GetFileNameWithoutExtension(JisKakData.ZumenPath);
                        }
                        JisKakData.ChumonsyoNo = CommonModule.ConvertToString(result["注文書番号"]);
                        JisKakData.TantouCd = CommonModule.ConvertToString(result["担当者コード"]);
                        JisKakData.Tantou = CommonModule.ConvertToString(result["担当者"]);
                        JisKakData.KakouTantou = CommonModule.ConvertToString(result["加工担当者"]);
                        JisKakData.Kakosaki = CommonModule.ConvertToString(result["加工先"]);
                        JisKakData.Nouki = CommonModule.ConvertToDateString(result["納期"]);
                        JisKakData.KyakuNouki = CommonModule.ConvertToDateString(result["客先指定納期"]);
                        JisKakData.Sinchoku = CommonModule.ConvertToString(result["作業状況名"]);
                        JisKakData.ZaiNyuka = CommonModule.ConvertToString(result["材料入荷先"]);
                        JisKakData.Zairyo = CommonModule.ConvertToString(result["材質"]);
                        JisKakData.ZaiSize = CommonModule.ConvertToString(result["サイズ"]);
                        JisKakData.ZaiQty = CommonModule.ConvertToString(result["材料数量"]);
                        JisKakData.NyukaDate = CommonModule.ConvertToDateString(result["入荷日"]);
                        JisKakData.isAdvance = CommonModule.ConvertToInt(result["先行手配"]);
                        JisKakData.isKumitate = CommonModule.ConvertToInt(result["組立有無"]);
                        JisKakDataList.Add(JisKakData);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    JisKakDataList = new List<JissekiKakou>();
                    return JisKakDataList;
                }
            }
            return JisKakDataList;
        }

        /// <summary>
        /// 図面情報のデータ数を検索する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="blockNo"></param>
        /// <param name="zumenBan"></param>
        /// <param name="kouteihyouNo"></param>
        /// <param name="chumonsyoNo"></param>
        /// <param name="kisyu"></param>
        /// <returns></returns>
        private int GetZumenInfoCount(string connectionString
                                        , string orderNo
                                        , string blockNo
                                        , string zumenBan
                                        , string kouteihyouNo
                                        , string chumonsyoNo
                                        , string kisyu)
        {
            var dataCnt = 0;
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  count(jkkd.*) as データ数" +
                                " from 実績加工データ jkkd" +
                                " left join 仕入先マスタ ssm" +
                                "   on jkkd.加工先コード = ssm.仕入先コード" +
                                " left join 検査実績データ kjd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 作業状況マスタ sjm" +
                                "   on sjm.作業状況コード = kjd.作業状況" +
                                " left join 実績材料データ jzd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 仕入先マスタ ssm2" +
                                "   on jzd.仕入先コード = ssm2.仕入先コード" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(jkkd.図面番号,'/','／')" +
                                " left join 図面データ zd2" +
                                "   on zd2.ファイル like replace('%jkkd.図面番号%','/','／')" +
                                " left join 作業者マスタ sam" +
                                "   on jkkd.担当者コード = sam.作業者コード" +
                                " left join オーダー管理状態テーブル okj" +
                                "   on jkkd.\"オーダーNO\" = okj.\"オーダーNO\" and jkkd.グループ = okj.グループ" +
                                " left join オーダー管理組立有無テーブル okku" +
                                "   on jkkd.\"オーダーNO\" = okku.\"オーダーNO\" and jkkd.グループ = okku.グループ" +
                                " left join オーダー管理部材テーブル okb" +
                                "   on jkkd.\"オーダーNO\" = okb.\"オーダーNO\" and jkkd.部材番号 = okb.部材番号" +
                                " left join 図面作成データ zsd" +
                                "   on jkkd.図面番号 = zsd.図面番号" +
                                " left join 作業者マスタ sam2" +
                                "   on zsd.作業者 = sam2.作業者コード" +
                                " where jkkd.\"オーダーNO\" like UPPER('%" + orderNo + "%')";
                if (blockNo != "")
                {
                    sqlCmdTxt += " and jkkd.\"引合NO\" LIKE '%" + blockNo + "%'";
                }
                if (zumenBan != "")
                {
                    sqlCmdTxt += " and UPPER(jkkd.図面番号) like UPPER('%" + zumenBan + "%')";
                }
                if (kouteihyouNo != "")
                {
                    sqlCmdTxt += " and jkkd.工程表番号 like UPPER('%" + kouteihyouNo + "%')";
                }
                if (chumonsyoNo != "")
                {
                    sqlCmdTxt += " and jkkd.注文書番号 like UPPER('%" + chumonsyoNo + "%')";
                }
                if (kisyu != "")
                {
                    sqlCmdTxt += " and okb.機種名 like UPPER('%" + kisyu + "%')";
                }

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);

                try
                {
                    var result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        dataCnt = CommonModule.ConvertToInt(result["データ数"]);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return 0;
                }
            }
            return dataCnt;
        }

        // オーダーNOにフォーカス時にEnterを押下すると検索
        private void txtOrderNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        // ブロックNoにフォーカス時にEnterを押下すると検索
        private void txtBlockNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        // 工程表番号にフォーカス時にEnterを押下すると検索
        private void txtKouteihyouNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        // 図面番号にフォーカス時にEnterを押下すると検索
        private void txtZumenBan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        // 注文書番号にフォーカス時にEnterを押下すると検索
        private void txtChumonsyoNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        // 機種名にフォーカス時にEnterを押下すると検索
        private void txtKisyu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }
    }
}
