﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZumenConsoleApp
{
    public class LoginUserInfo
    {
        // 作業者コード
        public string WorkerCd { get; set; }

        // 作業者名
        public string WorkerName { get; set; }

        public LoginUserInfo()
        {
            WorkerCd = string.Empty;
            WorkerName = string.Empty;
        }
    }
}
