﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class SendTransmissionMail : BaseView
    {
        private int selIndex = -1;
        private const string RECIPIENTINI_FILE = "\\\\PORTAL-SERVER\\app\\ZumenConsoleAppPublish\\recipient.ini";

        public SendTransmissionMail()
        {
            InitializeComponent();
        }

        // ロード時処理
        private void SendTransmissionMail_Load(object sender, EventArgs e)
        {
            try
            {
                List<ZumenWorker> zumenWorkerList = new List<ZumenWorker>();
                zumenWorkerList = CommonModule.GetZumenWorkers(Properties.Settings.Default.connectionString);
                if (zumenWorkerList.Count == 0)
                {
                    MessageBox.Show("作業者マスタよりデータを正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                // 送信者一覧作成
                foreach (var zw in zumenWorkerList)
                {
                    cmbFrom.Items.Add(zw);
                }
                // INIファイルより宛先の名前とメールアドレスの一覧を取得する
                string recipientNameStr = ClassINI.GetIniValue(RECIPIENTINI_FILE, "NAME", "RecipientNameStr");
                string recipientAddressStr = ClassINI.GetIniValue(RECIPIENTINI_FILE, "ADDRESS", "RecipientAddressStr");
                string recipientTantouCdStr = ClassINI.GetIniValue(RECIPIENTINI_FILE, "TANTOUCD", "RecipientTantouCdStr");
                // 分割する。
                char[] separator = new char[] { ',' };
                string[] recipientNames = recipientNameStr.Split(separator);
                string[] recipientAddresses = recipientAddressStr.Split(separator);
                string[] recipientTantouCd = recipientTantouCdStr.Split(separator);
                // 宛先一覧作成
                for (int i = 0; i < recipientNames.Length; i++)
                {
                    // 宛先リスト
                    cmbTo.Items.Add(
                                    new Recipient
                                    {
                                        Name = recipientNames[i],
                                        MailAddress = recipientAddresses[i],
                                        TantouCd = recipientTantouCd[i]
                                    }
                                );
                    if (recipientTantouCd[i] == MainDisp.selTantouCd)
                    {
                        selIndex = i;
                    }
                }
                cmbTo.SelectedIndex = selIndex;
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        // メール送信ボタン押下時処理
        private void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                ZumenWorker sd = (ZumenWorker)cmbFrom.SelectedItem;
                Recipient rc = (Recipient)cmbTo.SelectedItem;
                if (sd == null)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("送信者を選択してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                else if (rc == null)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("宛先を選択してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                //MailMessageの作成
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                //送信メールアドレス（メールアドレスはkakou-tab001@nakamurakikai.co.jpに統一する）
                msg.From = new System.Net.Mail.MailAddress("kakou-tab001@nakamurakikai.co.jp");
                //宛先
                msg.To.Add(new System.Net.Mail.MailAddress(rc.MailAddress));

                DialogResult dr = MessageBox.Show(rc.Name + "様にメール送信してもよろしいですか？", "送信確認", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                if (dr == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }

                //件名
                msg.Subject = txtSubject.Text;
                //本文
                msg.Body = "送信者：" + sd.HyoujiMei + "\n" + txtMailBody.Text;

                msg.Priority = System.Net.Mail.MailPriority.Normal;
                //メールの配達が遅れたとき、失敗したとき、正常に配達されたときに通知する
                msg.DeliveryNotificationOptions =
                    System.Net.Mail.DeliveryNotificationOptions.Delay |
                    System.Net.Mail.DeliveryNotificationOptions.OnFailure |
                    System.Net.Mail.DeliveryNotificationOptions.OnSuccess;

                // メールのアカウント、パスワードを設定
                System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                sc.Credentials = new System.Net.NetworkCredential("kakou-tab001@nakamurakikai.co.jp", "0KZ0j3oLOE");
                //SMTPサーバーなどを設定する
                sc.Host = "smtp.nakamurakikai.co.jp";
                sc.Port = 587;
                sc.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //メッセージを送信する
                sc.Send(msg);

                //後始末
                msg.Dispose();
                //後始末（.NET Framework 4.0以降）
                sc.Dispose();

                //メッセージボックスを表示する
                MessageBox.Show("メールを送信しました。",
                    "メール送信完了",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.None);

                Close();

            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Console.WriteLine("{0} Exception caught: ", e);
            }
        }

        // キャンセルボタン押下時処理
        private void btnCansel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
