﻿
using System;

namespace ZumenConsoleApp
{
    public class RirekiData
    {
        public string OrderNo { get; set; }
        public string ZumenBan { get; set; }
        public string RenBan { get; set; }
        public string ZumenPath { get; set; }
        public string FileName { get; set; }
        public string UpdateDater { get; set; }
        public string UpdateDateTime { get; set; }
        public string ProductName { get; set; }
    }
}
