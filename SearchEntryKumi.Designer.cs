﻿namespace ZumenConsoleApp.View
{
    partial class SearchEntryKumi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.btnOrderNoClr = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBlockNo = new System.Windows.Forms.TextBox();
            this.btnBlockNoClr = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKisyu = new System.Windows.Forms.TextBox();
            this.btnKisyuClr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // searchBtn
            // 
            this.searchBtn.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.searchBtn.Location = new System.Drawing.Point(235, 190);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(145, 62);
            this.searchBtn.TabIndex = 9;
            this.searchBtn.Text = "検索";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(10, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "オーダーNo";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo.Location = new System.Drawing.Point(205, 40);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(120, 31);
            this.txtOrderNo.TabIndex = 1;
            this.txtOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOrderNo_KeyDown);
            // 
            // btnOrderNoClr
            // 
            this.btnOrderNoClr.Location = new System.Drawing.Point(330, 40);
            this.btnOrderNoClr.Name = "btnOrderNoClr";
            this.btnOrderNoClr.Size = new System.Drawing.Size(50, 31);
            this.btnOrderNoClr.TabIndex = 2;
            this.btnOrderNoClr.Text = "クリア";
            this.btnOrderNoClr.UseVisualStyleBackColor = true;
            this.btnOrderNoClr.Click += new System.EventHandler(this.btnOrderNoClr_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(10, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "ブロックNo(下4けた)";
            // 
            // txtBlockNo
            // 
            this.txtBlockNo.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBlockNo.Location = new System.Drawing.Point(205, 90);
            this.txtBlockNo.Name = "txtBlockNo";
            this.txtBlockNo.Size = new System.Drawing.Size(120, 31);
            this.txtBlockNo.TabIndex = 4;
            this.txtBlockNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBlockNo_KeyDown);
            // 
            // btnBlockNoClr
            // 
            this.btnBlockNoClr.Location = new System.Drawing.Point(330, 90);
            this.btnBlockNoClr.Name = "btnBlockNoClr";
            this.btnBlockNoClr.Size = new System.Drawing.Size(50, 31);
            this.btnBlockNoClr.TabIndex = 5;
            this.btnBlockNoClr.Text = "クリア";
            this.btnBlockNoClr.UseVisualStyleBackColor = true;
            this.btnBlockNoClr.Click += new System.EventHandler(this.btnBlockNoClr_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(10, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 24);
            this.label4.TabIndex = 6;
            this.label4.Text = "機種名";
            // 
            // txtKisyu
            // 
            this.txtKisyu.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKisyu.Location = new System.Drawing.Point(205, 140);
            this.txtKisyu.Name = "txtKisyu";
            this.txtKisyu.Size = new System.Drawing.Size(120, 31);
            this.txtKisyu.TabIndex = 7;
            this.txtKisyu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKisyu_KeyDown);
            // 
            // btnKisyuClr
            // 
            this.btnKisyuClr.Location = new System.Drawing.Point(330, 140);
            this.btnKisyuClr.Name = "btnKisyuClr";
            this.btnKisyuClr.Size = new System.Drawing.Size(50, 31);
            this.btnKisyuClr.TabIndex = 8;
            this.btnKisyuClr.Text = "クリア";
            this.btnKisyuClr.UseVisualStyleBackColor = true;
            this.btnKisyuClr.Click += new System.EventHandler(this.btnKisyuClr_Click);
            // 
            // SearchEntryKumi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 268);
            this.Controls.Add(this.btnKisyuClr);
            this.Controls.Add(this.txtKisyu);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnBlockNoClr);
            this.Controls.Add(this.txtBlockNo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnOrderNoClr);
            this.Controls.Add(this.txtOrderNo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchBtn);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchEntryKumi";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "組立図検索";
            this.Load += new System.EventHandler(this.SearchEntry_Load);
            this.Controls.SetChildIndex(this.searchBtn, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.txtOrderNo, 0);
            this.Controls.SetChildIndex(this.btnOrderNoClr, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtBlockNo, 0);
            this.Controls.SetChildIndex(this.btnBlockNoClr, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.txtKisyu, 0);
            this.Controls.SetChildIndex(this.btnKisyuClr, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Button btnOrderNoClr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBlockNo;
        private System.Windows.Forms.Button btnBlockNoClr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtKisyu;
        private System.Windows.Forms.Button btnKisyuClr;
    }
}