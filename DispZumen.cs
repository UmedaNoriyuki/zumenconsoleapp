﻿using Npgsql;
using NpgsqlTypes;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using ZumenConsoleApp.View;

namespace ZumenConsoleApp
{
    public partial class DispZumen : Form
    {
        #region クラス変数    

        /// <summary>
        /// 工程表番号
        /// </summary>
        public string KouteiNo = string.Empty;

        #endregion

        #region コンストラクタ    

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DispZumen()
        {
            InitializeComponent();
        }

        #endregion

        #region イベント
        
        /// <summary>
        /// 画面ロード処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadView_F000_DispZunem(object sender, System.EventArgs e)
        {
            try
            {
                // プロパティ設定
                SetPropertyDeskCtr();

                string filePath = string.Empty;

                if (Get図面パス(KouteiNo).Rows.Count > 0)
                {
                    filePath = Get図面パス(KouteiNo).Rows[0]["図面パス"].ToString();
                }

                // 図面表示
                ShowZumen(filePath);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region private関数

        /// <summary>
        /// DeskCtrのプロパティを設定します
        /// </summary>
        private void SetPropertyDeskCtr()
        {
            axDeskCtrl.fullscreen = "yes";
            axDeskCtrl.SetShowAnnotation(true);
            axDeskCtrl.SetSheafViewMode(true);
            axDeskCtrl.SetCharDrawAdvance(true);
            axDeskCtrl.SetZoomWithPage();
            axDeskCtrl.ShowStandardToolBar(false);
            axDeskCtrl.ShowSheafBar(false);
            axDeskCtrl.ShowStatusBar(false);
        }

        /// <summary>
        /// 図面表示
        /// </summary>
        /// <param name="filePath"></param>
        private void ShowZumen(string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    filePath = filePath.Substring(filePath.IndexOf("図面データ\\"));
                    // 図面表示
                    axDeskCtrl.LoadFile(MainDisp.FILE_URL + filePath.Replace("#", "%23"));
                }
                else
                {
                    // ファイルが存在しなければNOIMAGE画像表示
                    axDeskCtrl.LoadFile(MainDisp.FILE_URL + "図面データ/NoImarge.xdw");
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 図面パス取得
        /// </summary>
        /// <returns></returns>
        public DataTable Get図面パス(string 工程表番号)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   CASE ");
                    query.Append("    WHEN A.図面パス IS NOT NULL  ");
                    query.Append("      THEN A.図面パス  ");
                    query.Append("    WHEN B.パス IS NOT NULL  ");
                    query.Append("      THEN B.パス  ");
                    query.Append("    WHEN C.パス IS NOT NULL  ");
                    query.Append("      THEN C.パス  ");
                    query.Append("   END AS 図面パス  ");
                    query.Append("  FROM 実績加工データ A ");
                    query.Append("  LEFT JOIN 図面データ B ");
                    query.Append("    ON B.ファイル = REPLACE(A.図面番号,'/','／') ");
                    query.Append("  LEFT JOIN 図面データ C ");
                    query.Append("    ON C.ファイル LIKE REPLACE('%A.図面番号%','/','／') ");
                    query.Append("  WHERE ");
                    query.Append("      A.工程表番号 = :工程表番号 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("工程表番号", NpgsqlDbType.Char));
                    cmd.Parameters["工程表番号"].Value = 工程表番号;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
