﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZumenConsoleApp.View;

namespace ZumenConsoleApp
{
    public partial class KakouIndication : Form
    {

        #region クラス変数  

        /// <summary>
        /// 工程表番号
        /// </summary>
        public string KouteiNo = string.Empty;

        public bool Flg = false;

        /// <summary>
        /// 画面ロードフラグ
        /// </summary>
        public bool LoadFlg = true;

        public bool ScanFlg = false;

        public DateTime Date = DateTime.Now;

        DataTable KouteiDt;

        /// <summary>
        /// ヘリサート工程有り無し
        /// </summary>
        private bool HelisertFlg = false;

        /// <summary>
        /// ピン入れ工程有り無し
        /// </summary>
        private bool PinFlg = false;

        /// <summary>
        /// 表面処理前検査工程有り無し
        /// </summary>
        private bool HyoshoFlg = false;

        #endregion

        #region コンストラクタ 

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KakouIndication()
        {
            InitializeComponent();
        }

        #endregion

        #region イベント

        /// <summary>
        /// ロード処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadKakouIndication(object sender, EventArgs e)
        {
            try
            {
                // 加工指示書コピーコンボボックス
                GetCmbCopy();
                this.txtWorker.Text = LoginInfo.氏名;

                // 工程リスト取得
                this.KouteiDt = GetKouteiMst();

                if (!string.IsNullOrWhiteSpace(KouteiNo))
                {
                    this.txtKouteiNo.Text = KouteiNo;
                    ClickBtnSearch(sender, e);
                }

                this.LoadFlg = false;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// dgv_工程表CellContentClickイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellContentClickdgv_工程表(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //列ヘッダーをクリックした場合などにキャンセルする。	
                if (e.RowIndex < 0) { return; }

                //クリックした列が対象列かチェックする。			
                DataGridView dgv = (DataGridView)sender;
                // 作業印ボタン押下時に終了時刻を設定
                if (dgv.Columns[e.ColumnIndex].Name == "作業者印")
                {
                    SetWorkerSign(e);
                }
                else if (dgv.Columns[e.ColumnIndex].Name == "チェック")
                {
                    if (string.IsNullOrWhiteSpace(this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value.ToString()))
                    {
                        this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value = "✓";
                    }
                    else
                    {
                        this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value = string.Empty;
                    }
                }
                else if (dgv.Columns[e.ColumnIndex].Name == "完了予定日")
                {
                    Calendar form = new Calendar();
                    form.StartPosition = FormStartPosition.CenterParent;
                    form.ShowDialog();
                    this.Date = form.Date;

                    this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value = this.Date.ToString("yyyy/MM/dd (ddd)");
                    dgv_工程表.Focus();
                }
                else if (dgv.Columns[e.ColumnIndex].Name == "作業完了日")
                {
                    if (string.IsNullOrWhiteSpace(this.dgv_工程表[e.ColumnIndex - 1, e.RowIndex].Value.ToString()))
                    {
                        return;
                    }

                    Calendar form = new Calendar();
                    form.StartPosition = FormStartPosition.CenterParent;
                    form.ShowDialog();
                    this.Date = form.Date;

                    this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value = this.Date.ToString("yyyy/MM/dd (ddd)");
                    dgv_工程表.Focus();
                }
                else if (dgv.Columns[e.ColumnIndex].Name == "作業工程名")
                {
                    List<KeyValuePairDto> List = new List<KeyValuePairDto>();

                    var siireSaki = GetKakouSaki();

                    DataTable dt = GetKouteiMst(siireSaki);

                    foreach (DataRow dr in dt.Rows)
                    {
                        KeyValuePairDto dto = new KeyValuePairDto
                        {
                            Key = int.Parse(dr["工程コード"].ToString()),
                            Value = dr["工程名"].ToString()
                        };
                        List.Add(dto);
                    }
                    SelectListBox listBox = new SelectListBox
                    {
                        listBox = List,
                        Text = "工程選択"
                    };
                    listBox.StartPosition = FormStartPosition.CenterParent;
                    listBox.ShowDialog();

                    if (!string.IsNullOrWhiteSpace(listBox.SelectKey.ToString()))
                    {
                        this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value = listBox.SelectValue;
                        this.dgv_工程表[e.ColumnIndex - 1, e.RowIndex].Value = listBox.SelectKey;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工先を取得します
        /// </summary>
        /// <returns></returns>
        private string GetKakouSaki()
        {
            DataTable dt = (DataTable)dgv_工程表.DataSource;
            int kouteiNo = Convert.ToInt32(this.dgv_工程表.CurrentRow.Cells["非表示工程順"].Value.ToString());
            string value = string.Empty;

            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToInt32(dr["非表示工程順"].ToString()) == kouteiNo)
                {
                    if (Convert.ToInt32(dr["作業工程順"].ToString()) == 1)
                    {
                        value = dr["加工先"].ToString();
                    }
                }
            }
            return value;
        }

        /// <summary>
        /// 作業者印チェック処理
        /// </summary>
        /// <param name="e"></param>
        private void SetWorkerSign(DataGridViewCellEventArgs e)
        {
            try
            {
                DataTable dt = (DataTable)dgv_工程表.DataSource;
                int count = 0;

                if (!string.IsNullOrWhiteSpace(this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value.ToString()))
                {
                    this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value = string.Empty;
                    this.dgv_工程表[e.ColumnIndex + 1, e.RowIndex].Value = DBNull.Value;
                    return;
                }

                foreach (DataRow dr in dt.Rows)
                {
                    if (e.RowIndex > count)
                    {
                        if (string.IsNullOrWhiteSpace(this.dgv_工程表[e.ColumnIndex, count].Value.ToString()))
                        {
                            MessageBox.Show("前工程の完了印がありません。");
                            break;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value.ToString()))
                        {
                            this.dgv_工程表[e.ColumnIndex, e.RowIndex].Value = this.txtWorker.Text;
                            this.dgv_工程表[e.ColumnIndex + 1, e.RowIndex].Value = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss (ddd)");
                            break;
                        }
                    }
                    count++;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// コピーして作成ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnCopy(object sender, EventArgs e)
        {
            try
            {
                // 入力チェック
                if (!InputCheck())
                {
                    return;
                }

                if (this.cmbCopy.SelectedIndex != 0)
                {
                    DataTable dt = GetKakouIndicationInfo(this.cmbCopy.SelectedValue.ToString(), true);
                    SetDgv_工程表(dt, true);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// KeyDownTxtKouteiNo処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeyDownTxtKouteiNo(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ClickBtnSearch(sender, e);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// CellMouseDownDgv_工程表処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellMouseDownDgv_工程表(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                // 入力チェック
                if (!InputCheck())
                {
                    return;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 検索ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnSearch(object sender, EventArgs e)
        {
            try
            {
                this.KouteiNo = this.txtKouteiNo.Text;
                // 入力チェック
                if (!InputCheck())
                {
                    return;
                }
                else
                {
                    DataTable dt = new DataTable();
                    DateTime date;
                    string kouteiNo = Microsoft.VisualBasic.Strings.StrConv(this.txtKouteiNo.Text, Microsoft.VisualBasic.VbStrConv.Narrow);
                    dt = GetKakouIndicationInfo(kouteiNo, false);
                    // 加工先コンボボックス
                    GetCmbKakou(kouteiNo);
                    //// 注意コンボボックス
                    //GetCmbNotice();
                    // 加工指示書データが取得できない場合
                    if (dt.Rows.Count == 0)
                    {
                        dt = GetZumenInfo(kouteiNo);
                        if (dt.Rows.Count == 0)
                        {
                            MessageBox.Show("工程表番号が違います。");
                            this.txtKouteiNo.Focus();
                            return;
                        }
                        else
                        {
                            if (DateTime.TryParse(dt.Rows[0]["納期"].ToString(), out date))
                            {
                                this.txtNouki.Text = date.ToString("yyyy/MM/dd (ddd)");
                            }
                            SetDgv_材料(dt);
                            DataTable tempDt = GetKakouIndicationCd(dt.Rows[0]["図面番号"].ToString());
                            // 同一図面の場合
                            if (tempDt.Rows.Count > 0)
                            {
                                List<int> list = GetKakouSaki(kouteiNo);

                                tempDt = GetKakouIndicationInfo(Int32.Parse(tempDt.Rows[0]["加工指示書コード"].ToString()), kouteiNo, list);

                                SetDgv_工程表(tempDt, true);
                            }
                            else
                            {
                                SetDgv_工程表(GetKouteiHyoInfo());
                            }
                        }
                    }
                    else
                    {
                        SetDgv_工程表(dt);
                        dt = GetZumenInfo(kouteiNo);
                        SetDgv_材料(dt);
                    }
                    this.txtZumenName.Text = dt.Rows[0]["図面番号"].ToString();
                    this.txtKisyu.Text = dt.Rows[0]["機種名"].ToString();
                    this.txtOrderNo.Text = dt.Rows[0]["オーダーNO"].ToString();
                    this.txtDelivLc.Text = dt.Rows[0]["納入場所"].ToString();

                    if (DateTime.TryParse(dt.Rows[0]["納期"].ToString(), out date))
                    {
                        this.txtNouki.Text = date.ToString("yyyy/MM/dd (ddd)");
                    }
                    // ピン入れ、ヘリサート工程チェック
                    SetInputCheckFlg(kouteiNo);
                }
                // データグリッドの設定を行います
                SetDataGridView();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工先を取得します
        /// </summary>
        /// <param name="kouteiNo"></param>
        /// <returns></returns>
        private List<int> GetKakouSaki(string kouteiNo)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    List<int> list = new List<int>();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT DISTINCT ");
                    query.Append("   加工先コード ");
                    query.Append("  FROM ");
                    query.Append("   実績工程データ ");
                    query.Append(" WHERE  ");
                    query.Append("   工程表番号 = UPPER(:工程表番号)");
                    query.Append(" AND 加工先コード NOT IN (0, 1, 2, 100000) ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("工程表番号", NpgsqlDbType.Char));
                    cmd.Parameters["工程表番号"].Value = kouteiNo;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    foreach (DataRow dr in dt.Rows)
                    {
                        list.Add(int.Parse(dr["加工先コード"].ToString()));
                    }

                    return list;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// ピン入れ、ヘリサート工程が必要なデータか
        /// </summary>
        /// <param name="kouteiNo"></param>
        private void SetInputCheckFlg(string kouteiNo)
        {
            DataTable dt = GetInputCheckDate(kouteiNo);
            this.HelisertFlg = false;
            this.PinFlg = false;
            this.HyoshoFlg = false;

            foreach (DataRow dr in dt.Rows)
            {
                if (dr["工程コード"].ToString().Equals("132"))
                {
                    this.HelisertFlg = true;
                }
                else if (dr["工程コード"].ToString().Equals("133"))
                {
                    this.PinFlg = true;
                }
                
                if(!(dr["加工先コード"].ToString().Equals("0")
                    || dr["加工先コード"].ToString().Equals("")
                    || dr["加工先コード"].ToString().Equals("1")
                    || dr["加工先コード"].ToString().Equals("2")
                    || dr["加工先コード"].ToString().Equals("100000")))
                {
                    this.HyoshoFlg = true;
                }
            }
        }

        /// <summary>
        /// 仕入先マスタを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetInputCheckDate(string kouteiNo)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   工程コード ");
                    query.Append("  ,加工先コード ");
                    query.Append("  FROM ");
                    query.Append("   実績工程データ ");
                    query.Append(" WHERE  ");
                    query.Append("   工程表番号 = UPPER(:工程表番号) ");
                    query.Append(" GROUP BY 工程コード,加工先コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("工程表番号", NpgsqlDbType.Char));
                    cmd.Parameters["工程表番号"].Value = kouteiNo;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// DataGridViewCellEnterイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellEnterDataGridView(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!this.Flg && !LoadFlg)
                {
                    this.Flg = true;
                    return;
                }

                DataGridView dgv = (DataGridView)sender;

                if (e.ColumnIndex == 1 || e.ColumnIndex == 4 || e.ColumnIndex == 6)
                {
                    SendKeys.Send("{F4}");
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 完了日リセットボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnReset(object sender, EventArgs e)
        {
            try
            {
                CompletionDateReset();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// EditingControlShowingイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditingControlShowingDgv_工程表(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                DataGridView dgv = (DataGridView)sender;
                // コンボボックスが選択された場合
                if (dgv.CurrentCell.OwningColumn.Name == "加工先" ||
                    dgv.CurrentCell.OwningColumn.Name == "作業工程")
                {
                    if (e.Control is DataGridViewComboBoxEditingControl control)
                    {
                        control.DropDown += (s, v) =>
                        {
                            control.BackColor = Color.White;
                            control.MaxDropDownItems = 20;
                            control.IntegralHeight = false;
                            control.Invalidate();
                        };
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程行追加ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnKouteiAdd(object sender, EventArgs e)
        {
            try
            {
                // 入力チェック
                if (!InputCheck())
                {
                    return;
                }

                int row = this.dgv_工程表.CurrentCell.RowIndex;
                DataTable dt = (DataTable)dgv_工程表.DataSource;
                int kouteiNo = Convert.ToInt32(this.dgv_工程表.CurrentRow.Cells["非表示工程順"].Value.ToString());
                int sagyoNo = Convert.ToInt32(this.dgv_工程表.CurrentRow.Cells["作業工程順"].Value.ToString());

                foreach (DataRow dRow in dt.Rows)
                {
                    if (Convert.ToInt32(dRow["非表示工程順"].ToString()) > kouteiNo)
                    {
                        dRow["非表示工程順"] = Convert.ToInt32(dRow["非表示工程順"].ToString()) + 1;
                        if (!string.IsNullOrWhiteSpace(dRow["工程順"].ToString()))
                        {
                            dRow["工程順"] = Convert.ToInt32(dRow["工程順"].ToString()) + 1;
                        }
                    }

                    // 挿入場所の設定
                    if (Convert.ToInt32(dRow["非表示工程順"].ToString()) == kouteiNo
                        && Convert.ToInt32(dRow["作業工程順"].ToString()) > sagyoNo)
                    {
                        row++;
                    }
                }

                DataRow dr = dt.NewRow();
                dr["工程順"] = kouteiNo + 1;
                dr["非表示工程順"] = kouteiNo + 1;
                dr["作業工程順"] = 1;
                dt.Rows.InsertAt(dr, row + 1);
            }
            catch (Exception ex)
            {
                BaseView.log.Error("工程行追加" + ex.Message + "\r\n" + ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// 工程行削除ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnKouteiDel(object sender, EventArgs e)
        {
            try
            {
                // 入力チェック
                if (!InputCheck())
                {
                    return;
                }

                DataTable dt = (DataTable)dgv_工程表.DataSource;
                int count = dt.AsEnumerable()
                      .Where(r => r.Field<int>("非表示工程順") != 1).ToList().Count;

                // 工程行が1つの場合
                if (count <= 0)
                {
                    return;
                }

                int kouteiNo = Convert.ToInt32(this.dgv_工程表.CurrentRow.Cells["非表示工程順"].Value.ToString());

                // 行削除
                dt.AsEnumerable()
                     .Where(r => r.Field<int>("非表示工程順") == kouteiNo)
                     .ToList().ForEach(r => dt.Rows.Remove(r));

                foreach (DataRow dRow in dt.Rows)
                {
                    if (Convert.ToInt32(dRow["非表示工程順"].ToString()) > kouteiNo)
                    {
                        dRow["非表示工程順"] = Convert.ToInt32(dRow["非表示工程順"].ToString()) - 1;
                        if (!string.IsNullOrWhiteSpace(dRow["工程順"].ToString()))
                        {
                            dRow["工程順"] = Convert.ToInt32(dRow["工程順"].ToString()) - 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BaseView.log.Error("工程行削除" + ex.Message + "\r\n" + ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// 作業工程追加ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnSagyoAdd(object sender, EventArgs e)
        {
            try
            {
                // 入力チェック
                if (!InputCheck())
                {
                    return;
                }

                DataTable dt = (DataTable)dgv_工程表.DataSource;
                int row = this.dgv_工程表.CurrentCell.RowIndex;
                int col = this.dgv_工程表.Columns["加工先"].DisplayIndex;
                int kouteiNo = Convert.ToInt32(this.dgv_工程表.CurrentRow.Cells["非表示工程順"].Value.ToString());
                int sagyoNo = Convert.ToInt32(this.dgv_工程表.CurrentRow.Cells["作業工程順"].Value.ToString());

                foreach (DataRow dRow in dt.Rows)
                {
                    if (Convert.ToInt32(dRow["非表示工程順"].ToString()) == kouteiNo)
                    {
                        if (Convert.ToInt32(dRow["作業工程順"].ToString()) > sagyoNo)
                        {
                            dRow["作業工程順"] = Convert.ToInt32(dRow["作業工程順"].ToString()) + 1;
                        }
                    }
                }

                DataRow dr = dt.NewRow();
                dr["非表示工程順"] = kouteiNo;
                dr["作業工程順"] = sagyoNo + 1;
                dt.Rows.InsertAt(dr, row + 1);

                // 詳細行は加工先コンボを使用不可
                this.dgv_工程表[col, row + 1].ReadOnly = true;
            }
            catch (Exception ex)
            {
                BaseView.log.Error("作業工程追加" + ex.Message + "\r\n" + ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// 取引先カレンダーボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnClientCalendar(object sender, EventArgs e)
        {
            try
            {
                ClientCalendar form = new ClientCalendar();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
            }
        }

        /// <summary>
        /// 図面表示ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtn_DispZumen(object sender, EventArgs e)
        {
            try
            {
                DispZumen form = new DispZumen();
                form.KouteiNo = this.txtKouteiNo.Text;
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
            }
        }

        /// <summary>
        /// 登録ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnRegister(object sender, EventArgs e)
        {
            try
            {
                // 入力チェック
                if (!RegistCheck())
                {
                    return;
                }

                // 不要な作業工程行削除
                SagyouKouteiDel();
                // 不要な工程行削除
                KouteiDel();

                // 更新処理
                KakouIndicationInfo dto = SetKakouIndicationInfo();
                RegistKakouIndicationInfo(dto);

                //// 画面初期化
                //FormInitialize();
                ClickBtnSearch(sender, e);

                MessageBox.Show("登録しました。");

            }
            catch (Exception ex)
            {
                BaseView.log.Error("登録ボタン" + ex.Message + "\r\n" + ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// 作業工程削除処理
        /// </summary>
        private void SagyouKouteiDel()
        {
            try
            {
                DataTable dt = (DataTable)dgv_工程表.DataSource;
                List<List<int>> list = new List<List<int>>();

                foreach (DataRow dr in dt.Rows)
                {

                    // 詳細行は加工先コンボを使用不可
                    if (string.IsNullOrWhiteSpace(dr["工程順"].ToString())
                        && (string.IsNullOrWhiteSpace(dr["加工先"].ToString())
                        || dr["加工先"].ToString().Equals("100000"))
                        && (string.IsNullOrWhiteSpace(dr["作業工程"].ToString())
                        || dr["作業工程"].ToString().Equals("0"))
                        && string.IsNullOrWhiteSpace(dr["作業者印"].ToString())
                        && string.IsNullOrWhiteSpace(dr["出荷"].ToString())
                        && string.IsNullOrWhiteSpace(dr["注意事項"].ToString())
                        )
                    {
                        var dto = new List<int>();
                        dto.Insert(0, Convert.ToInt32(dr["非表示工程順"].ToString()));
                        dto.Insert(1, Convert.ToInt32(dr["作業工程順"].ToString()));
                        list.Insert(0, dto);
                    }
                }
                SagyouKouteiDel(list);
            }
            catch (Exception ex)
            {
                BaseView.log.Error("工程行削除" + ex.Message + "\r\n" + ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// 作業工程削除処理
        /// </summary>
        /// <param name="list"></param>
        private void SagyouKouteiDel(List<List<int>> list)
        {
            try
            {
                foreach (var dto in list)
                {
                    DataTable dt = (DataTable)dgv_工程表.DataSource;

                    // 行削除
                    dt.AsEnumerable()
                         .Where(r => r.Field<int>("作業工程順") == dto[1] && r.Field<int>("非表示工程順") == dto[0])
                         .ToList().ForEach(r => dt.Rows.Remove(r));

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Convert.ToInt32(dr["非表示工程順"].ToString()) == dto[0])
                        {
                            if (Convert.ToInt32(dr["作業工程順"].ToString()) > dto[1])
                            {
                                dr["作業工程順"] = Convert.ToInt32(dr["作業工程順"].ToString()) - 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BaseView.log.Error("工程行削除" + ex.Message + "\r\n" + ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// 工程削除処理
        /// </summary>
        private void KouteiDel()
        {
            try
            {
                DataTable dt = (DataTable)dgv_工程表.DataSource;
                List<int> list = new List<int>();

                foreach (DataRow dr in dt.Rows)
                {
                    // 詳細行は加工先コンボを使用不可
                    if ((string.IsNullOrWhiteSpace(dr["加工先"].ToString())
                        || dr["加工先"].ToString().Equals("100000"))
                        && (string.IsNullOrWhiteSpace(dr["作業工程"].ToString())
                        || dr["作業工程"].ToString().Equals("0"))
                        && string.IsNullOrWhiteSpace(dr["作業者印"].ToString())
                        && string.IsNullOrWhiteSpace(dr["出荷"].ToString())
                        && string.IsNullOrWhiteSpace(dr["注意事項"].ToString())
                        )
                    {
                        list.Insert(0, Convert.ToInt32(dr["非表示工程順"].ToString()));
                    }
                }

                KouteiDel(list);
            }
            catch (Exception ex)
            {
                BaseView.log.Error("工程行削除" + ex.Message + "\r\n" + ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// 工程削除処理
        /// </summary>
        /// <param name="list"></param>
        private void KouteiDel(List<int> list)
        {
            try
            {
                foreach (var kouteiNo in list)
                {
                    DataTable dt = (DataTable)dgv_工程表.DataSource;

                    // 行削除
                    dt.AsEnumerable()
                         .Where(r => r.Field<int>("非表示工程順") == kouteiNo)
                         .ToList().ForEach(r => dt.Rows.Remove(r));

                    foreach (DataRow dRow in dt.Rows)
                    {
                        if (Convert.ToInt32(dRow["非表示工程順"].ToString()) > kouteiNo)
                        {
                            dRow["非表示工程順"] = Convert.ToInt32(dRow["非表示工程順"].ToString()) - 1;
                            if (!string.IsNullOrWhiteSpace(dRow["工程順"].ToString()))
                            {
                                dRow["工程順"] = Convert.ToInt32(dRow["工程順"].ToString()) - 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BaseView.log.Error("工程行削除" + ex.Message + "\r\n" + ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// 作業工程削除ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnSagyoDel(object sender, EventArgs e)
        {
            try
            {
                // 入力チェック
                if (!InputCheck())
                {
                    return;
                }

                DataTable dt = (DataTable)dgv_工程表.DataSource;
                int row = this.dgv_工程表.CurrentCell.RowIndex;
                int kouteiNo = Convert.ToInt32(this.dgv_工程表.CurrentRow.Cells["非表示工程順"].Value.ToString());
                int sagyoNo = Convert.ToInt32(this.dgv_工程表.CurrentRow.Cells["作業工程順"].Value.ToString());
                string dispKouteiNo = this.dgv_工程表.CurrentRow.Cells["工程順"].Value.ToString();

                if (string.IsNullOrWhiteSpace(dispKouteiNo))
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        if (Convert.ToInt32(dRow["非表示工程順"].ToString()) == kouteiNo)
                        {
                            if (Convert.ToInt32(dRow["作業工程順"].ToString()) > sagyoNo)
                            {
                                dRow["作業工程順"] = Convert.ToInt32(dRow["作業工程順"].ToString()) - 1;
                            }
                        }
                    }
                    dt.Rows.RemoveAt(row);
                }
                else
                {
                    MessageBox.Show("工程行です。");
                }
            }
            catch (Exception ex)
            {
                BaseView.log.Error("作業工程削除" + ex.Message + "\r\n" + ex.StackTrace);
                throw;
            }
        }

        #endregion

        #region private関数

        /// <summary>
        /// 加工先コンボボックスを設定します
        /// </summary>
        private void GetCmbKakou(string kouteiNo)
        {
            try
            {
                this.dgv_工程表.Columns.Remove("加工先");

                DataTable dt = GetShiireMst(kouteiNo);

                DataGridViewComboBoxColumn colDate = new DataGridViewComboBoxColumn();
                colDate.DataPropertyName = "加工先";
                colDate.DataSource = dt;
                colDate.ValueMember = "仕入先コード";
                colDate.DisplayMember = "仕入先略名";
                colDate.Name = "加工先";
                dgv_工程表.Columns.Insert(0, colDate);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書コピーコンボボックスを設定します
        /// </summary>
        private void GetCmbCopy()
        {
            try
            {
                DataTable dt = GetCopyCmb();
                DataRow dr = dt.NewRow();

                // 先頭に追加
                dr["図面番号"] = "最近作成した加工指示書";
                dt.Rows.InsertAt(dr, 0);
                // コンボボックスの設定
                this.cmbCopy.DataSource = dt;
                // 表示用の列を設定
                this.cmbCopy.DisplayMember = "図面番号";
                // データ用の列を設定
                this.cmbCopy.ValueMember = "工程表番号";
                this.cmbCopy.SelectedIndex = 0;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// データグリッドの設定を行います
        /// </summary>
        private void SetDataGridView()
        {
            try
            {
                this.dgv_工程表.AutoGenerateColumns = true;
                this.dgv_材料.AutoGenerateColumns = true;

                // DataGirdViewのTypeを取得
                Type dgvtype = typeof(DataGridView);
                // プロパティ設定の取得
                System.Reflection.PropertyInfo dgvPropertyInfo = dgvtype.GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                // 対象のDataGridViewにtrueをセットする
                dgvPropertyInfo.SetValue(dgv_工程表, true, null);
                dgvPropertyInfo.SetValue(dgv_材料, true, null);
                // 並び替えができないようにする
                foreach (DataGridViewColumn c in dgv_工程表.Columns)
                    c.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表情報を取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKouteiHyoInfo()
        {
            try
            {
                // データが存在しない場合
                DataTable dt = new DataTable();
                dt.Columns.Add("工程順", typeof(int));
                dt.Columns.Add("加工先", typeof(int));
                dt.Columns.Add("作業工程", typeof(int));
                dt.Columns.Add("作業工程名", typeof(string));
                dt.Columns.Add("出荷", typeof(string));
                dt.Columns.Add("完了予定日", typeof(DateTime));
                dt.Columns.Add("注意事項", typeof(string));
                dt.Columns.Add("チェック", typeof(string));
                dt.Columns.Add("作業者印", typeof(string));
                dt.Columns.Add("完了日", typeof(DateTime));
                dt.Columns.Add("作業工程順", typeof(int));
                dt.Columns.Add("非表示工程順", typeof(int));
                dt.Columns.Add("オーダーNO", typeof(string));
                dt.Columns.Add("加工指示書コード", typeof(int));
                dt.Columns.Add("氏名", typeof(string));

                DataRow dr = dt.NewRow();
                DateTime date;

                for (int i = 1; i <= 5; i++)
                {
                    dr = dt.NewRow();
                    dr["工程順"] = i;
                    dr["作業工程順"] = 1;
                    dr["非表示工程順"] = i;
                    dt.Rows.Add(dr);

                    if (i == 5)
                    {
                        dr["加工先"] = 2;
                        dr["作業工程"] = 29;
                        dr["作業工程名"] = "検査";
                        if (DateTime.TryParse(this.txtNouki.Text, out date))
                        {
                            dr["完了予定日"] = date.ToString("yyyy/MM/dd (ddd)");// work
                        }
                    }
                }
                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表データグリッドを設定します
        /// </summary>
        /// <param name="dt"></param>
        private void SetDgv_工程表(DataTable dt, bool copyFlg = false)
        {
            try
            {
                this.dgv_工程表.DataSource = dt;

                if (!copyFlg)
                {
                    this.lblKakouIndicationCd.Text = dt.Rows[0]["加工指示書コード"].ToString();
                    this.txtCreator.Text = dt.Rows[0]["氏名"].ToString();
                }

                this.dgv_工程表.Columns["工程順"].DisplayIndex = 0;
                this.dgv_工程表.Columns["加工先"].DisplayIndex = 1;
                this.dgv_工程表.Columns["作業工程名"].DisplayIndex = 2;
                this.dgv_工程表.Columns["完了予定日"].DisplayIndex = 3;
                this.dgv_工程表.Columns["出荷"].DisplayIndex = 4;
                this.dgv_工程表.Columns["注意事項"].DisplayIndex = 5;
                this.dgv_工程表.Columns["チェック"].DisplayIndex = 6;
                this.dgv_工程表.Columns["作業者印"].DisplayIndex = 7;
                this.dgv_工程表.Columns["作業完了日"].DisplayIndex = 8;
                this.dgv_工程表.Columns["作業工程"].Visible = false;
                this.dgv_工程表.Columns["オーダーNO"].Visible = false;
                this.dgv_工程表.Columns["加工指示書コード"].Visible = false;
                this.dgv_工程表.Columns["作業工程順"].Visible = false;
                this.dgv_工程表.Columns["非表示工程順"].Visible = false;
                this.dgv_工程表.Columns["氏名"].Visible = false;

                int col = this.dgv_工程表.Columns["加工先"].DisplayIndex;
                int row = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    // 詳細行は加工先コンボを使用不可
                    if (string.IsNullOrWhiteSpace(dr["工程順"].ToString()))
                    {
                        this.dgv_工程表[col, row].ReadOnly = true;
                    }
                    row++;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 材料データグリッドを設定します
        /// </summary>
        /// <param name="dt"></param>
        private void SetDgv_材料(DataTable dt)
        {
            try
            {
                this.dgv_材料.DataSource = dt;
                this.dgv_材料.Columns["メーカー"].DisplayIndex = 0;
                this.dgv_材料.Columns["材質"].DisplayIndex = 1;
                this.dgv_材料.Columns["サイズ"].DisplayIndex = 2;
                this.dgv_材料.Columns["数量"].DisplayIndex = 3;
                this.dgv_材料.Columns["単価"].DisplayIndex = 4;
                this.dgv_材料.Columns["金額"].DisplayIndex = 5;
                dgv_材料.Columns["オーダーNO"].Visible = false;
                dgv_材料.Columns["図面番号"].Visible = false;
                dgv_材料.Columns["納期"].Visible = false;
                dgv_材料.Columns["機種名"].Visible = false;
                dgv_材料.Columns["図面パス"].Visible = false;
                dgv_材料.Columns["納入場所"].Visible = false;
                dgv_材料.Columns["指定場所"].Visible = false;
                dgv_材料.Columns["ship_days"].Visible = false;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書データ取得(Excel用)
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndication(string kouteiNo, bool copyFlg = false)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("    CASE C.作業工程順 ");
                    query.Append("        WHEN 1 THEN B.工程順 ");
                    query.Append("        ELSE null ");
                    query.Append("    END 工程順 ");
                    query.Append("   ,CASE C.作業工程順 ");
                    query.Append("        WHEN 1 THEN D.仕入先略名 ");
                    query.Append("        ELSE null ");
                    query.Append("    END 加工先 ");
                    query.Append("   ,E.工程名 ");
                    query.Append("   ,C.完了予定日 ");
                    query.Append("   ,C.出荷 ");
                    query.Append("   ,TRIM(C.注意事項) AS 注意事項 ");
                    query.Append("   ,CASE C.チェック ");
                    query.Append("        WHEN true THEN '✓' ");
                    query.Append("        ELSE '' ");
                    query.Append("    END チェック ");
                    query.Append("   ,C.作業者印 AS 作業者印 ");
                    query.Append("   ,C.完了日 AS 完了日 ");
                    query.Append("  FROM 加工指示書データ A ");
                    query.Append("  LEFT JOIN 加工指示書工程データ B ");
                    query.Append("    ON A.加工指示書コード = B.加工指示書コード ");
                    query.Append("  LEFT JOIN 加工指示書作業データ C ");
                    query.Append("    ON B.加工指示書コード = C.加工指示書コード ");
                    query.Append("   AND B.工程順 = C.工程順 ");
                    query.Append("  LEFT JOIN 仕入先マスタ D ");
                    query.Append("    ON B.加工先 = D.仕入先コード ");
                    query.Append("  LEFT JOIN 工程マスタ E ");
                    query.Append("    ON C.作業工程 = E.工程コード ");
                    query.Append(" WHERE A.工程表番号 = UPPER(:工程表番号) ");
                    query.Append(" ORDER BY ");
                    query.Append("    B.工程順 ");
                    query.Append("   ,C.作業工程順 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("工程表番号", NpgsqlDbType.Varchar));
                    cmd.Parameters["工程表番号"].Value = kouteiNo;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書データ取得
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndicationInfo(string kouteiNo, bool copyFlg = false)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("    A.加工指示書コード ");

                    if (copyFlg)
                    {
                        query.Append("   ,'' AS 氏名 ");
                        query.Append("   ,CASE C.作業工程順 ");
                        query.Append("        WHEN 1 THEN  ");
                        query.Append("        CASE ");
                        query.Append("            WHEN B.加工先 in (0, 1,2) THEN B.加工先 ");
                        query.Append("        ELSE 100000  ");
                        query.Append("     END  ");
                        query.Append("        ELSE null  ");
                        query.Append("     END 加工先 ");
                    }
                    else
                    {
                        query.Append("   ,D.氏名 ");
                        query.Append("   ,CASE C.作業工程順 ");
                        query.Append("        WHEN 1 THEN B.加工先 ");
                        query.Append("        ELSE null ");
                        query.Append("    END 加工先 ");
                    }
                    query.Append("   ,CASE C.作業工程順 ");
                    query.Append("        WHEN 1 THEN B.工程順 ");
                    query.Append("        ELSE null ");
                    query.Append("    END 工程順 ");
                    query.Append("   ,A.\"オーダーNO\" ");
                    query.Append("   ,C.作業工程 ");
                    query.Append("   ,E.工程名 AS 作業工程名 ");
                    query.Append("   ,C.完了予定日 ");
                    query.Append("   ,C.出荷 ");
                    query.Append("   ,TRIM(C.注意事項) AS 注意事項 ");

                    if (copyFlg)
                    {
                        query.Append("   ,'' チェック ");
                        query.Append("   ,null AS 作業者印 ");
                        query.Append("   ,null AS 完了日 ");
                    }
                    else
                    {
                        query.Append("   ,CASE C.チェック ");
                        query.Append("        WHEN true THEN '✓' ");
                        query.Append("        ELSE '' ");
                        query.Append("    END チェック ");
                        query.Append("   ,C.作業者印 AS 作業者印 ");
                        query.Append("   ,C.完了日 AS 完了日 ");
                    }
                    query.Append("   ,C.作業工程順 ");
                    query.Append("   ,B.工程順 AS 非表示工程順 ");
                    query.Append("  FROM 加工指示書データ A ");
                    query.Append("  LEFT JOIN 加工指示書工程データ B ");
                    query.Append("    ON A.加工指示書コード = B.加工指示書コード ");
                    query.Append("  LEFT JOIN 加工指示書作業データ C ");
                    query.Append("    ON B.加工指示書コード = C.加工指示書コード ");
                    query.Append("   AND B.工程順 = C.工程順 ");
                    query.Append("  LEFT JOIN 社員マスタ D ");
                    query.Append("    ON A.作成者 = D.社員番号 ");
                    query.Append("  LEFT JOIN 工程マスタ E ");
                    query.Append("    ON C.作業工程 = 工程コード ");
                    query.Append(" WHERE A.工程表番号 = UPPER(:工程表番号) ");
                    query.Append(" ORDER BY ");
                    query.Append("    B.工程順 ");
                    query.Append("   ,C.作業工程順 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("工程表番号", NpgsqlDbType.Varchar));
                    cmd.Parameters["工程表番号"].Value = kouteiNo;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書データ取得
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndicationInfo(int kakouIndicationCd, string kouteiNo, List<int> list)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   A.加工指示書コード ");
                    query.Append("  ,CASE C.作業工程順 ");
                    query.Append("       WHEN 1 THEN ");
                    query.Append("           CASE  ");
                    query.Append("               WHEN B.加工先 in (0,1,2,10000 ");

                    for (int i = 0; i < list.Count; i++)
                    {
                        query.Append("," + list[i]);
                    }

                    query.Append("               ) ");
                    query.Append("               THEN B.加工先  ");
                    query.Append("               ELSE NULL   ");
                    query.Append("           END   ");
                    query.Append("       ELSE null ");
                    query.Append("   END 加工先 ");
                    query.Append("  ,CASE C.作業工程順 ");
                    query.Append("       WHEN 1 THEN B.工程順 ");
                    query.Append("       ELSE null ");
                    query.Append("   END 工程順 ");
                    query.Append("  ,A.\"オーダーNO\" ");
                    query.Append("  ,C.作業工程 ");
                    query.Append("  ,D.工程名 AS 作業工程名 ");
                    query.Append("  ,C.出荷 ");
                    query.Append("  ,TRIM(C.注意事項) AS 注意事項 ");
                    query.Append("  ,'' AS チェック ");
                    query.Append("  ,null AS 作業者印 ");
                    query.Append("  ,null AS 完了日 ");
                    query.Append("  ,C.作業工程順 ");
                    query.Append("  ,B.工程順 AS 非表示工程順 ");
                    query.Append("  ,'' AS 氏名 ");
                    query.Append(" FROM 加工指示書データ A ");
                    query.Append(" LEFT JOIN 加工指示書工程データ B ");
                    query.Append("   ON A.加工指示書コード = B.加工指示書コード ");
                    query.Append(" LEFT JOIN 加工指示書作業データ C ");
                    query.Append("   ON B.加工指示書コード = C.加工指示書コード ");
                    query.Append("  AND B.工程順 = C.工程順 ");
                    query.Append(" LEFT JOIN 工程マスタ D ");
                    query.Append("   ON C.作業工程 = 工程コード ");
                    query.Append("WHERE A.加工指示書コード = :加工指示書コード ");
                    query.Append("ORDER BY ");
                    query.Append("   B.工程順 ");
                    query.Append("  ,C.作業工程順 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("加工指示書コード", NpgsqlDbType.Integer));
                    cmd.Parameters["加工指示書コード"].Value = kakouIndicationCd;

                    cmd.Parameters.Add(new NpgsqlParameter("工程表番号", NpgsqlDbType.Varchar));
                    cmd.Parameters["工程表番号"].Value = kouteiNo;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);
                    dt.Columns.Add("完了予定日", typeof(DateTime));

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書データ取得
        /// </summary>
        /// <returns></returns>
        private DataTable GetKakouIndicationCd(string zumenName)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("    A.加工指示書コード ");
                    query.Append("   ,B.図面番号 ");
                    query.Append("  FROM 加工指示書データ A ");
                    query.Append("  LEFT JOIN 実績加工データ B ");
                    query.Append("    ON A.工程表番号 = B.工程表番号  ");
                    query.Append(" WHERE B.図面番号 = :図面番号 ");
                    query.Append(" ORDER BY ");
                    query.Append("    作成日時 DESC ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("図面番号", NpgsqlDbType.Varchar));
                    cmd.Parameters["図面番号"].Value = zumenName;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 実績加工データ
        /// </summary>
        /// <returns></returns>
        private DataTable GetZumenInfo(string kouteiNo)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   A.\"オーダーNO\" ");
                    query.Append("  ,A.図面番号 ");
                    query.Append("  ,A.納期 ");
                    query.Append("  ,D.機種名 ");
                    query.Append("  ,A.図面パス ");
                    query.Append("  ,B.材質 ");
                    query.Append("  ,B.サイズ ");
                    query.Append("  ,C.仕入先略名 AS メーカー ");
                    query.Append("  ,B.数量 ");
                    query.Append("  ,B.単価 ");
                    query.Append("  ,B.金額 ");
                    query.Append("  ,E.納入場所 ");
                    query.Append("  ,A.指定場所 ");
                    query.Append("  ,CASE        ");
                    query.Append("       WHEN F.出荷日数 IS NOT NULL then F.出荷日数   ");
                    query.Append("       WHEN G.出荷日数 IS NOT NULL then G.出荷日数   ");
                    query.Append("       WHEN H.出荷日数 IS NOT NULL then H.出荷日数   ");
                    query.Append("       WHEN I.出荷日数 IS NOT NULL then I.出荷日数   ");
                    query.Append("    ELSE -1                                          ");
                    query.Append("    END ship_days   ");

                    query.Append("  FROM 実績加工データ A ");
                    query.Append("  LEFT JOIN 実績材料データ B ");
                    query.Append("    USING(\"オーダーNO\", グループ, 連番)  ");
                    query.Append("  LEFT JOIN 仕入先マスタ C ");
                    query.Append("    ON B.仕入先コード = C.仕入先コード  ");
                    query.Append("  LEFT JOIN オーダー管理部材テーブル D ");
                    query.Append("    ON A.\"オーダーNO\" = D.\"オーダーNO\" ");
                    query.Append("   AND D.部材番号 = 1 ");
                    query.Append("  LEFT JOIN オーダー管理納入場所テーブル E ");
                    query.Append("    ON A.\"オーダーNO\" = E.\"オーダーNO\" ");
                    query.Append("   AND A.グループ = E.グループ ");
                    query.Append("  LEFT JOIN 納入場所マスタ F               ");
                    query.Append("    ON SUBSTR(E.納入場所, 1, 4) = F.納入場所コード ");
                    query.Append("  LEFT JOIN 納入場所マスタ G                       ");
                    query.Append("    ON SUBSTR(E.納入場所, 1, 3) = G.納入場所コード ");
                    query.Append("  LEFT JOIN 納入場所マスタ H                       ");
                    query.Append("    ON SUBSTR(E.納入場所, 1, 2) = H.納入場所コード ");
                    query.Append("  LEFT JOIN 納入場所マスタ I                       ");
                    query.Append("    ON SUBSTR(E.納入場所, 1, 1) = I.納入場所コード ");

                    query.Append(" WHERE A.工程表番号 = UPPER(:工程表番号) ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("工程表番号", NpgsqlDbType.Varchar));
                    cmd.Parameters["工程表番号"].Value = kouteiNo;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    string ord = string.Empty;
                    string delivLc = string.Empty;
                    string siteiPl = string.Empty;
                    int value = 0;
                    DateTime date;

                    var shipD = 0;
                    var shipDays = 0;

                    // 加工完了納期を計算します
                    foreach (DataRow dr in dt.Rows)
                    {
                        ord = dr["オーダーNO"].ToString();
                        delivLc = dr["納入場所"].ToString();
                        siteiPl = dr["指定場所"].ToString();
                        shipDays = int.Parse(dr["ship_days"].ToString());
                        if (shipDays == -1)
                        {
                            if (!int.TryParse(delivLc, out shipD) && delivLc == "")
                            {
                                shipD = 1;
                            }
                        }
                        else
                        {
                            shipD = shipDays;
                        }

                        value = GetInspectionTerm(ord, delivLc, siteiPl, shipD - 1);
                        int count = 0;

                        if (DateTime.TryParse(dr["納期"].ToString(), out date))
                        {
                            for (int i = 1; i <= value; i++)
                            {
                                if (date.AddDays(-(i + count)).DayOfWeek == DayOfWeek.Saturday || date.AddDays(-(i + count)).DayOfWeek == DayOfWeek.Sunday)
                                {
                                    count++;
                                    if (date.AddDays(-(i + count)).DayOfWeek == DayOfWeek.Saturday || date.AddDays(-(i + count)).DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        count++;
                                    }
                                }
                            }
                            dr["納期"] = date.AddDays(-1 * (value + count));
                        }
                    }

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工完了時間を取得します
        /// </summary>
        /// <param name="ord"></param>
        /// <param name="delivLc"></param>
        /// <param name="siteiPl"></param>
        /// <returns></returns>
        private int GetInspectionTerm(string ord, string delivLc, string siteiPl, int shipD)
        {
            try
            {
                int value = 2;
                if (delivLc.StartsWith("TEW2")
                    || delivLc.StartsWith("TEW3")
                    || delivLc.StartsWith("TEW4"))
                {
                    value = 4;
                }
                else if (delivLc.StartsWith("TOM")
                    || delivLc.StartsWith("KMM5"))
                {
                    value = 3;
                }
                else if (delivLc.StartsWith("KMM1")
                        || delivLc.StartsWith("KMM2")
                        || delivLc.StartsWith("KMM3")
                        || delivLc.StartsWith("KMM4")
                        || delivLc.StartsWith("KMC")
                        || delivLc.StartsWith("KDK")
                        || delivLc.StartsWith("FMC")
                        || delivLc.StartsWith("SMC")
                        || delivLc.StartsWith("KMM7")
                        || delivLc.StartsWith("NIK")
                        || delivLc.StartsWith("ADC")
                        || delivLc.StartsWith("OMC")
                        || delivLc.StartsWith("MMC")
                        || delivLc.StartsWith("IMH")
                        || ord.StartsWith("I")
                        || ord.StartsWith("R")
                        || ord.StartsWith("S")
                        || ord.StartsWith("V")
                        || ord.StartsWith("O"))
                {
                    value = 2;
                    if (ord.StartsWith("O"))
                    {
                        value = value + shipD;
                    }
                }
                else if (siteiPl.Contains("射水工場")
                        || delivLc.StartsWith("TMC")
                        || delivLc.StartsWith("HDS")
                        || delivLc.StartsWith("NDK")
                        || ord.StartsWith("JT")
                        || ord.StartsWith("JN")
                        || ord.StartsWith("JH")
                        || ord.StartsWith("T")
                        || ord.StartsWith("N")
                        || ord.StartsWith("H"))
                {
                    value = 1;
                }

                return value;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書データ登録
        /// </summary>
        /// <param name="dto"></param>
        private void RegistKakouIndicationInfo(KakouIndicationInfo dto)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    try
                    {
                        // 加工指示書データ登録
                        RegistKakouIndicationInfo(con, dto);
                        // 加工指示書工程データ登録
                        RegistKouteiInfo(con, dto.KouteiInfoList, dto.KakouIndicationCd);
                    }
                    catch (Exception e)
                    {
                        tran.Rollback();
                        throw e;
                    }
                    tran.Commit();
                }
            }
        }

        /// <summary>
        /// 工程情報登録
        /// </summary>
        /// <param name="con"></param>
        /// <param name="list"></param>
        /// <param name="kakouIndicationCd"></param>
        private void RegistKouteiInfo(NpgsqlConnection con, List<KouteiInfo> list, int kakouIndicationCd)
        {
            try
            {
                // デリート＆インサート
                DeleteKouteiInfo(con, kakouIndicationCd);
                DeleteSagyoInfo(con, kakouIndicationCd);

                foreach (var dto in list)
                {
                    // 登録
                    InsertKouteiInfo(con, dto, kakouIndicationCd);
                    RegistSagyoInfo(con, dto.SagyoInfoList, kakouIndicationCd);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書作業データ登録
        /// </summary>
        /// <param name="con"></param>
        /// <param name="list"></param>
        /// <param name="kakouIndicationCd"></param>
        private void RegistSagyoInfo(NpgsqlConnection con, List<SagyoInfo> list, int kakouIndicationCd)
        {
            try
            {
                foreach (var dto in list)
                {
                    // 登録
                    InsertSagyoInfo(con, dto, kakouIndicationCd);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書作業データ登録
        /// </summary>
        /// <param name="con"></param>
        /// <param name="dto"></param>
        /// <param name="kakouIndicationCd"></param>
        private void InsertSagyoInfo(NpgsqlConnection con, SagyoInfo dto, int kakouIndicationCd)
        {
            try
            {
                StringBuilder query = new StringBuilder();

                query.AppendLine("INSERT ");
                query.AppendLine("    INTO 加工指示書作業データ (  ");
                query.AppendLine("    加工指示書コード ");
                query.AppendLine("   ,工程順 ");
                query.AppendLine("   ,作業工程順 ");
                query.AppendLine("   ,作業工程 ");
                query.AppendLine("   ,完了予定日 ");
                query.AppendLine("   ,出荷 ");
                query.AppendLine("   ,注意事項 ");
                query.AppendLine("   ,チェック ");
                query.AppendLine("   ,作業者印 ");
                query.AppendLine("   ,完了日 ");
                query.AppendLine("   ,更新日時 ");
                query.AppendLine("   ,更新者) ");
                query.AppendLine("VALUES (  ");
                query.AppendLine("    :加工指示書コード ");
                query.AppendLine("   ,:工程順 ");
                query.AppendLine("   ,:作業工程順 ");
                query.AppendLine("   ,:作業工程 ");

                if (!(dto.CompletionDate == null))
                {
                    query.AppendLine("   ,:完了予定日 ");
                }
                else
                {
                    query.AppendLine("   ,null ");
                }

                query.AppendLine("   ,:出荷 ");
                query.AppendLine("   ,:注意事項 ");
                query.AppendLine("   ,:チェック ");
                query.AppendLine("   ,:作業者印 ");

                if (!(dto.CompletedDate == null))
                {
                    query.AppendLine("   ,:完了日 ");
                }
                else
                {
                    query.AppendLine("   ,null ");
                }

                query.AppendLine("   ,:更新日時 ");
                query.AppendLine("   ,:更新者) ");

                NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                cmd.Parameters.Add(new NpgsqlParameter("加工指示書コード", NpgsqlDbType.Integer));
                cmd.Parameters["加工指示書コード"].Value = kakouIndicationCd;

                cmd.Parameters.Add(new NpgsqlParameter("工程順", NpgsqlDbType.Integer));
                cmd.Parameters["工程順"].Value = dto.KouteiNo;

                cmd.Parameters.Add(new NpgsqlParameter("作業工程順", NpgsqlDbType.Integer));
                cmd.Parameters["作業工程順"].Value = dto.SagyoKouteiNo;

                cmd.Parameters.Add(new NpgsqlParameter("作業工程", NpgsqlDbType.Integer));
                cmd.Parameters["作業工程"].Value = dto.SagyoKoutei;

                if (!(dto.CompletionDate == null))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("完了予定日", NpgsqlDbType.Timestamp));
                    cmd.Parameters["完了予定日"].Value = dto.CompletionDate;
                }

                cmd.Parameters.Add(new NpgsqlParameter("出荷", NpgsqlDbType.Varchar));
                cmd.Parameters["出荷"].Value = dto.Shipment;

                cmd.Parameters.Add(new NpgsqlParameter("注意事項", NpgsqlDbType.Varchar));
                cmd.Parameters["注意事項"].Value = dto.Precautions.Trim();

                cmd.Parameters.Add(new NpgsqlParameter("チェック", NpgsqlDbType.Boolean));
                cmd.Parameters["チェック"].Value = dto.Check;

                cmd.Parameters.Add(new NpgsqlParameter("作業者印", NpgsqlDbType.Varchar));
                cmd.Parameters["作業者印"].Value = dto.Worker;

                if (!(dto.CompletedDate == null))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("完了日", NpgsqlDbType.Timestamp));
                    cmd.Parameters["完了日"].Value = dto.CompletedDate;
                }

                cmd.Parameters.Add(new NpgsqlParameter("更新日時", NpgsqlDbType.Timestamp));
                cmd.Parameters["更新日時"].Value = DateTime.Now;

                cmd.Parameters.Add(new NpgsqlParameter("更新者", NpgsqlDbType.Varchar));
                cmd.Parameters["更新者"].Value = LoginInfo.氏名;

                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書作業データ削除
        /// </summary>
        /// <param name="con"></param>
        /// <param name="kakouIndicationCd"></param>
        private void DeleteSagyoInfo(NpgsqlConnection con, int kakouIndicationCd)
        {
            try
            {
                StringBuilder query = new StringBuilder();

                query.AppendLine("DELETE FROM 加工指示書作業データ ");
                query.AppendLine("WHERE  ");
                query.AppendLine("    加工指示書コード = :加工指示書コード");

                NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                cmd.Parameters.Add(new NpgsqlParameter("加工指示書コード", NpgsqlDbType.Integer));
                cmd.Parameters["加工指示書コード"].Value = kakouIndicationCd;

                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書工程データ登録
        /// </summary>
        /// <param name="con"></param>
        /// <param name="dto"></param>
        /// <param name="kakouIndicationCd"></param>
        private void InsertKouteiInfo(NpgsqlConnection con, KouteiInfo dto, int kakouIndicationCd)
        {
            try
            {
                StringBuilder query = new StringBuilder();

                query.AppendLine("INSERT ");
                query.AppendLine("    INTO 加工指示書工程データ (  ");
                query.AppendLine("    加工指示書コード ");
                query.AppendLine("   ,工程順 ");
                query.AppendLine("   ,加工先) ");
                query.AppendLine("VALUES (  ");
                query.AppendLine("    :加工指示書コード ");
                query.AppendLine("   ,:工程順 ");
                query.AppendLine("   ,:加工先 ) ");

                NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                cmd.Parameters.Add(new NpgsqlParameter("加工指示書コード", NpgsqlDbType.Integer));
                cmd.Parameters["加工指示書コード"].Value = kakouIndicationCd;

                cmd.Parameters.Add(new NpgsqlParameter("工程順", NpgsqlDbType.Integer));
                cmd.Parameters["工程順"].Value = dto.KouteiNo;

                cmd.Parameters.Add(new NpgsqlParameter("加工先", NpgsqlDbType.Integer));
                cmd.Parameters["加工先"].Value = dto.KakouSaki;

                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書工程データ削除
        /// </summary>
        /// <param name="con"></param>
        /// <param name="kakouIndicationCd"></param>
        private void DeleteKouteiInfo(NpgsqlConnection con, int kakouIndicationCd)
        {
            try
            {
                StringBuilder query = new StringBuilder();

                query.AppendLine("DELETE FROM 加工指示書工程データ ");
                query.AppendLine("WHERE  ");
                query.AppendLine("    加工指示書コード = :加工指示書コード");

                NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                cmd.Parameters.Add(new NpgsqlParameter("加工指示書コード", NpgsqlDbType.Integer));
                cmd.Parameters["加工指示書コード"].Value = kakouIndicationCd;

                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書データ登録
        /// </summary>
        /// <param name="con"></param>
        /// <param name="dto"></param>
        private void RegistKakouIndicationInfo(NpgsqlConnection con, KakouIndicationInfo dto)
        {
            try
            {
                if (GetKakouIndicationInfo(this.txtKouteiNo.Text, false).Rows.Count > 0)
                {
                    // 更新
                    UpdateKakouIndicationInfo(con, dto);
                }
                else
                {
                    // プライマリーキー取得
                    dto.KakouIndicationCd = GetKakouIndicationCd(con);
                    // 登録
                    InsertKakouIndicationInfo(con, dto);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書データ更新
        /// </summary>
        /// <param name="con"></param>
        /// <param name="dto"></param>
        private void UpdateKakouIndicationInfo(NpgsqlConnection con, KakouIndicationInfo dto)
        {
            try
            {
                StringBuilder query = new StringBuilder();

                query.AppendLine("UPDATE 加工指示書データ ");
                query.AppendLine("SET  ");
                query.AppendLine("    現在工程 = :現在工程 ");
                query.AppendLine("   ,現在作業工程 = :現在作業工程 ");

                query.AppendLine("WHERE  ");
                query.AppendLine("    加工指示書コード = :加工指示書コード");

                NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                cmd.Parameters.Add(new NpgsqlParameter("加工指示書コード", NpgsqlDbType.Integer));
                cmd.Parameters["加工指示書コード"].Value = dto.KakouIndicationCd;

                cmd.Parameters.Add(new NpgsqlParameter("現在工程", NpgsqlDbType.Integer));
                cmd.Parameters["現在工程"].Value = dto.NowKoutei;

                cmd.Parameters.Add(new NpgsqlParameter("現在作業工程", NpgsqlDbType.Integer));
                cmd.Parameters["現在作業工程"].Value = dto.NowWorkKoutei;

                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書データ登録
        /// </summary>
        /// <param name="con"></param>
        /// <param name="dto"></param>
        private void InsertKakouIndicationInfo(NpgsqlConnection con, KakouIndicationInfo dto)
        {
            try
            {
                StringBuilder query = new StringBuilder();

                query.AppendLine("INSERT ");
                query.AppendLine("    INTO 加工指示書データ (  ");
                query.AppendLine("    加工指示書コード ");
                query.AppendLine("   ,工程表番号 ");
                query.AppendLine("   ,\"オーダーNO\" ");
                query.AppendLine("   ,現在工程 ");
                query.AppendLine("   ,現在作業工程 ");
                query.AppendLine("   ,作成日時 ");
                query.AppendLine("   ,作成者) ");
                query.AppendLine("VALUES (  ");
                query.AppendLine("    :加工指示書コード ");
                query.AppendLine("   ,:工程表番号 ");
                query.AppendLine("   ,:オーダーno ");
                query.AppendLine("   ,:現在工程 ");
                query.AppendLine("   ,:現在作業工程 ");
                query.AppendLine("   ,:作成日時 ");
                query.AppendLine("   ,:作成者 ) ");

                NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                cmd.Parameters.Add(new NpgsqlParameter("加工指示書コード", NpgsqlDbType.Integer));
                cmd.Parameters["加工指示書コード"].Value = dto.KakouIndicationCd;

                cmd.Parameters.Add(new NpgsqlParameter("工程表番号", NpgsqlDbType.Varchar));
                cmd.Parameters["工程表番号"].Value = dto.KouteihyouNo;

                cmd.Parameters.Add(new NpgsqlParameter("オーダーno", NpgsqlDbType.Varchar));
                cmd.Parameters["オーダーno"].Value = dto.OdaNo;

                cmd.Parameters.Add(new NpgsqlParameter("現在工程", NpgsqlDbType.Integer));
                cmd.Parameters["現在工程"].Value = dto.NowKoutei;

                cmd.Parameters.Add(new NpgsqlParameter("現在作業工程", NpgsqlDbType.Integer));
                cmd.Parameters["現在作業工程"].Value = dto.NowWorkKoutei;

                cmd.Parameters.Add(new NpgsqlParameter("作成日時", NpgsqlDbType.Timestamp));
                cmd.Parameters["作成日時"].Value = DateTime.Now;

                cmd.Parameters.Add(new NpgsqlParameter("作成者", NpgsqlDbType.Varchar));
                cmd.Parameters["作成者"].Value = dto.Author;

                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程マスタを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKouteiMst()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   工程コード ");
                    query.Append("  ,工程名 ");
                    query.Append("  FROM 工程マスタ ");
                    query.Append("  WHERE ");
                    query.Append("   作業区分 IN(0,2)");

                    query.Append(" ORDER BY ");
                    query.Append("   並び順 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程マスタを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetKouteiMst(string 仕入れ先)
        {
            try
            {
                DataTable dt = new DataTable();
                List<int> list = new List<int>();

                list.Add(94);
                list.Add(0);
                // 社内
                if (仕入れ先.Equals("0"))
                {
                    list.Add(122);
                    list.Add(3);
                    list.Add(6);
                    list.Add(12);
                    list.Add(13);
                    list.Add(17);
                    list.Add(25);
                    list.Add(29);
                    list.Add(100);
                    list.Add(101);
                    list.Add(102);
                    list.Add(103);
                    list.Add(104);
                    list.Add(105);
                    list.Add(106);
                    list.Add(107);
                    list.Add(108);
                    list.Add(109);
                    list.Add(110);
                    list.Add(111);
                    list.Add(112);
                    list.Add(113);
                    list.Add(114);
                    list.Add(115);
                    list.Add(116);
                    list.Add(117);
                    list.Add(118);
                    list.Add(119);
                    list.Add(120);
                    list.Add(121);
                    list.Add(131);
                    list.Add(134);
                }
                // パート
                else if (仕入れ先.Equals("1"))
                {
                    list.Add(84);
                    list.Add(131);
                    list.Add(132);
                    list.Add(133);
                    list.Add(134);
                    list.Add(135);
                    list.Add(136);
                }
                // 検査
                else if (仕入れ先.Equals("2"))
                {
                    list.Add(100);
                    list.Add(29);
                }
                // 三光鍍金
                else if (仕入れ先.Equals("19600"))
                {
                    list.Add(30);
                    list.Add(32);
                    list.Add(33);
                    list.Add(35);
                    list.Add(39);
                    list.Add(40);
                    list.Add(42);
                    list.Add(60);
                    list.Add(61);
                    list.Add(124);
                    list.Add(125);
                }
                // ユニゾーン
                else if (仕入れ先.Equals("15600"))
                {
                    list.Add(30);
                    list.Add(32);
                    list.Add(33);
                    list.Add(35);
                    list.Add(39);
                    list.Add(40);
                    list.Add(42);
                    list.Add(43);
                    list.Add(44);
                    list.Add(45);
                    list.Add(60);
                    list.Add(61);
                    list.Add(124);
                    list.Add(125);
                }
                // エムエー
                else if (仕入れ先.Equals("21600"))
                {
                    list.Add(43);
                    list.Add(44);
                    list.Add(45);
                }
                // 金属皮膜
                else if (仕入れ先.Equals("22600"))
                {
                    list.Add(30);
                    list.Add(36);
                }
                // アルバック
                else if (仕入れ先.Equals("18600"))
                {
                    list.Add(44);
                    list.Add(47);
                    list.Add(52);
                }
                // レイデント
                else if (仕入れ先.Equals("17600"))
                {
                    list.Add(46);
                    list.Add(48);
                    list.Add(49);
                    list.Add(126);
                }
                // 豊実精工
                else if (仕入れ先.Equals("17700"))
                {
                    list.Add(48);
                }
                // 植田アルマイト
                else if (仕入れ先.Equals("11600"))
                {
                    list.Add(41);
                }
                // ハイライト
                else if (仕入れ先.Equals("16600"))
                {
                    list.Add(36);
                }
                // 創進 コートせいでん
                else if (仕入れ先.Equals("26600") || 仕入れ先.Equals("29600"))
                {
                    list.Add(62);
                }
                // カナック
                else if (仕入れ先.Equals("10600"))
                {
                    list.Add(77);
                    list.Add(78);
                }
                // 吉田SKT
                else if (仕入れ先.Equals("12600"))
                {
                    list.Add(82);
                    list.Add(87);
                }
                // 旭プレシジョン
                else if (仕入れ先.Equals("10900"))
                {
                    list.Add(127);
                }
                // パーカー
                else if (仕入れ先.Equals("11000"))
                {
                    list.Add(123);
                }
                // 旭千代田
                else if (仕入れ先.Equals("23600"))
                {
                    list.Add(66);
                }
                // 小海ﾈｰﾑﾌﾟﾚｰﾄ
                else if (仕入れ先.Equals("28600"))
                {
                    list.Add(43);
                    list.Add(44);
                    list.Add(47);
                }
                // 北熱(熱処理)
                else if (仕入れ先.Equals("13600"))
                {
                    ;
                    list.Add(72);
                    list.Add(75);
                    list.Add(76);
                    list.Add(128);
                    list.Add(129);
                }
                // 北熱(熱処理)
                else if (仕入れ先.Equals("14600"))
                {
                    list.Add(63);
                    list.Add(64);
                    list.Add(98);
                    list.Add(130);
                }
                // 不二製作所
                else if (仕入れ先.Equals("18700"))
                {
                    list.Add(84);
                }

                dt = KouteiDt.AsEnumerable().Where(x =>
                         list.Contains((int)x["工程コード"])).CopyToDataTable();

                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 注意事項一覧を取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetNotice()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   TRIM(注意事項) AS 注意事項 ");
                    query.Append("  FROM 加工指示書作業データ ");
                    query.Append(" GROUP BY ");
                    query.Append("   注意事項 ");
                    query.Append(" ORDER BY 注意事項 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 仕入先マスタを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetShiireMst(string kouteiNo)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   仕入先コード ");
                    query.Append("  ,仕入先略名 ");
                    query.Append("  FROM ");
                    query.Append("   仕入先マスタ ");
                    query.Append("WHERE  ");
                    query.Append("   仕入先コード IN (0,1,2,100000) ");
                    query.Append("   OR 仕入先コード IN (  ");
                    query.Append("       SELECT");
                    query.Append("          DISTINCT (加工先コード) ");
                    query.Append("         FROM ");
                    query.Append("          実績工程データ ");
                    query.Append("       WHERE ");
                    query.Append("       工程表番号 = UPPER(:工程表番号)) ");
                    query.Append("   OR 仕入先コード IN (  ");
                    query.Append("       SELECT");
                    query.Append("          DISTINCT (B.加工先) ");
                    query.Append("         FROM ");
                    query.Append("          加工指示書データ A ");
                    query.Append("          LEFT JOIN 加工指示書工程データ B  ");
                    query.Append("          ON A.加工指示書コード = B.加工指示書コード ");
                    query.Append("       WHERE ");
                    query.Append("       A.工程表番号 = UPPER(:工程表番号)) ");
                    query.Append(" ORDER BY 仕入先コード = 100000 desc, 仕入先コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("工程表番号", NpgsqlDbType.Char));
                    cmd.Parameters["工程表番号"].Value = kouteiNo;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// データコピー用のコンボボックスを取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetCopyCmb()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   '図面番号:' || B.図面番号 AS 図面番号 ");
                    query.Append("  ,A.工程表番号 ");
                    query.Append("  FROM 加工指示書データ A ");
                    query.Append("  LEFT JOIN 実績加工データ B ");
                    query.Append("    ON A.工程表番号 = B.工程表番号  ");
                    query.Append(" WHERE ");
                    query.Append("   A.作成者 = :作成者 ");
                    query.Append(" ORDER BY ");
                    query.Append("   A.作成日時 DESC ");
                    query.Append(" LIMIT 20 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("作成者", NpgsqlDbType.Varchar));
                    cmd.Parameters["作成者"].Value = LoginInfo.社員番号;

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書のプライマリーキーを取得します
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        private int GetKakouIndicationCd(NpgsqlConnection con)
        {
            try
            {
                DataTable dt = new DataTable();
                StringBuilder query = new StringBuilder();

                query.Append("SELECT ");
                query.Append("   MAX(加工指示書コード) + 1 AS プライマリーキー   ");
                query.Append("  FROM 加工指示書データ ");

                NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                return int.Parse(dt.Rows[0]["プライマリーキー"].ToString());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 加工指示書データ設定
        /// </summary>
        /// <returns></returns>
        private KakouIndicationInfo SetKakouIndicationInfo()
        {
            try
            {
                KakouIndicationInfo dto = new KakouIndicationInfo();

                if (int.TryParse(this.lblKakouIndicationCd.Text, out int kakouIndicationCd))
                {
                    dto.KakouIndicationCd = kakouIndicationCd;
                }
                else
                {
                    dto.KakouIndicationCd = 0;
                }

                dto.KouteihyouNo = this.txtKouteiNo.Text.ToUpper();
                dto.OdaNo = this.txtOrderNo.Text;
                dto.Author = LoginInfo.社員番号;

                // 現在工程
                DataTable dt = (DataTable)dgv_工程表.DataSource;
                foreach (DataRow dr in dt.Rows)
                {
                    if (string.IsNullOrWhiteSpace(dr["作業者印"].ToString()))
                    {
                        dto.NowWorkKoutei = int.Parse(dr["作業工程順"].ToString());
                        dto.NowKoutei = int.Parse(dr["非表示工程順"].ToString());
                        break;
                    }
                }
                dto.KouteiInfoList = SetKouteiInfoList();

                return dto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程情報設定
        /// </summary>
        /// <returns></returns>
        private List<KouteiInfo> SetKouteiInfoList()
        {
            try
            {
                List<KouteiInfo> list = new List<KouteiInfo>();

                DataTable dt = (DataTable)dgv_工程表.DataSource;
                foreach (DataRow dr in dt.Rows)
                {
                    if (int.TryParse(dr["工程順"].ToString(), out int kouteiNo))
                    {
                        KouteiInfo dto = new KouteiInfo();
                        dto.KouteiNo = kouteiNo;
                        if (int.TryParse(dr["加工先"].ToString(), out int kakouSaki))
                        {
                            dto.KakouSaki = kakouSaki;
                        }
                        else
                        {
                            dto.KakouSaki = 100000;
                        }
                        dto.SagyoInfoList = SetSagyoInfoList(kouteiNo);
                        list.Add(dto);
                    }
                }
                return list;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 作業情報設定
        /// </summary>
        /// <param name="kouteiNo"></param>
        /// <returns></returns>
        private List<SagyoInfo> SetSagyoInfoList(int kouteiNo)
        {
            try
            {
                List<SagyoInfo> list = new List<SagyoInfo>();
                DateTime date;
                DataTable dt = (DataTable)dgv_工程表.DataSource;
                foreach (DataRow dr in dt.Rows)
                {
                    if (int.Parse(dr["非表示工程順"].ToString()) == kouteiNo)
                    {
                        SagyoInfo dto = new SagyoInfo();
                        dto.KouteiNo = kouteiNo;
                        dto.SagyoKouteiNo = int.Parse(dr["作業工程順"].ToString());

                        if (int.TryParse(dr["作業工程"].ToString(), out int sagyoKoutei))
                        {
                            dto.SagyoKoutei = sagyoKoutei;
                        }
                        else
                        {
                            dto.SagyoKoutei = 0;
                        }

                        if (DateTime.TryParse(dr["完了予定日"].ToString(), out date))
                        {
                            dto.CompletionDate = date;
                        }
                        else
                        {
                            dto.CompletionDate = null;
                        }

                        dto.Shipment = dr["出荷"].ToString();
                        dto.Precautions = dr["注意事項"].ToString();

                        if (dr["チェック"].ToString().Equals("✓"))
                        {
                            dto.Check = true;
                        }
                        else
                        {
                            dto.Check = false;
                        }

                        dto.Worker = dr["作業者印"].ToString();

                        if (DateTime.TryParse(dr["完了日"].ToString(), out date))
                        {
                            dto.CompletedDate = date;
                        }
                        else
                        {
                            dto.CompletedDate = null;
                        }
                        list.Add(dto);
                    }
                }
                return list;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 完了日リセット処理
        /// </summary>
        private void CompletionDateReset()
        {
            try
            {
                DataTable dt = (DataTable)dgv_工程表.DataSource;

                foreach (DataRow dr in dt.Rows)
                {
                    dr["完了予定日"] = DBNull.Value;
                    dr["完了日"] = DBNull.Value;
                    dr["チェック"] = string.Empty;
                    dr["作業者印"] = string.Empty;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <returns></returns>
        private bool RegistCheck()
        {
            if (this.txtKouteiNo.Text.Length != 8)
            {
                MessageBox.Show("工程表番号を入力してください。");
                this.txtKouteiNo.Focus();
                return false;
            }

            DataTable dt = new DataTable();

            dt = GetZumenInfo(this.txtKouteiNo.Text);

            if (!this.KouteiNo.Equals(this.txtKouteiNo.Text))
            {
                MessageBox.Show("工程表番号が違います。");
                this.txtKouteiNo.Focus();
                return false;
            }

            if (dt.Rows.Count < 1)
            {
                MessageBox.Show("工程表番号が違います。");
                this.txtKouteiNo.Focus();
                return false;
            }

            dt = (DataTable)dgv_工程表.DataSource;

            bool flg = false;
            // ヘリサート工程チェック
            if (HelisertFlg)
            {
                flg = true;
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["作業工程"].ToString().Equals("132"))
                    {
                        flg = false;
                    }
                }

                if (flg)
                {
                    MessageBox.Show("ヘリサート入工程が抜けています。");
                    return false;
                }
            }

            // ピン入工程チェック
            if (PinFlg)
            {
                flg = true;
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["作業工程"].ToString().Equals("133"))
                    {
                        flg = false;
                    }
                }

                if (flg)
                {
                    MessageBox.Show("ピン入工程が抜けています。");
                    return false;
                }
            }

            if(this.HyoshoFlg)
            {
                flg = true;
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["作業工程"].ToString().Equals("100"))
                    {
                        flg = false;
                    }
                }

                if (flg)
                {
                    MessageBox.Show("表面処理前検査工程が抜けています。");
                    return false;
                }
            }

            // 検査チェック
            DataRow lastRow = dt.Rows[dt.Rows.Count - 1];
            if (!lastRow["作業工程"].ToString().Equals("29"))
            {
                MessageBox.Show("最終工程には検査を設定してください。");
                return false;
            }

            // 表処業者、発、着チェック
            foreach (DataRow dr in dt.Rows)
            {
                if (!(dr["加工先"].ToString().Equals("0")
                    || dr["加工先"].ToString().Equals("")
                    || dr["加工先"].ToString().Equals("1")
                    || dr["加工先"].ToString().Equals("2")
                    || dr["加工先"].ToString().Equals("100000")))
                {
                    if (!string.IsNullOrWhiteSpace(dr["工程順"].ToString()))
                    {
                        if (!PlatingCheak(int.Parse(dr["工程順"].ToString())))
                        {
                            MessageBox.Show("表処業者の出荷日、到着日を設定してください。");
                            return false;
                        }
                    }
                }
            }

            // 表処業者チェック
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["出荷"].ToString().Equals("出荷"))
                {
                    if (dr["加工先"].ToString().Equals("0")
                    || dr["加工先"].ToString().Equals("")
                    || dr["加工先"].ToString().Equals("1")
                    || dr["加工先"].ToString().Equals("2")
                    || dr["加工先"].ToString().Equals("100000"))
                    {
                        MessageBox.Show("表処業者を設定してください。");
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 社員マスタ取得
        /// </summary>
        /// <returns></returns>
        private DataTable Get社員マスタ()
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.AppendLine("SELECT ");
                    query.AppendLine("   社員番号 ");
                    query.AppendLine("  ,氏名 ");
                    query.AppendLine("  FROM 社員マスタ ");
                    query.AppendLine(" WHERE ");
                    query.AppendLine("  退職フラグ <> true ");
                    query.AppendLine("  AND 部署 IN('管理部','品質管理部','加工技術部') ");
                    query.AppendLine(" ORDER BY ");
                    query.AppendLine("   社員番号 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 表処業者、発、着チェック
        /// </summary>
        /// <param name="koutei"></param>
        /// <returns></returns>
        private bool PlatingCheak(int koutei)
        {
            bool flg1 = false;
            bool flg2 = false;

            DataTable dt = (DataTable)dgv_工程表.DataSource;
            foreach (DataRow dr in dt.Rows)
            {
                if (koutei == int.Parse(dr["非表示工程順"].ToString()))
                {
                    if (dr["出荷"].ToString().Equals("出荷"))
                    {
                        flg1 = true;
                    }
                    else if (dr["出荷"].ToString().Equals("着"))
                    {
                        flg2 = true;
                    }
                }
            }

            if (flg1 && flg2)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <returns></returns>
        private bool InputCheck()
        {
            if (this.txtKouteiNo.Text.Length != 8)
            {
                MessageBox.Show("工程表番号を入力してください。");
                this.txtKouteiNo.Focus();
                return false;
            }

            return true;
        }

        #endregion

        /// <summary>
        /// Excelボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ClickBtnExcel(object sender, EventArgs e)
        {
            try
            {
                // 入力チェック
                if (!InputCheck())
                {
                    return;
                };

                //メッセージボックスを表示する
                DialogResult result = MessageBox.Show("Excel出力を行います。よろしいですか？",
                    "質問",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2);

                if (!(result == DialogResult.Yes))
                {
                    return;
                }

                // プログレスバー表示
                ProgressBar pb = new ProgressBar
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                pb.Show();

                string kouteiNo = this.txtKouteiNo.Text;

                DataTable dt = GetKakouIndication(kouteiNo);

                // 処理が重いため非同期で実行
                await Async(dt);
                pb.Close();

                MessageBox.Show("Excel出力が完了しました。");
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 非同期処理
        /// </summary>
        /// <returns></returns>
        private async Task<bool> Async(DataTable dt)
        {
            try
            {
                return await Task.Run(() => OutputExcel(dt));
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Excel出力を行います
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private bool OutputExcel(DataTable dt)
        {
            try
            {
                // Excel出力を行います
                Excel excel = new Excel();
                List<string> list = GetHedders();
                excel.OutputExcel(dt, "加工指示書", list, this.txtZumenName.Text);

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Excel出力用のヘッダーを設定します
        /// </summary>
        /// <returns></returns>

        private List<string> GetHedders()
        {
            try
            {
                List<string> list = new List<string>
                {
                    "工程順",
                    "加工先",
                    "工程名",
                    "完了予定日        ",
                    "出荷",
                    "注意事項",
                    "チェック",
                    "作業者印",
                    "完了日"
                };
                return list;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程表番号ダブルクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoubleClickTxtKouteiNo(object sender, EventArgs e)
        {
            this.txtKouteiNo.Text = string.Empty;
        }

        /// <summary>
        /// 作業者ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnWorker(object sender, EventArgs e)
        {
            List<KeyValuePairDto> List = new List<KeyValuePairDto>();
            DataTable dt = Get社員マスタ();

            foreach (DataRow dr in dt.Rows)
            {
                KeyValuePairDto dto = new KeyValuePairDto
                {
                    Key = int.Parse(dr["社員番号"].ToString()),
                    Value = dr["氏名"].ToString()
                };
                List.Add(dto);
            }
            SelectListBox listBox = new SelectListBox
            {
                listBox = List,
                Text = "作業者選択"
            };
            listBox.StartPosition = FormStartPosition.CenterParent;
            listBox.ShowDialog();

            if (!string.IsNullOrWhiteSpace(listBox.SelectValue))
            {
                this.txtWorker.Text = listBox.SelectValue;
                this.lbl作業者.Text = listBox.SelectKey;
            }
        }

        /// <summary>
        /// 注意事項一覧ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnAttention(object sender, EventArgs e)
        {
            Attention form = new Attention
            {
                StartPosition = FormStartPosition.CenterParent
            };
            form.ShowDialog();
        }

        /// <summary>
        /// 画面クロージング処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormClosingKakouIndication(object sender, FormClosingEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCreator.Text))
            {
                //メッセージボックスを表示する
                DialogResult result = MessageBox.Show("指示書が作成されていません。画面を閉じますか？",
                    "質問",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2);

                if (result == DialogResult.Cancel)
                {
                    //「キャンセル」が選択された時
                    e.Cancel = true;
                }
            }
        }
    }
}
