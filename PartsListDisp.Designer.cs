﻿namespace ZumenConsoleApp.View
{
    partial class PartsListDisp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtOrderNo1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtModel1 = new System.Windows.Forms.TextBox();
            this.totalCoverList = new System.Windows.Forms.DataGridView();
            this.ブロック番号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.略号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ブロック名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ブロック数台 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.編成区分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.加工品点数 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.購入品点数 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtOrderNo2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtModel2 = new System.Windows.Forms.TextBox();
            this.kumiZumenList = new System.Windows.Forms.DataGridView();
            this.ブロック番号2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.略号2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ブロック名2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.図番 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.図面名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.備考 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtOrderNo3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtModel3 = new System.Windows.Forms.TextBox();
            this.kakoList = new System.Windows.Forms.DataGridView();
            this.ブロック番号3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.略号3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ブロック名3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.図番2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.品名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.材質 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.表処 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.個数 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.員数 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.総数 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.単位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.備考2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.型番 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.メーカー名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtOrderNo4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtModel4 = new System.Windows.Forms.TextBox();
            this.kounyuList = new System.Windows.Forms.DataGridView();
            this.ブロック番号4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.略号4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ブロック名4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.型番2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.品名2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.メーカ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.個数2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.員数2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.総数2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.単位2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.パーツリスト変更通知 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.totalCoverList)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kumiZumenList)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kakoList)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kounyuList)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1443, 611);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtOrderNo1);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtModel1);
            this.tabPage1.Controls.Add(this.totalCoverList);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1435, 573);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "総表紙";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtOrderNo1
            // 
            this.txtOrderNo1.BackColor = System.Drawing.Color.Aquamarine;
            this.txtOrderNo1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo1.ForeColor = System.Drawing.Color.Gray;
            this.txtOrderNo1.Location = new System.Drawing.Point(132, 4);
            this.txtOrderNo1.Name = "txtOrderNo1";
            this.txtOrderNo1.ReadOnly = true;
            this.txtOrderNo1.Size = new System.Drawing.Size(115, 31);
            this.txtOrderNo1.TabIndex = 33;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(12, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 24);
            this.label4.TabIndex = 32;
            this.label4.Text = "オーダーNO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(248, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 24);
            this.label3.TabIndex = 30;
            this.label3.Text = "機種名";
            // 
            // txtModel1
            // 
            this.txtModel1.BackColor = System.Drawing.Color.Aquamarine;
            this.txtModel1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtModel1.ForeColor = System.Drawing.Color.Gray;
            this.txtModel1.Location = new System.Drawing.Point(336, 4);
            this.txtModel1.Name = "txtModel1";
            this.txtModel1.ReadOnly = true;
            this.txtModel1.Size = new System.Drawing.Size(500, 31);
            this.txtModel1.TabIndex = 31;
            // 
            // totalCoverList
            // 
            this.totalCoverList.AllowUserToAddRows = false;
            this.totalCoverList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.totalCoverList.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.totalCoverList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.totalCoverList.ColumnHeadersHeight = 50;
            this.totalCoverList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.totalCoverList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ブロック番号,
            this.略号,
            this.ブロック名,
            this.ブロック数台,
            this.編成区分,
            this.加工品点数,
            this.購入品点数});
            this.totalCoverList.EnableHeadersVisualStyles = false;
            this.totalCoverList.Location = new System.Drawing.Point(12, 44);
            this.totalCoverList.MultiSelect = false;
            this.totalCoverList.Name = "totalCoverList";
            this.totalCoverList.ReadOnly = true;
            this.totalCoverList.RowHeadersVisible = false;
            this.totalCoverList.RowTemplate.Height = 50;
            this.totalCoverList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.totalCoverList.Size = new System.Drawing.Size(1415, 492);
            this.totalCoverList.TabIndex = 27;
            // 
            // ブロック番号
            // 
            this.ブロック番号.DataPropertyName = "ブロック番号";
            this.ブロック番号.FillWeight = 195.8086F;
            this.ブロック番号.HeaderText = "ブロック番号";
            this.ブロック番号.Name = "ブロック番号";
            this.ブロック番号.ReadOnly = true;
            this.ブロック番号.Width = 250;
            // 
            // 略号
            // 
            this.略号.DataPropertyName = "略号";
            this.略号.FillWeight = 51.70336F;
            this.略号.HeaderText = "略号";
            this.略号.Name = "略号";
            this.略号.ReadOnly = true;
            this.略号.Width = 150;
            // 
            // ブロック名
            // 
            this.ブロック名.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ブロック名.DataPropertyName = "ブロック名";
            this.ブロック名.FillWeight = 51.70336F;
            this.ブロック名.HeaderText = "ブロック名";
            this.ブロック名.Name = "ブロック名";
            this.ブロック名.ReadOnly = true;
            // 
            // ブロック数台
            // 
            this.ブロック数台.DataPropertyName = "ブロック数／台";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ブロック数台.DefaultCellStyle = dataGridViewCellStyle2;
            this.ブロック数台.FillWeight = 151.0152F;
            this.ブロック数台.HeaderText = "ブロック数／台";
            this.ブロック数台.Name = "ブロック数台";
            this.ブロック数台.ReadOnly = true;
            this.ブロック数台.Width = 170;
            // 
            // 編成区分
            // 
            this.編成区分.DataPropertyName = "編成区分";
            this.編成区分.FillWeight = 41.36268F;
            this.編成区分.HeaderText = "編成区分";
            this.編成区分.Name = "編成区分";
            this.編成区分.ReadOnly = true;
            this.編成区分.Width = 150;
            // 
            // 加工品点数
            // 
            this.加工品点数.DataPropertyName = "加工品点数";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.加工品点数.DefaultCellStyle = dataGridViewCellStyle3;
            this.加工品点数.FillWeight = 51.70336F;
            this.加工品点数.HeaderText = "加工品点数";
            this.加工品点数.Name = "加工品点数";
            this.加工品点数.ReadOnly = true;
            this.加工品点数.Width = 150;
            // 
            // 購入品点数
            // 
            this.購入品点数.DataPropertyName = "購入品点数";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.購入品点数.DefaultCellStyle = dataGridViewCellStyle4;
            this.購入品点数.FillWeight = 51.70336F;
            this.購入品点数.HeaderText = "購入品点数";
            this.購入品点数.Name = "購入品点数";
            this.購入品点数.ReadOnly = true;
            this.購入品点数.Width = 150;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtOrderNo2);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.txtModel2);
            this.tabPage2.Controls.Add(this.kumiZumenList);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1435, 597);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "組立図";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtOrderNo2
            // 
            this.txtOrderNo2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtOrderNo2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo2.ForeColor = System.Drawing.Color.Gray;
            this.txtOrderNo2.Location = new System.Drawing.Point(132, 4);
            this.txtOrderNo2.Name = "txtOrderNo2";
            this.txtOrderNo2.ReadOnly = true;
            this.txtOrderNo2.Size = new System.Drawing.Size(115, 31);
            this.txtOrderNo2.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(12, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 24);
            this.label5.TabIndex = 33;
            this.label5.Text = "オーダーNO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(248, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 24);
            this.label2.TabIndex = 30;
            this.label2.Text = "機種名";
            // 
            // txtModel2
            // 
            this.txtModel2.BackColor = System.Drawing.Color.Aquamarine;
            this.txtModel2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtModel2.ForeColor = System.Drawing.Color.Gray;
            this.txtModel2.Location = new System.Drawing.Point(336, 4);
            this.txtModel2.Name = "txtModel2";
            this.txtModel2.ReadOnly = true;
            this.txtModel2.Size = new System.Drawing.Size(500, 31);
            this.txtModel2.TabIndex = 31;
            // 
            // kumiZumenList
            // 
            this.kumiZumenList.AllowUserToAddRows = false;
            this.kumiZumenList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kumiZumenList.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.kumiZumenList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.kumiZumenList.ColumnHeadersHeight = 50;
            this.kumiZumenList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.kumiZumenList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ブロック番号2,
            this.略号2,
            this.ブロック名2,
            this.図番,
            this.図面名称,
            this.備考});
            this.kumiZumenList.EnableHeadersVisualStyles = false;
            this.kumiZumenList.Location = new System.Drawing.Point(12, 44);
            this.kumiZumenList.MultiSelect = false;
            this.kumiZumenList.Name = "kumiZumenList";
            this.kumiZumenList.ReadOnly = true;
            this.kumiZumenList.RowHeadersVisible = false;
            this.kumiZumenList.RowTemplate.Height = 50;
            this.kumiZumenList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.kumiZumenList.Size = new System.Drawing.Size(1415, 516);
            this.kumiZumenList.TabIndex = 28;
            // 
            // ブロック番号2
            // 
            this.ブロック番号2.DataPropertyName = "ブロック番号";
            this.ブロック番号2.FillWeight = 75F;
            this.ブロック番号2.HeaderText = "ブロック番号";
            this.ブロック番号2.Name = "ブロック番号2";
            this.ブロック番号2.ReadOnly = true;
            this.ブロック番号2.Width = 250;
            // 
            // 略号2
            // 
            this.略号2.DataPropertyName = "略号";
            this.略号2.HeaderText = "略号";
            this.略号2.Name = "略号2";
            this.略号2.ReadOnly = true;
            this.略号2.Width = 150;
            // 
            // ブロック名2
            // 
            this.ブロック名2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ブロック名2.DataPropertyName = "ブロック名";
            this.ブロック名2.HeaderText = "ブロック名";
            this.ブロック名2.Name = "ブロック名2";
            this.ブロック名2.ReadOnly = true;
            // 
            // 図番
            // 
            this.図番.DataPropertyName = "図番";
            this.図番.FillWeight = 40F;
            this.図番.HeaderText = "図番";
            this.図番.Name = "図番";
            this.図番.ReadOnly = true;
            this.図番.Width = 250;
            // 
            // 図面名称
            // 
            this.図面名称.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.図面名称.DataPropertyName = "図面名称";
            this.図面名称.FillWeight = 80F;
            this.図面名称.HeaderText = "図面名称";
            this.図面名称.Name = "図面名称";
            this.図面名称.ReadOnly = true;
            // 
            // 備考
            // 
            this.備考.DataPropertyName = "備考";
            this.備考.HeaderText = "備考";
            this.備考.Name = "備考";
            this.備考.ReadOnly = true;
            this.備考.Width = 220;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtOrderNo3);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.txtModel3);
            this.tabPage3.Controls.Add(this.kakoList);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1435, 597);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "加工品";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtOrderNo3
            // 
            this.txtOrderNo3.BackColor = System.Drawing.Color.Aquamarine;
            this.txtOrderNo3.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo3.ForeColor = System.Drawing.Color.Gray;
            this.txtOrderNo3.Location = new System.Drawing.Point(132, 4);
            this.txtOrderNo3.Name = "txtOrderNo3";
            this.txtOrderNo3.ReadOnly = true;
            this.txtOrderNo3.Size = new System.Drawing.Size(115, 31);
            this.txtOrderNo3.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(12, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 24);
            this.label6.TabIndex = 33;
            this.label6.Text = "オーダーNO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(248, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 24);
            this.label1.TabIndex = 30;
            this.label1.Text = "機種名";
            // 
            // txtModel3
            // 
            this.txtModel3.BackColor = System.Drawing.Color.Aquamarine;
            this.txtModel3.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtModel3.ForeColor = System.Drawing.Color.Gray;
            this.txtModel3.Location = new System.Drawing.Point(336, 4);
            this.txtModel3.Name = "txtModel3";
            this.txtModel3.ReadOnly = true;
            this.txtModel3.Size = new System.Drawing.Size(500, 31);
            this.txtModel3.TabIndex = 31;
            // 
            // kakoList
            // 
            this.kakoList.AllowUserToAddRows = false;
            this.kakoList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kakoList.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.kakoList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.kakoList.ColumnHeadersHeight = 50;
            this.kakoList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.kakoList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ブロック番号3,
            this.略号3,
            this.ブロック名3,
            this.図番2,
            this.品名,
            this.材質,
            this.表処,
            this.個数,
            this.員数,
            this.総数,
            this.単位,
            this.備考2,
            this.型番,
            this.メーカー名});
            this.kakoList.EnableHeadersVisualStyles = false;
            this.kakoList.Location = new System.Drawing.Point(12, 44);
            this.kakoList.MultiSelect = false;
            this.kakoList.Name = "kakoList";
            this.kakoList.ReadOnly = true;
            this.kakoList.RowHeadersVisible = false;
            this.kakoList.RowTemplate.Height = 50;
            this.kakoList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.kakoList.Size = new System.Drawing.Size(1415, 516);
            this.kakoList.TabIndex = 29;
            // 
            // ブロック番号3
            // 
            this.ブロック番号3.DataPropertyName = "ブロック番号";
            this.ブロック番号3.FillWeight = 75F;
            this.ブロック番号3.HeaderText = "ブロック番号";
            this.ブロック番号3.Name = "ブロック番号3";
            this.ブロック番号3.ReadOnly = true;
            this.ブロック番号3.Width = 250;
            // 
            // 略号3
            // 
            this.略号3.DataPropertyName = "略号";
            this.略号3.HeaderText = "略号";
            this.略号3.Name = "略号3";
            this.略号3.ReadOnly = true;
            this.略号3.Width = 150;
            // 
            // ブロック名3
            // 
            this.ブロック名3.DataPropertyName = "ブロック名";
            this.ブロック名3.HeaderText = "ブロック名";
            this.ブロック名3.Name = "ブロック名3";
            this.ブロック名3.ReadOnly = true;
            this.ブロック名3.Width = 300;
            // 
            // 図番2
            // 
            this.図番2.DataPropertyName = "図番";
            this.図番2.FillWeight = 40F;
            this.図番2.HeaderText = "図番";
            this.図番2.Name = "図番2";
            this.図番2.ReadOnly = true;
            this.図番2.Width = 250;
            // 
            // 品名
            // 
            this.品名.DataPropertyName = "品名";
            this.品名.FillWeight = 80F;
            this.品名.HeaderText = "品名";
            this.品名.Name = "品名";
            this.品名.ReadOnly = true;
            this.品名.Width = 300;
            // 
            // 材質
            // 
            this.材質.DataPropertyName = "材質";
            this.材質.HeaderText = "材質";
            this.材質.Name = "材質";
            this.材質.ReadOnly = true;
            this.材質.Width = 200;
            // 
            // 表処
            // 
            this.表処.DataPropertyName = "表処";
            this.表処.HeaderText = "表処";
            this.表処.Name = "表処";
            this.表処.ReadOnly = true;
            this.表処.Width = 170;
            // 
            // 個数
            // 
            this.個数.DataPropertyName = "個数";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.個数.DefaultCellStyle = dataGridViewCellStyle7;
            this.個数.HeaderText = "個数";
            this.個数.Name = "個数";
            this.個数.ReadOnly = true;
            this.個数.Width = 70;
            // 
            // 員数
            // 
            this.員数.DataPropertyName = "員数";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.員数.DefaultCellStyle = dataGridViewCellStyle8;
            this.員数.HeaderText = "員数";
            this.員数.Name = "員数";
            this.員数.ReadOnly = true;
            this.員数.Width = 70;
            // 
            // 総数
            // 
            this.総数.DataPropertyName = "総数";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.総数.DefaultCellStyle = dataGridViewCellStyle9;
            this.総数.HeaderText = "総数";
            this.総数.Name = "総数";
            this.総数.ReadOnly = true;
            this.総数.Width = 70;
            // 
            // 単位
            // 
            this.単位.DataPropertyName = "単位";
            this.単位.HeaderText = "単位";
            this.単位.Name = "単位";
            this.単位.ReadOnly = true;
            this.単位.Width = 70;
            // 
            // 備考2
            // 
            this.備考2.DataPropertyName = "備考";
            this.備考2.HeaderText = "備考";
            this.備考2.Name = "備考2";
            this.備考2.ReadOnly = true;
            this.備考2.Width = 300;
            // 
            // 型番
            // 
            this.型番.DataPropertyName = "型番";
            this.型番.HeaderText = "型番";
            this.型番.Name = "型番";
            this.型番.ReadOnly = true;
            this.型番.Width = 300;
            // 
            // メーカー名
            // 
            this.メーカー名.DataPropertyName = "メーカー名";
            this.メーカー名.HeaderText = "メーカー名";
            this.メーカー名.Name = "メーカー名";
            this.メーカー名.ReadOnly = true;
            this.メーカー名.Width = 300;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.txtOrderNo4);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.txtModel4);
            this.tabPage4.Controls.Add(this.kounyuList);
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1435, 597);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "購入品";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtOrderNo4
            // 
            this.txtOrderNo4.BackColor = System.Drawing.Color.Aquamarine;
            this.txtOrderNo4.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo4.ForeColor = System.Drawing.Color.Gray;
            this.txtOrderNo4.Location = new System.Drawing.Point(132, 4);
            this.txtOrderNo4.Name = "txtOrderNo4";
            this.txtOrderNo4.ReadOnly = true;
            this.txtOrderNo4.Size = new System.Drawing.Size(115, 31);
            this.txtOrderNo4.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(12, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 24);
            this.label8.TabIndex = 33;
            this.label8.Text = "オーダーNO";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(248, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 24);
            this.label7.TabIndex = 10;
            this.label7.Text = "機種名";
            // 
            // txtModel4
            // 
            this.txtModel4.BackColor = System.Drawing.Color.Aquamarine;
            this.txtModel4.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtModel4.ForeColor = System.Drawing.Color.Gray;
            this.txtModel4.Location = new System.Drawing.Point(336, 4);
            this.txtModel4.Name = "txtModel4";
            this.txtModel4.ReadOnly = true;
            this.txtModel4.Size = new System.Drawing.Size(500, 31);
            this.txtModel4.TabIndex = 29;
            // 
            // kounyuList
            // 
            this.kounyuList.AllowUserToAddRows = false;
            this.kounyuList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kounyuList.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.kounyuList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.kounyuList.ColumnHeadersHeight = 50;
            this.kounyuList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.kounyuList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ブロック番号4,
            this.略号4,
            this.ブロック名4,
            this.型番2,
            this.品名2,
            this.メーカ,
            this.個数2,
            this.員数2,
            this.総数2,
            this.単位2,
            this.パーツリスト変更通知});
            this.kounyuList.EnableHeadersVisualStyles = false;
            this.kounyuList.Location = new System.Drawing.Point(12, 44);
            this.kounyuList.MultiSelect = false;
            this.kounyuList.Name = "kounyuList";
            this.kounyuList.ReadOnly = true;
            this.kounyuList.RowHeadersVisible = false;
            this.kounyuList.RowTemplate.Height = 50;
            this.kounyuList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.kounyuList.Size = new System.Drawing.Size(1415, 516);
            this.kounyuList.TabIndex = 30;
            // 
            // ブロック番号4
            // 
            this.ブロック番号4.DataPropertyName = "ブロック番号";
            this.ブロック番号4.FillWeight = 75F;
            this.ブロック番号4.HeaderText = "ブロック番号";
            this.ブロック番号4.Name = "ブロック番号4";
            this.ブロック番号4.ReadOnly = true;
            this.ブロック番号4.Width = 250;
            // 
            // 略号4
            // 
            this.略号4.DataPropertyName = "略号";
            this.略号4.HeaderText = "略号";
            this.略号4.Name = "略号4";
            this.略号4.ReadOnly = true;
            this.略号4.Width = 150;
            // 
            // ブロック名4
            // 
            this.ブロック名4.DataPropertyName = "ブロック名";
            this.ブロック名4.HeaderText = "ブロック名";
            this.ブロック名4.Name = "ブロック名4";
            this.ブロック名4.ReadOnly = true;
            this.ブロック名4.Width = 300;
            // 
            // 型番2
            // 
            this.型番2.DataPropertyName = "型番";
            this.型番2.FillWeight = 40F;
            this.型番2.HeaderText = "型番";
            this.型番2.Name = "型番2";
            this.型番2.ReadOnly = true;
            this.型番2.Width = 300;
            // 
            // 品名2
            // 
            this.品名2.DataPropertyName = "品名";
            this.品名2.FillWeight = 80F;
            this.品名2.HeaderText = "品名";
            this.品名2.Name = "品名2";
            this.品名2.ReadOnly = true;
            this.品名2.Width = 300;
            // 
            // メーカ
            // 
            this.メーカ.DataPropertyName = "メーカ";
            this.メーカ.HeaderText = "メーカ";
            this.メーカ.Name = "メーカ";
            this.メーカ.ReadOnly = true;
            this.メーカ.Width = 300;
            // 
            // 個数2
            // 
            this.個数2.DataPropertyName = "個数";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.個数2.DefaultCellStyle = dataGridViewCellStyle11;
            this.個数2.HeaderText = "個数";
            this.個数2.Name = "個数2";
            this.個数2.ReadOnly = true;
            this.個数2.Width = 70;
            // 
            // 員数2
            // 
            this.員数2.DataPropertyName = "員数";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.員数2.DefaultCellStyle = dataGridViewCellStyle12;
            this.員数2.HeaderText = "員数";
            this.員数2.Name = "員数2";
            this.員数2.ReadOnly = true;
            this.員数2.Width = 70;
            // 
            // 総数2
            // 
            this.総数2.DataPropertyName = "総数";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.総数2.DefaultCellStyle = dataGridViewCellStyle13;
            this.総数2.HeaderText = "総数";
            this.総数2.Name = "総数2";
            this.総数2.ReadOnly = true;
            this.総数2.Width = 70;
            // 
            // 単位2
            // 
            this.単位2.DataPropertyName = "単位";
            this.単位2.HeaderText = "単位";
            this.単位2.Name = "単位2";
            this.単位2.ReadOnly = true;
            this.単位2.Width = 70;
            // 
            // パーツリスト変更通知
            // 
            this.パーツリスト変更通知.DataPropertyName = "パーツリスト変更通知";
            this.パーツリスト変更通知.HeaderText = "パーツリスト変更通知";
            this.パーツリスト変更通知.Name = "パーツリスト変更通知";
            this.パーツリスト変更通知.ReadOnly = true;
            this.パーツリスト変更通知.Width = 300;
            // 
            // PartsListDisp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1443, 635);
            this.Controls.Add(this.tabControl1);
            this.KeyPreview = true;
            this.Name = "PartsListDisp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "パーツリスト";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PartsListDisp_Load);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.totalCoverList)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kumiZumenList)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kakoList)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kounyuList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView totalCoverList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtModel4;
        private System.Windows.Forms.DataGridView kumiZumenList;
        private System.Windows.Forms.DataGridView kakoList;
        private System.Windows.Forms.DataGridView kounyuList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtModel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtModel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtModel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ブロック番号2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 略号2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ブロック名2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 図番;
        private System.Windows.Forms.DataGridViewTextBoxColumn 図面名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 備考;
        private System.Windows.Forms.DataGridViewTextBoxColumn ブロック番号3;
        private System.Windows.Forms.DataGridViewTextBoxColumn 略号3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ブロック名3;
        private System.Windows.Forms.DataGridViewTextBoxColumn 図番2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 品名;
        private System.Windows.Forms.DataGridViewTextBoxColumn 材質;
        private System.Windows.Forms.DataGridViewTextBoxColumn 表処;
        private System.Windows.Forms.DataGridViewTextBoxColumn 個数;
        private System.Windows.Forms.DataGridViewTextBoxColumn 員数;
        private System.Windows.Forms.DataGridViewTextBoxColumn 総数;
        private System.Windows.Forms.DataGridViewTextBoxColumn 単位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 備考2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 型番;
        private System.Windows.Forms.DataGridViewTextBoxColumn メーカー名;
        private System.Windows.Forms.DataGridViewTextBoxColumn ブロック番号4;
        private System.Windows.Forms.DataGridViewTextBoxColumn 略号4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ブロック名4;
        private System.Windows.Forms.DataGridViewTextBoxColumn 型番2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 品名2;
        private System.Windows.Forms.DataGridViewTextBoxColumn メーカ;
        private System.Windows.Forms.DataGridViewTextBoxColumn 個数2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 員数2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 総数2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 単位2;
        private System.Windows.Forms.DataGridViewTextBoxColumn パーツリスト変更通知;
        private System.Windows.Forms.DataGridViewTextBoxColumn ブロック番号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 略号;
        private System.Windows.Forms.DataGridViewTextBoxColumn ブロック名;
        private System.Windows.Forms.DataGridViewTextBoxColumn ブロック数台;
        private System.Windows.Forms.DataGridViewTextBoxColumn 編成区分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 加工品点数;
        private System.Windows.Forms.DataGridViewTextBoxColumn 購入品点数;
        private System.Windows.Forms.TextBox txtOrderNo1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtOrderNo2;
        private System.Windows.Forms.TextBox txtOrderNo3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtOrderNo4;
        private System.Windows.Forms.Label label8;
    }
}