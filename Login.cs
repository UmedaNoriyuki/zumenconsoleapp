﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class Login : Form
    {
        #region コンストラクタ

        public Login()
        {
            InitializeComponent();
        }
        
        #endregion

        #region イベント
        
        /// <summary>
        /// 閉じるボタンクリックイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnClose(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// ログインボタンクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnLogin(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = GetUserInfo(this.txt_User.Text, this.txt_Password.Text);
                if (dt.Rows.Count == 0)
                {
                    this.txt_Info.Text = "【ログイン失敗】社員番号/パスワードに誤りがあります。";
                    return;
                }
                SetUserInfo(dt);

                this.DialogResult = DialogResult.OK;
                //次回表示内容を記憶
                Properties.Settings.Default.LoginUser = this.txt_User.Text;
                Properties.Settings.Default.LoginPass = this.txt_Password.Text;
                Properties.Settings.Default.Save();

                MainDisp form = new MainDisp();
                this.Visible = false;
                form.ShowDialog();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
            }
        }

        /// <summary>
        /// ユーザー情報を保持します
        /// </summary>
        /// <param name="dt"></param>
        private void SetUserInfo(DataTable dt)
        {
            this.txt_Info.Text = "";
            LoginInfo.社員番号 = dt.Rows[0]["社員番号"].ToString();
            LoginInfo.氏名 = dt.Rows[0]["氏名"].ToString();
            LoginInfo.部署 = dt.Rows[0]["部署"].ToString();
            LoginInfo.所属 = dt.Rows[0]["所属"].ToString();
            LoginInfo.メールアドレス = dt.Rows[0]["メールアドレス"].ToString();

            // 管理者
            if (Boolean.TryParse(dt.Rows[0]["管理者"].ToString(), out bool 管理者))
            {
                LoginInfo.管理者 = 管理者;
            }
            else
            {
                LoginInfo.管理者 = false;
            }

            // システム管理者
            if (Boolean.TryParse(dt.Rows[0]["システム管理者"].ToString(), out bool システム管理者))
            {
                LoginInfo.システム管理者 = 管理者;
            }
            else
            {
                LoginInfo.システム管理者 = false;
            }

            // 受入検査
            if (Boolean.TryParse(dt.Rows[0]["受入検査"].ToString(), out bool 受入検査))
            {
                LoginInfo.受入検査 = 受入検査;
            }
            else
            {
                LoginInfo.受入検査 = false;
            }

            // 作業者コード
            if (int.TryParse(dt.Rows[0]["作業者コード"].ToString(), out int 作業者コード))
            {
                LoginInfo.作業者コード = 作業者コード;
            }
            else
            {
                LoginInfo.作業者コード = 0;
            }

            LoginInfo.役職 = dt.Rows[0]["役職"].ToString();

            // cadcam
            if (Boolean.TryParse(dt.Rows[0]["cadcam"].ToString(), out bool cadcam))
            {
                LoginInfo.cadcam = cadcam;
            }
            else
            {
                LoginInfo.cadcam = false;
            }
        }

        /// <summary>
        /// Loadイベント
        /// 前回ログイン情報を表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadLogin(object sender, EventArgs e)
        {
            string[] cmds = Environment.GetCommandLineArgs();

            if (cmds.Length > 1)
            {
                if (cmds[1] != string.Empty)
                {
                    DataTable dt = GetUserInfo(cmds[2], cmds[3]);
                    SetUserInfo(dt);
                    KakouIndication form = new KakouIndication();
                    form.StartPosition = FormStartPosition.CenterScreen;
                    form.KouteiNo = cmds[1];
                    this.Visible = false;
                    form.ShowDialog();
                    this.Close();
                }
            }

            //設定情報より前回記録内容を初期表示
            this.txt_User.Text = Properties.Settings.Default.LoginUser;
            this.txt_Password.Text = Properties.Settings.Default.LoginPass;
        }

        #endregion

        #region privateメソッド
        
        /// <summary>
        /// ユーザ情報取得
        /// </summary>
        /// <param name="社員番号"></param>
        /// <param name="パスワード"></param>
        /// <returns></returns>
        private DataTable GetUserInfo(string 社員番号, string パスワード)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                // トランザクション開始
                using (var tran = con.BeginTransaction())
                {
                    try
                    {
                        DataTable dt = new DataTable();
                        StringBuilder query = new StringBuilder();
                        query.Append("SELECT \"社員番号\", \"氏名\", \"部署\", \"所属\", \"メールアドレス\", \"管理者\", \"システム管理者\", \"受入検査\", \"作業者コード\", \"役職\", \"cadcam\" ");
                        query.Append(" FROM \"社員マスタ\" ");
                        query.Append(" WHERE \"社員番号\"   = :社員番号 ");
                        query.Append("   AND \"パスワード\" = :パスワード ");

                        NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                        cmd.Parameters.Add(new NpgsqlParameter("社員番号", NpgsqlDbType.Varchar));
                        cmd.Parameters["社員番号"].Value = 社員番号;

                        cmd.Parameters.Add(new NpgsqlParameter("パスワード", NpgsqlDbType.Varchar));
                        cmd.Parameters["パスワード"].Value = パスワード;

                        var dataReader = cmd.ExecuteReader();
                        dt.Load(dataReader);

                        // 接続を閉じる
                        con.Close();
                        con.Dispose();

                        return dt;
                    }
                    catch (Exception e)
                    {
                        // ロールバック
                        tran.Rollback();
                        throw e;
                    }
                }
            }
        }
        
        #endregion
    
    }
}
