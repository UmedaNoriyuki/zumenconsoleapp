﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ZumenConsoleApp
{
    public partial class ProgressBar : Form
    {
        public ProgressBar()
        {
            InitializeComponent();
        }

        private Bitmap animatedImage;

        #region イベント

        //フォームのLoadイベントハンドラ
        private void OnLoadImages(object sender, EventArgs e)
        {
            try
            {
                //GIFアニメ画像の読み込み
                string appPath = System.Windows.Forms.Application.StartupPath;
                var image = appPath + "\\Resources\\Image.gif";
                //var image = System.IO.Path.GetFullPath(".\\Image\\Image.gif");
                animatedImage = new Bitmap(image);
                //フォームのPaintイベントハンドラを追加
                this.Paint += new PaintEventHandler(this.OnPaintForm);
                //アニメ開始
                ImageAnimator.Animate(animatedImage, new EventHandler(this.FrameChangedImage));
                this.label1.Text = string.Empty;
            }
            catch(Exception)
            {
                // gifアニメーションが表示できない場合ダイアログのみ表示
            }
        }

        /// <summary>
        /// イベントハンドラを呼び出し
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        private void FrameChangedImage(object o, EventArgs e)
        {
            try
            {
                //Paintイベントハンドラを呼び出す
                this.Invalidate();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// フォームのPaintイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPaintForm(object sender, PaintEventArgs e)
        {
            try
            {
                //フレームを進める
                ImageAnimator.UpdateFrames(animatedImage);
                //画像の表示
                e.Graphics.DrawImage(animatedImage, 32, 20);
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
