﻿namespace ZumenConsoleApp.View
{
    partial class Calendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmb_year = new System.Windows.Forms.ComboBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.cmb_month = new System.Windows.Forms.ComboBox();
            this.label137 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.txtNextMonth = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmb_year
            // 
            this.cmb_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_year.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmb_year.FormattingEnabled = true;
            this.cmb_year.Location = new System.Drawing.Point(73, 8);
            this.cmb_year.Name = "cmb_year";
            this.cmb_year.Size = new System.Drawing.Size(145, 44);
            this.cmb_year.TabIndex = 1;
            this.cmb_year.TextChanged += new System.EventHandler(this.TextChangedCmb_year);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label100.Location = new System.Drawing.Point(22, 12);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(39, 36);
            this.label100.TabIndex = 16;
            this.label100.Text = "年";
            // 
            // label91
            // 
            this.label91.BackColor = System.Drawing.Color.LightCoral;
            this.label91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label91.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label91.Location = new System.Drawing.Point(24, 68);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(120, 60);
            this.label91.TabIndex = 17;
            this.label91.Text = "日";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label92
            // 
            this.label92.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label92.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label92.Location = new System.Drawing.Point(144, 68);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(120, 60);
            this.label92.TabIndex = 18;
            this.label92.Text = "月";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label93
            // 
            this.label93.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label93.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label93.Location = new System.Drawing.Point(264, 68);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(120, 60);
            this.label93.TabIndex = 19;
            this.label93.Text = "火";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label94
            // 
            this.label94.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label94.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label94.Location = new System.Drawing.Point(384, 68);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(120, 60);
            this.label94.TabIndex = 20;
            this.label94.Text = "水";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label95
            // 
            this.label95.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label95.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label95.Location = new System.Drawing.Point(504, 68);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(120, 60);
            this.label95.TabIndex = 21;
            this.label95.Text = "木";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label96
            // 
            this.label96.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label96.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label96.Location = new System.Drawing.Point(624, 68);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(120, 60);
            this.label96.TabIndex = 22;
            this.label96.Text = "金";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label97
            // 
            this.label97.BackColor = System.Drawing.Color.LightSkyBlue;
            this.label97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label97.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label97.Location = new System.Drawing.Point(744, 68);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(120, 60);
            this.label97.TabIndex = 23;
            this.label97.Text = "土";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label2.Location = new System.Drawing.Point(144, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 60);
            this.label2.TabIndex = 24;
            this.label2.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label1.Location = new System.Drawing.Point(24, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 60);
            this.label1.TabIndex = 25;
            this.label1.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label3.Location = new System.Drawing.Point(264, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 60);
            this.label3.TabIndex = 26;
            this.label3.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label4.Location = new System.Drawing.Point(384, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 60);
            this.label4.TabIndex = 27;
            this.label4.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label5.Location = new System.Drawing.Point(504, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 60);
            this.label5.TabIndex = 28;
            this.label5.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label6.Location = new System.Drawing.Point(624, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 60);
            this.label6.TabIndex = 29;
            this.label6.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label7.Location = new System.Drawing.Point(744, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 60);
            this.label7.TabIndex = 30;
            this.label7.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label8.Location = new System.Drawing.Point(24, 188);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 60);
            this.label8.TabIndex = 31;
            this.label8.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(144, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 60);
            this.label9.TabIndex = 32;
            this.label9.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.Control;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(264, 188);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 60);
            this.label10.TabIndex = 33;
            this.label10.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.SystemColors.Control;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.Location = new System.Drawing.Point(384, 188);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 60);
            this.label11.TabIndex = 34;
            this.label11.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.SystemColors.Control;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.Location = new System.Drawing.Point(504, 188);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 60);
            this.label12.TabIndex = 35;
            this.label12.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.SystemColors.Control;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label13.Location = new System.Drawing.Point(624, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 60);
            this.label13.TabIndex = 36;
            this.label13.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.SystemColors.Control;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.Location = new System.Drawing.Point(744, 188);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(120, 60);
            this.label14.TabIndex = 37;
            this.label14.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label15.Location = new System.Drawing.Point(24, 248);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 60);
            this.label15.TabIndex = 38;
            this.label15.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.SystemColors.Control;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label16.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label16.Location = new System.Drawing.Point(144, 248);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 60);
            this.label16.TabIndex = 39;
            this.label16.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.SystemColors.Control;
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label17.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label17.Location = new System.Drawing.Point(264, 248);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(120, 60);
            this.label17.TabIndex = 40;
            this.label17.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.SystemColors.Control;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label18.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label18.Location = new System.Drawing.Point(384, 248);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(120, 60);
            this.label18.TabIndex = 41;
            this.label18.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label19.Location = new System.Drawing.Point(504, 248);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 60);
            this.label19.TabIndex = 42;
            this.label19.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.SystemColors.Control;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label20.Location = new System.Drawing.Point(624, 248);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(120, 60);
            this.label20.TabIndex = 43;
            this.label20.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.SystemColors.Control;
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label21.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label21.Location = new System.Drawing.Point(744, 248);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(120, 60);
            this.label21.TabIndex = 44;
            this.label21.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.SystemColors.Control;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label22.Location = new System.Drawing.Point(24, 308);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(120, 60);
            this.label22.TabIndex = 45;
            this.label22.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.SystemColors.Control;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label24.Location = new System.Drawing.Point(264, 308);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(120, 60);
            this.label24.TabIndex = 46;
            this.label24.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.SystemColors.Control;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label23.Location = new System.Drawing.Point(144, 308);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(120, 60);
            this.label23.TabIndex = 47;
            this.label23.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.SystemColors.Control;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label26.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label26.Location = new System.Drawing.Point(504, 308);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(120, 60);
            this.label26.TabIndex = 48;
            this.label26.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.SystemColors.Control;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label25.Location = new System.Drawing.Point(384, 308);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(120, 60);
            this.label25.TabIndex = 49;
            this.label25.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.SystemColors.Control;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label27.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label27.Location = new System.Drawing.Point(624, 308);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(120, 60);
            this.label27.TabIndex = 50;
            this.label27.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.SystemColors.Control;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label28.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label28.Location = new System.Drawing.Point(744, 308);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(120, 60);
            this.label28.TabIndex = 51;
            this.label28.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.SystemColors.Control;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label29.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label29.Location = new System.Drawing.Point(24, 368);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(120, 60);
            this.label29.TabIndex = 52;
            this.label29.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.SystemColors.Control;
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label30.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label30.Location = new System.Drawing.Point(144, 368);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(120, 60);
            this.label30.TabIndex = 53;
            this.label30.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.SystemColors.Control;
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label31.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label31.Location = new System.Drawing.Point(264, 368);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(120, 60);
            this.label31.TabIndex = 54;
            this.label31.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.SystemColors.Control;
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label32.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label32.Location = new System.Drawing.Point(384, 368);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(120, 60);
            this.label32.TabIndex = 55;
            this.label32.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.SystemColors.Control;
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label33.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label33.Location = new System.Drawing.Point(504, 368);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(120, 60);
            this.label33.TabIndex = 56;
            this.label33.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.SystemColors.Control;
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label34.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label34.Location = new System.Drawing.Point(624, 368);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(120, 60);
            this.label34.TabIndex = 57;
            this.label34.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.SystemColors.Control;
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label35.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label35.Location = new System.Drawing.Point(744, 368);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(120, 60);
            this.label35.TabIndex = 58;
            this.label35.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.SystemColors.Control;
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label36.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label36.Location = new System.Drawing.Point(24, 428);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(120, 60);
            this.label36.TabIndex = 59;
            this.label36.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.SystemColors.Control;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label37.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label37.Location = new System.Drawing.Point(144, 428);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(120, 60);
            this.label37.TabIndex = 60;
            this.label37.Click += new System.EventHandler(this.ClickDateLabel);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label38.Location = new System.Drawing.Point(247, 8);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(39, 36);
            this.label38.TabIndex = 61;
            this.label38.Text = "月";
            // 
            // cmb_month
            // 
            this.cmb_month.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_month.Font = new System.Drawing.Font("メイリオ", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmb_month.FormattingEnabled = true;
            this.cmb_month.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.cmb_month.Location = new System.Drawing.Point(293, 8);
            this.cmb_month.Name = "cmb_month";
            this.cmb_month.Size = new System.Drawing.Size(122, 44);
            this.cmb_month.TabIndex = 62;
            this.cmb_month.TextChanged += new System.EventHandler(this.TextChangedCmb_month);
            // 
            // label137
            // 
            this.label137.BackColor = System.Drawing.SystemColors.Control;
            this.label137.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label137.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label137.Location = new System.Drawing.Point(144, 918);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(120, 60);
            this.label137.TabIndex = 106;
            this.label137.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label136
            // 
            this.label136.BackColor = System.Drawing.SystemColors.Control;
            this.label136.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label136.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label136.Location = new System.Drawing.Point(24, 918);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(120, 60);
            this.label136.TabIndex = 105;
            this.label136.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label135
            // 
            this.label135.BackColor = System.Drawing.SystemColors.Control;
            this.label135.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label135.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label135.Location = new System.Drawing.Point(744, 858);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(120, 60);
            this.label135.TabIndex = 104;
            this.label135.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label134
            // 
            this.label134.BackColor = System.Drawing.SystemColors.Control;
            this.label134.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label134.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label134.Location = new System.Drawing.Point(624, 858);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(120, 60);
            this.label134.TabIndex = 103;
            this.label134.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label133
            // 
            this.label133.BackColor = System.Drawing.SystemColors.Control;
            this.label133.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label133.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label133.Location = new System.Drawing.Point(504, 858);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(120, 60);
            this.label133.TabIndex = 102;
            this.label133.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label132
            // 
            this.label132.BackColor = System.Drawing.SystemColors.Control;
            this.label132.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label132.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label132.Location = new System.Drawing.Point(384, 858);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(120, 60);
            this.label132.TabIndex = 101;
            this.label132.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label131
            // 
            this.label131.BackColor = System.Drawing.SystemColors.Control;
            this.label131.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label131.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label131.Location = new System.Drawing.Point(264, 858);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(120, 60);
            this.label131.TabIndex = 100;
            this.label131.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label130
            // 
            this.label130.BackColor = System.Drawing.SystemColors.Control;
            this.label130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label130.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label130.Location = new System.Drawing.Point(144, 858);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(120, 60);
            this.label130.TabIndex = 99;
            this.label130.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label129
            // 
            this.label129.BackColor = System.Drawing.SystemColors.Control;
            this.label129.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label129.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label129.Location = new System.Drawing.Point(24, 858);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(120, 60);
            this.label129.TabIndex = 98;
            this.label129.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label128
            // 
            this.label128.BackColor = System.Drawing.SystemColors.Control;
            this.label128.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label128.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label128.Location = new System.Drawing.Point(744, 798);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(120, 60);
            this.label128.TabIndex = 97;
            this.label128.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label127
            // 
            this.label127.BackColor = System.Drawing.SystemColors.Control;
            this.label127.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label127.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label127.Location = new System.Drawing.Point(624, 798);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(120, 60);
            this.label127.TabIndex = 96;
            this.label127.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label125
            // 
            this.label125.BackColor = System.Drawing.SystemColors.Control;
            this.label125.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label125.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label125.Location = new System.Drawing.Point(384, 798);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(120, 60);
            this.label125.TabIndex = 95;
            this.label125.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label126
            // 
            this.label126.BackColor = System.Drawing.SystemColors.Control;
            this.label126.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label126.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label126.Location = new System.Drawing.Point(504, 798);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(120, 60);
            this.label126.TabIndex = 94;
            this.label126.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label123
            // 
            this.label123.BackColor = System.Drawing.SystemColors.Control;
            this.label123.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label123.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label123.Location = new System.Drawing.Point(144, 798);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(120, 60);
            this.label123.TabIndex = 93;
            this.label123.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label124
            // 
            this.label124.BackColor = System.Drawing.SystemColors.Control;
            this.label124.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label124.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label124.Location = new System.Drawing.Point(264, 798);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(120, 60);
            this.label124.TabIndex = 92;
            this.label124.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label122
            // 
            this.label122.BackColor = System.Drawing.SystemColors.Control;
            this.label122.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label122.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label122.Location = new System.Drawing.Point(24, 798);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(120, 60);
            this.label122.TabIndex = 91;
            this.label122.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label121
            // 
            this.label121.BackColor = System.Drawing.SystemColors.Control;
            this.label121.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label121.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label121.Location = new System.Drawing.Point(744, 738);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(120, 60);
            this.label121.TabIndex = 90;
            this.label121.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label120
            // 
            this.label120.BackColor = System.Drawing.SystemColors.Control;
            this.label120.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label120.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label120.Location = new System.Drawing.Point(624, 738);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(120, 60);
            this.label120.TabIndex = 89;
            this.label120.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label119
            // 
            this.label119.BackColor = System.Drawing.SystemColors.Control;
            this.label119.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label119.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label119.Location = new System.Drawing.Point(504, 738);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(120, 60);
            this.label119.TabIndex = 88;
            this.label119.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label118
            // 
            this.label118.BackColor = System.Drawing.SystemColors.Control;
            this.label118.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label118.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label118.Location = new System.Drawing.Point(384, 738);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(120, 60);
            this.label118.TabIndex = 87;
            this.label118.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label117
            // 
            this.label117.BackColor = System.Drawing.SystemColors.Control;
            this.label117.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label117.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label117.Location = new System.Drawing.Point(264, 738);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(120, 60);
            this.label117.TabIndex = 86;
            this.label117.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label116
            // 
            this.label116.BackColor = System.Drawing.SystemColors.Control;
            this.label116.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label116.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label116.Location = new System.Drawing.Point(144, 738);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(120, 60);
            this.label116.TabIndex = 85;
            this.label116.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label115
            // 
            this.label115.BackColor = System.Drawing.SystemColors.Control;
            this.label115.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label115.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label115.Location = new System.Drawing.Point(24, 738);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(120, 60);
            this.label115.TabIndex = 84;
            this.label115.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label114
            // 
            this.label114.BackColor = System.Drawing.SystemColors.Control;
            this.label114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label114.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label114.Location = new System.Drawing.Point(744, 678);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(120, 60);
            this.label114.TabIndex = 83;
            this.label114.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label113
            // 
            this.label113.BackColor = System.Drawing.SystemColors.Control;
            this.label113.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label113.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label113.Location = new System.Drawing.Point(624, 678);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(120, 60);
            this.label113.TabIndex = 82;
            this.label113.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label112
            // 
            this.label112.BackColor = System.Drawing.SystemColors.Control;
            this.label112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label112.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label112.Location = new System.Drawing.Point(504, 678);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(120, 60);
            this.label112.TabIndex = 81;
            this.label112.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label111
            // 
            this.label111.BackColor = System.Drawing.SystemColors.Control;
            this.label111.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label111.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label111.Location = new System.Drawing.Point(384, 678);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(120, 60);
            this.label111.TabIndex = 80;
            this.label111.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label110
            // 
            this.label110.BackColor = System.Drawing.SystemColors.Control;
            this.label110.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label110.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label110.Location = new System.Drawing.Point(264, 678);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(120, 60);
            this.label110.TabIndex = 79;
            this.label110.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label109
            // 
            this.label109.BackColor = System.Drawing.SystemColors.Control;
            this.label109.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label109.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label109.Location = new System.Drawing.Point(144, 678);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(120, 60);
            this.label109.TabIndex = 78;
            this.label109.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label108
            // 
            this.label108.BackColor = System.Drawing.SystemColors.Control;
            this.label108.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label108.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label108.Location = new System.Drawing.Point(24, 678);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(120, 60);
            this.label108.TabIndex = 77;
            this.label108.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label107
            // 
            this.label107.BackColor = System.Drawing.SystemColors.Control;
            this.label107.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label107.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label107.Location = new System.Drawing.Point(744, 618);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(120, 60);
            this.label107.TabIndex = 76;
            this.label107.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label106
            // 
            this.label106.BackColor = System.Drawing.SystemColors.Control;
            this.label106.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label106.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label106.Location = new System.Drawing.Point(624, 618);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(120, 60);
            this.label106.TabIndex = 75;
            this.label106.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label105
            // 
            this.label105.BackColor = System.Drawing.SystemColors.Control;
            this.label105.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label105.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label105.Location = new System.Drawing.Point(504, 618);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(120, 60);
            this.label105.TabIndex = 74;
            this.label105.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label104
            // 
            this.label104.BackColor = System.Drawing.SystemColors.Control;
            this.label104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label104.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label104.Location = new System.Drawing.Point(384, 618);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(120, 60);
            this.label104.TabIndex = 73;
            this.label104.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label103
            // 
            this.label103.BackColor = System.Drawing.SystemColors.Control;
            this.label103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label103.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label103.Location = new System.Drawing.Point(264, 618);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(120, 60);
            this.label103.TabIndex = 72;
            this.label103.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label101
            // 
            this.label101.BackColor = System.Drawing.SystemColors.Control;
            this.label101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label101.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label101.Location = new System.Drawing.Point(24, 618);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(120, 60);
            this.label101.TabIndex = 71;
            this.label101.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label102
            // 
            this.label102.BackColor = System.Drawing.SystemColors.Control;
            this.label102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label102.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label102.Location = new System.Drawing.Point(144, 618);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(120, 60);
            this.label102.TabIndex = 70;
            this.label102.Click += new System.EventHandler(this.ClickNextDateLabel);
            // 
            // label76
            // 
            this.label76.BackColor = System.Drawing.Color.LightSkyBlue;
            this.label76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label76.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label76.Location = new System.Drawing.Point(744, 558);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(120, 60);
            this.label76.TabIndex = 69;
            this.label76.Text = "土";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label77
            // 
            this.label77.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label77.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label77.Location = new System.Drawing.Point(624, 558);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(120, 60);
            this.label77.TabIndex = 68;
            this.label77.Text = "金";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label78
            // 
            this.label78.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label78.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label78.Location = new System.Drawing.Point(504, 558);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(120, 60);
            this.label78.TabIndex = 67;
            this.label78.Text = "木";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label79
            // 
            this.label79.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label79.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label79.Location = new System.Drawing.Point(384, 558);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(120, 60);
            this.label79.TabIndex = 66;
            this.label79.Text = "水";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label80
            // 
            this.label80.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label80.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label80.Location = new System.Drawing.Point(264, 558);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(120, 60);
            this.label80.TabIndex = 65;
            this.label80.Text = "火";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label81
            // 
            this.label81.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label81.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label81.Location = new System.Drawing.Point(144, 558);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(120, 60);
            this.label81.TabIndex = 64;
            this.label81.Text = "月";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label82
            // 
            this.label82.BackColor = System.Drawing.Color.LightCoral;
            this.label82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label82.Font = new System.Drawing.Font("メイリオ", 18F);
            this.label82.Location = new System.Drawing.Point(24, 558);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(120, 60);
            this.label82.TabIndex = 63;
            this.label82.Text = "日";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNextMonth
            // 
            this.txtNextMonth.Font = new System.Drawing.Font("メイリオ", 18F);
            this.txtNextMonth.Location = new System.Drawing.Point(24, 502);
            this.txtNextMonth.Name = "txtNextMonth";
            this.txtNextMonth.ReadOnly = true;
            this.txtNextMonth.Size = new System.Drawing.Size(146, 43);
            this.txtNextMonth.TabIndex = 107;
            // 
            // Calendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 1001);
            this.Controls.Add(this.txtNextMonth);
            this.Controls.Add(this.label137);
            this.Controls.Add(this.label136);
            this.Controls.Add(this.label135);
            this.Controls.Add(this.label134);
            this.Controls.Add(this.label133);
            this.Controls.Add(this.label132);
            this.Controls.Add(this.label131);
            this.Controls.Add(this.label130);
            this.Controls.Add(this.label129);
            this.Controls.Add(this.label128);
            this.Controls.Add(this.label127);
            this.Controls.Add(this.label125);
            this.Controls.Add(this.label126);
            this.Controls.Add(this.label123);
            this.Controls.Add(this.label124);
            this.Controls.Add(this.label122);
            this.Controls.Add(this.label121);
            this.Controls.Add(this.label120);
            this.Controls.Add(this.label119);
            this.Controls.Add(this.label118);
            this.Controls.Add(this.label117);
            this.Controls.Add(this.label116);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.cmb_month);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.cmb_year);
            this.Name = "Calendar";
            this.Text = "社内カレンダー";
            this.Load += new System.EventHandler(this.View_F100_ClientCalendar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cmb_year;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cmb_month;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox txtNextMonth;
    }
}