﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.ComponentModel;
using System.Linq;
using ZXing;

namespace ZumenConsoleApp.View
{
    public partial class BarcodeReader : BaseView
    {

        #region クラス変数

        // Surface Goのカメラ定義
        // フロントカメラ
        private const int FrontCamera = 0;
        // バックカメラ
        private const int BackCamera = 1;

        // バーコードの内容
        public string Barcode = string.Empty;

        #endregion

        public BarcodeReader()
        {
            InitializeComponent();
        }

        #region イベント

        /// <summary>
        /// 画面起動時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadBarcodeReader(object sender, EventArgs e)   
        {
            // バックグランド処理1の実行開始
            this.backgroundWorker1.RunWorkerAsync();
        }

        /// <summary>
        /// バーコードスキャン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoWorkBackgroundWorker(object sender, DoWorkEventArgs e)
        {
            try
            {
                using (var capture = Cv.CreateCameraCapture(BackCamera))
                {
                    GetBarcod(capture);
                }
            }
            catch (Exception)
            {
                this.Close();
            }
        }

        /// <summary>
        /// バーコードスキャン
        /// </summary>
        /// <param name="capture"></param>
        private void GetBarcod(CvCapture capture)
        {
            try
            {
                IplImage frame = new IplImage();
                // オプション設定
                ZXing.BarcodeReader reader = new ZXing.BarcodeReader()
                {
                    AutoRotate = true,
                    Options = { TryHarder = true },
                };

                reader.Options = new ZXing.Common.DecodingOptions
                {
                    TryHarder = true,
                    PossibleFormats = new[] { BarcodeFormat.CODE_39 }.ToList()
                };

                Result result = null;
                while (true)
                {
                    // カメラからフレームを取得
                    frame = Cv.QueryFrame(capture);

                    // カメラの画像を表示
                    this.pictureBox.Image = frame.ToBitmap();

                    // コードの解析
                    result = reader.Decode(frame.ToBitmap());

                    if (result != null)
                    {
                        Barcode = result.Text;
                        capture.Dispose();
                        this.Close();
                    }
                }
            }
            catch (Exception)
            {
                capture.Dispose();
                this.Close();
            }
        }

        #endregion

    }
}
