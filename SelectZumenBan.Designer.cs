﻿namespace ZumenConsoleApp.View
{
    partial class SelectZumenBan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zumenBanList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // zumenBanList
            // 
            this.zumenBanList.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.zumenBanList.FormattingEnabled = true;
            this.zumenBanList.ItemHeight = 37;
            this.zumenBanList.Location = new System.Drawing.Point(31, 33);
            this.zumenBanList.Name = "zumenBanList";
            this.zumenBanList.Size = new System.Drawing.Size(424, 522);
            this.zumenBanList.TabIndex = 1;
            this.zumenBanList.DoubleClick += new System.EventHandler(this.zumenBanList_DoubleClick);
            // 
            // SelectZumenBan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 618);
            this.Controls.Add(this.zumenBanList);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectZumenBan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "図面番号選択";
            this.Load += new System.EventHandler(this.SelectZumenBan_Load);
            this.Controls.SetChildIndex(this.zumenBanList, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox zumenBanList;
    }
}