﻿namespace ZumenConsoleApp
{
    partial class SelectListBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectListBox = new System.Windows.Forms.ListBox();
            this.selectBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // selectListBox
            // 
            this.selectListBox.Font = new System.Drawing.Font("メイリオ", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.selectListBox.FormattingEnabled = true;
            this.selectListBox.ItemHeight = 48;
            this.selectListBox.Location = new System.Drawing.Point(24, 29);
            this.selectListBox.Name = "selectListBox";
            this.selectListBox.ScrollAlwaysVisible = true;
            this.selectListBox.Size = new System.Drawing.Size(648, 340);
            this.selectListBox.TabIndex = 0;
            this.selectListBox.DoubleClick += new System.EventHandler(this.DoubleClicklistBox);
            // 
            // selectBtn
            // 
            this.selectBtn.Font = new System.Drawing.Font("メイリオ", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.selectBtn.Location = new System.Drawing.Point(534, 374);
            this.selectBtn.Name = "selectBtn";
            this.selectBtn.Size = new System.Drawing.Size(138, 56);
            this.selectBtn.TabIndex = 1;
            this.selectBtn.Text = "選択";
            this.selectBtn.UseVisualStyleBackColor = true;
            this.selectBtn.Click += new System.EventHandler(this.ClickSelectBtn);
            // 
            // View_F000_SelectListBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 442);
            this.Controls.Add(this.selectBtn);
            this.Controls.Add(this.selectListBox);
            this.Name = "View_F000_SelectListBox";
            this.Load += new System.EventHandler(this.LoadListBox);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox selectListBox;
        private System.Windows.Forms.Button selectBtn;
    }
}