﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Npgsql;
using System.Data;
using System.Collections.Generic;
using System.Drawing;

namespace ZumenConsoleApp.View
{
    public partial class CalcWorkTime : BaseView
    {
        public static string orderNo { get; set; }
        public static string blockNo { get; set; }
        public static string mitumori { get; set; }
        public static string mokuhyo { get; set; }

        // 作業者リスト
        public static List<ZumenWorker> zumenWorkerList { get; set; }
        // 作業状況データリスト
        public static List<WorkList> wkList { get; set; }

        // 作業状態
        private const string WORK_START = "0";      // 作業開始
        private const string WORK_STOP = "1";       // 作業中断
        private const string WORK_RESTART = "2";    // 作業再開
        private const string WORK_END = "3";        // 作業終了

        public CalcWorkTime()
        {
            InitializeComponent();
        }

        private void SetMitumori_Load(object sender, EventArgs e)
        {
            try
            {
                zumenWorkerList = new List<ZumenWorker>();
                zumenWorkerList = CommonModule.GetZumenWorkers(Properties.Settings.Default.connectionString);
                if (zumenWorkerList.Count == 0)
                {
                    MessageBox.Show("作業者マスタよりデータを正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                foreach (var zw in zumenWorkerList)
                {
                    cmbZumenWorker.Items.Add(zw);
                }

                wkList = CommonModule.GetWorkInfo(Properties.Settings.Default.connectionString, orderNo, blockNo);
                if (CalcWorkTime.wkList == null)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("作業者実績データを正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                // 作業状況一覧
                WorkList.Clear();
                var tuujouTime = 0.00;
                var tuikaTime = 0.00;
                var fuguaiTime = 0.00;
                foreach (var dt in wkList)
                {
                    var blockNoNt4 = CommonModule.ConvertToString(blockNo).Split('-')[1];
                    var sumTime = "0";
                    if (dt.BlockNo == blockNo || blockNoNt4 == "0000")
                    {
                        if (dt.SumWorkTime != null && dt.SumWorkTime != "")
                        {
                            sumTime = dt.SumWorkTime;
                        }
                        switch (dt.Jisseki)
                        {
                            case "通常":
                                tuujouTime += double.Parse(sumTime);
                                break;
                            case "追加":
                                tuikaTime += double.Parse(sumTime);
                                break;
                            case "不具合":
                                fuguaiTime += double.Parse(sumTime);
                                break;
                        }
                    }

                    if (dt.BlockNo == blockNo)
                    {
                        if (dt.Jisseki == "見積")
                        {
                            txtMitumori.Text = dt.Mitumori;
                            txtMokuhyo.Text = dt.Mokuhyo;
                            continue;
                        }

                        var startTm = "";
                        if (dt.ReStartTime != null && dt.ReStartTime != "")
                        {
                            startTm = dt.ReStartTime + "再開 ";
                        }
                        else if (dt.StartTime != null && dt.StartTime != "")
                        {
                            startTm = dt.StartTime + "開始 ";
                        }
                        WorkList.Items.Insert(0, "[" + dt.Jisseki + "]" + " " + dt.Worker + " " + startTm + "(" + dt.WorkState + ")" + " 作業時間：" + sumTime + "時間");
                        switch (dt.WorkState)
                        {
                            case "作業中":
                                WorkList.Items[0].BackColor = Color.GreenYellow;
                                break;
                            case "作業中断":
                                WorkList.Items[0].BackColor = Color.Red;
                                break;
                            case "作業終了":
                                WorkList.Items[0].BackColor = Color.DeepSkyBlue;
                                break;
                        }
                    }
                }
                // 作業時間記載
                txtTuujou.Text = tuujouTime.ToString();
                txtTuika.Text = tuikaTime.ToString();
                txtFuguai.Text = fuguaiTime.ToString();
            }
            catch (Exception ex)
            {
                // メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 設定ボタン押下時処理
        private void setBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var conn = Properties.Settings.Default.connectionString;
                var inpMitumori = 0.00;
                var inpMokuhyo = 0.00;
                if (!Double.TryParse(txtMitumori.Text, out inpMitumori) || !Double.TryParse(txtMokuhyo.Text, out inpMokuhyo))
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("見積、目標を正しく入力してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                if (inpMitumori < 0 || inpMitumori > 10000 || inpMokuhyo < 0 || inpMokuhyo > 10000)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("見積、目標は0～10000で設定してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                var checkData = false;
                if (CountData(conn, orderNo, blockNo) == 0)
                {
                    checkData = InsertMitumori(conn, orderNo, blockNo, Math.Round(inpMitumori, 2), Math.Round(inpMokuhyo, 2));
                }
                else if (CountData(conn, orderNo, blockNo) > 0)
                {
                    checkData = UpdateMitumori(conn, orderNo, blockNo, Math.Round(inpMitumori, 2), Math.Round(inpMokuhyo, 2));
                }
                if (!checkData)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("作業者実績データを正しく更新できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                else
                {
                    mitumori = inpMitumori.ToString();
                    mokuhyo = inpMokuhyo.ToString();
                    //メッセージボックスを表示する
                    MessageBox.Show("作業者実績データを更新しました。",
                        "確認",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.None);
                }
                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 作業者実績データ件数を取得する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="blockNo"></param>
        /// <returns></returns>
        private int CountData(string connectionString, string orderNo, string blockNo)
        {
            var count = 0;
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var cmd = new NpgsqlCommand("select count(*) as cnt " +
                                            " from 作業者実績データ " +
                                            " where \"オーダーNO\" = :orderNo " +
                                            "  and ブロック番号 = :blockNo ", con);

                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });
                cmd.Parameters.Add(new NpgsqlParameter("blockNo", DbType.String) { Value = blockNo });

                try
                {
                    var result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        count = CommonModule.ConvertToInt(result["cnt"].ToString());
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return -1;
                }
            }
            return count;
        }

        /// <summary>
        /// 見積・目標時間登録
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="blockNo"></param>
        /// <param name="mitumori"></param>
        /// <param name="mokuhyo"></param>
        /// <returns></returns>
        private bool InsertMitumori(string connectionString, string orderNo, string blockNo, double mitumori, double mokuhyo)
        {
            var cmd = new NpgsqlCommand();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "insert into " +
                                "  作業者実績データ(" +
                                "  \"オーダーNO\" " +
                                "  ,ブロック番号 " +
                                "  ,実績区分 " +
                                "  ,作業者コード " +
                                "  ,見積時間 " +
                                "  ,目標時間 " +
                                "  ,更新日時 " +
                                "  )values(" +
                                "  :orderNo " +
                                "  ,:blockNo " +
                                "  ,'9' " +
                                "  ,-1 " +
                                "  ,:mitumori " +
                                "  ,:mokuhyo " +
                                "  ,now()) " ;

                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });
                cmd.Parameters.Add(new NpgsqlParameter("blockNo", DbType.String) { Value = blockNo });
                cmd.Parameters.Add(new NpgsqlParameter("mitumori", DbType.Double) { Value = mitumori });
                cmd.Parameters.Add(new NpgsqlParameter("mokuhyo", DbType.Double) { Value = mokuhyo });

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 見積・目標時間更新
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="blockNo"></param>
        /// <param name="mitumori"></param>
        /// <param name="mokuhyo"></param>
        /// <returns></returns>
        private bool UpdateMitumori(string connectionString, string orderNo, string blockNo, double mitumori, double mokuhyo)
        {
            var cmd = new NpgsqlCommand();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "update " +
                                "  作業者実績データ" +
                                "  set 見積時間 = :mitumori" +
                                "  ,目標時間 = :mokuhyo" +
                                " where \"オーダーNO\" = :orderNo" +
                                "  and ブロック番号 = :blockNo" ;

                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("mitumori", DbType.Double) { Value = mitumori });
                cmd.Parameters.Add(new NpgsqlParameter("mokuhyo", DbType.Double) { Value = mokuhyo });
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });
                cmd.Parameters.Add(new NpgsqlParameter("blockNo", DbType.String) { Value = blockNo });

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return false;
                }
            }
            return true;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                // 図面作業者、作業種類必須チェック
                if (!CheckZumenWork())
                {
                    return;
                }
                var conn = Properties.Settings.Default.connectionString;
                var selWkList = WorkSelect(conn, blockNo);
                if (selWkList == null)
                {
                    MessageBox.Show("作業者実績データより正しくデータを取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                if (selWkList.WorkState == "0")
                {
                    MessageBox.Show("すでに作業開始しています。", "確認", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                DialogResult dr = MessageBox.Show("作業開始してもよろしいですか？", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                if (dr == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
                if (selWkList.BlockNo == null || selWkList.BlockNo == "")
                {
                    var checkInsert = WorkInsert(conn, blockNo);
                    if (!checkInsert)
                    {
                        MessageBox.Show("作業者実績データに正しくデータを登録できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    var checkUpdate = WorkUpdate(conn, selWkList, "2");
                    if (!checkUpdate)
                    {
                        MessageBox.Show("作業者実績データを更新できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        return;
                    }
                }
                wkList = CommonModule.GetWorkInfo(conn, orderNo, "");
                if (wkList == null)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("作業者実績データを正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                MessageBox.Show("作業を開始しました。", "作業開始", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                // 図面作業者、作業種類必須チェック
                if (!CheckZumenWork())
                {
                    return;
                }
                var conn = Properties.Settings.Default.connectionString;
                var selWkList = WorkSelect(conn, blockNo);
                if (selWkList == null)
                {
                    MessageBox.Show("作業者実績データより正しくデータを取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                if (selWkList.WorkState == null || selWkList.WorkState == "2")
                {
                    MessageBox.Show("作業が開始されていません。", "確認", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (selWkList.WorkState == "1")
                {
                    MessageBox.Show("すでに中断しています。", "確認", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                DialogResult dr = MessageBox.Show("作業中断してもよろしいですか？", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                if (dr == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
                var checkUpdate = WorkUpdate(conn, selWkList, "1");
                if (!checkUpdate)
                {
                    MessageBox.Show("作業者実績データを更新できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }
                wkList = CommonModule.GetWorkInfo(conn, orderNo, "");
                if (wkList == null)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("作業者実績データを正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                MessageBox.Show("作業を中断しました。", "作業中断", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        private void btnEnd_Click(object sender, EventArgs e)
        {
            try
            {
                // 図面作業者、作業種類必須チェック
                if (!CheckZumenWork())
                {
                    return;
                }
                var conn = Properties.Settings.Default.connectionString;
                var selWkList = WorkSelect(conn, blockNo);
                if (selWkList == null)
                {
                    MessageBox.Show("作業者実績データより正しくデータを取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                if (selWkList.WorkState == null || selWkList.WorkState == "2")
                {
                    MessageBox.Show("作業が開始されていません。", "確認", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (selWkList.WorkState == "1")
                {
                    DialogResult dr = MessageBox.Show("作業を中断していますが、終了しますか？", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                    if (dr == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }
                }
                else
                {
                    DialogResult dr = MessageBox.Show("作業終了してもよろしいですか？", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                    if (dr == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }
                }
                var checkUpdate = WorkUpdate(conn, selWkList, "3");
                if (!checkUpdate)
                {
                    MessageBox.Show("作業者実績データを更新できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }
                wkList = CommonModule.GetWorkInfo(conn, orderNo, "");
                if (wkList == null)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("作業者実績データを正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                MessageBox.Show("作業を終了しました。", "作業終了", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        // 図面作業者、作業種類必須チェック
        private bool CheckZumenWork()
        {
            if (cmbZumenWorker.Text.Length == 0)
            {
                //メッセージボックスを表示する
                MessageBox.Show("図面作業者を入力してください。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
            if (cmbSagyo.Text.Length == 0)
            {
                //メッセージボックスを表示する
                MessageBox.Show("作業種類を選択してください。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 作業時間一覧件数を取得する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="blockNo"></param>
        /// <returns></returns>
        private WorkList WorkSelect(string connectionString, string blockNo)
        {
            var wkList = new WorkList();
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var cmd = new NpgsqlCommand("select 作業開始時間 " +
                                            " ,作業再開時間 " +
                                            " ,累積作業時間 " +
                                            " ,作業区分 " +
                                            " from 作業者実績データ " +
                                            " where \"オーダーNO\" = :orderNo " +
                                            "  and ブロック番号 = :blockNo " +
                                            "  and 実績区分 = :jisseki " +
                                            "  and 作業者コード = :sagyousya ", con);

                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });
                cmd.Parameters.Add(new NpgsqlParameter("blockNo", DbType.String) { Value = blockNo });
                cmd.Parameters.Add(new NpgsqlParameter("jisseki", DbType.String) { Value = cmbSagyo.SelectedIndex.ToString() });
                ZumenWorker zw = (ZumenWorker)cmbZumenWorker.SelectedItem;
                cmd.Parameters.Add(new NpgsqlParameter("sagyousya", DbType.Int32) { Value = zw.TantouCode });

                try
                {
                    var result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        wkList.BlockNo = blockNo;
                        wkList.StartTime = result["作業開始時間"].ToString();
                        wkList.ReStartTime = result["作業再開時間"].ToString();
                        wkList.SumWorkTime = result["累積作業時間"].ToString();
                        wkList.WorkState = result["作業区分"].ToString();
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return null;
                }
            }
            return wkList;
        }

        /// <summary>
        /// 作業時間の実績を作業者実績データ(DB)に登録する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="blockNo"></param>
        /// <returns></returns>
        private bool WorkInsert(string connectionString, string blockNo)
        {
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    var cmd = new NpgsqlCommand("insert into 作業者実績データ " +
                                                "  (\"オーダーNO\"" +
                                                "  ,ブロック番号" +
                                                "  ,実績区分 " +
                                                "  ,作業者コード " +
                                                "  ,見積時間 " +
                                                "  ,目標時間 " +
                                                "  ,作業開始時間 " +
                                                "  ,作業区分 " +
                                                "  ,更新日時)" +
                                                "  values " +
                                                "  (:orderNo " +
                                                "  ,:blockNo " +
                                                "  ,:jisseki " +
                                                "  ,:sagyousya " +
                                                "  ,:mitumori " +
                                                "  ,:mokuhyo " +
                                                "  ,now() " +
                                                "  ,0 " +
                                                "  ,now()) ", con);
                    cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });
                    cmd.Parameters.Add(new NpgsqlParameter("blockNo", DbType.String) { Value = blockNo });
                    cmd.Parameters.Add(new NpgsqlParameter("jisseki", DbType.String) { Value = cmbSagyo.SelectedIndex.ToString() });
                    ZumenWorker zw = (ZumenWorker)cmbZumenWorker.SelectedItem;
                    cmd.Parameters.Add(new NpgsqlParameter("sagyousya", DbType.Int32) { Value = zw.TantouCode });
                    cmd.Parameters.Add(new NpgsqlParameter("mitumori", DbType.Double) { Value = double.Parse(txtMitumori.Text) });
                    cmd.Parameters.Add(new NpgsqlParameter("mokuhyo", DbType.Double) { Value = double.Parse(txtMokuhyo.Text) });

                    try
                    {
                        var result = cmd.ExecuteNonQuery();
                        Debug.WriteLine($"[result]{result}");
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine($"[err]{e.Message}");
                        tran.Rollback();
                        return false;
                    }
                    tran.Commit();
                }
            }
            return true;
        }

        /// <summary>
        /// 作業者実績データ(DB)を更新する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="blockNo"></param>
        /// <param name="act"></param>
        /// <returns></returns>
        private bool WorkUpdate(string connectionString, WorkList selWkList, string act)
        {
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    var sqltext = "update 作業者実績データ ";
                    switch (act)
                    {
                        // 作業開始時
                        case WORK_START:
                            sqltext += "  set 作業開始時間 = now() " +
                                       " , 作業区分 = '0' ";
                            break;
                        // 作業中断時
                        case WORK_STOP:
                            sqltext += "  set 作業中断時間 = now() " +
                                       " , 作業区分 = '1' ";
                            DateTime dt1 = DateTime.Parse(selWkList.StartTime);
                            //昼食休憩時間
                            TimeSpan tlunch = new TimeSpan(0, 0, 0);
                            double dbHour = 0.00;
                            if (selWkList.ReStartTime != "")
                            {
                                dt1 = DateTime.Parse(selWkList.ReStartTime);
                            }
                            string strhhmm = dt1.ToString("HHmm");
                            int inthhmm = Int16.Parse(strhhmm);
                            string nowstrhhmm = DateTime.Now.ToString("HHmm");
                            int nowinthhmm = Int16.Parse(nowstrhhmm);
                            // 昼食休憩時間をまたぐ場合は作業時間から40分除く
                            if (inthhmm < 1250 && nowinthhmm >= 1250)
                            {
                                tlunch = new TimeSpan(0, 40, 0);
                            }
                            TimeSpan ts = DateTime.Now - dt1;
                            ts -= tlunch;
                            dbHour = ts.TotalHours;
                            sqltext += " , 累積作業時間 = " + (Math.Round(dbHour, 2) + CommonModule.ConvertToDouble(selWkList.SumWorkTime)) + "";

                            break;
                        // 作業再開時
                        case WORK_RESTART:
                            sqltext += "  set 作業再開時間 = now() " +
                                       " , 作業区分 = '0' ";
                            break;
                        // 作業終了時
                        case WORK_END:
                            sqltext += "  set 作業終了時間 = now() " +
                                       "  ,作業中断時間 = null " +
                                       "  ,作業再開時間 = null " +
                                       "  ,作業区分 = '2' ";
                            if (selWkList.WorkState != "1")
                            {
                                dt1 = DateTime.Parse(selWkList.StartTime);
                                //昼食休憩時間
                                tlunch = new TimeSpan(0, 0, 0);
                                dbHour = 0.00;
                                if (selWkList.ReStartTime != "")
                                {
                                    dt1 = DateTime.Parse(selWkList.ReStartTime);
                                }
                                strhhmm = dt1.ToString("HHmm");
                                inthhmm = Int16.Parse(strhhmm);
                                nowstrhhmm = DateTime.Now.ToString("HHmm");
                                nowinthhmm = Int16.Parse(nowstrhhmm);
                                // 昼食休憩時間をまたぐ場合は作業時間から40分除く
                                if (inthhmm < 1250 && nowinthhmm >= 1250)
                                {
                                    tlunch = new TimeSpan(0, 40, 0);
                                }
                                ts = DateTime.Now - dt1;
                                ts -= tlunch;
                                dbHour = ts.TotalHours;
                                sqltext += " , 累積作業時間 = " + (Math.Round(dbHour, 2) + CommonModule.ConvertToDouble(selWkList.SumWorkTime)) + "";
                            }
                            break;
                    }
                    sqltext += " ,更新日時 = now() ";
                    sqltext += " where \"オーダーNO\" = :orderNo " +
                               "  and ブロック番号 = :blockNo " +
                               "  and 実績区分 = :jisseki" +
                               "  and 作業者コード = :sagyousya";

                    var cmd = new NpgsqlCommand(sqltext, con);

                    cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });
                    cmd.Parameters.Add(new NpgsqlParameter("blockNo", DbType.String) { Value = selWkList.BlockNo });
                    cmd.Parameters.Add(new NpgsqlParameter("jisseki", DbType.String) { Value = cmbSagyo.SelectedIndex.ToString() });
                    ZumenWorker zw = (ZumenWorker)cmbZumenWorker.SelectedItem;
                    cmd.Parameters.Add(new NpgsqlParameter("sagyousya", DbType.Int32) { Value = zw.TantouCode });

                    try
                    {
                        var result = cmd.ExecuteNonQuery();
                        Debug.WriteLine($"[result]{result}");
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine($"[err]{e.Message}");
                        tran.Rollback();
                        return false;
                    }
                    tran.Commit();
                }
            }
            return true;
        }
    }
}
