﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Npgsql;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace ZumenConsoleApp.View
{
    public partial class ScanDialog : BaseView
    {

        // バーコードの内容
        public string Barcode = string.Empty;

        public ScanDialog()
        {
            InitializeComponent();
        }

        // 図面履歴取得
        public void ReadZumenHistly(List<string> KouteiNoList)
        {
            try
            {
                var conn = Properties.Settings.Default.connectionString;
                List<JissekiKakou> jkDataList = new List<JissekiKakou>();
                jkDataList = GetZumenList(conn, KouteiNoList);

                MainDisp.JkList = new List<JissekiKakou>();

                foreach (var jkData in jkDataList)
                {
                    MainDisp.JkList.Add(jkData);
                    // 図面検索条件に追加
                    SearchEntry.JisKak = new JissekiKakou();
                    SearchEntry.JisKak.OrderNo = jkData.OrderNo;
                    SearchEntry.JisKak.BlockNo = jkData.BlockNo;
                    SearchEntry.JisKak.KouteihyouNo = jkData.KouteihyouNo;
                    SearchEntry.JisKak.ZumenBan = jkData.ZumenBan;
                    SearchEntry.JisKak.ChumonsyoNo = jkData.ChumonsyoNo;

                    // 組立図検索条件に追加
                    SearchEntryKumi.JkData = new JissekiKakou();
                    SearchEntryKumi.JkData.OrderNo = jkData.OrderNo;
                    SearchEntryKumi.JkData.BlockNo = jkData.BlockNo;
                }

                // trueにしないとリストが更新されない
                MainDisp.isScan = true;
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 図面情報取得
        public void ReadZumenInfo()
        {
            try
            {
                var conn = Properties.Settings.Default.connectionString;
                var jkData = new JissekiKakou();
                jkData = GetZumenInfo(conn, Barcode);
                if (jkData == null || jkData.OrderNo == "" || !File.Exists(jkData.ZumenPath))
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面ファイルがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);

                    textBox1.Clear();
                    textBox1.Focus();
                    return;
                }
                if (MainDisp.JkList != null && MainDisp.JkList.Count > 0)
                {
                    var findDt = MainDisp.JkList.Find(x => x.OrderNo == jkData.OrderNo && x.Renno == jkData.Renno);
                    if (findDt != null && findDt.OrderNo != "")
                    {
                        //メッセージボックスを表示する
                        MessageBox.Show("既に同じ図面がスキャンされています。",
                            "エラー",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);

                        textBox1.Clear();
                        textBox1.Focus();
                        return;
                    }
                }
                if (!MainDisp.scanFlg)
                {
                    //MainDisp.JkList = new List<JissekiKakou>();
                }
                if (GetSameZumenCount(conn, jkData.ZumenBan) > 1)
                {
                    jkData.isMultiZumen = true;
                }
                else
                {
                    jkData.isMultiZumen = false;
                }
                MainDisp.JkList.Add(jkData);
                // 図面検索条件に追加
                SearchEntry.JisKak = new JissekiKakou();
                SearchEntry.JisKak.OrderNo = jkData.OrderNo;
                SearchEntry.JisKak.BlockNo = jkData.BlockNo;
                SearchEntry.JisKak.KouteihyouNo = jkData.KouteihyouNo;
                SearchEntry.JisKak.ZumenBan = jkData.ZumenBan;
                SearchEntry.JisKak.ChumonsyoNo = jkData.ChumonsyoNo;

                // 組立図検索条件に追加
                SearchEntryKumi.JkData = new JissekiKakou();
                SearchEntryKumi.JkData.OrderNo = jkData.OrderNo;
                SearchEntryKumi.JkData.BlockNo = jkData.BlockNo;

                MainDisp.isScan = true;

                Debug.WriteLine(textBox1.Text);
                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 読み取りボタン押下時処理
        public void scanBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var conn = Properties.Settings.Default.connectionString;
                var jkData = new JissekiKakou();
                string hankaku = Microsoft.VisualBasic.Strings.StrConv(textBox1.Text, Microsoft.VisualBasic.VbStrConv.Narrow);
                jkData = GetZumenInfo(conn, hankaku);
                if (jkData == null || jkData.OrderNo == "" || !File.Exists(jkData.ZumenPath))
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面ファイルがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    textBox1.Clear();
                    textBox1.Focus();
                    return;
                }
                if (MainDisp.JkList != null && MainDisp.JkList.Count > 0)
                {
                    var findDt = MainDisp.JkList.Find(x => x.OrderNo == jkData.OrderNo && x.Renno == jkData.Renno);
                    if (findDt != null && findDt.OrderNo != "")
                    {
                        //メッセージボックスを表示する
                        MessageBox.Show("既に同じ図面がスキャンされています。",
                            "エラー",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        textBox1.Clear();
                        textBox1.Focus();
                        return;
                    }
                }
                if (GetSameZumenCount(conn, jkData.ZumenBan) > 1)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("他に未加工の同じ図面があります。「同図面一覧」タブを確認してください。",
                        "注意",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    jkData.isMultiZumen = true;
                }
                else
                {
                    jkData.isMultiZumen = false;
                }
                MainDisp.JkList.Add(jkData);
                // 図面検索条件に追加
                SearchEntry.JisKak = new JissekiKakou();
                SearchEntry.JisKak.OrderNo = jkData.OrderNo;
                SearchEntry.JisKak.BlockNo = jkData.BlockNo;
                SearchEntry.JisKak.KouteihyouNo = jkData.KouteihyouNo;
                SearchEntry.JisKak.ZumenBan = jkData.ZumenBan;
                SearchEntry.JisKak.ChumonsyoNo = jkData.ChumonsyoNo;

                // 組立図検索条件に追加
                SearchEntryKumi.JkData = new JissekiKakou();
                SearchEntryKumi.JkData.OrderNo = jkData.OrderNo;
                SearchEntryKumi.JkData.BlockNo = jkData.BlockNo;

                MainDisp.isScan = true;

                Debug.WriteLine(textBox1.Text);
                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 図面情報を抽出する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="KouteiNoList"></param>
        /// <returns></returnsKouteiNoList
        private List<JissekiKakou> GetZumenList(string connectionString, List<string> KouteiNoList)
        {
            List<JissekiKakou> list = new List<JissekiKakou>();
            var cmd = new NpgsqlCommand();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  jkkd.\"オーダーNO\"" +
                                "  ,jkkd.グループ" +
                                "  ,jkkd.連番" +
                                "  ,jkkd.工程表番号" +
                                "  ,jkkd.\"引合NO\"" +
                                "  ,jkkd.図面番号" +
                                "  ,jkkd.品名" +
                                "  ,jkkd.数量 as 加工数量" +
                                "  ,jkkd.納期" +
                                "  ,jkkd.客先指定納期" +
                                "  ,case when jkkd.図面パス is not null then jkkd.図面パス " +
                                "   when zd.パス is not null then zd.パス " +
                                "   when zd2.パス is not null then zd2.パス end as 図面パス" +
                                "  ,jkkd.担当者コード as 担当者コード" +
                                "  ,sam.作業者名 as 担当者" +
                                "  ,sam2.作業者名 as 加工担当者" +
                                "  ,jkkd.注文書番号" +
                                "  ,jkkd.注文書行番号" +
                                "  ,ssm.仕入先略名 as 加工先" +
                                "  ,sjm.作業状況名" +
                                "  ,ssm2.仕入先略名 as 材料入荷先" +
                                "  ,jzd.材質" +
                                "  ,jzd.サイズ" +
                                "  ,jzd.数量 as 材料数量" +
                                "  ,jzd.納期 as 入荷日" +
                                "  ,okj.先行手配" +
                                "  ,okku.組立有無" +
                                " from 実績加工データ jkkd" +
                                " left join 仕入先マスタ ssm" +
                                "   on jkkd.加工先コード = ssm.仕入先コード" +
                                " left join 検査実績データ kjd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 作業状況マスタ sjm" +
                                "   on sjm.作業状況コード = kjd.作業状況" +
                                " left join 実績材料データ jzd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 仕入先マスタ ssm2" +
                                "   on jzd.仕入先コード = ssm2.仕入先コード" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(jkkd.図面番号,'/','／')" +
                                " left join 図面データ zd2" +
                                "   on zd2.ファイル like replace('%jkkd.図面番号%','/','／')" +
                                " left join 作業者マスタ sam" +
                                "   on jkkd.担当者コード = sam.作業者コード" +
                                " left join オーダー管理状態テーブル okj" +
                                "   on jkkd.\"オーダーNO\" = okj.\"オーダーNO\" and jkkd.グループ = okj.グループ" +
                                " left join オーダー管理組立有無テーブル okku" +
                                "   on jkkd.\"オーダーNO\" = okku.\"オーダーNO\" and jkkd.グループ = okku.グループ" +
                                " left join 図面作成データ zsd" +
                                "   on jkkd.図面番号 = zsd.図面番号" +
                                " left join 作業者マスタ sam2" +
                                "   on zsd.作業者 = sam2.作業者コード";

                sqlCmdTxt += " where jkkd.工程表番号 IN (";

                for (int i = 0; i < KouteiNoList.Count(); i++)
                {
                    if (i < (KouteiNoList.Count() - 1))
                    {
                        sqlCmdTxt += ":param" + i + ", ";
                    }
                    else
                    {
                        sqlCmdTxt += ":param" + i;
                    }
                }
                sqlCmdTxt += ")";

                cmd = new NpgsqlCommand(sqlCmdTxt, con);

                for (int j = 0; j < KouteiNoList.Count(); j++)
                {
                    cmd.Parameters.Add(new NpgsqlParameter("param" + j, DbType.String) { Value = KouteiNoList[j] });
                }

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        JissekiKakou JisKakData = new JissekiKakou();
                        JisKakData.SearchMethod = 1;
                        JisKakData.OrderNo = CommonModule.ConvertToString(result["オーダーNO"]);
                        JisKakData.Group = CommonModule.ConvertToString(result["グループ"]);
                        JisKakData.Renno = CommonModule.ConvertToString(result["連番"]);
                        JisKakData.KouteihyouNo = CommonModule.ConvertToString(result["工程表番号"]);
                        JisKakData.BlockNo = CommonModule.ConvertToString(result["引合NO"]);
                        JisKakData.ZumenBan = CommonModule.ConvertToString(result["図面番号"]);
                        JisKakData.ProductName = CommonModule.ConvertToString(result["品名"]);
                        JisKakData.KakoQty = CommonModule.ConvertToString(result["加工数量"]);
                        JisKakData.ZumenPath = CommonModule.ConvertToString(result["図面パス"]);
                        if (JisKakData.ZumenPath != "")
                        {
                            JisKakData.FileName = System.IO.Path.GetFileNameWithoutExtension(JisKakData.ZumenPath);
                        }
                        JisKakData.ChumonsyoNo = CommonModule.ConvertToString(result["注文書番号"]);
                        JisKakData.TantouCd = CommonModule.ConvertToString(result["担当者コード"]);
                        JisKakData.Tantou = CommonModule.ConvertToString(result["担当者"]);
                        JisKakData.KakouTantou = CommonModule.ConvertToString(result["加工担当者"]);
                        JisKakData.Kakosaki = CommonModule.ConvertToString(result["加工先"]);
                        JisKakData.Nouki = CommonModule.ConvertToDateString(result["納期"]);
                        JisKakData.KyakuNouki = CommonModule.ConvertToDateString(result["客先指定納期"]);
                        JisKakData.Sinchoku = CommonModule.ConvertToString(result["作業状況名"]);
                        JisKakData.ZaiNyuka = CommonModule.ConvertToString(result["材料入荷先"]);
                        JisKakData.Zairyo = CommonModule.ConvertToString(result["材質"]);
                        JisKakData.ZaiSize = CommonModule.ConvertToString(result["サイズ"]);
                        JisKakData.ZaiQty = CommonModule.ConvertToString(result["材料数量"]);
                        JisKakData.NyukaDate = CommonModule.ConvertToDateString(result["入荷日"]);
                        JisKakData.isAdvance = CommonModule.ConvertToInt(result["先行手配"]);
                        JisKakData.isKumitate = CommonModule.ConvertToInt(result["組立有無"]);
                        list.Add(JisKakData);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                }
            }
            return list;
        }

        /// <summary>
        /// 図面情報を抽出する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="scanString"></param>
        /// <returns></returns>
        private JissekiKakou GetZumenInfo(string connectionString, string scanString)
        {
            var JisKakData = new JissekiKakou();
            var cmd = new NpgsqlCommand();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  jkkd.\"オーダーNO\"" +
                                "  ,jkkd.グループ" +
                                "  ,jkkd.連番" +
                                "  ,jkkd.工程表番号" +
                                "  ,jkkd.\"引合NO\"" +
                                "  ,jkkd.図面番号" +
                                "  ,jkkd.品名" +
                                "  ,jkkd.数量 as 加工数量" +
                                "  ,jkkd.納期" +
                                "  ,jkkd.客先指定納期" +
                                "  ,case when jkkd.図面パス is not null then jkkd.図面パス " +
                                "   when zd.パス is not null then zd.パス " +
                                "   when zd2.パス is not null then zd2.パス end as 図面パス" +
                                "  ,jkkd.担当者コード as 担当者コード" +
                                "  ,sam.作業者名 as 担当者" +
                                "  ,sam2.作業者名 as 加工担当者" +
                                "  ,jkkd.注文書番号" +
                                "  ,jkkd.注文書行番号" +
                                "  ,ssm.仕入先略名 as 加工先" +
                                "  ,sjm.作業状況名" +
                                "  ,ssm2.仕入先略名 as 材料入荷先" +
                                "  ,jzd.材質" +
                                "  ,jzd.サイズ" +
                                "  ,jzd.数量 as 材料数量" +
                                "  ,jzd.納期 as 入荷日" +
                                "  ,okj.先行手配" +
                                "  ,okku.組立有無" +
                                " from 実績加工データ jkkd" +
                                " left join 仕入先マスタ ssm" +
                                "   on jkkd.加工先コード = ssm.仕入先コード" +
                                " left join 検査実績データ kjd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 作業状況マスタ sjm" +
                                "   on sjm.作業状況コード = kjd.作業状況" +
                                " left join 実績材料データ jzd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 仕入先マスタ ssm2" +
                                "   on jzd.仕入先コード = ssm2.仕入先コード" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(jkkd.図面番号,'/','／')" +
                                " left join 図面データ zd2" +
                                "   on zd2.ファイル like replace('%jkkd.図面番号%','/','／')" +
                                " left join 作業者マスタ sam" +
                                "   on jkkd.担当者コード = sam.作業者コード" +
                                " left join オーダー管理状態テーブル okj" +
                                "   on jkkd.\"オーダーNO\" = okj.\"オーダーNO\" and jkkd.グループ = okj.グループ" +
                                " left join オーダー管理組立有無テーブル okku" +
                                "   on jkkd.\"オーダーNO\" = okku.\"オーダーNO\" and jkkd.グループ = okku.グループ" +
                                " left join 図面作成データ zsd" +
                                "   on jkkd.図面番号 = zsd.図面番号" +
                                " left join 作業者マスタ sam2" +
                                "   on zsd.作業者 = sam2.作業者コード";

                if (scanString.Length == 8)
                {
                    // 工程表番号で検索
                    var kouteihyouNo = scanString;
                    sqlCmdTxt += " where jkkd.工程表番号 = UPPER(:kouteihyouNo)" +
                                 " order by jkkd.更新日時";
                    cmd = new NpgsqlCommand(sqlCmdTxt, con);
                    cmd.Parameters.Add(new NpgsqlParameter("kouteihyouNo", DbType.String) { Value = kouteihyouNo });
                }
                else if (scanString.Length == 11)
                {
                    // 注文書番号で検索
                    var chumonsyoNo = scanString.Substring(0, 8);
                    var chumonsyoGyoNo = int.Parse(scanString.Substring(8, 3));
                    sqlCmdTxt += " where jkkd.注文書番号 = UPPER(:chumonsyoNo)" +
                                 " and jkkd.注文書行番号 = :chumonsyoGyoNo" +
                                 " order by jkkd.更新日時";
                    cmd = new NpgsqlCommand(sqlCmdTxt, con);
                    cmd.Parameters.Add(new NpgsqlParameter("chumonsyoNo", DbType.String) { Value = chumonsyoNo });
                    cmd.Parameters.Add(new NpgsqlParameter("chumonsyoGyoNo", DbType.Int32) { Value = chumonsyoGyoNo });
                }
                else
                {
                    JisKakData = new JissekiKakou();
                    return JisKakData;
                }

                try
                {
                    var result = cmd.ExecuteReader();
                    var IsHasData = false;
                    while (result.Read())
                    {
                        if (IsHasData)
                        {
                            JisKakData = new JissekiKakou();
                            return JisKakData;
                        }
                        JisKakData = new JissekiKakou();
                        JisKakData.SearchMethod = 1;
                        JisKakData.OrderNo = CommonModule.ConvertToString(result["オーダーNO"]);
                        JisKakData.Group = CommonModule.ConvertToString(result["グループ"]);
                        JisKakData.Renno = CommonModule.ConvertToString(result["連番"]);
                        JisKakData.KouteihyouNo = CommonModule.ConvertToString(result["工程表番号"]);
                        JisKakData.BlockNo = CommonModule.ConvertToString(result["引合NO"]);
                        JisKakData.ZumenBan = CommonModule.ConvertToString(result["図面番号"]);
                        JisKakData.ProductName = CommonModule.ConvertToString(result["品名"]);
                        JisKakData.KakoQty = CommonModule.ConvertToString(result["加工数量"]);
                        JisKakData.ZumenPath = CommonModule.ConvertToString(result["図面パス"]);
                        if (JisKakData.ZumenPath != "")
                        {
                            JisKakData.FileName = System.IO.Path.GetFileNameWithoutExtension(JisKakData.ZumenPath);
                        }
                        JisKakData.ChumonsyoNo = CommonModule.ConvertToString(result["注文書番号"]);
                        JisKakData.TantouCd = CommonModule.ConvertToString(result["担当者コード"]);
                        JisKakData.Tantou = CommonModule.ConvertToString(result["担当者"]);
                        JisKakData.KakouTantou = CommonModule.ConvertToString(result["加工担当者"]);
                        JisKakData.Kakosaki = CommonModule.ConvertToString(result["加工先"]);
                        JisKakData.Nouki = CommonModule.ConvertToDateString(result["納期"]);
                        JisKakData.KyakuNouki = CommonModule.ConvertToDateString(result["客先指定納期"]);
                        JisKakData.Sinchoku = CommonModule.ConvertToString(result["作業状況名"]);
                        JisKakData.ZaiNyuka = CommonModule.ConvertToString(result["材料入荷先"]);
                        JisKakData.Zairyo = CommonModule.ConvertToString(result["材質"]);
                        JisKakData.ZaiSize = CommonModule.ConvertToString(result["サイズ"]);
                        JisKakData.ZaiQty = CommonModule.ConvertToString(result["材料数量"]);
                        JisKakData.NyukaDate = CommonModule.ConvertToDateString(result["入荷日"]);
                        JisKakData.isAdvance = CommonModule.ConvertToInt(result["先行手配"]);
                        JisKakData.isKumitate = CommonModule.ConvertToInt(result["組立有無"]);
                        IsHasData = true;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    JisKakData = new JissekiKakou();
                    return JisKakData;
                }
            }
            return JisKakData;
        }

        /// <summary>
        /// 同一図面の数を抽出
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <returns></returns>
        private int GetSameZumenCount(string connectionString, string zumenBan)
        {
            var cmd = new NpgsqlCommand();

            var cnt = 0;

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  count(*) as cnt" +
                                " from 実績加工データ jkkd" +
                                " left join 仕入先マスタ ssm" +
                                "   on jkkd.加工先コード = ssm.仕入先コード" +
                                " left join 検査実績データ kjd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 作業状況マスタ sjm" +
                                "   on sjm.作業状況コード = kjd.作業状況" +
                                " left join 図面作成データ zsd" +
                                "   on jkkd.図面番号 = zsd.図面番号" +
                                " left join 作業者マスタ sgm" +
                                "   on zsd.作業者 = sgm.作業者コード" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(jkkd.図面番号,'/','／')" +
                                " left join 図面データ zd2" +
                                "   on zd2.ファイル like replace('%jkkd.図面番号%','/','／')" +
                                " left join 作業者マスタ sam" +
                                "   on jkkd.担当者コード = sam.作業者コード" +
                                " left join オーダー管理状態テーブル okj" +
                                "   on jkkd.\"オーダーNO\" = okj.\"オーダーNO\" and jkkd.グループ = okj.グループ" +
                                " left join オーダー管理組立有無テーブル okku" +
                                "   on jkkd.\"オーダーNO\" = okku.\"オーダーNO\" and jkkd.グループ = okku.グループ" +
                                " where jkkd.\"図面番号\" = UPPER(:zumenBan)" +
                                "   and jkkd.納期 >= now() + '-1 year'" +
                                "   and kjd.作業状況 is null";
                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = zumenBan });

                try
                {
                    var result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        cnt = CommonModule.ConvertToInt(result["cnt"]);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return -1;
                }
            }
            return cnt;
        }

        public void ScanDialog_Load(object sender, EventArgs e)
        {
            // バーコードリーダの内容を設定
            this.textBox1.Text = this.Barcode;
        }

    }
}
