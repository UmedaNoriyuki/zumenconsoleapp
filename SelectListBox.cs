﻿
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ZumenConsoleApp
{
    public partial class SelectListBox : Form
    {
        #region クラス変数

        // ListBoxの内容
        public List<KeyValuePairDto> listBox = new List<KeyValuePairDto>();

        // 選択内容
        public string SelectValue = string.Empty;
        public string SelectKey = string.Empty;

        #endregion

        public SelectListBox()
        {
            InitializeComponent();
        }

        #region イベント

        /// <summary>
        /// 画面起動時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadListBox(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < listBox.Count; i++)
                {
                    this.selectListBox.Items.Add(listBox[i]);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 選択ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickSelectBtn(object sender, EventArgs e)
        {
            try 
            {
                if (selectListBox.SelectedIndex >= 0)
                {
                    this.SelectValue = ((KeyValuePairDto)selectListBox.SelectedItem).Value.ToString();
                    this.SelectKey = ((KeyValuePairDto)selectListBox.SelectedItem).Key.ToString();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("選択されていません。");
                    return;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// リストボックスダブルクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoubleClicklistBox(object sender, EventArgs e)
        {
            try
            {
                if (selectListBox.SelectedIndex >= 0)
                {
                    this.SelectValue = ((KeyValuePairDto)selectListBox.SelectedItem).Value.ToString();
                    this.SelectKey = ((KeyValuePairDto)selectListBox.SelectedItem).Key.ToString();
                    this.Close();
                }
            }
            catch
            {
                throw;
            }

        }

        #endregion

    }
}
