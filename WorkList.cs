﻿
namespace ZumenConsoleApp
{
    public class WorkList
    {
        public string OrderNo { get; set; }
        public string BlockNo { get; set; }
        public string Jisseki { get; set; }
        public string Worker { get; set; }
        public string Mitumori { get; set; }
        public string Mokuhyo { get; set; }
        public string StartTime { get; set; }
        public string ReStartTime { get; set; }
        public string WorkState { get; set; }
        public string SumWorkTime { get; set; }
    }
}
