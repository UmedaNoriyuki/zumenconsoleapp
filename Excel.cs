﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace ZumenConsoleApp
{
    class Excel
    {
        #region public関数

        /// <summary>
        /// Excel出力を行います
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="totalAmount"></param>
        public void OutputExcel(DataTable dt, string titile, List<string> hedders,string zumenName)
        {
            try
            {
                // 出力先を設定します
                string outputPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\" + titile + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
                int count = 0;
                bool existFlg = true;

                // 同じ名前のファイルがあるかチェック
                while (existFlg)
                {
                    if (File.Exists(outputPath))
                    {
                        count++;
                        outputPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\" + titile + DateTime.Now.ToString("yyyyMMdd") +"(" + count +")" + ".xlsx";
                    }
                    else
                    {
                        existFlg = false;
                    }
                }

                // Excelファイルを作成します
                CreateExcel(outputPath, dt, titile, hedders, zumenName);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region private関数

        /// <summary>
        /// Excelを作成します
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="dt"></param>
        /// <param name="titile"></param>
        /// <param name="hedders"></param>
        private void CreateExcel(string filePath, DataTable dt, string titile, List<string> hedders,string zumenName)
        {
            try
            {
                // ブック作成
                var book = CreateNewBook(filePath);

                // シート作成
                book.CreateSheet(titile);
                ISheet sheet = book.GetSheet(titile);

                int rowCount = 0;
                int colCount = 0;

                WriteCell(book, sheet, colCount, rowCount, "図面番号") ;
                colCount++;
                WriteCell(book, sheet, colCount, rowCount, zumenName);
                rowCount++;
                rowCount++;

                colCount = 0;
                // ヘッダー書き込み
                foreach (var hedder in hedders)
                {
                    WriteCell(book, sheet, colCount, rowCount, hedder);
                    colCount++;
                }

                foreach (DataRow dr in dt.Rows)
                {
                    rowCount++;
                    colCount = 0;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        // 数値の場合
                        if (double.TryParse(dr[dc.Caption].ToString(), out double d))
                        {
                            WriteCell(book, sheet, colCount, rowCount, d);
                        }
                        // 日付の場合
                        else if(DateTime.TryParse(dr[dc.Caption].ToString(), out DateTime date))
                        {
                            WriteCell(book, sheet, colCount, rowCount, date);
                        }
                        // 文字列
                        else
                        {
                            WriteCell(book, sheet, colCount, rowCount, dr[dc.Caption].ToString());
                        }
                        colCount++;
                    }
                }

                colCount = 0;
                // 列幅自動調整
                foreach (var hedder in hedders)
                {
                    sheet.AutoSizeColumn(colCount, true);
                    colCount++;
                }

                // Excelを保存
                using (var fs = new FileStream(filePath, FileMode.Create))
                {
                    book.Write(fs);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// ブックを作成します
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private IWorkbook CreateNewBook(string filePath)
        {
            try
            {
                IWorkbook book;
                var extension = Path.GetExtension(filePath);

                // HSSF => Microsoft Excel(xls形式)(excel 97-2003)
                // XSSF => Office Open XML Workbook形式(xlsx形式)(excel 2007以降)
                if (extension == ".xls")
                {
                    book = new HSSFWorkbook();
                }
                else if (extension == ".xlsx")
                {
                    book = new XSSFWorkbook();
                }
                else
                {
                    throw new ApplicationException("CreateNewBook: invalid extension");
                }

                return book;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Cell設定(日付)
        /// </summary>
        /// <param name="book"></param>
        /// <param name="sheet"></param>
        /// <param name="columnIndex"></param>
        /// <param name="rowIndex"></param>
        /// <param name="value"></param>
        private void WriteCell(IWorkbook book, ISheet sheet, int columnIndex, int rowIndex, DateTime value)
        {
            try
            {
                IRow row = sheet.GetRow(rowIndex) ?? sheet.CreateRow(rowIndex);
                ICell cell = row.GetCell(columnIndex) ?? row.CreateCell(columnIndex);
                // セル書式を設定 
                cell.CellStyle = GetCellStyle(book);
                // 日付はYYYY年MM月DD日で表示
                cell.CellStyle.DataFormat = book.CreateDataFormat().GetFormat("YYYY年M月D日");
                // 値書き込み
                cell.SetCellValue(value);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Cell設定(文字列)
        /// </summary>
        /// <param name="book"></param>
        /// <param name="sheet"></param>
        /// <param name="columnIndex"></param>
        /// <param name="rowIndex"></param>
        /// <param name="value"></param>
        private void WriteCell(IWorkbook book, ISheet sheet, int columnIndex, int rowIndex, string value)
        {
            try
            {
                IRow row = sheet.GetRow(rowIndex) ?? sheet.CreateRow(rowIndex);
                ICell cell = row.GetCell(columnIndex) ?? row.CreateCell(columnIndex);
                // セル書式を設定 
                cell.CellStyle = GetCellStyle(book);
                // 値書き込み
                cell.SetCellValue(value);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Cell設定(数値)
        /// </summary>
        /// <param name="book"></param>
        /// <param name="sheet"></param>
        /// <param name="columnIndex"></param>
        /// <param name="rowIndex"></param>
        /// <param name="value"></param>
        private void WriteCell(IWorkbook book, ISheet sheet, int columnIndex, int rowIndex, double value)
        {
            try
            {
                IRow row = sheet.GetRow(rowIndex) ?? sheet.CreateRow(rowIndex);
                ICell cell = row.GetCell(columnIndex) ?? row.CreateCell(columnIndex);
                // セル書式を設定 
                cell.CellStyle = GetCellStyle(book);
                // 値書き込み
                cell.SetCellValue(value);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// セルの書式を設定します
        /// </summary>
        /// <returns></returns>
        private ICellStyle GetCellStyle(IWorkbook book)
        {
            try
            {
                ICellStyle cellStyle = book.CreateCellStyle();
                // 文字書式を設定 
                cellStyle.SetFont(GetFontStyle(book));
                // 罫線の設定をします
                // 罫線を引くと時間がかかる
                cellStyle = SetCellBorder(cellStyle);
                // 水平方向設定
                cellStyle.VerticalAlignment = VerticalAlignment.Center;

                return cellStyle;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 文字の設定をします
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        private IFont GetFontStyle(IWorkbook book)
        {
            try
            {
                IFont font = book.CreateFont();
                font.FontName = "メイリオ";
                font.FontHeightInPoints = 11;

                return font;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 罫線の設定をします
        /// </summary>
        /// <param name="cellStyle"></param>
        /// <returns></returns>
        private ICellStyle SetCellBorder(ICellStyle cellStyle)
        {
            try
            {
                // 罫線(上)
                cellStyle.BorderTop = BorderStyle.Thin;
                // 罫線(下) 
                cellStyle.BorderBottom = BorderStyle.Thin;
                // 罫線(左)
                cellStyle.BorderLeft = BorderStyle.Thin;
                // 罫線(右)
                cellStyle.BorderRight = BorderStyle.Thin;

                return cellStyle;
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
