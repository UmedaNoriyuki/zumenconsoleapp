﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class Calendar : Form
    {

        #region クラス変数       

        private bool LoadFlg = false;

        public DateTime Date = DateTime.Now;

        #endregion

        #region コンストラクタ

        public Calendar()
        {
            InitializeComponent();
        }

        #endregion

        #region イベント

        /// <summary>
        /// 画面ロード処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void View_F100_ClientCalendar_Load(object sender, EventArgs e)
        {
            try
            {
                // ダブルバッファリングを有効にする
                this.DoubleBuffered = true;

                // 年コンボボックスを作成します
                GetYearCmb();

                // コンボボックス初期値
                this.cmb_year.SelectedIndex = this.cmb_year.FindStringExact(DateTime.Now.ToString("yyyy"));
                this.cmb_month.SelectedIndex = this.cmb_month.FindStringExact(DateTime.Now.ToString("MM"));

                // 項目の設定
                SetCalendar();


                LoadFlg = true;

            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// DateLabelクリックイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickDateLabel(object sender, EventArgs e)
        {
            try
            {
                string label = ((Label)sender).Text;

                if (string.IsNullOrWhiteSpace(label))
                {
                    return;
                }
                string date = cmb_year.Text + "/" + cmb_month.Text + "/" + label.Substring(0, 2) + " 00:00:00";
                this.Date = DateTime.Parse(date);
                this.Close();
            }
            catch
            {
                throw;
            }
        }

        private void ClickNextDateLabel(object sender, EventArgs e)
        {
            try
            {
                string label = ((Label)sender).Text;

                if (string.IsNullOrWhiteSpace(label))
                {
                    return;
                }
                string month = txtNextMonth.Text.Substring(0, 2);
                string year = string.Empty;

                if (!month.Equals("01"))
                {
                    year = cmb_year.Text;
                }
                else
                {
                    int i;
                    if (int.TryParse(cmb_year.Text, out i))
                    {
                        year = (i + 1).ToString();
                    }
                    else
                    {
                        year = cmb_year.Text;
                    }
                }

                string date = year + "/" + month + "/" + label.Substring(0, 2) + " 00:00:00";              
                
                this.Date = DateTime.Parse(date);
                this.Close();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 月コンボボックスTextChangedイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextChangedCmb_month(object sender, EventArgs e)
        {
            try
            {
                if (LoadFlg)
                {
                    // 項目の設定
                    SetCalendar();
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 年コンボボックスTextChangedイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextChangedCmb_year(object sender, EventArgs e)
        {
            try
            {
                if (LoadFlg)
                {
                    // 項目の設定
                    SetCalendar();
                }
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// 仕入先コンボボックスTextChangedイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextChangedCmb_shiiresaki(object sender, EventArgs e)
        {
            try
            {
                if (LoadFlg)
                {
                    // 項目の設定
                    SetCalendar();
                }
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// 項目の設定を設定します
        /// </summary>
        private void SetCalendar()
        {
            try
            {
                // データを取得
                DataTable dt = Getカレンダーマスタ();

                if (dt.Rows.Count <= 0)
                {
                    // 既存の内容を消す
                    for (int i = 1; i <= 37; i++)
                    {
                        Control[] cList = this.Controls.Find("label" + i.ToString(), true);
                        ((Label)cList[0]).Text = string.Empty;
                    }
                    MessageBox.Show("登録されていません。");
                }
                else
                {
                    // 画面項目を設定します。
                    SetForm();
                    SetNextMonthForm();
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region private関数

        /// <summary>
        /// 対象月コンボボックス作成
        /// </summary>
        private void GetYearCmb()
        {
            try
            {
                this.cmb_year.Items.Add(DateTime.Now.ToString("yyyy"));
                this.cmb_year.Items.Add(DateTime.Now.AddYears(1).ToString("yyyy"));
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 取引先カレンダーマスタを取得します
        /// </summary>
        private void SetForm()
        {
            try
            {
                // 既存の内容を消す
                for (int i = 1; i <= 37; i++)
                {
                    Control[] cList = this.Controls.Find("label" + i.ToString(), true);
                    ((Label)cList[0]).Text = string.Empty;
                }

                // データを取得
                DataTable dt = Getカレンダーマスタ();
                string day = DateTime.Parse(dt.Rows[0]["日付"].ToString()).ToString("ddd");
                string startLabel = GetStartLabel(day);
                int count = int.Parse(startLabel.Substring(startLabel.Length - 1, 1));
                foreach (DataRow dr in dt.Rows)
                {
                    Control[] cList = this.Controls.Find("label" + count.ToString(), true);
                    ((Label)cList[0]).Text = DateTime.Parse(dr["日付"].ToString()).ToString("dd") + Environment.NewLine + dr["休日フラグ"].ToString();
                    DateTime date = DateTime.Now;

                    if (date.ToString("yyyyMMdd").Equals(DateTime.Parse(dr["日付"].ToString()).ToString("yyyyMMdd")))
                    {
                        ((Label)cList[0]).BackColor = SystemColors.ControlDark;
                    }
                    else
                    {
                        ((Label)cList[0]).BackColor = SystemColors.Control;
                    }

                    if (dr["休日フラグ"].ToString().Equals("休業日"))
                    {
                        ((Label)cList[0]).ForeColor = Color.Red;
                    }
                    else
                    {
                        ((Label)cList[0]).ForeColor = Color.Black;
                    }
                    count++;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 取引先カレンダーマスタを取得します
        /// </summary>
        private void SetNextMonthForm()
        {
            try
            {
                // 既存の内容を消す
                for (int i = 1; i <= 37; i++)
                {
                    Control[] cList = this.Controls.Find("label" + (100+i).ToString(), true);
                    ((Label)cList[0]).Text = string.Empty;
                }

                // データを取得
                DataTable dt = GetNextMonthカレンダーマスタ();
                if(dt.Rows.Count < 1)
                {
                    return;
                }
                string day = DateTime.Parse(dt.Rows[0]["日付"].ToString()).ToString("ddd");
                string startLabel = GetStartLabel(day);
                int count = int.Parse(startLabel.Substring(startLabel.Length - 1, 1)) + 100;
                foreach (DataRow dr in dt.Rows)
                {
                    Control[] cList = this.Controls.Find("label" + count.ToString(), true);
                    ((Label)cList[0]).Text = DateTime.Parse(dr["日付"].ToString()).ToString("dd") + Environment.NewLine + dr["休日フラグ"].ToString();

                    DateTime date = DateTime.Now;

                    if (date.ToString("yyyyMMdd").Equals(DateTime.Parse(dr["日付"].ToString()).ToString("yyyyMMdd")))
                    {
                        ((Label)cList[0]).BackColor = SystemColors.ControlDark;
                    }
                    else
                    {
                        ((Label)cList[0]).BackColor = SystemColors.Control;
                    }

                    if (dr["休日フラグ"].ToString().Equals("休業日"))
                    {
                        ((Label)cList[0]).ForeColor = Color.Red;
                    }
                    else
                    {
                        ((Label)cList[0]).ForeColor = Color.Black;
                    }
                    count++;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 月初めのラベルを取得します
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        private string GetStartLabel(string day)
        {
            try
            {
                string startLabel = string.Empty;

                switch (day)
                {
                    case "日":
                        startLabel = "label1";
                        break;
                    case "月":
                        startLabel = "label2";
                        break;
                    case "火":
                        startLabel = "label3";
                        break;
                    case "水":
                        startLabel = "label4";
                        break;
                    case "木":
                        startLabel = "label5";
                        break;
                    case "金":
                        startLabel = "label6";
                        break;
                    case "土":
                        startLabel = "label7";
                        break;
                    default:
                        break;
                }

                return startLabel;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 取引先カレンダーマスタを取得します
        /// </summary>
        private DataTable Getカレンダーマスタ()
        {
            try
            {
                string date = cmb_year.Text + "/" + cmb_month.Text + "/1 00:00:00";
                

                DataTable dt = Getカレンダーマスタ(DateTime.Parse(date));

                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 取引先カレンダーマスタを取得します
        /// </summary>
        private DataTable GetNextMonthカレンダーマスタ()
        {
            try
            {
                string date = cmb_year.Text + "/" + cmb_month.Text + "/1 00:00:00";

                DataTable dt = Getカレンダーマスタ(DateTime.Parse(date).AddMonths(1));
                txtNextMonth.Text = DateTime.Parse(date).AddMonths(1).ToString("MM") + "月";

                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// カレンダー取得
        /// </summary>
        /// <returns></returns>
        private DataTable Getカレンダーマスタ(DateTime 年月)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   日付");
                    query.Append("  ,CASE 休日フラグ ");
                    query.Append("        WHEN FALSE THEN '営業日' ");
                    query.Append("        WHEN TRUE THEN '休業日' ");
                    query.Append("    END 休日フラグ ");
                    query.Append("  FROM ");
                    query.Append("   社内カレンダーマスタ ");
                    query.Append(" WHERE  ");
                    query.Append("   日付 BETWEEN :年月from  AND :年月to  ");
                    query.Append(" ORDER BY 日付 ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("年月from", NpgsqlDbType.Timestamp));
                    cmd.Parameters["年月from"].Value = 年月;

                    cmd.Parameters.Add(new NpgsqlParameter("年月to", NpgsqlDbType.Timestamp));
                    cmd.Parameters["年月to"].Value = 年月.AddMonths(1).AddDays(-1);

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    // 接続を閉じる
                    con.Close();
                    con.Dispose();

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
