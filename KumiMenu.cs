﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class KumiMenu : BaseView
    {
        public KumiMenu()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 作業時間計算ボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCalcWorkTime_Click(object sender, EventArgs e)
        {
            try
            {
                // 作業時間計算ダイアログ表示
                var a = new CalcWorkTime();
                a.ShowDialog();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        /// <summary>
        /// パーツリストボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPartsListDisp_Click(object sender, EventArgs e)
        {
            try
            {
                // パーツリストダイアログ表示
                var a = new PartsListDisp();
                a.Show();

                // メニューは閉じる
                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }
    }
}
