﻿namespace ZumenConsoleApp.View
{
    partial class SearchRireki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.txtZumenBan = new System.Windows.Forms.TextBox();
            this.btnOrderNoClr = new System.Windows.Forms.Button();
            this.btnZumenBanClr = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtKisyu = new System.Windows.Forms.TextBox();
            this.btnKisyuClr = new System.Windows.Forms.Button();
            this.chkOnlyRecentRireki = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // searchBtn
            // 
            this.searchBtn.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.searchBtn.Location = new System.Drawing.Point(242, 231);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(145, 62);
            this.searchBtn.TabIndex = 10;
            this.searchBtn.Text = "検索";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(10, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "オーダーNo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(10, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 24);
            this.label3.TabIndex = 3;
            this.label3.Text = "図面番号";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo.Location = new System.Drawing.Point(130, 40);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(180, 31);
            this.txtOrderNo.TabIndex = 1;
            this.txtOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOrderNo_KeyDown);
            // 
            // txtZumenBan
            // 
            this.txtZumenBan.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZumenBan.Location = new System.Drawing.Point(130, 90);
            this.txtZumenBan.Name = "txtZumenBan";
            this.txtZumenBan.Size = new System.Drawing.Size(180, 31);
            this.txtZumenBan.TabIndex = 4;
            this.txtZumenBan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtZumenBan_KeyDown);
            // 
            // btnOrderNoClr
            // 
            this.btnOrderNoClr.Location = new System.Drawing.Point(320, 40);
            this.btnOrderNoClr.Name = "btnOrderNoClr";
            this.btnOrderNoClr.Size = new System.Drawing.Size(50, 31);
            this.btnOrderNoClr.TabIndex = 2;
            this.btnOrderNoClr.Text = "クリア";
            this.btnOrderNoClr.UseVisualStyleBackColor = true;
            this.btnOrderNoClr.Click += new System.EventHandler(this.btnOrderNoClr_Click);
            // 
            // btnZumenBanClr
            // 
            this.btnZumenBanClr.Location = new System.Drawing.Point(320, 90);
            this.btnZumenBanClr.Name = "btnZumenBanClr";
            this.btnZumenBanClr.Size = new System.Drawing.Size(50, 31);
            this.btnZumenBanClr.TabIndex = 5;
            this.btnZumenBanClr.Text = "クリア";
            this.btnZumenBanClr.UseVisualStyleBackColor = true;
            this.btnZumenBanClr.Click += new System.EventHandler(this.btnZumenBanClr_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(10, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 24);
            this.label6.TabIndex = 6;
            this.label6.Text = "機種名";
            // 
            // txtKisyu
            // 
            this.txtKisyu.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKisyu.Location = new System.Drawing.Point(130, 140);
            this.txtKisyu.Name = "txtKisyu";
            this.txtKisyu.Size = new System.Drawing.Size(180, 31);
            this.txtKisyu.TabIndex = 7;
            this.txtKisyu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKisyu_KeyDown);
            // 
            // btnKisyuClr
            // 
            this.btnKisyuClr.Location = new System.Drawing.Point(320, 140);
            this.btnKisyuClr.Name = "btnKisyuClr";
            this.btnKisyuClr.Size = new System.Drawing.Size(50, 31);
            this.btnKisyuClr.TabIndex = 8;
            this.btnKisyuClr.Text = "クリア";
            this.btnKisyuClr.UseVisualStyleBackColor = true;
            this.btnKisyuClr.Click += new System.EventHandler(this.btnKisyuClr_Click);
            // 
            // chkOnlyRecentRireki
            // 
            this.chkOnlyRecentRireki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkOnlyRecentRireki.Location = new System.Drawing.Point(110, 190);
            this.chkOnlyRecentRireki.Name = "chkOnlyRecentRireki";
            this.chkOnlyRecentRireki.Size = new System.Drawing.Size(265, 20);
            this.chkOnlyRecentRireki.TabIndex = 9;
            this.chkOnlyRecentRireki.Text = "最後に編集した図面の履歴のみ表示";
            this.chkOnlyRecentRireki.UseVisualStyleBackColor = true;
            // 
            // SearchRireki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 308);
            this.Controls.Add(this.chkOnlyRecentRireki);
            this.Controls.Add(this.btnKisyuClr);
            this.Controls.Add(this.txtKisyu);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnZumenBanClr);
            this.Controls.Add(this.btnOrderNoClr);
            this.Controls.Add(this.txtZumenBan);
            this.Controls.Add(this.txtOrderNo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchBtn);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchRireki";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "履歴検索";
            this.Load += new System.EventHandler(this.SearchEntry_Load);
            this.Controls.SetChildIndex(this.searchBtn, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtOrderNo, 0);
            this.Controls.SetChildIndex(this.txtZumenBan, 0);
            this.Controls.SetChildIndex(this.btnOrderNoClr, 0);
            this.Controls.SetChildIndex(this.btnZumenBanClr, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.txtKisyu, 0);
            this.Controls.SetChildIndex(this.btnKisyuClr, 0);
            this.Controls.SetChildIndex(this.chkOnlyRecentRireki, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.TextBox txtZumenBan;
        private System.Windows.Forms.Button btnOrderNoClr;
        private System.Windows.Forms.Button btnZumenBanClr;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtKisyu;
        private System.Windows.Forms.Button btnKisyuClr;
        private System.Windows.Forms.CheckBox chkOnlyRecentRireki;
    }
}