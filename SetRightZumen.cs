﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class SetRightZumen : BaseView
    {
        public SetRightZumen()
        {
            InitializeComponent();
        }

        private void SetRightZumen_Load(object sender, EventArgs e)
        {
            try
            {
                foreach (var blItem in MainDisp.BlockList)
                {
                    var blockTag = CommonModule.ConvertToString(blItem).Split('_');
                    var selRightBlockTag = FullDisp.selRightBlockNo.Split('_');
                    TreeNode itm = new TreeNode();
                    itm.Text = blockTag[0];
                    itm.Tag = blockTag[1];
                    rightZumenList.Nodes.Add(itm);
                    if (blockTag[0].Contains("組立図") && selRightBlockTag[1] == "k" && blockTag[1] == selRightBlockTag[0])
                    {
                        rightZumenList.SelectedNode = itm;
                        rightZumenList.SelectedNode.BackColor = Color.Yellow;
                    }
                    else if (blockTag[0].Contains("部品図") && selRightBlockTag[1] == "b" && blockTag[1] == selRightBlockTag[0])
                    {
                        rightZumenList.SelectedNode = itm;
                        rightZumenList.SelectedNode.BackColor = Color.Yellow;
                    }
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 設定ボタン押下時処理
        private void setBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (rightZumenList.SelectedNode == null || rightZumenList.SelectedNode.Index < 0)
                {
                    return;
                }
                if (rightZumenList.SelectedNode.Text.Contains("組立図"))
                {
                    FullDisp.selRightBlockNo = rightZumenList.SelectedNode.Tag + "_k";
                }
                else
                {
                    FullDisp.selRightBlockNo = rightZumenList.SelectedNode.Tag + "_b";
                }
                FullDisp.isRightZumen = true;
                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
