﻿namespace ZumenConsoleApp.View
{
    partial class SelectOrderNo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.orderNoList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // orderNoList
            // 
            this.orderNoList.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.orderNoList.FormattingEnabled = true;
            this.orderNoList.ItemHeight = 48;
            this.orderNoList.Location = new System.Drawing.Point(25, 33);
            this.orderNoList.Name = "orderNoList";
            this.orderNoList.Size = new System.Drawing.Size(245, 532);
            this.orderNoList.TabIndex = 1;
            this.orderNoList.DoubleClick += new System.EventHandler(this.orderNoList_DoubleClick);
            // 
            // SelectOrderNo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(296, 618);
            this.Controls.Add(this.orderNoList);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectOrderNo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "オーダーNO選択";
            this.Load += new System.EventHandler(this.SelectOrderNo_Load);
            this.Controls.SetChildIndex(this.orderNoList, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox orderNoList;
    }
}