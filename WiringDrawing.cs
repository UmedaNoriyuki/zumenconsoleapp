﻿using AxDESKCTRLLib;
using FujiXerox.DocuWorks.Toolkit;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class WiringDrawing : BaseView
    {
        public WiringDrawing()
        {
            InitializeComponent();
        }

        #region クラス変数

        // dataGridView用データテーブル
        private DataTable Dt = new DataTable();

        // TEMPファイル保存用のリスト
        private List<ZumenInfo> List = new List<ZumenInfo>();
        readonly string DRAWING_URL = "\\\\TS-XEL980\\share\\図面ファイル\\htdocs\\ドキュワークス\\";

        private string BinderPath = string.Empty;

        // DBエラー時
        private bool ErrorFlg = false;

        private bool SearchFlg = false;

        #endregion

        #region イベント

        /// <summary>
        /// キープレス処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyPressWiringDrawing(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    OnClickBtnSearch(sender, e);
                    break;
                case Keys.F6:
                    OnClickZumenDocuBtn(sender, e);
                    break;
                case Keys.F7:
                    OnClickBtnCancel(sender, e);
                    break;
                case Keys.F8:
                    OnClickBtnCmp(sender, e);
                    break;
                case Keys.F9:
                    OnClickBtnEstSearch(sender, e);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 画面起動処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLoadWiringDrawing(object sender, EventArgs e)
        {
            this.DoubleBuffered = true;
            this.txtOrderNo.Text = Properties.Settings.Default.OrderNo;
            SetPropertyDeskCtr();
        }

        /// <summary>
        /// 検索ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnClickBtnSearch(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtOrderNo.Text))
                {
                    MessageBox.Show("オーダーNOを入力してください。");
                    return;
                }

                SearchFlg = false;

                // 図面編集者をクリアする
                MainDisp.RirekiUpdate(txtOrderNo.Text, Environment.MachineName);

                // 画面を操作不能にする
                this.Enabled = false;

                // プログレスバー表示
                ProgressBar pb = new ProgressBar
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                pb.Show();

                // 非同期で実行
                await Async();

                pb.Close();

                // 配線図面ダイアログを前面に表示
                this.TopMost = false;

                this.dataGridView.DataSource = Dt;
                this.dataGridView.Columns["図面パス"].Visible = false;
                this.dataGridView.Columns["オーダーNO"].Visible = false;
                this.dataGridView.Columns["グループ"].Visible = false;
                this.dataGridView.Columns["連番"].Visible = false;

                if (Dt.Rows.Count < 1)
                {
                    MessageBox.Show("オーダーNOを正しく入力してください。");
                }

                // TEMPファイル保存用のリストを再作成
                List = new List<ZumenInfo>();

                this.Enabled = true;

                // 検索オーダーNOを保存しておく
                Properties.Settings.Default.OrderNo = this.txtOrderNo.Text;
                Properties.Settings.Default.Save();

                ErrorFlg = false;
                SearchFlg = true;

            }
            catch (Exception ex)
            {
                ErrorFlg = true;
                OnClickBtnSearch(sender, e);

            }
        }

        /// <summary>
        /// 編集ボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickZumenDocuBtn(object sender, EventArgs e)
        {
            try
            {
                if(!MainDisp.isDw9)
                {
                    MessageBox.Show("DocuWorks9がインストールされていません。");
                    return;
                }

                DataTable dt = Dt.Clone();

                // 編集対象を選択
                foreach (DataRow dr in Dt.Rows)
                {
                    if (Convert.ToBoolean(dr["編集"]) == true)
                    {
                        dt.ImportRow(dr);
                    }
                }

                // 編集対象が選択されていない場合
                if (dt.Rows.Count < 1)
                {
                    MessageBox.Show("図面を選択してください。");
                    return;
                }

                // ボタンを使用不可にする。
                BtnEnableFalse();

                // 既存のTempファイルを削除
                DeleteTempFile();

                // 編集用のバインダーを作成します
                this.BinderPath = GetXDWBinder(dt);
                // コピーしたローカルファイルをDocuworksで開く
                Process p = Process.Start(BinderPath);

                //イベントハンドラがフォームを作成したスレッドで実行されるようにする
                p.SynchronizingObject = this;
                //イベントハンドラの追加
                p.Exited += new EventHandler(ExitedProcess);
                //プロセスが終了したときに Exited イベントを発生させる
                p.EnableRaisingEvents = true;

            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
                this.Enabled = true;
                this.Activate();
            }
        }

        /// <summary>
        /// Docuworksバインダー編集終了後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitedProcess(object sender, EventArgs e)
        {
            // 編集用のバインダーを解体します
            DeprocessXDWBinder(BinderPath);
            // 後処理
            PostProcess();
            // ボタン使用可能にする
            BtnEnableTrue();
            // 再検索
            OnClickBtnSearch(sender, e);
            // バインダーファイルを削除
            File.Delete(BinderPath);
        }

        /// <summary>
        /// Tempファイルを削除
        /// </summary>
        private void DeleteTempFile()
        {
            try
            {
                string[] extensions = { "xbd", "xdw" };

                // 既存のTempファイルを取得
                string[] failes = GetFiles(Path.GetTempPath(), extensions);

                // 既存のTempファイルを削除
                foreach (var path in failes)
                {
                    File.Delete(path);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 図面ビューアーを閉じるときに処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClosingForm(object sender, FormClosingEventArgs e)
        {
            //ローカルコンピュータ上で実行されている"dwviewer"という名前の
            //すべてのプロセスを取得
            Process[] ps = Process.GetProcessesByName("dwviewer");
            // Docuworksファイルが開かれているかチェック
            bool isOpnDocu = false;
            foreach (Process p in ps)
            {
                isOpnDocu = true;
            }
            if (isOpnDocu)
            {
                //メッセージボックスを表示する
                MessageBox.Show("編集中のDocuworksファイルを閉じてください。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// データグリッドクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickDataGridView(object sender, EventArgs e)
        {
            try
            {
                // 背景色変更
                if (dataGridView.Rows[(sender as DataGridView).CurrentCell.RowIndex].DefaultCellStyle.BackColor == SystemColors.Highlight)
                {
                    dataGridView.Rows[(sender as DataGridView).CurrentCell.RowIndex].DefaultCellStyle.SelectionBackColor = SystemColors.HighlightText;
                    dataGridView.Rows[(sender as DataGridView).CurrentCell.RowIndex].DefaultCellStyle.BackColor = SystemColors.HighlightText;
                }
                else
                {
                    dataGridView.Rows[(sender as DataGridView).CurrentCell.RowIndex].DefaultCellStyle.SelectionBackColor = SystemColors.Highlight;
                    dataGridView.Rows[(sender as DataGridView).CurrentCell.RowIndex].DefaultCellStyle.BackColor = SystemColors.Highlight;
                }

                if ((sender as DataGridView).CurrentCell is DataGridViewCheckBoxCell)
                {
                    string failePath = dataGridView[(sender as DataGridView).CurrentCell.ColumnIndex + 5, (sender as DataGridView).CurrentCell.RowIndex].Value.ToString();

                    // 図面が存在しない場合
                    if (string.IsNullOrWhiteSpace(failePath))
                    {
                        MessageBox.Show("図面が存在しません。");
                        return;
                    }

                    if ((sender as DataGridView).CurrentCell.OwningColumn.HeaderText == "編集")
                    {
                        if (Convert.ToBoolean((sender as DataGridView).CurrentCell.Value) == false)
                        {
                            // 編集中の場合return
                            string editor = dataGridView[(sender as DataGridView).CurrentCell.ColumnIndex + 1, (sender as DataGridView).CurrentCell.RowIndex].Value.ToString();

                            if (editor.Length > 2)
                            {
                                MessageBox.Show("編集中の図面です。");
                                return;
                            }
                            dataGridView[(sender as DataGridView).CurrentCell.ColumnIndex, (sender as DataGridView).CurrentCell.RowIndex].Value = true;
                        }
                        else
                        {
                            dataGridView[(sender as DataGridView).CurrentCell.ColumnIndex, (sender as DataGridView).CurrentCell.RowIndex].Value = false;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// データグリッド選択行変更処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectionChangedDataGridView(object sender, EventArgs e)
        {
            try
            {
                if (this.dataGridView.CurrentRow == null)
                {
                    return;
                }

                string failePath = this.dataGridView.CurrentRow.Cells["図面パス"].Value.ToString();

                if (string.IsNullOrWhiteSpace(failePath))
                {
                    // 図面表示
                    axDeskCtrlZumen.LoadFile(MainDisp.FILE_URL + "図面データ/NoImarge.xdw");
                }
                else
                {
                    failePath = failePath.Substring(DRAWING_URL.Length, failePath.Length - DRAWING_URL.Length);
                    // 図面表示
                    axDeskCtrlZumen.LoadFile(MainDisp.FILE_URL + failePath.Replace("\\", "/"));
                }
                SetPropertyDeskCtr();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 取消しボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickBtnCancel(object sender, EventArgs e)
        {
            try
            {
                List<ZumenInfo> list = GetUpdateDate();

                if (list.Count <= 0)
                {
                    MessageBox.Show("図面を選択してください。");
                    return;
                }

                string msg = string.Empty;
                string oderNo = string.Empty;
                string grp = string.Empty;
                int ren = 0;

                foreach (ZumenInfo dto in list)
                {
                    if (dto.Status.Equals("完了"))
                    {
                        // 作業状況を未完了にする。
                        msg += dto.ZumenName + "の完了を取消しました。" + Environment.NewLine;
                        oderNo = dto.OrderNo;
                        grp = dto.Grp;
                        ren = dto.Ren;
                        DeleteWorkSituation(oderNo, grp, ren);
                    }
                }

                //　更新対象無し
                if (msg.Equals(string.Empty))
                {
                    MessageBox.Show("更新対象がありません。");
                    return;
                }

                MessageBox.Show(msg);

                // 再検索
                OnClickBtnSearch(sender, e);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// チェックボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckedChangedCheckBox(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(this.txtOrderNo.Text))
            {
                // 再検索
                OnClickBtnSearch(sender, e);
            }
        }

        /// <summary>
        /// 完了ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickBtnCmp(object sender, EventArgs e)
        {
            try
            {
                List<ZumenInfo> list = GetUpdateDate();

                if (list.Count <= 0)
                {
                    MessageBox.Show("図面を選択してください。");
                    return;
                }

                string msg = string.Empty;
                string oderNo = string.Empty;
                string grp = string.Empty;
                int ren = 0;

                foreach (ZumenInfo dto in list)
                {
                    if (!dto.Status.Equals("完了"))
                    {
                        // 作業状況を完了にする。
                        msg += dto.ZumenName + "を完了にしました。" + Environment.NewLine;
                        oderNo = dto.OrderNo;
                        grp = dto.Grp;
                        ren = dto.Ren;
                        InsertWorkSituation(oderNo, grp, ren);
                    }
                }

                //　更新対象無し
                if (msg.Equals(string.Empty))
                {
                    MessageBox.Show("更新対象がありません。");
                    return;
                }

                MessageBox.Show(msg);

                // 再検索
                OnClickBtnSearch(sender, e);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 更新対象のデータを取得します。
        /// </summary>
        private List<ZumenInfo> GetUpdateDate()
        {
            List<ZumenInfo> list = new List<ZumenInfo>();
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                if (row.DefaultCellStyle.BackColor == SystemColors.Highlight)
                {
                    // 更新データを整理
                    ZumenInfo dto = new ZumenInfo
                    {
                        ZumenName = row.Cells["図面番号"].Value.ToString() + "-" + row.Cells["品名"].Value.ToString(),
                        OrderNo = row.Cells["オーダーNO"].Value.ToString(),
                        Grp = row.Cells["グループ"].Value.ToString(),
                        Ren = (int)(row.Cells["連番"].Value),
                        Status = row.Cells["作業状況"].Value.ToString(),
                    };

                    list.Add(dto);
                }
            }

            return list;
        }

        /// <summary>
        /// 見積検索ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnClickBtnEstSearch(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtOrderNo.Text))
                {
                    MessageBox.Show("オーダーNOを入力してください。");
                    return;
                }

                // 画面を操作不能にする
                this.Enabled = false;

                // プログレスバー表示
                ProgressBar pb = new ProgressBar
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                pb.Show();

                // 非同期で実行
                await AsyncEst();

                pb.Close();

                // 配線図面ダイアログを前面に表示
                this.TopMost = false;

                if (RefKumiJob.files.Length == 1)
                {
                    //配線工数ファイルを起動する
                    Process p = Process.Start(CommonModule.ConvertToString(RefKumiJob.files[0]));
                }
                else if (RefKumiJob.files.Length > 1)
                {
                    // 工数参照ダイアログ表示
                    RefKumiJob a = new RefKumiJob();
                    a.Show();
                }
                else
                {
                    MessageBox.Show("該当する配線工数ファイルがありません。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                this.Enabled = true;

            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        #endregion

        #region private関数

        /// <summary>
        /// 非同期処理
        /// </summary>
        /// <returns></returns>
        private async Task<bool> Async()
        {
            try
            {
                return await Task.Run(() => GetWiringDrawingInfo());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 非同期処理(見積検索)
        /// </summary>
        /// <returns></returns>
        private async Task<bool> AsyncEst()
        {
            try
            {
                return await Task.Run(() => SearchWiringEst());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 図面情報を取得します
        /// </summary>
        /// <returns></returns>
        private bool GetWiringDrawingInfo()
        {
            try
            {
                DataTable WiringDrawingList = new DataTable();
                // エラー時暫定対応
                if (!ErrorFlg)
                {
                    // 図面情報取得
                    WiringDrawingList = GetWiringDrawing(this.txtOrderNo.Text, this.checkBox.Checked);
                }
                else
                {
                    WiringDrawingList = GetErrorWiringDrawing(this.txtOrderNo.Text, this.checkBox.Checked);
                }

                // 図面履歴情報取得
                DataTable WiringDrawingRirekiList = GetWiringDrawingRireki(this.txtOrderNo.Text);
                // データグリッド表示用データテーブル作成
                Dt = GetWiringDrawingData(WiringDrawingList, WiringDrawingRirekiList);

                return true;
            }
            catch
            {
                return true;
            }
        }

        /// <summary>
        /// 配線作業実績データを削除します
        /// </summary>
        /// <param name="oderNo"></param>
        /// <param name="grp"></param>
        /// <param name="ren"></param>
        private void DeleteWorkSituation(string oderNo, string grp, int ren)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                // トランザクション開始
                using (var tran = con.BeginTransaction())
                {
                    try
                    {
                        StringBuilder query = new StringBuilder();

                        query.AppendLine("DELETE");
                        query.AppendLine("  FROM 配線作業実績データ");
                        query.AppendLine(" WHERE \"オーダーNO\" = :オーダーno ");
                        query.AppendLine("   AND グループ = :グループ ");
                        query.AppendLine("   AND 連番 = :連番 ");

                        NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                        cmd.Parameters.Add(new NpgsqlParameter("オーダーno", NpgsqlDbType.Varchar));
                        cmd.Parameters["オーダーno"].Value = oderNo;

                        cmd.Parameters.Add(new NpgsqlParameter("グループ", NpgsqlDbType.Varchar));
                        cmd.Parameters["グループ"].Value = grp;

                        cmd.Parameters.Add(new NpgsqlParameter("連番", NpgsqlDbType.Integer));
                        cmd.Parameters["連番"].Value = ren;

                        cmd.ExecuteNonQuery();

                        tran.Commit();
                    }
                    catch (Exception e)
                    {
                        //　重複時
                        log.Error("配線作業実績データ重複" + e.Message + "\r\n" + e.StackTrace);
                    }
                }
            }
        }

        /// <summary>
        /// 配線作業実績データを作成します
        /// </summary>
        /// <param name="oderNo"></param>
        /// <param name="grp"></param>
        /// <param name="ren"></param>
        private void InsertWorkSituation(string oderNo, string grp, int ren)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                // トランザクション開始
                using (var tran = con.BeginTransaction())
                {
                    try
                    {
                        StringBuilder query = new StringBuilder();

                        query.AppendLine("INSERT ");
                        query.AppendLine("    INTO 配線作業実績データ (  ");
                        query.AppendLine("    \"オーダーNO\" ");
                        query.AppendLine("   ,グループ ");
                        query.AppendLine("   ,連番 ");
                        query.AppendLine("   ,作業状況 )");
                        query.AppendLine("VALUES (  ");
                        query.AppendLine("    :オーダーno ");
                        query.AppendLine("   ,:グループ ");
                        query.AppendLine("   ,:連番 ");
                        query.AppendLine("   ,:作業状況) ");

                        NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                        cmd.Parameters.Add(new NpgsqlParameter("オーダーno", NpgsqlDbType.Varchar));
                        cmd.Parameters["オーダーno"].Value = oderNo;

                        cmd.Parameters.Add(new NpgsqlParameter("グループ", NpgsqlDbType.Varchar));
                        cmd.Parameters["グループ"].Value = grp;

                        cmd.Parameters.Add(new NpgsqlParameter("連番", NpgsqlDbType.Integer));
                        cmd.Parameters["連番"].Value = ren;

                        cmd.Parameters.Add(new NpgsqlParameter("作業状況", NpgsqlDbType.Integer));
                        cmd.Parameters["作業状況"].Value = 1;

                        cmd.ExecuteNonQuery();

                        tran.Commit();
                    }
                    catch (Exception e)
                    {
                        //　重複時
                        log.Error("配線作業実績データ重複" + e.Message + "\r\n" + e.StackTrace);
                    }
                }
            }
        }

        /// <summary>
        /// 図面データに履歴データを紐づけます
        /// </summary>
        /// <param name="WiringDrawingList"></param>
        /// <param name="WiringDrawingRirekiList"></param>
        /// <returns></returns>
        private DataTable GetWiringDrawingData(DataTable WiringDrawingList, DataTable WiringDrawingRirekiList)
        {
            DataTable dt = WiringDrawingList.Clone();
            string drawingName = string.Empty;

            // 図面データに履歴データを紐づける
            foreach (DataRow dr in WiringDrawingList.Rows)
            {
                dt.ImportRow(dr);
                foreach (DataRow dr2 in WiringDrawingRirekiList.Rows)
                {
                    if (drawingName.Equals(dr2["図面番号"].ToString()))
                    {
                        continue;
                    }

                    drawingName = dr2["図面番号"].ToString();

                    if (dr["図面番号"].ToString().Equals(dr2["図面番号"].ToString()))
                    {
                        dr2["作業状況"] = dr["作業状況"];
                        // 作業状況完了のもにはチェックをいれない。
                        if (dr["作業状況"].ToString().Equals("完了"))
                        {
                            dr2["編集"] = false;
                        }
                        dr2["品名"] = dr["品名"];
                        dr2["グループ"] = dr["グループ"];
                        dr2["連番"] = dr["連番"];
                        dt.ImportRow(dr2);
                    }
                    // 実績加工データの図面番号の形式が～Aの場合、実績加工履歴データの改正前図面番号(～)を探す
                    else if (dr2["図面番号"].ToString().Equals(dr["図面番号"].ToString().Substring(0, dr["図面番号"].ToString().Length - 1))
                        && Regex.IsMatch(dr["図面番号"].ToString().Substring(dr["図面番号"].ToString().Length - 1, 1), "[A-Z]"))
                    {
                        dr2["作業状況"] = dr["作業状況"];
                        // 作業状況完了のもにはチェックをいれない。
                        if (dr["作業状況"].ToString().Equals("完了"))
                        {
                            dr2["編集"] = false;
                        }
                        dr2["品名"] = dr["品名"];
                        dr2["グループ"] = dr["グループ"];
                        dr2["連番"] = dr["連番"];
                        dt.ImportRow(dr2);
                    }
                    // 実績加工データの図面番号の形式が～Bの場合、実績加工履歴データの改正前図面番号(～A)を探す
                    else if (dr2["図面番号"].ToString().Substring(0, dr2["図面番号"].ToString().Length - 1).Equals(dr["図面番号"].ToString().Substring(0, dr["図面番号"].ToString().Length - 1))
                       && Regex.IsMatch(dr["図面番号"].ToString().Substring(dr["図面番号"].ToString().Length - 1, 1), "[A-Z]")
                       && Regex.IsMatch(dr2["図面番号"].ToString().Substring(dr2["図面番号"].ToString().Length - 1, 1), "[A-Z]"))
                    {
                        dr2["作業状況"] = dr["作業状況"];
                        // 作業状況完了のもにはチェックをいれない。
                        if (dr["作業状況"].ToString().Equals("完了"))
                        {
                            dr2["編集"] = false;
                        }
                        dr2["品名"] = dr["品名"];
                        dr2["グループ"] = dr["グループ"];
                        dr2["連番"] = dr["連番"];
                        dt.ImportRow(dr2);
                    }
                    // 実績加工データの図面番号の形式が～A#01の場合、実績加工履歴データの改正前図面番号(～#01)を探す
                    else if (dr2["図面番号"].ToString().Substring(0, dr2["図面番号"].ToString().Length - 3).Equals(dr["図面番号"].ToString().Substring(0, dr["図面番号"].ToString().Length - 4))
                      && dr2["図面番号"].ToString().Length > 5
                      && dr2["図面番号"].ToString().Substring(0, dr2["図面番号"].ToString().Length - 2).Equals(dr["図面番号"].ToString().Substring(0, dr["図面番号"].ToString().Length - 2))
                      && dr2["図面番号"].ToString().Substring(dr2["図面番号"].ToString().Length - 2, 1).Equals("#"))
                    {
                        dr2["作業状況"] = dr["作業状況"];
                        // 作業状況完了のもにはチェックをいれない。
                        if (dr["作業状況"].ToString().Equals("完了"))
                        {
                            dr2["編集"] = false;
                        }
                        dr2["品名"] = dr["品名"];
                        dr2["グループ"] = dr["グループ"];
                        dr2["連番"] = dr["連番"];
                        dt.ImportRow(dr2);
                    }
                    // 実績加工データの図面番号の形式が～B#01の場合、実績加工履歴データの改正前図面番号(～A#01)を探す
                    else if (dr2["図面番号"].ToString().Substring(0, dr2["図面番号"].ToString().Length - 4).Equals(dr["図面番号"].ToString().Substring(0, dr["図面番号"].ToString().Length - 4))
                      && dr2["図面番号"].ToString().Length > 5
                      && dr2["図面番号"].ToString().Substring(0, dr2["図面番号"].ToString().Length - 2).Equals(dr["図面番号"].ToString().Substring(0, dr["図面番号"].ToString().Length - 2))
                      && dr2["図面番号"].ToString().Substring(dr2["図面番号"].ToString().Length - 2, 1).Equals("#")
                      && Regex.IsMatch(dr2["図面番号"].ToString().Substring(dr2["図面番号"].ToString().Length - 3, 1), "[A-Z]"))
                    {
                        dr2["作業状況"] = dr["作業状況"];
                        // 作業状況完了のもにはチェックをいれない。
                        if (dr["作業状況"].ToString().Equals("完了"))
                        {
                            dr2["編集"] = false;
                        }
                        dr2["品名"] = dr["品名"];
                        dr2["グループ"] = dr["グループ"];
                        dr2["連番"] = dr["連番"];
                        dt.ImportRow(dr2);
                    }
                }
            }

            return dt;
        }

        /// <summary>
        /// 配線図情報を取得します
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        private DataTable GetWiringDrawing(string orderNo, bool cmpChk)
        {
            DataTable dt = new DataTable();

            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "   DISTINCT jkkd.図面番号" +
                                "  ,false AS 編集 " +
                                "  ,case when hjd.作業状況 = 1 then '完了' " +
                                "   when jkr.図面編集者 is not null then '編集中:' ||  jkr.図面編集者 " +
                                "   when hjd.作業状況 is null then '' end as 作業状況" +
                                "  ,jkkd.\"オーダーNO\"" +
                                "  ,jkkd.品名" +
                                "  ,'元図面' AS 編集図面" +
                                "  ,case when jkkd.図面パス is not null then jkkd.図面パス " +
                                "   when zd.パス is not null then zd.パス " +
                                "   when zd2.パス is not null then zd2.パス end as 図面パス" +
                                "  ,jkkd.グループ" +
                                "  ,jkkd.連番" +
                                " from 実績加工データ jkkd" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(jkkd.図面番号,'/','／')" +
                                " left join 図面データ zd2" +
                                "   on zd2.ファイル like replace('%jkkd.図面番号%','/','／')" +
                                " left join 図面作成データ zsd" +
                                "   on jkkd.図面番号 = zsd.図面番号" +
                                " left join 作業者マスタ sam2" +
                                "   on zsd.作業者 = sam2.作業者コード" +
                                " left join 配線作業実績データ hjd " +
                                "   on jkkd.\"オーダーNO\" = hjd.\"オーダーNO\"" +
                                "  and jkkd.グループ = hjd.グループ" +
                                "  and jkkd.連番 = hjd.連番" +
                                " left join 実績加工履歴データ jkr " +
                                "   on jkkd.図面番号 = jkr.図面番号" +
                                "  and jkkd.\"オーダーNO\" = jkr.\"オーダーNO\"" +
                " where jkkd.\"オーダーNO\" = UPPER(:orderNo) " +
                " and substr(jkkd.図面番号, position('-' in jkkd.図面番号) + 1,1) = '7' ";

                if (cmpChk)
                {
                    sqlCmdTxt += "and hjd.作業状況 is null ";
                }

                sqlCmdTxt += " order by jkkd.図面番号" + " limit 500";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                // 接続を閉じる
                con.Close();
                con.Dispose();

                return dt;
            }
        }

        /// <summary>
        /// 配線図情報を取得します(エラー時暫定対応)
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        private DataTable GetErrorWiringDrawing(string orderNo, bool cmpChk)
        {
            DataTable dt = new DataTable();

            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "   DISTINCT jkkd.図面番号" +
                                "  ,false AS 編集 " +
                                "  ,case when hjd.作業状況 = 1 then '完了' " +
                                "   when jkr.図面編集者 is not null then '編集中:' ||  jkr.図面編集者 " +
                                "   when hjd.作業状況 is null then '' end as 作業状況" +
                                "  ,jkkd.\"オーダーNO\"" +
                                "  ,jkkd.品名" +
                                "  ,'元図面' AS 編集図面" +
                                "  ,case when jkkd.図面パス is not null then jkkd.図面パス " +
                                "   when zd.パス is not null then zd.パス end as 図面パス" +
                                "  ,jkkd.グループ" +
                                "  ,jkkd.連番" +
                                " from 実績加工データ jkkd" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(jkkd.図面番号,'/','／')" +
                                " left join 図面作成データ zsd" +
                                "   on jkkd.図面番号 = zsd.図面番号" +
                                " left join 作業者マスタ sam2" +
                                "   on zsd.作業者 = sam2.作業者コード" +
                                " left join 配線作業実績データ hjd " +
                                "   on jkkd.\"オーダーNO\" = hjd.\"オーダーNO\"" +
                                "  and jkkd.グループ = hjd.グループ" +
                                "  and jkkd.連番 = hjd.連番" +
                                " left join 実績加工履歴データ jkr " +
                                "   on jkkd.図面番号 = jkr.図面番号" +
                                "  and jkkd.\"オーダーNO\" = jkr.\"オーダーNO\"" +
                " where jkkd.\"オーダーNO\" = UPPER(:orderNo) " +
                " and substr(jkkd.図面番号, position('-' in jkkd.図面番号) + 1,1) = '7' ";

                if (cmpChk)
                {
                    sqlCmdTxt += "and hjd.作業状況 is null ";
                }

                sqlCmdTxt += " order by jkkd.図面番号" + " limit 500";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                // 接続を閉じる
                con.Close();
                con.Dispose();

                return dt;
            }
        }

        /// <summary>
        /// 配線図履歴を取得します
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <returns></returns>
        public DataTable GetWiringDrawingRireki(string orderNo)
        {
            DataTable dt = new DataTable();

            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  図面番号" +
                                "  ,case when 図面編集者 isnull then true else false end AS 編集 " +
                                "  ,case when 図面編集者 is not null then '編集中:' ||  図面編集者 " +
                                "   else ''end as 作業状況" +
                                "  ,\"オーダーNO\"" +
                                "  ,'' AS 品名" +
                                "  ,'編集図面' || 連番 AS 編集図面" +
                                "  , 図面パス" +
                                "  , '' AS グループ" +
                                "  , 0 AS 連番" +
                                "  , 連番 AS 履歴連番" +
                                " from 実績加工履歴データ " +
                                " where \"オーダーNO\" = UPPER('" + orderNo + "')" +
                                "   and 連番 <> 0  " +
                                "   and 図面パス IS NOT NULL   " +
                                "   and substr(図面番号, position('-' in 図面番号) + 1,1) = '7' " +
                                "   order by 図面番号 , 履歴連番 DESC";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                // 接続を閉じる
                con.Close();
                con.Dispose();

                return dt;
            }
        }

        /// <summary>
        /// バインダーを解体します
        /// </summary>
        /// <param name="binderPath"></param>
        private void DeprocessXDWBinder(string binderPath)
        {
            Xdwapi.XDW_DOCUMENT_HANDLE handle = new Xdwapi.XDW_DOCUMENT_HANDLE();
            Xdwapi.XDW_OPEN_MODE_EX mode = new Xdwapi.XDW_OPEN_MODE_EX
            {
                Option = Xdwapi.XDW_OPEN_UPDATE,
                AuthMode = Xdwapi.XDW_AUTH_NODIALOGUE
            };
            Xdwapi.XDW_OpenDocumentHandle(binderPath, ref handle, mode);
            int count = 0;
            foreach (var dto in List)
            {
                count++;
                Xdwapi.XDW_GetDocumentFromBinder(handle, count, dto.LocalZumenPath);
            }

            Xdwapi.XDW_SaveDocument(handle);
            Xdwapi.XDW_CloseDocumentHandle(handle);

        }

        /// <summary>
        /// 編集用のバインダーを作成します
        /// </summary>
        /// <returns></returns>
        private string GetXDWBinder(DataTable dt)
        {

            // バインダー作成
            // xbdの一時ファイル名
            string binderPath = Path.ChangeExtension(Path.GetTempFileName(), ".xbd");

            Xdwapi.XDW_DOCUMENT_HANDLE handle = new Xdwapi.XDW_DOCUMENT_HANDLE();
            Xdwapi.XDW_OPEN_MODE_EX mode = new Xdwapi.XDW_OPEN_MODE_EX
            {
                Option = Xdwapi.XDW_OPEN_UPDATE,
                AuthMode = Xdwapi.XDW_AUTH_NODIALOGUE
            };

            Xdwapi.XDW_CreateBinder(binderPath, null);
            Xdwapi.XDW_OpenDocumentHandle(binderPath, ref handle, mode);

            int count = 0;
            var conn = Properties.Settings.Default.connectionString;
            // バインダーに DocuWorks 文書を挿入する 
            // 編集対象を選択
            foreach (DataRow dr in dt.Rows)
            {
                string localFilePath = Path.GetTempPath() + CleanInput(dr["図面番号"].ToString()) + "-" + CleanInput(dr["品名"].ToString()) + "-" + CleanInput(dr["編集図面"].ToString()) + ".xdw";

                // 共有フォルダ上の図面を取得
                File.Copy(dr["図面パス"].ToString(), localFilePath);
                FileInfo file = new FileInfo(localFilePath);

                ZumenInfo dto = new ZumenInfo
                {
                    LocalZumenPath = localFilePath,
                    ZumenPath = dr["図面パス"].ToString(),
                    ZumenName = dr["図面番号"].ToString(),
                    OrderNo = dr["オーダーNO"].ToString(),
                    Hinmei = dr["品名"].ToString(),
                    WorkSituation = dr["作業状況"].ToString(),
                };
                List.Add(dto);

                count++;
                Xdwapi.XDW_InsertDocumentToBinder(handle, count, localFilePath);
                // TENPファイルを削除
                File.Delete(localFilePath);

                // 図面編集者を設定
                string zumenEditor = MainDisp.GetZumenEditor(conn, dr["図面番号"].ToString(), dr["オーダーNO"].ToString());
                if (!String.IsNullOrEmpty(zumenEditor))
                {
                    if (zumenEditor == "新規")
                    {
                        // 新規で図面を編集する場合は実績加工履歴データに連番0のデータを登録
                        MainDisp.RirekiNewInsert(conn, dr["図面番号"].ToString(), dr["オーダーNO"].ToString(), Environment.MachineName);
                    }
                }
                else
                {
                    // 図面を編集している端末名が登録されていない場合は実績加工履歴データ.図面編集者に自端末名を登録
                    MainDisp.RirekiUpdate(conn, dr["図面番号"].ToString(), dr["オーダーNO"].ToString(), Environment.MachineName);
                }
            }

            Xdwapi.XDW_SaveDocument(handle);
            Xdwapi.XDW_CloseDocumentHandle(handle);

            return binderPath;

        }

        /// <summary>
        /// 後処理をする
        /// </summary>
        private void PostProcess()
        {
            var conn = Properties.Settings.Default.connectionString;
            foreach (var dto in List)
            {
                var maxRenno = MainDisp.GetMaxRenno(conn, dto.ZumenName);
                if (maxRenno < 0)
                {
                    MessageBox.Show("実績加工履歴データより正しく取得できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    this.Enabled = true;
                    this.Activate();
                    return;
                }

                var zumenPath = MainDisp.RIREKI_FOLDER + dto.ZumenName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + maxRenno + ".xdw";
                var renno = 1;
                while (File.Exists(zumenPath))
                {
                    MessageBox.Show("すでに同じ名前の図面ファイルがあります。しばらくお待ちください。",
                    "注意",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                    zumenPath = MainDisp.RIREKI_FOLDER + dto.ZumenName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + renno + ".xdw";
                    renno += 1;
                }
                // ローカルTMPファイルを共有フォルダにコピー
                File.Copy(dto.LocalZumenPath, zumenPath);

                // 実績加工履歴テーブルにデータ追加
                var checkImport = MainDisp.RirekiInsert(conn, dto.ZumenName, maxRenno + 1, zumenPath, dto.OrderNo);
                if (!checkImport)
                {
                    MessageBox.Show("実績加工履歴データに正しく登録できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    this.Enabled = true;
                    this.Activate();
                    return;
                }

                // 図面編集者をクリアする
                MainDisp.RirekiUpdate(conn, dto.ZumenName, dto.OrderNo, string.Empty);
                // TENPファイルを削除
                File.Delete(dto.LocalZumenPath);
            }
        }

        /// <summary>
        /// ファイル名に使用できない文字を取り除きます
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        static string CleanInput(string strIn)
        {
            try
            {
                return Regex.Replace(strIn, @"[^\w\.@-]", "",
                                     RegexOptions.None, TimeSpan.FromSeconds(1.5));
            }
            catch (RegexMatchTimeoutException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 指定した拡張子のファイルを取得
        /// </summary>
        /// <param name="path"></param>
        /// <param name="extensions"></param>
        /// <returns></returns>
        public static string[] GetFiles(string path, string[] extensions)
        {
            return Directory
                .GetFiles(path, "*.*")
                .Where(c => extensions.Any(extension => c.EndsWith(extension)))
                .ToArray();
        }

        /// <summary>
        /// 各種ボタンを使用不可にします
        /// </summary>
        private void BtnEnableFalse()
        {
            btnSearch.Enabled = false;

            btnZumenDocu.Enabled = false;

            btnCancel.Enabled = false;

            btnCmp.Enabled = false;

            checkBox.Enabled = false;
        }

        /// <summary>
        /// 各種ボタンを使用可能にします
        /// </summary>
        private void BtnEnableTrue()
        {
            btnSearch.Enabled = true;

            btnZumenDocu.Enabled = true;

            btnCancel.Enabled = true;

            btnCmp.Enabled = true;

            checkBox.Enabled = true;
        }

        /// <summary>
        /// 配線見積工数を検索します
        /// </summary>
        /// <returns></returns>
        private bool SearchWiringEst()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtOrderNo.Text))
                {
                    return true;
                }

                //オーダーNOの先頭1文字から検索するフォルダ名を取得
                List<string> folderNameList = GetFolderName(txtOrderNo.Text.Substring(0, 1));
                List<string> partsList = new List<string>();

                if (folderNameList.Count > 0)
                {
                    //特定のフォルダ内を検索
                    foreach (var folderName in folderNameList)
                    {
                        string[] tmp = Directory.GetDirectories(@"\\landiskjimusyo\disk\G\パーツリスト\" + folderName, "*" + txtOrderNo.Text + "*", SearchOption.TopDirectoryOnly);

                        foreach (var dir in tmp)
                        {
                            partsList.AddRange(Directory.GetFiles(dir, "*電気*見積*.xls*", SearchOption.AllDirectories).ToList());
                        }
                    }
                }
                else
                {
                    string[] tmp = Directory.GetDirectories(@"\\landiskjimusyo\disk\G\パーツリスト", "*" + txtOrderNo.Text + "*", SearchOption.AllDirectories);

                    foreach (var dir in tmp)
                    {
                        partsList.AddRange(Directory.GetFiles(dir, "*電気*見積*.xls*", SearchOption.AllDirectories).ToList());
                    }
                }
                RefKumiJob.files = partsList.ToArray();

                return true;
            }
            catch
            {
                return true;
            }
        }

        /// <summary>
        /// オーダーNOの先頭1文字から検索するフォルダ名を取得
        /// </summary>
        /// <param name="iniChar"></param>
        /// <returns></returns>
        private List<string> GetFolderName(string iniChar)
        {
            List<string> folderNameList = new List<string>();
            switch (iniChar)
            {
                case "T":
                    folderNameList.Add("富山村田");
                    break;
                case "M":
                    folderNameList.Add("野洲");
                    folderNameList.Add("岡山村田");
                    break;
                case "K":
                    folderNameList.Add("金沢村田");
                    break;
                case "U":
                    folderNameList.Add("金津村田");
                    folderNameList.Add("小松村田");
                    folderNameList.Add("ワクラ村田");
                    break;
                case "F":
                    folderNameList.Add("鯖江村田");
                    break;
                case "N":
                    folderNameList.Add("ハクイ村田");
                    break;
                case "H":
                    folderNameList.Add("氷見村田");
                    break;
                case "I":
                    folderNameList.Add("石川サンケン");
                    break;
                case "V":
                    folderNameList.Add("コーセル");
                    break;
                case "D":
                    folderNameList.Add("東京電波");
                    break;
                case "B":
                    folderNameList.Add("社内向け設備");
                    break;
            }
            return folderNameList;
        }

        /// <summary>
        /// DeskCtrのプロパティを設定します
        /// </summary>
        private void SetPropertyDeskCtr()
        {
            List<AxDeskCtrl> axDeskCtrl = GetAllAxDeskCtrl(this);

            foreach (AxDeskCtrl ctrl in axDeskCtrl)
            {
                ctrl.fullscreen = "yes";
                ctrl.SetShowAnnotation(true);
                ctrl.SetSheafViewMode(true);
                ctrl.SetCharDrawAdvance(true);
                ctrl.SetZoomWithPage();
                ctrl.ShowStandardToolBar(false);
                ctrl.ShowSheafBar(false);
                ctrl.ShowStatusBar(false);
            }
        }

        /// <summary>
        /// AxDeskCtrlを取得します
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected List<AxDeskCtrl> GetAllAxDeskCtrl(Control parent)
        {
            List<AxDeskCtrl> controls = new List<AxDeskCtrl>();

            foreach (Control child in parent.Controls)
            {
                controls.AddRange(GetAllAxDeskCtrl(child));
            }

            if (parent is AxDeskCtrl)
            {
                controls.Add((AxDeskCtrl)parent);
            }

            return controls;
        }

        #endregion

        /// <summary>
        /// CheckedChangedイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckedChangedchkCheck(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dataGridView.DataSource;
            if (this.chkCheck.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (!dt.Rows[i]["作業状況"].ToString().Equals("完了"))
                    {
                        // 図面が存在しない場合
                        if (string.IsNullOrWhiteSpace(dt.Rows[i]["図面パス"].ToString()))
                        {
                            dt.Rows[i]["編集"] = false;
                        }
                        else if (i == dt.Rows.Count - 1 && dt.Rows[i]["編集図面"].ToString().Equals("元図面"))
                        {
                            dt.Rows[i]["編集"] = true;
                        }
                        else if (dt.Rows[i + 1]["編集図面"].ToString().Equals("元図面"))
                        {
                            dt.Rows[i]["編集"] = true;
                        }
                    }
                }
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dr["編集"] = false;
                }
            }
        }
    }
}
