﻿using System;

namespace ZumenConsoleApp
{
    public class LoginInfo
    {
        public static string 社員番号 { get; set; }
        public static string 氏名 { get; set; }
        public static string 部署 { get; set; }
        public static string 所属 { get; set; }
        public static string メールアドレス { get; set; }
        public static Boolean 管理者 { get; set; }
        public static Boolean システム管理者 { get; set; }
        public static Boolean 受入検査 { get; set; }
        public static int 作業者コード { get; set; }
        public static string 役職 { get; set; }

        public static Boolean cadcam { get; set; }
    }

}
