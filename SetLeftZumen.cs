﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class SetLeftZumen : BaseView
    {
        public SetLeftZumen()
        {
            InitializeComponent();
        }

        private void SetLeftZumen_Load(object sender, EventArgs e)
        {
            try
            {
                foreach (var blItem in MainDisp.BlockList)
                {
                    var blockTag = CommonModule.ConvertToString(blItem).Split('_');
                    var selLeftBlockTag = FullDisp.selLeftBlockNo.Split('_');
                    TreeNode itm = new TreeNode();
                    itm.Text = blockTag[0];
                    itm.Tag = blockTag[1];
                    leftZumenList.Nodes.Add(itm);
                    if (blockTag[0].Contains("組立図") && selLeftBlockTag[1] == "k" && blockTag[1] == selLeftBlockTag[0])
                    {
                        leftZumenList.SelectedNode = itm;
                        leftZumenList.SelectedNode.BackColor = Color.Yellow;
                    }
                    else if (blockTag[0].Contains("部品図") && selLeftBlockTag[1] == "b" && blockTag[1] == selLeftBlockTag[0])
                    {
                        leftZumenList.SelectedNode = itm;
                        leftZumenList.SelectedNode.BackColor = Color.Yellow;
                    }
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // 設定ボタン押下時処理
        private void setBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (leftZumenList.SelectedNode == null || leftZumenList.SelectedNode.Index < 0)
                {
                    return;
                }
                if (leftZumenList.SelectedNode.Text.Contains("組立図"))
                {
                    FullDisp.selLeftBlockNo = leftZumenList.SelectedNode.Tag + "_k";
                }
                else
                {
                    FullDisp.selLeftBlockNo = leftZumenList.SelectedNode.Tag + "_b";
                }
                FullDisp.isLeftZumen = true;
                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
