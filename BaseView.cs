﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ZumenConsoleApp.View
{
    public partial class BaseView : Form
    {
        public BaseView()
        {

            InitializeComponent();

            // テスト環境、本番環境をわかりやすくする。
            var conn = Properties.Settings.Default.connectionString;
            if (!conn.Contains("192.168.0.100"))
            {
                BackColor = Color.Pink; 
            }
        }

        public static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// キープレス処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyPressBaseView(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    OnClickHelp(sender, e);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// ヘルプファイルを表示します。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickHelp(object sender, EventArgs e)
        {
            Help.ShowHelp(this, ".\\Help\\図面ビューアーアプリ.chm");
        }
    }
}
