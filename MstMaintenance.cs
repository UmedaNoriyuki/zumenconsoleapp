﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ZumenConsoleApp
{
    public partial class MstMaintenance : Form
    {

        #region クラス変数    

        // 1:加工　2:工程
        public int Mode = 0;

        #endregion

        #region コンストラクタ

        public MstMaintenance()
        {
            InitializeComponent();
        }

        #endregion

        #region イベント

        /// <summary>
        /// 画面ロード処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadMstMaintenance(object sender, EventArgs e)
        {
            try
            {
                // DataGirdViewのTypeを取得
                Type dgvtype = typeof(DataGridView);
                // プロパティ設定の取得
                System.Reflection.PropertyInfo dgvPropertyInfo = dgvtype.GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                // 対象のDataGridViewにtrueをセットする
                dgvPropertyInfo.SetValue(dataGridView, true, null);

                // データグリッドの設定
                SetDataGridView();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 編集ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtn_編集(object sender, EventArgs e)
        {
            try
            {
                MstRegist form = new MstRegist
                {
                    Cd = Convert.ToInt32(Convert.ToInt32(this.dataGridView.CurrentRow.Cells["コード"].Value.ToString()))
                };

                if (this.Mode == 1)
                {
                    form.Mode = 2;
                }
                else if (this.Mode == 2)
                {
                    form.Mode = 4;
                }
                form.ShowDialog();
                // データグリッドの設定
                SetDataGridView();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 登録ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtn_登録(object sender, EventArgs e)
        {
            try
            {
                MstRegist form = new MstRegist();
                if(this.Mode == 1)
                {
                    form.Mode = 1;
                }
                else if (this.Mode == 2)
                {
                    form.Mode = 3;
                }
                form.ShowDialog();
                // データグリッドの設定
                SetDataGridView();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 検索ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtn_検索(object sender, EventArgs e)
        {
            try
            {
                // データグリッドの設定
                SetDataGridView();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region private関数

        /// <summary>
        /// データグリッドビューを設定します
        /// </summary>
        private void SetDataGridView()
        {
            try
            {
                int cdFrom = 0;
                int cdTo = 0;
                string name = string.Empty;

                if (!string.IsNullOrWhiteSpace(txt_cdFrom.Text))
                {
                    cdFrom = Convert.ToInt32(txt_cdFrom.Text);
                }

                if (!string.IsNullOrWhiteSpace(txt_cdTo.Text))
                {
                    cdTo = Convert.ToInt32(txt_cdTo.Text);
                }

                if (!string.IsNullOrWhiteSpace(txt_name.Text))
                {
                    name = txt_name.Text;
                }

                DataTable dt = new DataTable();

                // データを取得
                if (this.Mode == 1)
                {
                    dt = Get加工先マスタ(cdFrom, cdTo, name);
                }
                else if (this.Mode == 2)
                {
                    dt = Get工程マスタ(cdFrom, cdTo, name);
                }
                this.dataGridView.DataSource = dt;
                SetBtnEnable(dt.Rows.Count);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// ボタンの制御を行います
        /// </summary>
        /// <param name="count"></param>
        private void SetBtnEnable(int count)
        {
            if (count == 0)
            {
                this.btn_編集.Enabled = false;
            }
            else
            {
                this.btn_編集.Enabled = true;
            }
        }

        /// <summary>
        /// 加工先マスタを取得します
        /// </summary>
        /// <param name="cdFrom"></param>
        /// <param name="cdTo"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private DataTable Get加工先マスタ(int cdFrom, int cdTo,string name)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {

                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   加工先コード AS コード ");
                    query.Append("  ,加工先名 AS 名称 ");
                    query.Append("  FROM 加工指示書加工先マスタ ");
                    query.Append(" WHERE ");
                    query.Append("   加工先コード <> 0 ");

                    if (cdTo != 0)
                    {
                        query.Append(" AND  加工先コード BETWEEN :加工先コードFrom AND :加工先コードTo ");
                    }
                    else
                    {
                        query.Append(" AND  加工先コード >= :加工先コードFrom ");
                    }

                    if(!string.IsNullOrWhiteSpace(name))
                    {
                        query.Append(" AND  加工先名 LIKE '%" + name + "%' ");
                    }

                    query.Append(" ORDER BY ");
                    query.Append("   加工先コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("加工先コードFrom", NpgsqlDbType.Integer));
                    cmd.Parameters["加工先コードFrom"].Value = cdFrom;

                    if (cdTo != 0)
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("加工先コードTo", NpgsqlDbType.Integer));
                        cmd.Parameters["加工先コードTo"].Value = cdTo;
                    }

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 工程マスタを取得します
        /// </summary>
        /// <param name="cdFrom"></param>
        /// <param name="cdTo"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private DataTable Get工程マスタ(int cdFrom, int cdTo, string name)
        {
            try
            {
                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    StringBuilder query = new StringBuilder();

                    query.Append("SELECT ");
                    query.Append("   工程コード AS コード ");
                    query.Append("  ,工程名 AS 名称 ");
                    query.Append("  FROM 加工指示書工程マスタ ");
                    query.Append(" WHERE ");
                    query.Append("   工程コード <> 0 ");

                    if (cdTo != 0)
                    {
                        query.Append(" AND  工程コード BETWEEN :工程コードFrom AND :工程コードTo ");
                    }
                    else
                    {
                        query.Append(" AND  工程コード >= :工程コードFrom ");
                    }

                    if (!string.IsNullOrWhiteSpace(name))
                    {
                        query.Append(" AND  工程名 LIKE '%" + name + "%' ");
                    }

                    query.Append(" ORDER BY ");
                    query.Append("   工程コード ");

                    NpgsqlCommand cmd = new NpgsqlCommand(query.ToString(), con);

                    cmd.Parameters.Add(new NpgsqlParameter("工程コードFrom", NpgsqlDbType.Integer));
                    cmd.Parameters["工程コードFrom"].Value = cdFrom;

                    if (cdTo != 0)
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("工程コードTo", NpgsqlDbType.Integer));
                        cmd.Parameters["工程コードTo"].Value = cdTo;
                    }

                    var dataReader = cmd.ExecuteReader();
                    dt.Load(dataReader);

                    return dt;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
