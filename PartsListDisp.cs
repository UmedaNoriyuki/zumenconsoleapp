﻿using System;
using System.Windows.Forms;
using Npgsql;
using System.Data;

namespace ZumenConsoleApp.View
{
    public partial class PartsListDisp : BaseView
    {
        public static string orderNo { get; set; }

        public PartsListDisp()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ロード時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PartsListDisp_Load(object sender, EventArgs e)
        {
            try
            {
                // オーダーNO設定
                txtOrderNo1.Text = txtOrderNo2.Text = txtOrderNo3.Text = txtOrderNo4.Text = orderNo;
                // 機種名設定
                txtModel1.Text = txtModel2.Text = txtModel3.Text = txtModel4.Text = GetModelName();

                // 総表紙を抽出する
                if (!GetTotalCoverInfo())
                {
                    MessageBox.Show("総表紙を取得できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }

                // 組立図を抽出する
                if (!GetKumiZumenInfo())
                {
                    MessageBox.Show("組立図を取得できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }

                // 加工品を抽出する
                if (!GetKakouInfo())
                {
                    MessageBox.Show("加工品を取得できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }

                // 購入品を抽出する
                if (!GetKounyuInfo())
                {
                    MessageBox.Show("購入品を取得できませんでした。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 機種名を取得する
        /// </summary>
        /// <returns></returns>
        private string GetModelName()
        {
            var modelName = "";

            var cmd = new NpgsqlCommand();

            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select " +
                                "  機種名" +
                                " from オーダー管理部材テーブル " +
                                " where \"オーダーNO\" = UPPER(:orderNo)";
                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                var result = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(result);

                if (dt.Rows.Count == 0)
                {
                    return "";
                }

                modelName = CommonModule.ConvertToString(dt.Rows[0]["機種名"]);

                // 接続を閉じる
                con.Close();
                con.Dispose();
            }
            return modelName;
        }

        /// <summary>
        /// 総表紙を抽出する
        /// </summary>
        /// <returns></returns>
        private bool GetTotalCoverInfo()
        {
            var cmd = new NpgsqlCommand();

            totalCoverList.Rows.Clear();

            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  a.ブロック番号" +
                                "  ,a.略号" +
                                "  ,a.ブロック名" +
                                "  ,a.ブロック数／台" +
                                "  ,a.編成区分" +
                                "  ,a.加工品点数" +
                                "  ,a.購入品点数" +
                                " from \"ess村田PL総表紙\" a" +
                                " left join \"ess村田PLオーダー情報\" b" +
                                "   on b.plid = a.plid" +
                                " where b.\"REPROオーダーNO\" = UPPER(:orderNo)" +
                                " order by a.rownum";
                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                var result = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(result);

                if (dt.Rows.Count == 0)
                {
                    return false;
                }

                this.totalCoverList.DataSource = dt;

                // 接続を閉じる
                con.Close();
                con.Dispose();
            }
            return true;
        }

        /// <summary>
        /// 組立図を抽出する
        /// </summary>
        /// <returns></returns>
        private bool GetKumiZumenInfo()
        {
            var cmd = new NpgsqlCommand();

            kumiZumenList.Rows.Clear();

            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  a.ブロック番号" +
                                "  ,a.略号" +
                                "  ,a.ブロック名" +
                                "  ,a.図番" +
                                "  ,a.図面名称" +
                                "  ,a.備考" +
                                " from \"ess村田PL組立図\" a" +
                                " left join \"ess村田PLオーダー情報\" b" +
                                "   on b.plid = a.plid" +
                                " where b.\"REPROオーダーNO\" = UPPER(:orderNo)" +
                                " order by a.rownum";
                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                var result = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(result);

                if (dt.Rows.Count == 0)
                {
                    return false;
                }

                this.kumiZumenList.DataSource = dt;

                // 接続を閉じる
                con.Close();
                con.Dispose();

            }
            return true;
        }

        /// <summary>
        /// 加工品を抽出する
        /// </summary>
        /// <returns></returns>
        private bool GetKakouInfo()
        {
            var cmd = new NpgsqlCommand();

            kakoList.Rows.Clear();

            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  a.ブロック番号" +
                                "  ,a.略号" +
                                "  ,a.ブロック名" +
                                "  ,a.図番" +
                                "  ,a.品名" +
                                "  ,a.材質" +
                                "  ,a.表処" +
                                "  ,a.個数" +
                                "  ,a.員数" +
                                "  ,a.総数" +
                                "  ,a.単位" +
                                "  ,a.備考" +
                                "  ,a.型番" +
                                "  ,a.メーカー名" +
                                " from \"ess村田PL加工品\" a" +
                                " left join \"ess村田PLオーダー情報\" b" +
                                "   on b.plid = a.plid" +
                                " where b.\"REPROオーダーNO\" = UPPER(:orderNo)" +
                                " order by a.rownum";
                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                var result = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(result);

                if (dt.Rows.Count == 0)
                {
                    return false;
                }

                this.kakoList.DataSource = dt;

                // 接続を閉じる
                con.Close();
                con.Dispose();

            }
            return true;
        }

        /// <summary>
        /// 購入品を抽出する
        /// </summary>
        /// <returns></returns>
        private bool GetKounyuInfo()
        {
            var cmd = new NpgsqlCommand();

            kounyuList.Rows.Clear();

            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  a.ブロック番号" +
                                "  ,a.略号" +
                                "  ,a.ブロック名" +
                                "  ,a.型番" +
                                "  ,a.品名" +
                                "  ,a.メーカ" +
                                "  ,a.個数" +
                                "  ,a.員数" +
                                "  ,a.総数" +
                                "  ,a.単位" +
                                "  ,a.パーツリスト変更通知" +
                                " from \"ess村田PL購入品\" a" +
                                " left join \"ess村田PLオーダー情報\" b" +
                                "   on b.plid = a.plid" +
                                " where b.\"REPROオーダーNO\" = UPPER(:orderNo)" +
                                " order by a.rownum";
                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                var result = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(result);

                if (dt.Rows.Count == 0)
                {
                    return false;
                }

                this.kounyuList.DataSource = dt;

                // 接続を閉じる
                con.Close();
                con.Dispose();
            }
            return true;
        }
    }
}
