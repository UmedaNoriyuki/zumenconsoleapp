﻿using System;
using System.Collections.Generic;
using Npgsql;
using System.Diagnostics;
using System.Data;

namespace ZumenConsoleApp
{
    public class CommonModule
    {
        private const string JISSEKI_TUUJOU = "0";
        private const string JISSEKI_TUIKA = "1";
        private const string JISSEKI_FUGUAI = "2";
        private const string JISSEKI_MITUMORI = "9";
        private const string WORKSTATE_WORKING = "0";
        private const string WORKSTATE_STOP = "1";
        private const string WORKSTATE_END = "2";

        /// <summary>
        /// 図面履歴情報を抽出する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <returns></returns>
        public static List<RirekiData> GetZumenRirekiInfo(string connectionString, string zumenBan)
        {
            var rirekiDataList = new List<RirekiData>();
            var rirekiData = new RirekiData();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  図面番号" +
                                "  ,更新日時" +
                                "  ,更新者" +
                                "  ,図面パス" +
                                "  ,\"オーダーNO\"" +
                                " from 実績加工履歴データ " +
                                " where 図面番号 = :zumenBan" +
                                "   or (図面番号 = substr(:zumenBan, 1, length(:zumenBan) - 1)" +
                                "     and substr(:zumenBan, LENGTH(:zumenBan)) ~ '[A-Z]') " +
                                "   or (substr(図面番号, 1, length(図面番号) - 1) = substr(:zumenBan, 1, length(:zumenBan) - 1) " +
                                "     and substr(:zumenBan, LENGTH(:zumenBan)) ~ '[A-Z]' and substr(図面番号, LENGTH(図面番号)) ~ '[A-Z]') " +
                                "   or (LENGTH(図面番号) > 5 and substr(図面番号, 1, length(図面番号) - 3) = substr(:zumenBan, 1, length(:zumenBan) - 4) " +
                                "     and substr(図面番号, length(図面番号) - 2) = substr(:zumenBan, length(:zumenBan) - 2) and substr(図面番号, LENGTH(図面番号) - 2, 1) = '#')" +
                                "   or (LENGTH(図面番号) > 5 and substr(図面番号, 1, length(図面番号) - 4) = substr(:zumenBan, 1, length(:zumenBan) - 4) " +
                                "     and substr(図面番号, length(図面番号) - 2) = substr(:zumenBan, length(:zumenBan) - 2) and substr(図面番号, LENGTH(図面番号) - 2, 1) = '#' " +
                                "     and substr(図面番号, LENGTH(図面番号) - 3, 1) ~ '[A-Z]')" +
                                " order by 更新日時";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = zumenBan });

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        rirekiData = new RirekiData();
                        rirekiData.OrderNo = ConvertToString(result["オーダーNO"]);
                        rirekiData.ZumenBan = ConvertToString(result["図面番号"]);
                        rirekiData.ZumenPath = ConvertToString(result["図面パス"]);
                        rirekiData.FileName = System.IO.Path.GetFileNameWithoutExtension(rirekiData.ZumenPath);
                        rirekiData.UpdateDater = ConvertToString(result["更新者"]);
                        rirekiData.UpdateDateTime = DateTime.Parse(result["更新日時"].ToString()).ToString();
                        rirekiDataList.Add(rirekiData);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return rirekiDataList;
                }
            }
            return rirekiDataList;
        }

        /// <summary>
        /// 作業情報を検索する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="blockNo"></param>
        /// <returns></returns>
        public static List<WorkList> GetWorkInfo(string connectionString
                                        , string orderNo
                                        , string blockNo)
        {
            var WorkListData = new List<WorkList>();
            var WorkData = new WorkList();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select " +
                                " sj.*" +
                                " ,sm.作業者名" +
                                " from 作業者実績データ sj " +
                                " left join 作業者マスタ sm " +
                                "  on sj.作業者コード = sm.作業者コード " +
                                " where \"オーダーNO\" = UPPER('" + orderNo + "')";
                if (blockNo != "")
                {
                    sqlCmdTxt += " and substr(ブロック番号, 8) = '" + blockNo + "'";
                }

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        WorkData = new WorkList();
                        WorkData.OrderNo = CommonModule.ConvertToString(result["オーダーNO"]);
                        WorkData.BlockNo = CommonModule.ConvertToString(result["ブロック番号"]);
                        WorkData.Mitumori = CommonModule.ConvertToString(result["見積時間"]);
                        WorkData.Mokuhyo = CommonModule.ConvertToString(result["目標時間"]);
                        switch (CommonModule.ConvertToString(result["実績区分"]))
                        {
                            case JISSEKI_TUUJOU:
                                WorkData.Jisseki = "通常";
                                break;
                            case JISSEKI_TUIKA:
                                WorkData.Jisseki = "追加";
                                break;
                            case JISSEKI_FUGUAI:
                                WorkData.Jisseki = "不具合";
                                break;
                            case JISSEKI_MITUMORI:
                                WorkData.Jisseki = "見積";
                                break;
                        }
                        WorkData.Worker = CommonModule.ConvertToString(result["作業者名"]);
                        WorkData.StartTime = CommonModule.ConvertToDateTimeString(result["作業開始時間"]);
                        WorkData.ReStartTime = CommonModule.ConvertToDateTimeString(result["作業再開時間"]);
                        switch (CommonModule.ConvertToString(result["作業区分"]))
                        {
                            case WORKSTATE_WORKING:
                                WorkData.WorkState = "作業中";
                                break;
                            case WORKSTATE_STOP:
                                WorkData.WorkState = "作業中断";
                                break;
                            case WORKSTATE_END:
                                WorkData.WorkState = "作業終了";
                                break;
                        }
                        WorkData.SumWorkTime = CommonModule.ConvertToString(result["累積作業時間"]);
                        WorkListData.Add(WorkData);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return null;
                }
            }
            return WorkListData;
        }

        /// <summary>
        /// 作業者一覧を取得する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static List<ZumenWorker> GetZumenWorkers(string connectionString)
        {
            List<ZumenWorker> zumenWorkerList = new List<ZumenWorker>();
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var cmd = new NpgsqlCommand("select 作業者コード " +
                                            " ,right('00' || 作業者コード,3) || ' ' || replace( replace(作業者名,'　','') ,' ','') 作業者名 " +
                                            " from 作業者マスタ " +
                                            " where 作業者コード < 200 " +
                                            " order by 1", con);

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        zumenWorkerList.Add(
                            new ZumenWorker
                            {
                                HyoujiMei = result["作業者名"].ToString(),
                                TantouCode = (int)result["作業者コード"]
                            }
                        );
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    zumenWorkerList = new List<ZumenWorker>();
                    return zumenWorkerList;
                }
            }
            return zumenWorkerList;
        }

        /// <summary>
        /// 機種名よりオーダーNOを取得する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="kisyu"></param>
        /// <param name="caseFlg">0:組立図検索時</param>
        /// <returns></returns>
        public static List<string> GetOrderNoFromKisyu(string connectionString, string kisyu, int caseFlg)
        {
            List<string> orderNoList = new List<string>();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select distinct okb.\"オーダーNO\"" +
                                " from オーダー管理部材テーブル okb";
                // 組立図検索の場合
                if (caseFlg == 0)
                {
                    sqlCmdTxt += " inner join \"ess村田PLオーダー情報\" od" +
                                "   on od.\"REPROオーダーNO\" = okb.\"オーダーNO\"";
                }
                sqlCmdTxt += " where okb.機種名 like '%" + kisyu + "%' ";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        orderNoList.Add(CommonModule.ConvertToString(result["オーダーNO"]));
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    orderNoList = new List<string>();
                    return orderNoList;
                }
            }
            return orderNoList;
        }

        public static string ConvertToString(object obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            else
            {
                return obj.ToString();
            }

        }

        public static int ConvertToInt(object obj)
        {
            if (obj == null || obj.ToString() == "")
            {
                return 0;
            }
            else
            {
                return int.Parse(obj.ToString());
            }

        }

        public static double ConvertToDouble(object obj)
        {
            if (obj == null || obj.ToString() == "")
            {
                return 0.00;
            }
            else
            {
                return double.Parse(obj.ToString());
            }

        }

        public static string ConvertToDateTimeString(object obj)
        {
            if (obj == null || obj.ToString() == "")
            {
                return string.Empty;
            }
            else
            {
                return DateTime.Parse(obj.ToString()).ToString("yyyy/MM/dd HH:mm:ss");
            }

        }

        public static string ConvertToDateString(object obj)
        {
            if (obj == null || obj.ToString() == "")
            {
                return string.Empty;
            }
            else
            {
                return DateTime.Parse(obj.ToString()).ToString("yyyy/MM/dd");
            }

        }
    }
}
