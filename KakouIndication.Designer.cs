﻿namespace ZumenConsoleApp
{
    partial class KakouIndication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCopy = new System.Windows.Forms.Button();
            this.cmbCopy = new System.Windows.Forms.ComboBox();
            this.label59 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtNouki = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.txtKisyu = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.btnZumenDisp = new System.Windows.Forms.Button();
            this.btnSagyoDel = new System.Windows.Forms.Button();
            this.btnSagyoAdd = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.btnKouteiDel = new System.Windows.Forms.Button();
            this.btnKouteiAdd = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.txtZumenName = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.dgv_工程表 = new System.Windows.Forms.DataGridView();
            this.工程順 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.加工先 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.作業工程名 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.完了予定日 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.出荷 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.注意事項 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.チェック = new System.Windows.Forms.DataGridViewButtonColumn();
            this.作業者印 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.作業完了日 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.txtKouteiNo = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.btnRegister = new System.Windows.Forms.Button();
            this.btnClientCalendar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgv_材料 = new System.Windows.Forms.DataGridView();
            this.メーカー = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.材質 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.サイズ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.単価 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.金額 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblKakouIndicationCd = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnExcel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDelivLc = new System.Windows.Forms.TextBox();
            this.btnWorker = new System.Windows.Forms.Button();
            this.lbl作業者 = new System.Windows.Forms.Label();
            this.txtWorker = new System.Windows.Forms.TextBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCalendarColumn1 = new ZumenConsoleApp.DataGridViewCalendarColumn();
            this.dataGridViewCalendarColumn2 = new ZumenConsoleApp.DataGridViewCalendarColumn();
            this.btnMemo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_工程表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_材料)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCopy
            // 
            this.btnCopy.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnCopy.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCopy.Location = new System.Drawing.Point(974, 8);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(175, 44);
            this.btnCopy.TabIndex = 4;
            this.btnCopy.Text = "コピーして作成";
            this.btnCopy.UseVisualStyleBackColor = false;
            this.btnCopy.Click += new System.EventHandler(this.ClickBtnCopy);
            // 
            // cmbCopy
            // 
            this.cmbCopy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCopy.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbCopy.FormattingEnabled = true;
            this.cmbCopy.Location = new System.Drawing.Point(632, 12);
            this.cmbCopy.Name = "cmbCopy";
            this.cmbCopy.Size = new System.Drawing.Size(320, 32);
            this.cmbCopy.TabIndex = 3;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label59.Location = new System.Drawing.Point(12, 12);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(130, 24);
            this.label59.TabIndex = 71;
            this.label59.Text = "工程表番号";
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnSearch.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(362, 8);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(92, 44);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "検索";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.ClickBtnSearch);
            // 
            // txtNouki
            // 
            this.txtNouki.BackColor = System.Drawing.Color.Aquamarine;
            this.txtNouki.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.txtNouki.ForeColor = System.Drawing.Color.Gray;
            this.txtNouki.Location = new System.Drawing.Point(784, 188);
            this.txtNouki.Name = "txtNouki";
            this.txtNouki.ReadOnly = true;
            this.txtNouki.Size = new System.Drawing.Size(170, 31);
            this.txtNouki.TabIndex = 69;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label58.Location = new System.Drawing.Point(614, 188);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(154, 24);
            this.label58.TabIndex = 68;
            this.label58.Text = "検査完了納期";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.BackColor = System.Drawing.Color.Aquamarine;
            this.txtOrderNo.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.txtOrderNo.ForeColor = System.Drawing.Color.Gray;
            this.txtOrderNo.Location = new System.Drawing.Point(132, 231);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.ReadOnly = true;
            this.txtOrderNo.Size = new System.Drawing.Size(170, 31);
            this.txtOrderNo.TabIndex = 67;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label57.Location = new System.Drawing.Point(12, 233);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(112, 24);
            this.label57.TabIndex = 66;
            this.label57.Text = "オーダーNo";
            // 
            // txtKisyu
            // 
            this.txtKisyu.BackColor = System.Drawing.Color.Aquamarine;
            this.txtKisyu.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.txtKisyu.ForeColor = System.Drawing.Color.Gray;
            this.txtKisyu.Location = new System.Drawing.Point(420, 188);
            this.txtKisyu.Name = "txtKisyu";
            this.txtKisyu.ReadOnly = true;
            this.txtKisyu.Size = new System.Drawing.Size(170, 31);
            this.txtKisyu.TabIndex = 65;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label56.Location = new System.Drawing.Point(323, 190);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(82, 24);
            this.label56.TabIndex = 64;
            this.label56.Text = "機種名";
            // 
            // btnZumenDisp
            // 
            this.btnZumenDisp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZumenDisp.BackColor = System.Drawing.Color.Orange;
            this.btnZumenDisp.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnZumenDisp.Location = new System.Drawing.Point(924, 64);
            this.btnZumenDisp.Name = "btnZumenDisp";
            this.btnZumenDisp.Size = new System.Drawing.Size(105, 44);
            this.btnZumenDisp.TabIndex = 7;
            this.btnZumenDisp.Text = " 図面表示";
            this.btnZumenDisp.UseVisualStyleBackColor = false;
            this.btnZumenDisp.Click += new System.EventHandler(this.ClickBtn_DispZumen);
            // 
            // btnSagyoDel
            // 
            this.btnSagyoDel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnSagyoDel.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSagyoDel.Location = new System.Drawing.Point(799, 225);
            this.btnSagyoDel.Name = "btnSagyoDel";
            this.btnSagyoDel.Size = new System.Drawing.Size(92, 44);
            this.btnSagyoDel.TabIndex = 12;
            this.btnSagyoDel.Text = "削除";
            this.btnSagyoDel.UseVisualStyleBackColor = false;
            this.btnSagyoDel.Click += new System.EventHandler(this.ClickBtnSagyoDel);
            // 
            // btnSagyoAdd
            // 
            this.btnSagyoAdd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnSagyoAdd.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSagyoAdd.Location = new System.Drawing.Point(701, 225);
            this.btnSagyoAdd.Name = "btnSagyoAdd";
            this.btnSagyoAdd.Size = new System.Drawing.Size(92, 44);
            this.btnSagyoAdd.TabIndex = 11;
            this.btnSagyoAdd.Text = "追加";
            this.btnSagyoAdd.UseVisualStyleBackColor = false;
            this.btnSagyoAdd.Click += new System.EventHandler(this.ClickBtnSagyoAdd);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label54.Location = new System.Drawing.Point(584, 231);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(106, 24);
            this.label54.TabIndex = 60;
            this.label54.Text = "作業工程";
            // 
            // btnKouteiDel
            // 
            this.btnKouteiDel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnKouteiDel.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnKouteiDel.Location = new System.Drawing.Point(485, 225);
            this.btnKouteiDel.Name = "btnKouteiDel";
            this.btnKouteiDel.Size = new System.Drawing.Size(92, 44);
            this.btnKouteiDel.TabIndex = 10;
            this.btnKouteiDel.Text = "削除";
            this.btnKouteiDel.UseVisualStyleBackColor = false;
            this.btnKouteiDel.Click += new System.EventHandler(this.ClickBtnKouteiDel);
            // 
            // btnKouteiAdd
            // 
            this.btnKouteiAdd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnKouteiAdd.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnKouteiAdd.Location = new System.Drawing.Point(389, 225);
            this.btnKouteiAdd.Name = "btnKouteiAdd";
            this.btnKouteiAdd.Size = new System.Drawing.Size(92, 44);
            this.btnKouteiAdd.TabIndex = 9;
            this.btnKouteiAdd.Text = "追加";
            this.btnKouteiAdd.UseVisualStyleBackColor = false;
            this.btnKouteiAdd.Click += new System.EventHandler(this.ClickBtnKouteiAdd);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label53.Location = new System.Drawing.Point(323, 231);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(58, 24);
            this.label53.TabIndex = 57;
            this.label53.Text = "工程";
            // 
            // txtZumenName
            // 
            this.txtZumenName.BackColor = System.Drawing.Color.Aquamarine;
            this.txtZumenName.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.txtZumenName.ForeColor = System.Drawing.Color.Gray;
            this.txtZumenName.Location = new System.Drawing.Point(132, 188);
            this.txtZumenName.Name = "txtZumenName";
            this.txtZumenName.ReadOnly = true;
            this.txtZumenName.Size = new System.Drawing.Size(170, 31);
            this.txtZumenName.TabIndex = 56;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label52.Location = new System.Drawing.Point(12, 188);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(106, 24);
            this.label52.TabIndex = 55;
            this.label52.Text = "図面番号";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgv_工程表
            // 
            this.dgv_工程表.AllowUserToAddRows = false;
            this.dgv_工程表.AllowUserToDeleteRows = false;
            this.dgv_工程表.AllowUserToResizeColumns = false;
            this.dgv_工程表.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgv_工程表.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_工程表.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_工程表.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_工程表.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_工程表.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_工程表.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_工程表.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.工程順,
            this.加工先,
            this.作業工程名,
            this.完了予定日,
            this.出荷,
            this.注意事項,
            this.チェック,
            this.作業者印,
            this.作業完了日});
            this.dgv_工程表.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_工程表.Location = new System.Drawing.Point(12, 276);
            this.dgv_工程表.MultiSelect = false;
            this.dgv_工程表.Name = "dgv_工程表";
            this.dgv_工程表.RowHeadersVisible = false;
            this.dgv_工程表.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgv_工程表.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_工程表.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgv_工程表.RowTemplate.Height = 28;
            this.dgv_工程表.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgv_工程表.Size = new System.Drawing.Size(1257, 340);
            this.dgv_工程表.TabIndex = 14;
            this.dgv_工程表.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellContentClickdgv_工程表);
            this.dgv_工程表.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellEnterDataGridView);
            this.dgv_工程表.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.CellMouseDownDgv_工程表);
            this.dgv_工程表.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.EditingControlShowingDgv_工程表);
            // 
            // 工程順
            // 
            this.工程順.DataPropertyName = "工程順";
            this.工程順.HeaderText = "工程順";
            this.工程順.Name = "工程順";
            this.工程順.ReadOnly = true;
            this.工程順.Width = 91;
            // 
            // 加工先
            // 
            this.加工先.DataPropertyName = "加工先";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.加工先.DefaultCellStyle = dataGridViewCellStyle3;
            this.加工先.HeaderText = "加工依頼先";
            this.加工先.Name = "加工先";
            this.加工先.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.加工先.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.加工先.Width = 129;
            // 
            // 作業工程名
            // 
            this.作業工程名.DataPropertyName = "作業工程名";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.作業工程名.DefaultCellStyle = dataGridViewCellStyle4;
            this.作業工程名.HeaderText = "作業工程";
            this.作業工程名.Name = "作業工程名";
            this.作業工程名.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.作業工程名.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.作業工程名.Width = 110;
            // 
            // 完了予定日
            // 
            this.完了予定日.DataPropertyName = "完了予定日";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS UI Gothic", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle5.Format = "yyyy/MM/dd (ddd)";
            dataGridViewCellStyle5.NullValue = null;
            this.完了予定日.DefaultCellStyle = dataGridViewCellStyle5;
            this.完了予定日.HeaderText = "完了予定日";
            this.完了予定日.Name = "完了予定日";
            this.完了予定日.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.完了予定日.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.完了予定日.Width = 129;
            // 
            // 出荷
            // 
            this.出荷.DataPropertyName = "出荷";
            this.出荷.HeaderText = "";
            this.出荷.Items.AddRange(new object[] {
            "",
            "出荷",
            "着"});
            this.出荷.Name = "出荷";
            this.出荷.Width = 5;
            // 
            // 注意事項
            // 
            this.注意事項.DataPropertyName = "注意事項";
            this.注意事項.HeaderText = "注意事項";
            this.注意事項.Name = "注意事項";
            this.注意事項.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.注意事項.Width = 110;
            // 
            // チェック
            // 
            this.チェック.DataPropertyName = "チェック";
            this.チェック.HeaderText = "✓";
            this.チェック.Name = "チェック";
            this.チェック.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.チェック.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.チェック.Text = "";
            this.チェック.Width = 53;
            // 
            // 作業者印
            // 
            this.作業者印.DataPropertyName = "作業者印";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.作業者印.DefaultCellStyle = dataGridViewCellStyle6;
            this.作業者印.HeaderText = "作業者印";
            this.作業者印.Name = "作業者印";
            this.作業者印.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.作業者印.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.作業者印.Width = 110;
            // 
            // 作業完了日
            // 
            this.作業完了日.DataPropertyName = "完了日";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            dataGridViewCellStyle7.Format = "yyyy/MM/dd (ddd)";
            dataGridViewCellStyle7.NullValue = null;
            this.作業完了日.DefaultCellStyle = dataGridViewCellStyle7;
            this.作業完了日.HeaderText = "作業完了日";
            this.作業完了日.Name = "作業完了日";
            this.作業完了日.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.作業完了日.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.作業完了日.Width = 129;
            // 
            // txtKouteiNo
            // 
            this.txtKouteiNo.BackColor = System.Drawing.Color.White;
            this.txtKouteiNo.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.txtKouteiNo.ForeColor = System.Drawing.Color.Black;
            this.txtKouteiNo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKouteiNo.Location = new System.Drawing.Point(146, 12);
            this.txtKouteiNo.MaxLength = 9;
            this.txtKouteiNo.Name = "txtKouteiNo";
            this.txtKouteiNo.Size = new System.Drawing.Size(192, 31);
            this.txtKouteiNo.TabIndex = 1;
            this.txtKouteiNo.DoubleClick += new System.EventHandler(this.DoubleClickTxtKouteiNo);
            this.txtKouteiNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownTxtKouteiNo);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label60.Location = new System.Drawing.Point(468, 12);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(157, 24);
            this.label60.TabIndex = 75;
            this.label60.Text = "指示書のコピー";
            // 
            // btnRegister
            // 
            this.btnRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRegister.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnRegister.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRegister.Location = new System.Drawing.Point(1180, 225);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(92, 44);
            this.btnRegister.TabIndex = 14;
            this.btnRegister.Text = "登録";
            this.btnRegister.UseVisualStyleBackColor = false;
            this.btnRegister.Click += new System.EventHandler(this.ClickBtnRegister);
            // 
            // btnClientCalendar
            // 
            this.btnClientCalendar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClientCalendar.BackColor = System.Drawing.Color.Orange;
            this.btnClientCalendar.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClientCalendar.Location = new System.Drawing.Point(1164, 64);
            this.btnClientCalendar.Name = "btnClientCalendar";
            this.btnClientCalendar.Size = new System.Drawing.Size(105, 44);
            this.btnClientCalendar.TabIndex = 8;
            this.btnClientCalendar.Text = "表処業者     カレンダー";
            this.btnClientCalendar.UseVisualStyleBackColor = false;
            this.btnClientCalendar.Click += new System.EventHandler(this.ClickBtnClientCalendar);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label1.Location = new System.Drawing.Point(12, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 24);
            this.label1.TabIndex = 78;
            this.label1.Text = "材料情報";
            // 
            // dgv_材料
            // 
            this.dgv_材料.AllowUserToAddRows = false;
            this.dgv_材料.AllowUserToDeleteRows = false;
            this.dgv_材料.AllowUserToResizeColumns = false;
            this.dgv_材料.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgv_材料.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_材料.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_材料.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_材料.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_材料.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_材料.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_材料.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.メーカー,
            this.材質,
            this.サイズ,
            this.数量,
            this.単価,
            this.金額});
            this.dgv_材料.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_材料.Location = new System.Drawing.Point(12, 114);
            this.dgv_材料.MultiSelect = false;
            this.dgv_材料.Name = "dgv_材料";
            this.dgv_材料.ReadOnly = true;
            this.dgv_材料.RowHeadersVisible = false;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgv_材料.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgv_材料.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgv_材料.RowTemplate.Height = 28;
            this.dgv_材料.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgv_材料.Size = new System.Drawing.Size(1257, 53);
            this.dgv_材料.TabIndex = 79;
            // 
            // メーカー
            // 
            this.メーカー.DataPropertyName = "メーカー";
            this.メーカー.HeaderText = "メーカー";
            this.メーカー.Name = "メーカー";
            this.メーカー.ReadOnly = true;
            this.メーカー.Width = 93;
            // 
            // 材質
            // 
            this.材質.DataPropertyName = "材質";
            dataGridViewCellStyle11.Font = new System.Drawing.Font("MS UI Gothic", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.材質.DefaultCellStyle = dataGridViewCellStyle11;
            this.材質.HeaderText = "材質";
            this.材質.Name = "材質";
            this.材質.ReadOnly = true;
            this.材質.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.材質.Width = 72;
            // 
            // サイズ
            // 
            this.サイズ.DataPropertyName = "サイズ";
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.サイズ.DefaultCellStyle = dataGridViewCellStyle12;
            this.サイズ.HeaderText = "サイズ";
            this.サイズ.Name = "サイズ";
            this.サイズ.ReadOnly = true;
            this.サイズ.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.サイズ.Width = 80;
            // 
            // 数量
            // 
            this.数量.DataPropertyName = "数量";
            this.数量.HeaderText = "数量";
            this.数量.Name = "数量";
            this.数量.ReadOnly = true;
            this.数量.Width = 72;
            // 
            // 単価
            // 
            this.単価.DataPropertyName = "単価";
            this.単価.HeaderText = "単価";
            this.単価.Name = "単価";
            this.単価.ReadOnly = true;
            this.単価.Width = 72;
            // 
            // 金額
            // 
            this.金額.DataPropertyName = "金額";
            this.金額.HeaderText = "金額";
            this.金額.Name = "金額";
            this.金額.ReadOnly = true;
            this.金額.Width = 72;
            // 
            // lblKakouIndicationCd
            // 
            this.lblKakouIndicationCd.AutoSize = true;
            this.lblKakouIndicationCd.Location = new System.Drawing.Point(460, 36);
            this.lblKakouIndicationCd.Name = "lblKakouIndicationCd";
            this.lblKakouIndicationCd.Size = new System.Drawing.Size(35, 12);
            this.lblKakouIndicationCd.TabIndex = 80;
            this.lblKakouIndicationCd.Text = "label2";
            this.lblKakouIndicationCd.Visible = false;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnReset.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnReset.Location = new System.Drawing.Point(1072, 225);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(92, 44);
            this.btnReset.TabIndex = 13;
            this.btnReset.Text = "完了日リセット";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.ClickBtnReset);
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnExcel.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnExcel.Location = new System.Drawing.Point(1178, 8);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(92, 44);
            this.btnExcel.TabIndex = 81;
            this.btnExcel.Text = "Excel";
            this.btnExcel.UseVisualStyleBackColor = false;
            this.btnExcel.Click += new System.EventHandler(this.ClickBtnExcel);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(12, 623);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(368, 12);
            this.label2.TabIndex = 82;
            this.label2.Text = "*最終工程には検査を、表処の前には、表面処理前検査を設定してください。";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label4.Location = new System.Drawing.Point(12, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 24);
            this.label4.TabIndex = 85;
            this.label4.Text = "指示書作成者";
            // 
            // txtCreator
            // 
            this.txtCreator.BackColor = System.Drawing.Color.Aquamarine;
            this.txtCreator.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.txtCreator.ForeColor = System.Drawing.Color.Gray;
            this.txtCreator.Location = new System.Drawing.Point(172, 56);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(208, 31);
            this.txtCreator.TabIndex = 86;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.label5.Location = new System.Drawing.Point(970, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 24);
            this.label5.TabIndex = 87;
            this.label5.Text = "納入場所";
            // 
            // txtDelivLc
            // 
            this.txtDelivLc.BackColor = System.Drawing.Color.Aquamarine;
            this.txtDelivLc.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.txtDelivLc.ForeColor = System.Drawing.Color.Gray;
            this.txtDelivLc.Location = new System.Drawing.Point(1099, 188);
            this.txtDelivLc.Name = "txtDelivLc";
            this.txtDelivLc.ReadOnly = true;
            this.txtDelivLc.Size = new System.Drawing.Size(170, 31);
            this.txtDelivLc.TabIndex = 88;
            // 
            // btnWorker
            // 
            this.btnWorker.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnWorker.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnWorker.Location = new System.Drawing.Point(472, 51);
            this.btnWorker.Name = "btnWorker";
            this.btnWorker.Size = new System.Drawing.Size(92, 44);
            this.btnWorker.TabIndex = 89;
            this.btnWorker.Text = "作業者";
            this.btnWorker.UseVisualStyleBackColor = false;
            this.btnWorker.Click += new System.EventHandler(this.ClickBtnWorker);
            // 
            // lbl作業者
            // 
            this.lbl作業者.AutoSize = true;
            this.lbl作業者.Location = new System.Drawing.Point(820, 64);
            this.lbl作業者.Name = "lbl作業者";
            this.lbl作業者.Size = new System.Drawing.Size(57, 12);
            this.lbl作業者.TabIndex = 90;
            this.lbl作業者.Text = "lbl_作業者";
            this.lbl作業者.Visible = false;
            // 
            // txtWorker
            // 
            this.txtWorker.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.txtWorker.Location = new System.Drawing.Point(570, 58);
            this.txtWorker.Name = "txtWorker";
            this.txtWorker.ReadOnly = true;
            this.txtWorker.Size = new System.Drawing.Size(198, 31);
            this.txtWorker.TabIndex = 91;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "工程順";
            this.dataGridViewTextBoxColumn1.HeaderText = "工程順";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 120;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "メーカー";
            this.dataGridViewTextBoxColumn2.HeaderText = "メーカー";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.Width = 93;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "材質";
            dataGridViewCellStyle14.Font = new System.Drawing.Font("MS UI Gothic", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn3.HeaderText = "材質";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.Width = 72;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "サイズ";
            dataGridViewCellStyle15.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn4.HeaderText = "サイズ";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.Width = 80;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "数量";
            dataGridViewCellStyle16.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn5.HeaderText = "数量";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.Width = 72;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "単価";
            this.dataGridViewTextBoxColumn6.HeaderText = "単価";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 72;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "金額";
            this.dataGridViewTextBoxColumn7.HeaderText = "金額";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 72;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "金額";
            this.dataGridViewTextBoxColumn8.HeaderText = "金額";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 72;
            // 
            // dataGridViewCalendarColumn1
            // 
            this.dataGridViewCalendarColumn1.DataPropertyName = "完了予定日";
            dataGridViewCellStyle17.Font = new System.Drawing.Font("MS UI Gothic", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dataGridViewCalendarColumn1.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewCalendarColumn1.HeaderText = "完了予定日";
            this.dataGridViewCalendarColumn1.Name = "dataGridViewCalendarColumn1";
            this.dataGridViewCalendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCalendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCalendarColumn1.Width = 129;
            // 
            // dataGridViewCalendarColumn2
            // 
            this.dataGridViewCalendarColumn2.DataPropertyName = "完了日";
            dataGridViewCellStyle18.Font = new System.Drawing.Font("MS UI Gothic", 14.25F);
            this.dataGridViewCalendarColumn2.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewCalendarColumn2.HeaderText = "作業完了日";
            this.dataGridViewCalendarColumn2.Name = "dataGridViewCalendarColumn2";
            this.dataGridViewCalendarColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCalendarColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCalendarColumn2.Width = 129;
            // 
            // btnMemo
            // 
            this.btnMemo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMemo.BackColor = System.Drawing.Color.Orange;
            this.btnMemo.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMemo.Location = new System.Drawing.Point(1044, 64);
            this.btnMemo.Name = "btnMemo";
            this.btnMemo.Size = new System.Drawing.Size(105, 44);
            this.btnMemo.TabIndex = 92;
            this.btnMemo.Text = "注意事項一覧";
            this.btnMemo.UseVisualStyleBackColor = false;
            this.btnMemo.Click += new System.EventHandler(this.ClickBtnAttention);
            // 
            // KakouIndication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 647);
            this.Controls.Add(this.btnMemo);
            this.Controls.Add(this.txtWorker);
            this.Controls.Add(this.lbl作業者);
            this.Controls.Add(this.btnWorker);
            this.Controls.Add(this.txtDelivLc);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCreator);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnExcel);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.lblKakouIndicationCd);
            this.Controls.Add(this.dgv_材料);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.txtKouteiNo);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.cmbCopy);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtNouki);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.txtOrderNo);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.txtKisyu);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.btnSagyoDel);
            this.Controls.Add(this.btnSagyoAdd);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.btnKouteiDel);
            this.Controls.Add(this.btnKouteiAdd);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.txtZumenName);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.dgv_工程表);
            this.Controls.Add(this.btnClientCalendar);
            this.Controls.Add(this.btnZumenDisp);
            this.Name = "KakouIndication";
            this.Text = "加工指示書";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClosingKakouIndication);
            this.Load += new System.EventHandler(this.LoadKakouIndication);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_工程表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_材料)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.ComboBox cmbCopy;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtNouki;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtKisyu;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Button btnZumenDisp;
        private System.Windows.Forms.Button btnSagyoDel;
        private System.Windows.Forms.Button btnSagyoAdd;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Button btnKouteiDel;
        private System.Windows.Forms.Button btnKouteiAdd;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtZumenName;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.DataGridView dgv_工程表;
        private System.Windows.Forms.TextBox txtKouteiNo;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewCalendarColumn dataGridViewCalendarColumn1;
        private DataGridViewCalendarColumn dataGridViewCalendarColumn2;
        private System.Windows.Forms.Button btnClientCalendar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgv_材料;
        private System.Windows.Forms.Label lblKakouIndicationCd;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridViewTextBoxColumn メーカー;
        private System.Windows.Forms.DataGridViewTextBoxColumn 材質;
        private System.Windows.Forms.DataGridViewTextBoxColumn サイズ;
        private System.Windows.Forms.DataGridViewTextBoxColumn 数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 単価;
        private System.Windows.Forms.DataGridViewTextBoxColumn 金額;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCreator;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDelivLc;
        private System.Windows.Forms.Button btnWorker;
        private System.Windows.Forms.Label lbl作業者;
        private System.Windows.Forms.TextBox txtWorker;
        private System.Windows.Forms.DataGridViewTextBoxColumn 工程順;
        private System.Windows.Forms.DataGridViewComboBoxColumn 加工先;
        private System.Windows.Forms.DataGridViewButtonColumn 作業工程名;
        private System.Windows.Forms.DataGridViewButtonColumn 完了予定日;
        private System.Windows.Forms.DataGridViewComboBoxColumn 出荷;
        private System.Windows.Forms.DataGridViewTextBoxColumn 注意事項;
        private System.Windows.Forms.DataGridViewButtonColumn チェック;
        private System.Windows.Forms.DataGridViewButtonColumn 作業者印;
        private System.Windows.Forms.DataGridViewButtonColumn 作業完了日;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.Button btnMemo;
    }
}