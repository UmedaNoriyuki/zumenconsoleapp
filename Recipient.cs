﻿
namespace ZumenConsoleApp
{
    public class Recipient
    {
        public string MailAddress { get; set; }
        public string Name { get; set; }
        public string TantouCd { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
