﻿namespace ZumenConsoleApp.View
{
    partial class FullDisp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FullDisp));
            this.axDeskCtrlLeft = new AxDESKCTRLLib.AxDeskCtrl();
            this.axDwCtrlEdRight = new AxDWCTRLEDLib.AxDwCtrlEd();
            this.axDwCtrlRight = new AxDWCTRLLib.AxDwCtrl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.LeftToolStripMenuItemList = new System.Windows.Forms.ToolStripMenuItem();
            this.LeftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RightToolStripMenuItemList = new System.Windows.Forms.ToolStripMenuItem();
            this.DoubleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightNtBtn = new System.Windows.Forms.Button();
            this.rightBkBtn = new System.Windows.Forms.Button();
            this.leftBkBtn = new System.Windows.Forms.Button();
            this.leftNtBtn = new System.Windows.Forms.Button();
            this.lblLeftCnt = new System.Windows.Forms.Label();
            this.lblRightCnt = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrlLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axDwCtrlEdRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axDwCtrlRight)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // axDeskCtrlLeft
            // 
            this.axDeskCtrlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.axDeskCtrlLeft.Enabled = true;
            this.axDeskCtrlLeft.Location = new System.Drawing.Point(0, 62);
            this.axDeskCtrlLeft.Name = "axDeskCtrlLeft";
            this.axDeskCtrlLeft.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axDeskCtrlLeft.OcxState")));
            this.axDeskCtrlLeft.Size = new System.Drawing.Size(800, 573);
            this.axDeskCtrlLeft.TabIndex = 3;
            // 
            // axDwCtrlEdRight
            // 
            this.axDwCtrlEdRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axDwCtrlEdRight.Enabled = true;
            this.axDwCtrlEdRight.Location = new System.Drawing.Point(800, 62);
            this.axDwCtrlEdRight.Name = "axDwCtrlEdRight";
            this.axDwCtrlEdRight.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axDwCtrlEdRight.OcxState")));
            this.axDwCtrlEdRight.Size = new System.Drawing.Size(784, 573);
            this.axDwCtrlEdRight.TabIndex = 4;
            // 
            // axDwCtrlRight
            // 
            this.axDwCtrlRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axDwCtrlRight.Enabled = true;
            this.axDwCtrlRight.Location = new System.Drawing.Point(800, 62);
            this.axDwCtrlRight.Name = "axDwCtrlRight";
            this.axDwCtrlRight.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axDwCtrlRight.OcxState")));
            this.axDwCtrlRight.Size = new System.Drawing.Size(784, 573);
            this.axDwCtrlRight.TabIndex = 17;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Yu Gothic UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LeftToolStripMenuItemList,
            this.LeftToolStripMenuItem,
            this.RightToolStripMenuItem,
            this.RightToolStripMenuItemList,
            this.DoubleToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 24);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1584, 38);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // LeftToolStripMenuItemList
            // 
            this.LeftToolStripMenuItemList.Name = "LeftToolStripMenuItemList";
            this.LeftToolStripMenuItemList.Size = new System.Drawing.Size(172, 34);
            this.LeftToolStripMenuItemList.Text = "図面一覧（左）";
            this.LeftToolStripMenuItemList.Click += new System.EventHandler(this.LeftZumenMenuClick);
            // 
            // LeftToolStripMenuItem
            // 
            this.LeftToolStripMenuItem.Name = "LeftToolStripMenuItem";
            this.LeftToolStripMenuItem.Size = new System.Drawing.Size(109, 34);
            this.LeftToolStripMenuItem.Text = "左側表示";
            this.LeftToolStripMenuItem.Click += new System.EventHandler(this.LeftToolStripMenuItem_Click);
            // 
            // RightToolStripMenuItem
            // 
            this.RightToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.RightToolStripMenuItem.Name = "RightToolStripMenuItem";
            this.RightToolStripMenuItem.Size = new System.Drawing.Size(109, 34);
            this.RightToolStripMenuItem.Text = "右側表示";
            this.RightToolStripMenuItem.Click += new System.EventHandler(this.RightToolStripMenuItem_Click);
            // 
            // RightToolStripMenuItemList
            // 
            this.RightToolStripMenuItemList.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.RightToolStripMenuItemList.Name = "RightToolStripMenuItemList";
            this.RightToolStripMenuItemList.Size = new System.Drawing.Size(172, 34);
            this.RightToolStripMenuItemList.Text = "図面一覧（右）";
            this.RightToolStripMenuItemList.Click += new System.EventHandler(this.RightZumenMenuClick);
            // 
            // DoubleToolStripMenuItem
            // 
            this.DoubleToolStripMenuItem.Name = "DoubleToolStripMenuItem";
            this.DoubleToolStripMenuItem.Size = new System.Drawing.Size(109, 34);
            this.DoubleToolStripMenuItem.Text = "両側表示";
            this.DoubleToolStripMenuItem.Click += new System.EventHandler(this.DoubleToolStripMenuItem_Click);
            // 
            // rightNtBtn
            // 
            this.rightNtBtn.BackColor = System.Drawing.Color.White;
            this.rightNtBtn.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rightNtBtn.Location = new System.Drawing.Point(1541, 296);
            this.rightNtBtn.Name = "rightNtBtn";
            this.rightNtBtn.Size = new System.Drawing.Size(50, 50);
            this.rightNtBtn.TabIndex = 11;
            this.rightNtBtn.Text = ">";
            this.rightNtBtn.UseVisualStyleBackColor = false;
            this.rightNtBtn.Click += new System.EventHandler(this.rightNtBtn_Click);
            // 
            // rightBkBtn
            // 
            this.rightBkBtn.BackColor = System.Drawing.Color.White;
            this.rightBkBtn.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rightBkBtn.Location = new System.Drawing.Point(784, 296);
            this.rightBkBtn.Name = "rightBkBtn";
            this.rightBkBtn.Size = new System.Drawing.Size(50, 50);
            this.rightBkBtn.TabIndex = 12;
            this.rightBkBtn.Text = "<";
            this.rightBkBtn.UseVisualStyleBackColor = false;
            this.rightBkBtn.Click += new System.EventHandler(this.rightBkBtn_Click);
            // 
            // leftBkBtn
            // 
            this.leftBkBtn.BackColor = System.Drawing.Color.White;
            this.leftBkBtn.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.leftBkBtn.Location = new System.Drawing.Point(0, 296);
            this.leftBkBtn.Name = "leftBkBtn";
            this.leftBkBtn.Size = new System.Drawing.Size(50, 50);
            this.leftBkBtn.TabIndex = 13;
            this.leftBkBtn.Text = "<";
            this.leftBkBtn.UseVisualStyleBackColor = false;
            this.leftBkBtn.Click += new System.EventHandler(this.leftBkBtn_Click);
            // 
            // leftNtBtn
            // 
            this.leftNtBtn.BackColor = System.Drawing.Color.White;
            this.leftNtBtn.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.leftNtBtn.Location = new System.Drawing.Point(738, 296);
            this.leftNtBtn.Name = "leftNtBtn";
            this.leftNtBtn.Size = new System.Drawing.Size(50, 50);
            this.leftNtBtn.TabIndex = 14;
            this.leftNtBtn.Text = ">";
            this.leftNtBtn.UseVisualStyleBackColor = false;
            this.leftNtBtn.Click += new System.EventHandler(this.leftNtBtn_Click);
            // 
            // lblLeftCnt
            // 
            this.lblLeftCnt.AutoSize = true;
            this.lblLeftCnt.Location = new System.Drawing.Point(743, 92);
            this.lblLeftCnt.Name = "lblLeftCnt";
            this.lblLeftCnt.Size = new System.Drawing.Size(0, 12);
            this.lblLeftCnt.TabIndex = 15;
            this.lblLeftCnt.Visible = false;
            // 
            // lblRightCnt
            // 
            this.lblRightCnt.AutoSize = true;
            this.lblRightCnt.Location = new System.Drawing.Point(1224, 170);
            this.lblRightCnt.Name = "lblRightCnt";
            this.lblRightCnt.Size = new System.Drawing.Size(0, 12);
            this.lblRightCnt.TabIndex = 16;
            this.lblRightCnt.Visible = false;
            // 
            // FullDisp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 635);
            this.Controls.Add(this.lblRightCnt);
            this.Controls.Add(this.lblLeftCnt);
            this.Controls.Add(this.leftNtBtn);
            this.Controls.Add(this.leftBkBtn);
            this.Controls.Add(this.rightBkBtn);
            this.Controls.Add(this.rightNtBtn);
            this.Controls.Add(this.axDwCtrlEdRight);
            this.Controls.Add(this.axDwCtrlRight);
            this.Controls.Add(this.axDeskCtrlLeft);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FullDisp";
            this.Text = "全画面表示";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FullDisp_FormClosing);
            this.Load += new System.EventHandler(this.FullDisp_Load);
            this.SizeChanged += new System.EventHandler(this.FullDisp_SizeChanged);
            this.Controls.SetChildIndex(this.menuStrip1, 0);
            this.Controls.SetChildIndex(this.axDeskCtrlLeft, 0);
            this.Controls.SetChildIndex(this.axDwCtrlRight, 0);
            this.Controls.SetChildIndex(this.axDwCtrlEdRight, 0);
            this.Controls.SetChildIndex(this.rightNtBtn, 0);
            this.Controls.SetChildIndex(this.rightBkBtn, 0);
            this.Controls.SetChildIndex(this.leftBkBtn, 0);
            this.Controls.SetChildIndex(this.leftNtBtn, 0);
            this.Controls.SetChildIndex(this.lblLeftCnt, 0);
            this.Controls.SetChildIndex(this.lblRightCnt, 0);
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrlLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axDwCtrlEdRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axDwCtrlRight)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxDESKCTRLLib.AxDeskCtrl axDeskCtrlLeft;
        private AxDWCTRLEDLib.AxDwCtrlEd axDwCtrlEdRight;
        private AxDWCTRLLib.AxDwCtrl axDwCtrlRight;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem LeftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RightToolStripMenuItem;
        private System.Windows.Forms.Button rightNtBtn;
        private System.Windows.Forms.Button rightBkBtn;
        private System.Windows.Forms.Button leftBkBtn;
        private System.Windows.Forms.Button leftNtBtn;
        private System.Windows.Forms.ToolStripMenuItem RightToolStripMenuItemList;
        private System.Windows.Forms.ToolStripMenuItem LeftToolStripMenuItemList;
        private System.Windows.Forms.Label lblLeftCnt;
        private System.Windows.Forms.Label lblRightCnt;
        private System.Windows.Forms.ToolStripMenuItem DoubleToolStripMenuItem;
    }
}