﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Npgsql;
using System.Diagnostics;
using System.Data;
using System.Linq;

namespace ZumenConsoleApp.View
{
    public partial class SearchRireki : BaseView
    {
        public static JissekiKakou JisKak { get; set; }
        public SearchRireki()
        {
            InitializeComponent();
        }

        private void SearchEntry_Load(object sender, EventArgs e)
        {
            if (JisKak != null)
            {
                // 保持したデータを検索条件に表示
                txtOrderNo.Text = JisKak.OrderNo;
                txtZumenBan.Text = JisKak.ZumenBan;
                txtKisyu.Text = JisKak.Kisyu;
                chkOnlyRecentRireki.Checked = JisKak.chkOnlyRecentRireki;
            }
        }

        // 検索ボタン押下時処理
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var conn = Properties.Settings.Default.connectionString;
                // オーダーNoまたは機種名は必須入力
                if ((txtOrderNo.Text == null || txtOrderNo.Text == "") && (txtKisyu.Text == null || txtKisyu.Text == ""))
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("オーダーNoまたは機種名を入力してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                // 図面番号は必須入力
                if (txtZumenBan.Text == null || txtZumenBan.Text == "")
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面番号を入力してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                var orderNo = txtOrderNo.Text;
                // オーダーNOが未入力で機種名が入力されている場合、機種名に紐づくオーダーNOの候補を探す
                if ((txtOrderNo.Text == null || txtOrderNo.Text == "") && txtKisyu.Text != null && txtKisyu.Text != "")
                {
                    SelectOrderNo.ordList = CommonModule.GetOrderNoFromKisyu(conn, txtKisyu.Text, 1);
                    if (SelectOrderNo.ordList.Count == 1)
                    {
                        orderNo = SelectOrderNo.ordList[0];
                    }
                    else if (SelectOrderNo.ordList.Count > 1)
                    {
                        // オーダーNO選択ダイアログ表示
                        var a = new SelectOrderNo();
                        a.ShowDialog();
                        if (!SelectOrderNo.selOrdFlg)
                        {
                            return;
                        }
                        orderNo = SelectOrderNo.orderNo;
                    }
                    else
                    {
                        //メッセージボックスを表示する
                        MessageBox.Show("図面データがありません。",
                            "エラー",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return;
                    }
                }

                var zumenBan = txtZumenBan.Text;
                // 入力した図面番号の候補を探す
                SelectZumenBan.zuBanList = GetZumenBanFromList(conn, orderNo, zumenBan);
                if (SelectZumenBan.zuBanList.Count == 1)
                {
                    zumenBan = SelectZumenBan.zuBanList[0];
                }
                else if (SelectZumenBan.zuBanList.Count > 1)
                {
                    // 図面番号選択ダイアログ表示
                    var a = new SelectZumenBan();
                    a.ShowDialog();
                    if (!SelectZumenBan.selZubanFlg)
                    {
                        return;
                    }
                    zumenBan = SelectZumenBan.zumenBan;
                }
                else
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面データがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                // 実績加工履歴テーブルより履歴データを取得
                var rirekiDataList = new List<RirekiData>();
                rirekiDataList = GetZumenRirekiInfo(conn, zumenBan, orderNo, chkOnlyRecentRireki.Checked);
                if (rirekiDataList == null || rirekiDataList.Count == 0)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("履歴データがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                MainDisp.RirekiDataList = new List<RirekiData>();
                MainDisp.FullRirekiDataList = new List<RirekiData>();
                // 履歴データを追加
                foreach (var rireki in rirekiDataList)
                {
                    if (!rireki.ZumenPath.Contains("履歴データ") && !rireki.ZumenPath.Contains("図面データ"))
                    {
                        continue;
                    }
                    MainDisp.FullRirekiDataList.Add(rireki);
                }
                MainDisp.RirekiDataList = MainDisp.FullRirekiDataList;

                JisKak = new JissekiKakou();
                JisKak.OrderNo = orderNo;
                JisKak.ZumenBan = zumenBan;
                JisKak.Kisyu = txtKisyu.Text;
                JisKak.chkOnlyRecentRireki = chkOnlyRecentRireki.Checked;

                MainDisp.isRirekiSearch = true;

                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // オーダーNoクリア
        private void btnOrderNoClr_Click(object sender, EventArgs e)
        {
            txtOrderNo.Clear();
            txtOrderNo.Focus();
        }

        // 図面番号クリア
        private void btnZumenBanClr_Click(object sender, EventArgs e)
        {
            txtZumenBan.Clear();
            txtZumenBan.Focus();
        }

        // 機種名クリア
        private void btnKisyuClr_Click(object sender, EventArgs e)
        {
            txtKisyu.Clear();
            txtKisyu.Focus();
        }

        // オーダーNOにフォーカス時にEnterを押下すると検索
        private void txtOrderNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        // 図面番号にフォーカス時にEnterを押下すると検索
        private void txtZumenBan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        // 機種名にフォーカス時にEnterを押下すると検索
        private void txtKisyu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        /// <summary>
        /// 図面履歴情報を抽出する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <param name="orderNo"></param>
        /// <param name="onlyRecentFlg"></param>
        /// <returns></returns>
        private List<RirekiData> GetZumenRirekiInfo(string connectionString, string zumenBan, string orderNo, bool onlyRecentFlg)
        {
            var rirekiDataList = new List<RirekiData>();
            var rirekiData = new RirekiData();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "(select " +
                                "  図面番号" +
                                "  ,null 更新日時" +
                                "  ,null 更新者" +
                                "  ,case when jkkd.図面パス is not null then jkkd.図面パス " +
                                "   when zd.パス is not null then zd.パス " +
                                "   when zd2.パス is not null then zd2.パス end as 図面パス" +
                                "  ,\"オーダーNO\"" +
                                "  ,品名 " +
                                " from 実績加工データ jkkd " +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(jkkd.図面番号,'/','／')" +
                                " left join 図面データ zd2" +
                                "   on zd2.ファイル like replace('%jkkd.図面番号%','/','／')" +
                                " where \"オーダーNO\" = :orderNo " +
                                "   and 図面番号 = :zumenBan" +
                                "  limit 1) " +
                                "  union all " +
                                " (select distinct" +
                                "  図面番号" +
                                "  ,更新日時" +
                                "  ,更新者" +
                                "  ,図面パス" +
                                "  ,\"オーダーNO\"" +
                                "  ,null 品名 " +
                                " from 実績加工履歴データ " +
                                " where(図面番号 = :zumenBan" +
                                "   or (図面番号 = substr(:zumenBan, 1, length(:zumenBan) - 1)" +
                                "     and substr(:zumenBan, LENGTH(:zumenBan)) ~ '[A-Z]') " +
                                "   or (substr(図面番号, 1, length(図面番号) - 1) = substr(:zumenBan, 1, length(:zumenBan) - 1) " +
                                "     and substr(:zumenBan, LENGTH(:zumenBan)) ~ '[A-Z]' and substr(図面番号, LENGTH(図面番号)) ~ '[A-Z]') " +
                                "   or (LENGTH(図面番号) > 5 and substr(図面番号, 1, length(図面番号) - 3) = substr(:zumenBan, 1, length(:zumenBan) - 4) " +
                                "     and substr(図面番号, length(図面番号) - 2) = substr(:zumenBan, length(:zumenBan) - 2) and substr(図面番号, LENGTH(図面番号) - 2, 1) = '#')" +
                                "   or (LENGTH(図面番号) > 5 and substr(図面番号, 1, length(図面番号) - 4) = substr(:zumenBan, 1, length(:zumenBan) - 4) " +
                                "     and substr(図面番号, length(図面番号) - 2) = substr(:zumenBan, length(:zumenBan) - 2) and substr(図面番号, LENGTH(図面番号) - 2, 1) = '#'))" +
                                "   and 連番 > 0 " +
                                "   order by 更新日時)";
                var cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = zumenBan });
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        rirekiData = new RirekiData();
                        rirekiData.OrderNo = CommonModule.ConvertToString(result["オーダーNO"]);
                        rirekiData.ZumenBan = CommonModule.ConvertToString(result["図面番号"]);
                        rirekiData.ZumenPath = CommonModule.ConvertToString(result["図面パス"]);
                        rirekiData.FileName = System.IO.Path.GetFileNameWithoutExtension(rirekiData.ZumenPath);
                        rirekiData.UpdateDater = CommonModule.ConvertToString(result["更新者"]);
                        rirekiData.UpdateDateTime = CommonModule.ConvertToDateTimeString(result["更新日時"]);
                        rirekiData.ProductName = CommonModule.ConvertToString(result["品名"]);
                        rirekiDataList.Add(rirekiData);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return rirekiDataList;
                }
            }
            return rirekiDataList;
        }

        /// <summary>
        /// 候補の図面番号のリストより図面番号を取得する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="txtZuBan"></param>
        /// <returns></returns>
        private List<string> GetZumenBanFromList(string connectionString, string orderNo, string txtZuBan)
        {
            List<string> zumenBanList = new List<string>();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select 図面番号 from 実績加工データ" +
                                " where \"オーダーNO\" = '" + orderNo + "' " +
                                "   and 図面番号 like '%" + txtZuBan + "%' " +
                                " order by 図面番号 ";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        zumenBanList.Add(CommonModule.ConvertToString(result["図面番号"]));
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    zumenBanList = new List<string>();
                    return zumenBanList;
                }
            }
            return zumenBanList;
        }
    }
}
