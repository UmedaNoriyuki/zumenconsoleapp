﻿namespace ZumenConsoleApp
{
    partial class DispZumen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DispZumen));
            this.axDeskCtrl = new AxDESKCTRLLib.AxDeskCtrl();
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrl)).BeginInit();
            this.SuspendLayout();
            // 
            // axDeskCtrl
            // 
            this.axDeskCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axDeskCtrl.Enabled = true;
            this.axDeskCtrl.Location = new System.Drawing.Point(0, 0);
            this.axDeskCtrl.Name = "axDeskCtrl";
            this.axDeskCtrl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axDeskCtrl.OcxState")));
            this.axDeskCtrl.Size = new System.Drawing.Size(999, 626);
            this.axDeskCtrl.TabIndex = 3;
            // 
            // DispZumen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(999, 626);
            this.Controls.Add(this.axDeskCtrl);
            this.Name = "DispZumen";
            this.Text = "DispZumen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.LoadView_F000_DispZunem);
            ((System.ComponentModel.ISupportInitialize)(this.axDeskCtrl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxDESKCTRLLib.AxDeskCtrl axDeskCtrl;
    }
}