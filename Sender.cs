﻿
namespace ZumenConsoleApp
{
    public class Sender
    {
        public string MailAddress { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
