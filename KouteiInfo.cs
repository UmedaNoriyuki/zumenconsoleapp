﻿
namespace ZumenConsoleApp
{
    public class KouteiInfo
    {
        public string KouteiJun { get; set; }
        public string Kakosaki { get; set; }
        public string SagyoKoutei { get; set; }
        public string YoteiDate { get; set; }
        public string Biko { get; set; }
    }
}
