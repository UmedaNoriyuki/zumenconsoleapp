﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Npgsql;
using System.Diagnostics;

namespace ZumenConsoleApp.View
{
    public partial class SearchEntryKumi : BaseView
    {
        public static JissekiKakou JkData { get; set; }
        public SearchEntryKumi()
        {
            InitializeComponent();
        }

        private void SearchEntry_Load(object sender, EventArgs e)
        {
            if (JkData != null)
            {
                // 保持したデータを検索条件に表示
                txtOrderNo.Text = JkData.OrderNo;
                txtBlockNo.Text = JkData.BlockNo;
                txtKisyu.Text = JkData.Kisyu;
            }
        }

        // 検索ボタン押下時処理
        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var conn = Properties.Settings.Default.connectionString;
                var dataCnt = 0;
                // オーダーNoまたは機種名は必須入力
                if ((txtOrderNo.Text == null || txtOrderNo.Text == "") && (txtKisyu.Text == null || txtKisyu.Text == ""))
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("オーダーNoまたは機種名を入力してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                var orderNo = txtOrderNo.Text;
                // オーダーNOが未入力で機種名が入力されている場合、機種名に紐づくオーダーNOの候補を探す
                if ((txtOrderNo.Text == null || txtOrderNo.Text == "") && txtKisyu.Text != null && txtKisyu.Text != "")
                {
                    SelectOrderNo.ordList = CommonModule.GetOrderNoFromKisyu(conn, txtKisyu.Text, 0);
                    // オーダーNOの候補が1つの場合はオーダーNOを確定させる
                    if (SelectOrderNo.ordList.Count == 1)
                    {
                        orderNo = SelectOrderNo.ordList[0];
                    }
                    else if (SelectOrderNo.ordList.Count > 1)
                    {
                        // オーダーNOの候補が複数ある場合はオーダーNO選択ダイアログ表示
                        var a = new SelectOrderNo();
                        a.ShowDialog();
                        if (!SelectOrderNo.selOrdFlg)
                        {
                            return;
                        }
                        // 選択したオーダーNOを確定させる
                        orderNo = SelectOrderNo.orderNo;
                    }
                    else
                    {
                        //メッセージボックスを表示する
                        MessageBox.Show("図面データがありません。",
                            "エラー",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return;
                    }
                }
                if (orderNo == "")
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面データがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                dataCnt = GetZumenInfoCount(conn, orderNo, txtBlockNo.Text);
                if (dataCnt == 0)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面データがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                else if (dataCnt > 5000)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面データが多すぎます。条件を指定してください。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                var kumiList = GetKumiZumenInfo(conn, orderNo, txtBlockNo.Text);
                var buhinList = GetBuhinZumenInfo(conn, orderNo, txtBlockNo.Text);

                if ((kumiList == null || kumiList.Count == 0) && (buhinList == null || buhinList.Count == 0))
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("図面ファイルがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                MainDisp.KumiList = kumiList;
                MainDisp.CldList = buhinList;

                MainDisp.isKumiSearch = true;

                JkData = new JissekiKakou();
                JkData.OrderNo = txtOrderNo.Text;
                JkData.BlockNo = txtBlockNo.Text;
                JkData.Kisyu = txtKisyu.Text;

                this.Close();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // オーダーNoクリア
        private void btnOrderNoClr_Click(object sender, EventArgs e)
        {
            txtOrderNo.Clear();
            txtOrderNo.Focus();
        }

        // ブロックNoクリア
        private void btnBlockNoClr_Click(object sender, EventArgs e)
        {
            txtBlockNo.Clear();
            txtBlockNo.Focus();
        }

        // 機種名クリア
        private void btnKisyuClr_Click(object sender, EventArgs e)
        {
            txtKisyu.Clear();
            txtKisyu.Focus();
        }

        // オーダーNOにフォーカス時にEnterを押下すると検索
        private void txtOrderNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        // ブロックNoにフォーカス時にEnterを押下すると検索
        private void txtBlockNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        // 機種名にフォーカス時にEnterを押下すると検索
        private void txtKisyu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchBtn_Click(sender, e);
            }
        }

        /// <summary>
        /// 組立図面情報を検索する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="blockNo"></param>
        /// <returns></returns>
        private List<PartsList> GetKumiZumenInfo(string connectionString
                                        , string orderNo
                                        , string blockNo)
        {
            var PartsListData = new List<PartsList>();
            var PartsData = new PartsList();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select distinct" +
                                "  km.*" +
                                "  ,case when jk.図面番号 is not null then jk.図面番号 " +
                                "   when jk2.図面番号 is not null then jk2.図面番号 " +
                                "   when jk3.図面番号 is not null and SUBSTR(km.図番, LENGTH(km.図番)) ~ '[A-Za-z]' then jk3.図面番号 " +
                                "   when zd.ファイル is not null then zd.ファイル" +
                                "   when km.図番 is not null then km.図番 end as 図面番号" +
                                "  ,od.\"REPROオーダーNO\"" +
                                "  ,case when jk.図面パス is not null then jk.図面パス " +
                                "   when jk2.図面パス is not null then jk2.図面パス " +
                                "   when jk3.図面パス is not null and SUBSTR(km.図番, LENGTH(km.図番)) ~ '[A-Za-z]' then jk3.図面パス " +
                                "   when zd.パス is not null then zd.パス end as 図面パス" +
                                "  ,sm.仕入先略名" +
                                " from \"ess村田PL組立図\" km" +
                                " left join \"ess村田PLオーダー情報\" od" +
                                "   on od.plid = km.plid" +
                                " left join 実績加工データ jk" +
                                "   on jk.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and jk.図面番号 = km.図番" +
                                " left join 仕入先マスタ sm" +
                                "   on sm.仕入先コード = jk.加工先コード" +
                                " left join 実績加工データ jk2" +
                                "   on jk2.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and substr(jk2.図面番号, 1, length(jk2.図面番号) - 1) = km.図番" +
                                " left join 実績加工データ jk3" +
                                "   on jk3.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and substr(jk3.図面番号, 1, length(jk3.図面番号) - 1) = substr(km.図番, 1, length(km.図番) - 1)" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(km.図番,'/','／')" +
                                " where od.\"REPROオーダーNO\" = UPPER('" + orderNo + "')";
                if (blockNo != "")
                {
                    sqlCmdTxt += " and substr(km.ブロック番号, 8) LIKE '%" + blockNo + "%'";
                }
                sqlCmdTxt += " order by km.plid, km.rownum";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        PartsData = new PartsList();
                        PartsData.Plid = CommonModule.ConvertToString(result["plid"]);
                        PartsData.Rownum = CommonModule.ConvertToString(result["rownum"]);
                        PartsData.OrderNo = CommonModule.ConvertToString(result["REPROオーダーNO"]);
                        PartsData.BlockNo = CommonModule.ConvertToString(result["ブロック番号"]);
                        PartsData.ZumenBan = CommonModule.ConvertToString(result["図面番号"]);
                        PartsData.Qty = "1";
                        PartsData.ProductName = CommonModule.ConvertToString(result["図面名称"]);
                        PartsData.ZumenPath = CommonModule.ConvertToString(result["図面パス"]);
                        PartsData.kakoSaki = CommonModule.ConvertToString(result["仕入先略名"]);
                        if (PartsData.ZumenPath != "")
                        {
                            PartsData.FileName = System.IO.Path.GetFileNameWithoutExtension(PartsData.ZumenPath);
                        }
                        PartsListData.Add(PartsData);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    PartsListData = new List<PartsList>();
                    return PartsListData;
                }
            }
            return PartsListData;
        }

        /// <summary>
        /// 部品図面情報を検索する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="blockNo"></param>
        /// <returns></returns>
        private List<PartsList> GetBuhinZumenInfo(string connectionString
                                        , string orderNo
                                        , string blockNo)
        {
            var PartsListData = new List<PartsList>();
            var PartsData = new PartsList();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select distinct" +
                                "  kk.*" +
                                "  ,case when jk.図面番号 is not null then jk.図面番号 " +
                                "   when jk2.図面番号 is not null then jk2.図面番号 " +
                                "   when jk3.図面番号 is not null and SUBSTR(kk.図番, LENGTH(kk.図番)) ~ '[A-Za-z]' then jk3.図面番号 " +
                                "   when jk4.図面番号 is not null and SPLIT_PART(kk.図番, '#', 2) <> '' then jk4.図面番号 " +
                                "   when kk.図番 is not null then kk.図番 end as 図面番号" +
                                "  ,od.\"REPROオーダーNO\"" +
                                "  ,case when jk.図面パス is not null then jk.図面パス " +
                                "   when jk2.図面パス is not null then jk2.図面パス " +
                                "   when jk3.図面パス is not null and SUBSTR(kk.図番, LENGTH(kk.図番)) ~ '[A-Za-z]' then jk3.図面パス " +
                                "   when jk4.図面パス is not null and SPLIT_PART(kk.図番, '#', 2) <> '' then jk4.図面パス " +
                                "   when zd.パス is not null then zd.パス end as 図面パス" +
                                "  ,sm.仕入先略名" +
                                " from \"ess村田PL加工品\" kk" +
                                " left join \"ess村田PLオーダー情報\" od" +
                                "   on od.plid = kk.plid" +
                                " left join 実績加工データ jk" +
                                "   on jk.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and jk.図面番号 = kk.図番" +
                                " left join 実績加工データ jk2" +
                                "   on jk2.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and SUBSTR(jk2.図面番号, 1, LENGTH(jk2.図面番号) - 1) = kk.図番" +
                                " left join 実績加工データ jk3" +
                                "   on jk3.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and SUBSTR(jk3.図面番号, 1, LENGTH(jk3.図面番号) - 1) = SUBSTR(kk.図番, 1, LENGTH(kk.図番) - 1)" +
                                " left join 実績加工データ jk4" +
                                "   on jk4.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and SUBSTR(jk4.図面番号, 1, LENGTH(jk4.図面番号) - 4) = SUBSTR(kk.図番, 1, LENGTH(kk.図番) - 3)" +
                                "     and LENGTH(jk4.図面番号) > 5" +
                                "     and SUBSTR(jk4.図面番号, LENGTH(jk4.図面番号) - 2) = SUBSTR(kk.図番, LENGTH(kk.図番) - 2)" +
                                " left join 仕入先マスタ sm" +
                                "   on sm.仕入先コード = jk.加工先コード" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(kk.図番,'/','／')" +
                                " where od.\"REPROオーダーNO\" = UPPER('" + orderNo + "')";
                if (blockNo != "")
                {
                    sqlCmdTxt += " and substr(kk.ブロック番号, 8) LIKE '%" + blockNo + "%'";
                }
                sqlCmdTxt += " order by kk.plid, kk.rownum";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        PartsData = new PartsList();
                        PartsData.Plid = CommonModule.ConvertToString(result["plid"]);
                        PartsData.Rownum = CommonModule.ConvertToString(result["rownum"]);
                        PartsData.OrderNo = CommonModule.ConvertToString(result["REPROオーダーNO"]);
                        PartsData.BlockNo = CommonModule.ConvertToString(result["ブロック番号"]);
                        PartsData.ZumenBan = CommonModule.ConvertToString(result["図面番号"]);
                        PartsData.Qty = CommonModule.ConvertToString(result["総数"]);
                        PartsData.ProductName = CommonModule.ConvertToString(result["品名"]);
                        PartsData.ZumenPath = CommonModule.ConvertToString(result["図面パス"]);
                        PartsData.kakoSaki = CommonModule.ConvertToString(result["仕入先略名"]);
                        if (PartsData.ZumenPath != "")
                        {
                            PartsData.FileName = System.IO.Path.GetFileNameWithoutExtension(PartsData.ZumenPath);
                        }
                        PartsListData.Add(PartsData);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    PartsListData = new List<PartsList>();
                    return PartsListData;
                }
            }
            return PartsListData;
        }

        /// <summary>
        /// 図面情報のデータ数を検索する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="blockNo"></param>
        /// <returns></returns>
        private int GetZumenInfoCount(string connectionString
                                        , string orderNo
                                        , string blockNo)
        {
            var dataCnt = 0;
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  count(km.*) as データ数" +
                                " from \"ess村田PL組立図\" km" +
                                " left join \"ess村田PLオーダー情報\" od" +
                                "   on od.plid = km.plid" +
                                " left join 実績加工データ jk" +
                                "   on jk.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and jk.図面番号 = km.図番" +
                                " left join 仕入先マスタ sm" +
                                "   on sm.仕入先コード = jk.加工先コード" +
                                " left join 実績加工データ jk2" +
                                "   on jk2.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and substr(jk2.図面番号, 1, length(jk2.図面番号) - 1) = km.図番" +
                                " left join 実績加工データ jk3" +
                                "   on jk3.\"オーダーNO\" = od.\"REPROオーダーNO\"" +
                                "     and substr(jk3.図面番号, 1, length(jk3.図面番号) - 1) = substr(km.図番, 1, length(km.図番) - 1)" +
                                " left join 図面データ zd" +
                                "   on zd.ファイル = replace(km.図番,'/','／')" +
                                " where od.\"REPROオーダーNO\" = UPPER('" + orderNo + "')";
                if (blockNo != "")
                {
                    sqlCmdTxt += " and substr(km.ブロック番号, 8) LIKE '%" + blockNo + "%'";
                }

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);

                try
                {
                    var result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        dataCnt = CommonModule.ConvertToInt(result["データ数"]);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return 0;
                }
            }
            return dataCnt;
        }
    }
}
