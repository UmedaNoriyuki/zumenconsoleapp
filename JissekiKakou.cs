﻿
namespace ZumenConsoleApp
{
    public class JissekiKakou
    {
        /// <summary>
        /// 1:スキャン　2:図面検索 3:組図
        /// </summary>
        public int SearchMethod { get; set; }
        public string OrderNo { get; set; }
        public string Group { get; set; }
        public string Renno { get; set; }
        public string BlockNo { get; set; }
        public string ZumenBan { get; set; }
        public string ProductName { get; set; }
        public string KakoQty { get; set; }
        public string ZumenPath { get; set; }
        public string FileName { get; set; }
        public string KouteihyouNo { get; set; }
        public string ChumonsyoNo { get; set; }
        public string TantouCd { get; set; }
        public string Tantou { get; set; }
        public string KakouTantou { get; set; }
        public string Kakosaki { get; set; }
        public string Nouki { get; set; }
        public string KyakuNouki { get; set; }
        public string Sinchoku { get; set; }
        public string ZaiNyuka { get; set; }
        public string Zairyo { get; set; }
        public string ZaiSize { get; set; }
        public string ZaiQty { get; set; }
        public string NyukaDate { get; set; }
        public bool isMultiZumen { get; set; }
        public int isAdvance { get; set; }
        public int isKumitate { get; set; }
        public string Kisyu { get; set; }
        public bool chkOnlyRecentRireki { get; set; }
    }
}
