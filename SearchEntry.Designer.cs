﻿namespace ZumenConsoleApp.View
{
    partial class SearchEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.txtZumenBan = new System.Windows.Forms.TextBox();
            this.txtKouteihyouNo = new System.Windows.Forms.TextBox();
            this.txtChumonsyoNo = new System.Windows.Forms.TextBox();
            this.btnOrderNoClr = new System.Windows.Forms.Button();
            this.btnKouteihyoClr = new System.Windows.Forms.Button();
            this.btnZumenBanClr = new System.Windows.Forms.Button();
            this.btnChumonsyoNoClr = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBlockNo = new System.Windows.Forms.TextBox();
            this.btnBlockNoClr = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtKisyu = new System.Windows.Forms.TextBox();
            this.btnKisyuClr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // searchBtn
            // 
            this.searchBtn.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.searchBtn.Location = new System.Drawing.Point(275, 352);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(145, 62);
            this.searchBtn.TabIndex = 18;
            this.searchBtn.Text = "検索";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(10, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "オーダーNo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(10, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 24);
            this.label3.TabIndex = 9;
            this.label3.Text = "図面番号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(10, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 24);
            this.label4.TabIndex = 6;
            this.label4.Text = "工程表番号";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(10, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 24);
            this.label5.TabIndex = 15;
            this.label5.Text = "注文書番号";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOrderNo.Location = new System.Drawing.Point(160, 40);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(200, 31);
            this.txtOrderNo.TabIndex = 1;
            this.txtOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOrderNo_KeyDown);
            // 
            // txtZumenBan
            // 
            this.txtZumenBan.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZumenBan.Location = new System.Drawing.Point(160, 190);
            this.txtZumenBan.Name = "txtZumenBan";
            this.txtZumenBan.Size = new System.Drawing.Size(200, 31);
            this.txtZumenBan.TabIndex = 10;
            this.txtZumenBan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtZumenBan_KeyDown);
            // 
            // txtKouteihyouNo
            // 
            this.txtKouteihyouNo.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKouteihyouNo.Location = new System.Drawing.Point(160, 140);
            this.txtKouteihyouNo.Name = "txtKouteihyouNo";
            this.txtKouteihyouNo.Size = new System.Drawing.Size(200, 31);
            this.txtKouteihyouNo.TabIndex = 7;
            this.txtKouteihyouNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKouteihyouNo_KeyDown);
            // 
            // txtChumonsyoNo
            // 
            this.txtChumonsyoNo.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChumonsyoNo.Location = new System.Drawing.Point(160, 290);
            this.txtChumonsyoNo.Name = "txtChumonsyoNo";
            this.txtChumonsyoNo.Size = new System.Drawing.Size(200, 31);
            this.txtChumonsyoNo.TabIndex = 16;
            this.txtChumonsyoNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChumonsyoNo_KeyDown);
            // 
            // btnOrderNoClr
            // 
            this.btnOrderNoClr.Location = new System.Drawing.Point(370, 40);
            this.btnOrderNoClr.Name = "btnOrderNoClr";
            this.btnOrderNoClr.Size = new System.Drawing.Size(50, 31);
            this.btnOrderNoClr.TabIndex = 2;
            this.btnOrderNoClr.Text = "クリア";
            this.btnOrderNoClr.UseVisualStyleBackColor = true;
            this.btnOrderNoClr.Click += new System.EventHandler(this.btnOrderNoClr_Click);
            // 
            // btnKouteihyoClr
            // 
            this.btnKouteihyoClr.Location = new System.Drawing.Point(370, 140);
            this.btnKouteihyoClr.Name = "btnKouteihyoClr";
            this.btnKouteihyoClr.Size = new System.Drawing.Size(50, 31);
            this.btnKouteihyoClr.TabIndex = 8;
            this.btnKouteihyoClr.Text = "クリア";
            this.btnKouteihyoClr.UseVisualStyleBackColor = true;
            this.btnKouteihyoClr.Click += new System.EventHandler(this.btnKouteihyoClr_Click);
            // 
            // btnZumenBanClr
            // 
            this.btnZumenBanClr.Location = new System.Drawing.Point(370, 190);
            this.btnZumenBanClr.Name = "btnZumenBanClr";
            this.btnZumenBanClr.Size = new System.Drawing.Size(50, 31);
            this.btnZumenBanClr.TabIndex = 11;
            this.btnZumenBanClr.Text = "クリア";
            this.btnZumenBanClr.UseVisualStyleBackColor = true;
            this.btnZumenBanClr.Click += new System.EventHandler(this.btnZumenBanClr_Click);
            // 
            // btnChumonsyoNoClr
            // 
            this.btnChumonsyoNoClr.Location = new System.Drawing.Point(370, 290);
            this.btnChumonsyoNoClr.Name = "btnChumonsyoNoClr";
            this.btnChumonsyoNoClr.Size = new System.Drawing.Size(50, 31);
            this.btnChumonsyoNoClr.TabIndex = 17;
            this.btnChumonsyoNoClr.Text = "クリア";
            this.btnChumonsyoNoClr.UseVisualStyleBackColor = true;
            this.btnChumonsyoNoClr.Click += new System.EventHandler(this.btnChumonsyoNoClr_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(10, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "ブロックNo";
            // 
            // txtBlockNo
            // 
            this.txtBlockNo.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBlockNo.Location = new System.Drawing.Point(160, 90);
            this.txtBlockNo.Name = "txtBlockNo";
            this.txtBlockNo.Size = new System.Drawing.Size(200, 31);
            this.txtBlockNo.TabIndex = 4;
            
            this.txtBlockNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBlockNo_KeyDown);
            // 
            // btnBlockNoClr
            // 
            this.btnBlockNoClr.Location = new System.Drawing.Point(370, 90);
            this.btnBlockNoClr.Name = "btnBlockNoClr";
            this.btnBlockNoClr.Size = new System.Drawing.Size(50, 31);
            this.btnBlockNoClr.TabIndex = 5;
            this.btnBlockNoClr.Text = "クリア";
            this.btnBlockNoClr.UseVisualStyleBackColor = true;
            this.btnBlockNoClr.Click += new System.EventHandler(this.btnBlockNoClr_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(10, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "機種名";
            // 
            // txtKisyu
            // 
            this.txtKisyu.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKisyu.Location = new System.Drawing.Point(160, 240);
            this.txtKisyu.Name = "txtKisyu";
            this.txtKisyu.Size = new System.Drawing.Size(200, 31);
            this.txtKisyu.TabIndex = 13;
            this.txtKisyu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKisyu_KeyDown);
            // 
            // btnKisyuClr
            // 
            this.btnKisyuClr.Location = new System.Drawing.Point(370, 240);
            this.btnKisyuClr.Name = "btnKisyuClr";
            this.btnKisyuClr.Size = new System.Drawing.Size(50, 31);
            this.btnKisyuClr.TabIndex = 14;
            this.btnKisyuClr.Text = "クリア";
            this.btnKisyuClr.UseVisualStyleBackColor = true;
            this.btnKisyuClr.Click += new System.EventHandler(this.btnKisyuClr_Click);
            // 
            // SearchEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 426);
            this.Controls.Add(this.btnKisyuClr);
            this.Controls.Add(this.txtKisyu);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnBlockNoClr);
            this.Controls.Add(this.txtBlockNo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnChumonsyoNoClr);
            this.Controls.Add(this.btnZumenBanClr);
            this.Controls.Add(this.btnKouteihyoClr);
            this.Controls.Add(this.btnOrderNoClr);
            this.Controls.Add(this.txtChumonsyoNo);
            this.Controls.Add(this.txtKouteihyouNo);
            this.Controls.Add(this.txtZumenBan);
            this.Controls.Add(this.txtOrderNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchBtn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchEntry";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "図面検索";
            this.Load += new System.EventHandler(this.SearchEntry_Load);
            this.Controls.SetChildIndex(this.searchBtn, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.txtOrderNo, 0);
            this.Controls.SetChildIndex(this.txtZumenBan, 0);
            this.Controls.SetChildIndex(this.txtKouteihyouNo, 0);
            this.Controls.SetChildIndex(this.txtChumonsyoNo, 0);
            this.Controls.SetChildIndex(this.btnOrderNoClr, 0);
            this.Controls.SetChildIndex(this.btnKouteihyoClr, 0);
            this.Controls.SetChildIndex(this.btnZumenBanClr, 0);
            this.Controls.SetChildIndex(this.btnChumonsyoNoClr, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtBlockNo, 0);
            this.Controls.SetChildIndex(this.btnBlockNoClr, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.txtKisyu, 0);
            this.Controls.SetChildIndex(this.btnKisyuClr, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.TextBox txtZumenBan;
        private System.Windows.Forms.TextBox txtKouteihyouNo;
        private System.Windows.Forms.TextBox txtChumonsyoNo;
        private System.Windows.Forms.Button btnOrderNoClr;
        private System.Windows.Forms.Button btnKouteihyoClr;
        private System.Windows.Forms.Button btnZumenBanClr;
        private System.Windows.Forms.Button btnChumonsyoNoClr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBlockNo;
        private System.Windows.Forms.Button btnBlockNoClr;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtKisyu;
        private System.Windows.Forms.Button btnKisyuClr;
    }
}