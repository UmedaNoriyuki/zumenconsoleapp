﻿namespace ZumenConsoleApp
{
    class ZumenInfo
    {
        /// <summary>
        /// TENPファイルのパス
        /// </summary>
        public string LocalZumenPath { get; set; }

        /// <summary>
        /// 図面パス
        /// </summary>
        public string ZumenPath { get; set; }

        /// <summary>
        /// 図面名
        /// </summary>
        public string ZumenName { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Hinmei { get; set; }

        /// <summary>
        /// 作業状況
        /// </summary>
        public string WorkSituation { get; set; }

        /// <summary>
        /// オーダーNo
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// グループ
        /// </summary>
        public string Grp { get; set; }

        /// <summary>
        /// 連番
        /// </summary>
        public int Ren { get; set; }

        /// <summary>
        /// 状況
        /// </summary>
        public string Status { get; set; }
    }
}
