﻿namespace ZumenConsoleApp.View
{
    partial class CalcWorkTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMitumori = new System.Windows.Forms.TextBox();
            this.btnSet = new System.Windows.Forms.Button();
            this.txtMokuhyo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbZumenWorker = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbSagyo = new System.Windows.Forms.ComboBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnEnd = new System.Windows.Forms.Button();
            this.txtFuguai = new System.Windows.Forms.TextBox();
            this.txtTuika = new System.Windows.Forms.TextBox();
            this.txtTuujou = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.WorkList = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // txtMitumori
            // 
            this.txtMitumori.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMitumori.Location = new System.Drawing.Point(122, 28);
            this.txtMitumori.Name = "txtMitumori";
            this.txtMitumori.Size = new System.Drawing.Size(150, 31);
            this.txtMitumori.TabIndex = 0;
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(297, 23);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(104, 55);
            this.btnSet.TabIndex = 1;
            this.btnSet.Text = "設定";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.setBtn_Click);
            // 
            // txtMokuhyo
            // 
            this.txtMokuhyo.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMokuhyo.Location = new System.Drawing.Point(122, 70);
            this.txtMokuhyo.Name = "txtMokuhyo";
            this.txtMokuhyo.Size = new System.Drawing.Size(150, 31);
            this.txtMokuhyo.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(35, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "見積";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(35, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "目標";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(12, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 19);
            this.label5.TabIndex = 40;
            this.label5.Text = "図面作業者";
            // 
            // cmbZumenWorker
            // 
            this.cmbZumenWorker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbZumenWorker.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbZumenWorker.FormattingEnabled = true;
            this.cmbZumenWorker.Location = new System.Drawing.Point(122, 135);
            this.cmbZumenWorker.Name = "cmbZumenWorker";
            this.cmbZumenWorker.Size = new System.Drawing.Size(200, 27);
            this.cmbZumenWorker.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(31, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 19);
            this.label6.TabIndex = 48;
            this.label6.Text = "作業種類";
            // 
            // cmbSagyo
            // 
            this.cmbSagyo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSagyo.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbSagyo.FormattingEnabled = true;
            this.cmbSagyo.Items.AddRange(new object[] {
            "通常",
            "追加",
            "不具合"});
            this.cmbSagyo.Location = new System.Drawing.Point(122, 175);
            this.cmbSagyo.Name = "cmbSagyo";
            this.cmbSagyo.Size = new System.Drawing.Size(200, 27);
            this.cmbSagyo.TabIndex = 49;
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.GreenYellow;
            this.btnStart.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnStart.Location = new System.Drawing.Point(338, 143);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(80, 54);
            this.btnStart.TabIndex = 50;
            this.btnStart.Text = "作業開始";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Red;
            this.btnStop.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnStop.Location = new System.Drawing.Point(424, 143);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(77, 55);
            this.btnStop.TabIndex = 51;
            this.btnStop.Text = "作業中断";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnEnd
            // 
            this.btnEnd.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnEnd.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnd.Location = new System.Drawing.Point(507, 143);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(79, 55);
            this.btnEnd.TabIndex = 52;
            this.btnEnd.Text = "作業終了";
            this.btnEnd.UseVisualStyleBackColor = false;
            this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
            // 
            // txtFuguai
            // 
            this.txtFuguai.BackColor = System.Drawing.Color.Aquamarine;
            this.txtFuguai.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuguai.ForeColor = System.Drawing.Color.Gray;
            this.txtFuguai.Location = new System.Drawing.Point(122, 364);
            this.txtFuguai.Name = "txtFuguai";
            this.txtFuguai.ReadOnly = true;
            this.txtFuguai.Size = new System.Drawing.Size(70, 26);
            this.txtFuguai.TabIndex = 67;
            // 
            // txtTuika
            // 
            this.txtTuika.BackColor = System.Drawing.Color.Aquamarine;
            this.txtTuika.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTuika.ForeColor = System.Drawing.Color.Gray;
            this.txtTuika.Location = new System.Drawing.Point(122, 327);
            this.txtTuika.Name = "txtTuika";
            this.txtTuika.ReadOnly = true;
            this.txtTuika.Size = new System.Drawing.Size(70, 26);
            this.txtTuika.TabIndex = 66;
            // 
            // txtTuujou
            // 
            this.txtTuujou.BackColor = System.Drawing.Color.Aquamarine;
            this.txtTuujou.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTuujou.ForeColor = System.Drawing.Color.Gray;
            this.txtTuujou.Location = new System.Drawing.Point(122, 291);
            this.txtTuujou.Name = "txtTuujou";
            this.txtTuujou.ReadOnly = true;
            this.txtTuujou.Size = new System.Drawing.Size(70, 26);
            this.txtTuujou.TabIndex = 65;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label19.Location = new System.Drawing.Point(50, 367);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 19);
            this.label19.TabIndex = 62;
            this.label19.Text = "不具合";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label18.Location = new System.Drawing.Point(69, 330);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 19);
            this.label18.TabIndex = 61;
            this.label18.Text = "追加";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label15.Location = new System.Drawing.Point(69, 294);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 19);
            this.label15.TabIndex = 60;
            this.label15.Text = "通常";
            // 
            // WorkList
            // 
            this.WorkList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WorkList.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.WorkList.ForeColor = System.Drawing.SystemColors.ControlText;
            this.WorkList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.WorkList.HideSelection = false;
            this.WorkList.LabelWrap = false;
            this.WorkList.Location = new System.Drawing.Point(212, 208);
            this.WorkList.MultiSelect = false;
            this.WorkList.Name = "WorkList";
            this.WorkList.Size = new System.Drawing.Size(653, 182);
            this.WorkList.TabIndex = 68;
            this.WorkList.UseCompatibleStateImageBehavior = false;
            this.WorkList.View = System.Windows.Forms.View.List;
            // 
            // CalcWorkTime
            // 
            this.AcceptButton = this.btnSet;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 395);
            this.Controls.Add(this.WorkList);
            this.Controls.Add(this.txtFuguai);
            this.Controls.Add(this.txtTuika);
            this.Controls.Add(this.txtTuujou);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnEnd);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.cmbSagyo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbZumenWorker);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMokuhyo);
            this.Controls.Add(this.btnSet);
            this.Controls.Add(this.txtMitumori);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CalcWorkTime";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "作業時間計算";
            this.Load += new System.EventHandler(this.SetMitumori_Load);
            this.Controls.SetChildIndex(this.txtMitumori, 0);
            this.Controls.SetChildIndex(this.btnSet, 0);
            this.Controls.SetChildIndex(this.txtMokuhyo, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.cmbZumenWorker, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.cmbSagyo, 0);
            this.Controls.SetChildIndex(this.btnStart, 0);
            this.Controls.SetChildIndex(this.btnStop, 0);
            this.Controls.SetChildIndex(this.btnEnd, 0);
            this.Controls.SetChildIndex(this.label15, 0);
            this.Controls.SetChildIndex(this.label18, 0);
            this.Controls.SetChildIndex(this.label19, 0);
            this.Controls.SetChildIndex(this.txtTuujou, 0);
            this.Controls.SetChildIndex(this.txtTuika, 0);
            this.Controls.SetChildIndex(this.txtFuguai, 0);
            this.Controls.SetChildIndex(this.WorkList, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMitumori;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.TextBox txtMokuhyo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbZumenWorker;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbSagyo;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnEnd;
        private System.Windows.Forms.TextBox txtFuguai;
        private System.Windows.Forms.TextBox txtTuika;
        private System.Windows.Forms.TextBox txtTuujou;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ListView WorkList;
    }
}