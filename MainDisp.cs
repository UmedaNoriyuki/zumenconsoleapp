﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Npgsql;
using FujiXerox.DocuWorks.Toolkit;
using System.Drawing;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;
using AxDESKCTRLLib;

namespace ZumenConsoleApp.View
{
    public partial class MainDisp : BaseView
    {
        // 図面データリスト
        public static List<JissekiKakou> JkList { get; set; }
        // 履歴データリスト
        public static List<RirekiData> RirekiDataList { get; set; }
        // 他オーダーNOも含めた履歴データリスト
        public static List<RirekiData> FullRirekiDataList { get; set; }
        // 問い合わせデータリスト
        public static List<RirekiData> QueryDataList { get; set; }
        // 組立図面データリスト
        public static List<PartsList> KumiList { get; set; }
        // 部品図面データリスト
        public static List<PartsList> CldList { get; set; }
        // ブロック番号リスト
        public static List<String> BlockList { get; set; }
        // スキャンしたかどうか
        public static bool isScan = false;
        // 図面検索したかどうか
        public static bool isZumenSearch = false;
        // 組立図検索したかどうか
        public static bool isKumiSearch = false;
        // スキャンで図面データを取り込んだかどうか
        public static bool scanFlg = false;
        // 履歴検索したかどうか
        public static bool isRirekiSearch = false;
        // 端末にDocuWorks9が入っているかどうか
        public static bool isDw9 = true;
        // 図面作業者登録情報
        BindingList<WorkingRegister> WorkingRegisterList = new BindingList<WorkingRegister>();
        // 図面作業者登録情報(No)
        public int no = 0;
        // 図面の担当者コード
        public static string selTantouCd = "";
        public const string ZUMEN_URL = @"http://ts-xel980:9000/rpc/cat/share/図面ファイル/htdocs/ドキュワークス/図面データ/";
        //public const string ZUMEN_URL = "\\\\TS-XEL980\\share\\図面ファイル\\htdocs\\ドキュワークス\\図面データ\\";

        public const string RIREKI_URL = @"http://ts-xel980:9000/rpc/cat/share/図面ファイル/htdocs/ドキュワークス/履歴データ/";
        //public const string RIREKI_URL = "\\\\TS-XEL980\\share\\図面ファイル\\htdocs\\ドキュワークス\\履歴データ\\";

        public const string QUERY_URL = @"http://ts-xel980:9000/rpc/cat/share/図面ファイル/htdocs/ドキュワークス/問い合わせデータ/";
        //public const string QUERY_URL = "\\\\TS-XEL980\\share\\図面ファイル\\htdocs\\ドキュワークス\\問い合わせデータ\\";

        public const string FILE_URL = @"http://ts-xel980:9000/rpc/cat/share/図面ファイル/htdocs/ドキュワークス/";
        //public const string FILE_URL = "\\\\TS-XEL980\\share\\図面ファイル\\htdocs\\ドキュワークス\\";

        public const string RIREKI_FOLDER = "\\\\TS-XEL980\\share\\図面ファイル\\htdocs\\ドキュワークス\\履歴データ\\";
        public const string QUERY_FOLDER = "\\\\TS-XEL980\\share\\図面ファイル\\htdocs\\ドキュワークス\\問い合わせデータ\\";
        public const string ERROR_FILE = "\\\\PORTAL-SERVER\\app\\ZumenConsoleAppPublish\\エラー.xdw";

        // タブコントロール
        private const int ZUME_ORDER_SEARCH = 0;           // 「図面・オーダー検索」タブ
        private const int ZUMENRIREKI_TAB = 1;             // 「図面履歴」タブ
        private const int ZUMENJOUHOU_TAB = 2;             // 「図面情報」タブ
        private const int KOUTEIJOUHOU_TAB = 3;            // 「工程情報」タブ
        private const int SAMEZUMENINFO_TAB = 4;           // 「同図面一覧」タブ
        private const int ZUMENQUERY_TAB = 5;              // 「図面問い合わせ」タブ
        private const int ZUMENWORKINGREGISTER_TAB = 6;    // 「図面作業者登録」タブ

        private const int EVERYTHING_REQUEST_FILE_NAME = 0x00000001;
        private const int EVERYTHING_REQUEST_PATH = 0x00000002;

        // 複数起動防止
        // 配線図面
        public WiringDrawing View = null;
        // 加工指示書
        public KakouIndication View2 = null;
        // 加工指示書一覧
        public KakouIndicationList View3 = null;

        // 組図フラグ
        private bool KumiFlg = false;

        private bool ZumenDispFlg = false;

        [DllImport(@"dll\x86\Everything32.dll")]
        public static extern void Everything_SetRequestFlags(UInt32 dwRequestFlags);
        [DllImport(@"dll\x86\Everything32.dll", CharSet = CharSet.Unicode)]
        public static extern bool Everything_Query(bool bWait);
        [DllImport(@"dll\x86\Everything32.dll")]
        public static extern UInt32 Everything_GetNumResults();
        [DllImport(@"dll\x86\Everything32.dll", CharSet = CharSet.Unicode)]
        public static extern int Everything_SetSearch(string lpSearchString);
        [DllImport(@"dll\x86\Everything32.dll", CharSet = CharSet.Unicode)]
        public static extern void Everything_GetResultFullPathName(UInt32 nIndex, StringBuilder lpString, UInt32 nMaxCount);

        public MainDisp()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 起動時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainDisp_Load(object sender, EventArgs e)
        {
            try
            {
                // テスト環境、本番環境をわかりやすくする。
                var conn = Properties.Settings.Default.connectionString;
                if (!conn.Contains("192.168.0.100"))
                {
                    for (int i = 0; i < tabControl.TabPages.Count; i++)
                    {
                        tabControl.TabPages[i].BackColor = Color.Pink;
                    }
                }

                // 加工場温度表示PCの場合
                var ip = GetIPAddress();
                if(ip.Equals("192.168.0.197"))
                {
                    ZumenDispFlg = true;
                }

                // インスタンス作成
                JkList = new List<JissekiKakou>();

                if (!String.IsNullOrEmpty(Properties.Settings.Default.KouteiNo))
                {
                    // 履歴表示
                    char[] del = { '/' };

                    string[] KouteiNos = Properties.Settings.Default.KouteiNo.Split(del);
                    List<string> KouteiNoList = new List<string>();
                    KouteiNoList.AddRange(KouteiNos);

                    ReadZumenHistly(KouteiNoList);
                }

                //  文字なめらか表示設定
                axDeskCtrlZumen.SetCharDrawAdvance(true);
                axDeskCtrlRireki.SetCharDrawAdvance(true);
                axDeskCtrlQuery.SetCharDrawAdvance(true);

                // 端末にインストールされているDocuWorksのバージョンを確認
                Microsoft.Win32.RegistryKey rkeyDwd = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\FujiXerox\MPM3\SystemInfo");
                Microsoft.Win32.RegistryKey rkeyDwvl = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\FujiXerox\DocuWorks Content Filter\SystemInfo");
                if (rkeyDwd != null && rkeyDwvl != null)
                {
                    int MV = (int)rkeyDwd.GetValue("MajorVersion");
                    if (MV == 7)
                    {
                        isDw9 = false;
                    }
                }
                SetPropertyDeskCtr();

                // ログイン者名表示
                this.lbl名前.Text = LoginInfo.氏名 + "でログインしています。";
            }
            catch (Exception ex)
            {
                log.Error("アプリ起動処理" + ex.Message + "\r\n" + ex.StackTrace);
            }
        }

        private string GetIPAddress()
        {
            //IPアドレス
            string ip;

            try
            {
                //ホスト名を取得
                string hostname = System.Net.Dns.GetHostName();

                //ホスト名からIPアドレスを取得
                System.Net.IPAddress[] addr_arr = System.Net.Dns.GetHostAddresses(hostname);

                //探す
                ip = "";
                foreach (System.Net.IPAddress addr in addr_arr)
                {
                    string addr_str = addr.ToString();

                    //IPv4 && localhostでない
                    if (addr_str.IndexOf(".") > 0 && !addr_str.StartsWith("127."))
                    {
                        ip = addr_str;
                        break;
                    }
                }
            }
            catch (Exception)
            {
                ip = "";
            }
            return ip;
        }

        /// <summary>
        /// 同図面があった場合、「同図面一覧」タブを赤にする。ない場合は戻す
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            //対象のTabControlを取得
            TabControl tab = (TabControl)sender;
            //タブページのテキストを取得
            string txt = tab.TabPages[e.Index].Text;

            //タブのテキストと背景を描画するためのブラシを決定する
            Brush foreBrush, backBrush;

            //「同図面一覧」タブを背景を赤、文字色を白にする
            if (e.Index == SAMEZUMENINFO_TAB)
            {
                //タブのテキストを赤、背景を青とする
                foreBrush = Brushes.White;
                backBrush = Brushes.Red;
            }
            else
            {
                //タブのテキストを黒、背景を白とする
                foreBrush = Brushes.Black;
                backBrush = Brushes.White;
            }
            Font drawFont = new Font("MS UI Gothic", 17);
            //StringFormatを作成
            StringFormat sf = new StringFormat();
            //中央に表示する
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            //背景の描画
            e.Graphics.FillRectangle(backBrush, e.Bounds);
            //Textの描画
            e.Graphics.DrawString(txt, drawFont, foreBrush, e.Bounds, sf);

        }

        /// <summary>
        /// 図面検索ボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zumenSearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                scanFlg = false;
                // 図面検索ダイアログ表示
                var a = new SearchEntry();
                a.StartPosition = FormStartPosition.CenterParent;
                a.ShowDialog();
                if (JkList != null && JkList.Count > 0 && isZumenSearch)
                {
                    isZumenSearch = false;
                    // 組立リスト、部品リストを初期化
                    KumiList = null;
                    CldList = null;

                    // 図面リストの作成
                    makeZumenList();
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        /// <summary>
        /// スキャンボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScanBtn_Click(object sender, EventArgs e)
        {
            // 複数回押下防止
            this.scanBtn.Enabled = false;

            try
            {
                // スキャンダイアログ表示
                var a = new ScanDialog();
                if (this.rdoKamera.Checked)
                {
                    // バーコード読み取り
                    var barcodeReader = new BarcodeReader();
                    barcodeReader.ShowDialog();
                    if (!String.IsNullOrWhiteSpace(barcodeReader.Barcode))
                    {
                        a.Barcode = barcodeReader.Barcode;
                        a.ScanDialog_Load(sender, e);
                        a.scanBtn_Click(sender, e);
                    }
                    else
                    {
                        // スキャンダイアログ表示
                        a.ShowDialog();
                    }
                }
                else
                {
                    // スキャンダイアログ表示
                    a.ShowDialog();
                }

                if (JkList != null && JkList.Count > 0 && isScan)
                {
                    isScan = false;

                    // 組立リスト、部品リストを初期化
                    KumiList = null;
                    CldList = null;

                    // 図面リストの作成
                    makeZumenList();

                    scanFlg = true;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.scanBtn.Enabled = true;
            }
        }

        // 図面情報取得
        private void ReadZumenInfo(string kouteiNo)
        {
            try
            {
                var a = new ScanDialog();
                a.Barcode = kouteiNo;

                //　データ取得
                a.ReadZumenInfo();
                if (JkList != null && JkList.Count > 0 && isScan)
                {
                    isScan = false;

                    // 組立リスト、部品リストを初期化
                    KumiList = null;
                    CldList = null;

                    // 図面リストの作成
                    makeZumenList();

                    scanFlg = true;
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        // 図面情報取得
        private void ReadZumenHistly(List<string> KouteiNoList)
        {
            try
            {
                var a = new ScanDialog();

                //　データ取得
                a.ReadZumenHistly(KouteiNoList);
                if (JkList != null && JkList.Count > 0 && isScan)
                {
                    isScan = false;

                    // 組立リスト、部品リストを初期化
                    KumiList = null;
                    CldList = null;

                    // 図面リストの作成
                    makeZumenList();

                    scanFlg = true;
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        private void rirekiList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rirekiList.Items.Count > 0 && RirekiDataList != null && RirekiDataList.Count > 0)
                {
                    if (rirekiList.SelectedIndex != -1)
                    {
                        // 履歴情報表示
                        DisplayRirekiInfo(rirekiList.SelectedIndex);
                        // 図面表示
                        DisplayZumen();
                    }
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        private void queryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (queryList.Items.Count > 0 && QueryDataList != null && QueryDataList.Count > 0)
                {
                    if (queryList.SelectedIndex != -1)
                    {
                        // 問い合わせ情報表示
                        DisplayQueryInfo(queryList.SelectedIndex);
                        // 図面表示
                        DisplayQueryZumen();
                    }
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        /// <summary>
        /// 編集ボタン(履歴データ)押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rirekiDocuBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (RirekiDataList == null || RirekiDataList.Count == 0 || rirekiList.SelectedIndex == -1)
                {
                    return;
                }
                var zumenPath = RirekiDataList.ElementAt(rirekiList.SelectedIndex).ZumenPath;
                var zumenBan = RirekiDataList.ElementAt(rirekiList.SelectedIndex).ZumenBan;
                var fileName = RirekiDataList.ElementAt(0).FileName;
                var orderNo = txtOrderNo2.Text;
                // 図面ファイルが存在しない場合、エラーとする
                if (!File.Exists(zumenPath))
                {
                    MessageBox.Show("図面ファイルがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                var editFlg = false;
                var conn = Properties.Settings.Default.connectionString;
                // 現在、図面を編集している端末名を確認
                string zumenEditor = GetZumenEditor(conn, zumenBan, orderNo);
                if (!String.IsNullOrEmpty(zumenEditor))
                {
                    if (zumenEditor == "新規")
                    {
                        editFlg = true;
                        // 新規で図面を編集する場合は実績加工履歴データに連番0のデータを登録
                        RirekiNewInsert(conn, zumenBan, orderNo, Environment.MachineName);
                    }
                }
                else
                {
                    editFlg = true;
                    // 図面を編集している端末名が登録されていない場合は実績加工履歴データ.図面編集者に自端末名を登録
                    RirekiUpdate(conn, zumenBan, orderNo, Environment.MachineName);
                }
                // 既に他の端末が図面を開いている場合は閲覧モードで開く
                if (!editFlg)
                {
                    MessageBox.Show("「" + zumenEditor + "が開いています。閲覧モードで開きます。」",
                        "閲覧モード確認",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
                this.Enabled = false;

                string localFilePath = "";
                localFilePath = Path.ChangeExtension(Path.GetTempFileName(), ".xdw");

                // 共有フォルダ上の履歴データをローカルTMPにコピー
                System.IO.File.Copy(zumenPath, localFilePath);
                DateTime bkDtUpdate = System.IO.File.GetLastWriteTime(localFilePath);

                // コピーしたローカルファイルをDocuworksで開く
                System.Diagnostics.Process p =
                    System.Diagnostics.Process.Start(localFilePath, fileName + "," + zumenBan + "," + orderNo + "," + bkDtUpdate.ToString("yyyyMMddHHmmss.fff") + "," + editFlg);

                p.EnableRaisingEvents = true;
                // Docuworksを閉じたときに編集した図面を登録する処理を追加
                p.Exited += setRirekiParam;

                this.Enabled = true;
                this.Activate();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
                this.Enabled = true;
                this.Activate();
            }
        }

        /// <summary>
        /// Docuworksファイルを閉じたときに行う処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void setRirekiParam(object sender, EventArgs e)
        {
            try
            {
                string localFilePath = ((Process)sender).StartInfo.FileName;
                string[] zumenInfo = ((Process)sender).StartInfo.Arguments.Split(',');
                string fileName = zumenInfo[0];
                string zumenBan = zumenInfo[1];
                string orderNo = zumenInfo[2];
                DateTime bkDtUpdate = DateTime.ParseExact(zumenInfo[3], "yyyyMMddHHmmss.fff"
                    , System.Globalization.DateTimeFormatInfo.InvariantInfo
                    , System.Globalization.DateTimeStyles.NoCurrentDateDefault);
                bool editFlg = bool.Parse(zumenInfo[4]);

                // 編集した図面を登録
                registRireki(localFilePath, fileName, zumenBan, orderNo, bkDtUpdate, editFlg);
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        /// <summary>
        /// 編集した図面を登録
        /// </summary>
        /// <param name="localFilePath"></param>
        /// <param name="fileName"></param>
        /// <param name="zumenBan"></param>
        /// <param name="orderNo"></param>
        /// <param name="bkDtUpdate"></param>
        /// <param name="editFlg"></param>
        private void registRireki(string localFilePath, string fileName, string zumenBan, string orderNo, DateTime bkDtUpdate, bool editFlg)
        {
            var conn = Properties.Settings.Default.connectionString;
            string zumenEditor = GetZumenEditor(conn, zumenBan, orderNo);
            // 編集し保存したファイルを履歴フォルダに追加
            DateTime afDtUpdate = System.IO.File.GetLastWriteTime(localFilePath);
            if (bkDtUpdate < afDtUpdate)
            {
                var zumenPath = RIREKI_FOLDER + fileName + "_" + afDtUpdate.ToString("yyyyMMddHHmmss") + ".xdw";
                var renno = 1;
                while (File.Exists(zumenPath))
                {
                    MessageBox.Show("すでに同じ名前の図面ファイルがあります。しばらくお待ちください。",
                    "注意",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                    zumenPath = RIREKI_FOLDER + fileName + "_" + afDtUpdate.ToString("yyyyMMddHHmmss") + "_" + renno + ".xdw";
                    renno += 1;
                }
                // ローカルTMPファイルを共有フォルダにコピー
                System.IO.File.Copy(localFilePath, zumenPath);
                // 閲覧モードで開いていた場合は履歴に残さない
                if (!editFlg)
                {
                    MessageBox.Show("「閲覧モードで開いているため、履歴には残りません。」",
                    "閲覧モード確認",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                    this.Enabled = true;
                    return;
                }
                // 長時間(日を跨いで)図面を開いていた時は履歴に残さない
                if (!zumenEditor.Equals(Environment.MachineName))
                {
                    MessageBox.Show("「長時間図面を開いていたため、履歴には残りません。」",
                    "確認",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                    this.Enabled = true;
                    return;
                }
                DialogResult dr = MessageBox.Show("編集した図面を履歴として残しますか？", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    var maxRenno = GetMaxRenno(conn, zumenBan);
                    if (maxRenno < 0)
                    {
                        MessageBox.Show("実績加工履歴データより正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        this.Enabled = true;
                        this.Activate();
                        return;
                    }
                    // 実績加工履歴テーブルにデータ追加
                    var checkImport = RirekiInsert(conn, zumenBan, maxRenno + 1, zumenPath, orderNo);
                    if (!checkImport)
                    {
                        MessageBox.Show("実績加工履歴データに正しく登録できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        this.Enabled = true;
                        this.Activate();
                        return;
                    }
                }
            }

            // 図面編集者をクリアする
            if (editFlg && zumenEditor.Equals(Environment.MachineName))
            {
                RirekiUpdate(conn, zumenBan, orderNo, String.Empty);
            }
        }

        /// <summary>
        /// 編集ボタン(図面データ)押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zumenDocuBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var zumenPath = "";
                var zumenBan = "";
                var zumenFile = "";
                var orderNo = "";
                if ((JkList.Count == 0 || JkList == null) && (KumiList == null || CldList == null)
                     || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
                {
                    return;
                }
                else if (JkList != null && JkList.Count != 0 && !this.KumiFlg)
                {
                    List<JissekiKakou> seljkList = new List<JissekiKakou>();
                    JissekiKakou jkData = new JissekiKakou();
                    if (zumenList.SelectedNode == null)
                    {
                        jkData = JkList.ElementAt(0);
                    }
                    else
                    {
                        seljkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                        jkData = seljkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                    }
                    zumenPath = jkData.ZumenPath;
                    zumenBan = jkData.ZumenBan;
                    zumenFile = jkData.FileName;
                    orderNo = jkData.OrderNo;
                }
                else if (KumiList != null && CldList != null)
                {
                    List<PartsList> selPtList = new List<PartsList>();
                    PartsList ptData = new PartsList();
                    if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                    {
                        ptData = KumiList.ElementAt(0);
                    }
                    else
                    {
                        var tag = CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Split('_');
                        if (tag.Length != 4)
                        {
                            return;
                        }
                        if (tag[3] == "k")
                        {
                            selPtList = KumiList.FindAll(x => x.Plid == tag[0]);
                            ptData = selPtList.Find(x => x.Rownum == tag[1]);
                        }
                        else
                        {
                            selPtList = CldList.FindAll(x => x.Plid == tag[0]);
                            ptData = selPtList.Find(x => x.Rownum == tag[1]);
                        }
                    }
                    zumenPath = ptData.ZumenPath;
                    zumenBan = ptData.ZumenBan;
                    zumenFile = ptData.FileName;
                    orderNo = ptData.OrderNo;
                }

                // 図面ファイルが存在しない場合、エラーとする
                if (!File.Exists(zumenPath))
                {
                    MessageBox.Show("図面ファイルがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                var editFlg = false;
                var conn = Properties.Settings.Default.connectionString;
                // 現在、図面を編集している端末名を確認
                string zumenEditor = GetZumenEditor(conn, zumenBan, orderNo);
                if (!String.IsNullOrEmpty(zumenEditor))
                {
                    if (zumenEditor == "新規")
                    {
                        editFlg = true;
                        // 新規で図面を編集する場合は実績加工履歴データに連番0のデータを登録
                        RirekiNewInsert(conn, zumenBan, orderNo, Environment.MachineName);
                    }
                }
                else
                {
                    editFlg = true;
                    // 図面を編集している端末名が登録されていない場合は実績加工履歴データ.図面編集者に自端末名を登録
                    RirekiUpdate(conn, zumenBan, orderNo, Environment.MachineName);
                }

                // 既に他の端末が図面を開いている場合は閲覧モードで開く
                if (!editFlg)
                {
                    MessageBox.Show("「" + zumenEditor + "が開いています。閲覧モードで開きます。」",
                        "閲覧モード確認",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }

                this.Enabled = false;

                string localFilePath = "";
                localFilePath = Path.ChangeExtension(Path.GetTempFileName(), ".xdw");

                // 共有フォルダ上の履歴データをローカルTMPにコピー
                System.IO.File.Copy(zumenPath, localFilePath);
                DateTime bkDtUpdate = System.IO.File.GetLastWriteTime(localFilePath);

                // コピーしたローカルファイルをDocuworksで開く
                System.Diagnostics.Process p =
                    System.Diagnostics.Process.Start(localFilePath, zumenFile + "," + zumenBan + "," + orderNo + "," + bkDtUpdate.ToString("yyyyMMddHHmmss.fff") + "," + editFlg);

                p.EnableRaisingEvents = true;
                // Docuworksを閉じたときに編集した図面を登録する処理を追加
                p.Exited += setRirekiParam;

                this.Enabled = true;
                this.Activate();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
                this.Enabled = true;
                this.Activate();
            }
        }

        // 編集ボタン(問い合わせデータ)押下時処理
        private void queryDocuBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (QueryDataList == null || QueryDataList.Count == 0 || queryList.SelectedIndex == -1)
                {
                    return;
                }
                var zumenPath = QueryDataList.ElementAt(queryList.SelectedIndex).ZumenPath;
                var zumenBan = QueryDataList.ElementAt(queryList.SelectedIndex).ZumenBan;
                var originalZumenBan = QueryDataList.ElementAt(0).ZumenBan;
                var fileName = QueryDataList.ElementAt(0).FileName;
                var orderNo = txtOrderNo4.Text;
                // 図面ファイルが存在しない場合、エラーとする
                if (!File.Exists(zumenPath))
                {
                    MessageBox.Show("図面ファイルがありません。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
                this.Enabled = false;

                string localFilePath = "";
                localFilePath = Path.ChangeExtension(Path.GetTempFileName(), ".xdw");

                // 共有フォルダ上の問い合わせデータをローカルTMPにコピー
                System.IO.File.Copy(zumenPath, localFilePath);
                DateTime bkDtUpdate = System.IO.File.GetLastWriteTime(localFilePath);

                // コピーしたローカルファイルをDocuworksで開く
                System.Diagnostics.Process p =
                    System.Diagnostics.Process.Start(localFilePath);
                p.WaitForExit();

                // 編集し保存したファイルを図面問い合わせフォルダに追加
                DateTime afDtUpdate = System.IO.File.GetLastWriteTime(localFilePath);
                if (bkDtUpdate < afDtUpdate)
                {
                    var conn = Properties.Settings.Default.connectionString;
                    var maxRenno = GetMaxRenno(conn, zumenBan);
                    if (maxRenno < 0)
                    {
                        MessageBox.Show("実績加工履歴データより正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        this.Enabled = true;
                        this.Activate();
                        return;
                    }
                    zumenPath = QUERY_FOLDER + fileName + "_" + afDtUpdate.ToString("yyyyMMddHHmmss") + ".xdw";
                    var renno = 1;
                    while (File.Exists(zumenPath))
                    {
                        MessageBox.Show("すでに同じ名前の図面ファイルがあります。しばらくお待ちください。",
                        "注意",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                        zumenPath = QUERY_FOLDER + fileName + "_" + afDtUpdate.ToString("yyyyMMddHHmmss") + "_" + renno + ".xdw";
                        renno += 1;
                    }
                    // ローカルTMPファイルを共有フォルダにコピー
                    System.IO.File.Copy(localFilePath, zumenPath);
                    // 実績加工履歴テーブルにデータ追加
                    var checkImport = RirekiInsert(conn, zumenBan, maxRenno + 1, zumenPath, orderNo);
                    if (!checkImport)
                    {
                        MessageBox.Show("実績加工履歴データに正しく登録できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        this.Enabled = true;
                        this.Activate();
                        return;
                    }

                    var originalData = new RirekiData();
                    originalData = QueryDataList.ElementAt(0);
                    QueryDataList = new List<RirekiData>();
                    FullRirekiDataList = new List<RirekiData>();
                    FullRirekiDataList.Add(originalData);
                    var queryDataList = new List<RirekiData>();
                    queryDataList = CommonModule.GetZumenRirekiInfo(conn, originalZumenBan);
                    foreach (var query in queryDataList)
                    {
                        if (!query.ZumenPath.Contains("問い合わせデータ"))
                        {
                            continue;
                        }
                        FullRirekiDataList.Add(query);
                    }
                    if (chkIncOtherOrder.Checked)
                    {
                        QueryDataList = FullRirekiDataList;
                    }
                    else
                    {
                        QueryDataList = FullRirekiDataList.FindAll(x => x.OrderNo == orderNo || x.OrderNo == "9999999");
                    }
                    queryList.Items.Clear();
                    // 問い合わせリストにデータ追加
                    foreach (var item in QueryDataList)
                    {
                        var updttime = "";
                        if (item.UpdateDateTime != "")
                        {
                            updttime = "(" + item.UpdateDateTime + ")";
                        }
                        queryList.Items.Add(item.ZumenBan + updttime);
                    }
                    queryList.SelectedIndex = queryList.Items.Count - 1;
                }

                this.Enabled = true;
                this.Activate();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
                this.Enabled = true;
                this.Activate();
            }
        }

        /// <summary>
        /// タブクリックイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var selTab = tabControl.SelectedTab;
                if (selTab.TabIndex == ZUMENRIREKI_TAB)
                {
                    var zumenPath = "";
                    var zumenBan = "";
                    var zumenFile = "";
                    var orderNo = "";
                    if ((JkList.Count == 0 || JkList == null) && (KumiList == null || CldList == null)
                        || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
                    {
                        return;
                    }
                    else if (JkList != null && !this.KumiFlg)
                    {
                        List<JissekiKakou> selJkList = new List<JissekiKakou>();
                        JissekiKakou jkData = new JissekiKakou();
                        if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                        {
                            jkData = JkList.ElementAt(0);
                        }
                        else
                        {
                            selJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                            jkData = selJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                        }
                        zumenPath = jkData.ZumenPath;
                        zumenBan = jkData.ZumenBan;
                        zumenFile = jkData.FileName;
                        orderNo = jkData.OrderNo;
                    }
                    else if (KumiList != null && CldList != null)
                    {
                        List<PartsList> selPtList = new List<PartsList>();
                        PartsList ptData = new PartsList();
                        if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                        {
                            ptData = KumiList.ElementAt(0);
                        }
                        else
                        {
                            var tag = CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Split('_');
                            if (tag.Length != 4)
                            {
                                return;
                            }
                            if (tag[3] == "k")
                            {
                                selPtList = KumiList.FindAll(x => x.Plid == tag[0]);
                                ptData = selPtList.Find(x => x.Rownum == tag[1]);
                            }
                            else
                            {
                                selPtList = CldList.FindAll(x => x.Plid == tag[0]);
                                ptData = selPtList.Find(x => x.Rownum == tag[1]);
                            }
                        }
                        zumenPath = ptData.ZumenPath;
                        zumenBan = ptData.ZumenBan;
                        zumenFile = ptData.FileName;
                        orderNo = ptData.OrderNo;
                    }

                    // 履歴検索に検索条件保持
                    SearchRireki.JisKak = new JissekiKakou();
                    SearchRireki.JisKak.OrderNo = orderNo;
                    SearchRireki.JisKak.ZumenBan = zumenBan;

                    RirekiDataList = new List<RirekiData>();
                    FullRirekiDataList = new List<RirekiData>();
                    var originalData = new RirekiData();
                    // 元データを追加
                    originalData.OrderNo = orderNo;
                    originalData.ZumenBan = zumenBan;
                    originalData.ZumenPath = zumenPath;
                    originalData.FileName = zumenFile;
                    originalData.UpdateDater = "";
                    originalData.UpdateDateTime = "";
                    FullRirekiDataList.Add(originalData);
                    // 実績加工履歴テーブルより履歴データを取得
                    var conn = Properties.Settings.Default.connectionString;
                    var rirekiDataList = new List<RirekiData>();
                    rirekiDataList = CommonModule.GetZumenRirekiInfo(conn, zumenBan);
                    // 履歴データを追加
                    foreach (var rireki in rirekiDataList)
                    {
                        if (!rireki.ZumenPath.Contains("履歴データ"))
                        {
                            continue;
                        }
                        FullRirekiDataList.Add(rireki);
                    }
                    if (chkIncOtherOrder.Checked)
                    {
                        RirekiDataList = FullRirekiDataList;
                    }
                    else
                    {
                        RirekiDataList = FullRirekiDataList.FindAll(x => x.OrderNo == orderNo || x.OrderNo == "9999999");
                    }
                    // 履歴リストにデータ追加
                    rirekiList.Items.Clear();
                    foreach (var item in RirekiDataList)
                    {
                        var updttime = "";
                        if (item.UpdateDateTime != "")
                        {
                            updttime = "(" + item.UpdateDateTime + ")";
                        }
                        rirekiList.Items.Add(item.ZumenBan + updttime);
                    }

                    if (rirekiList.Items.Count > 0 && rirekiList.SelectedIndex == -1)
                    {
                        rirekiList.SelectedIndex = 0;
                    }
                    // 図面表示
                    DisplayZumen();
                }
                else if (selTab.TabIndex == ZUMENJOUHOU_TAB)
                {
                    if ((JkList.Count == 0 || JkList == null) || (KumiList != null || CldList != null)
                        || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
                    {
                        return;
                    }
                    List<JissekiKakou> selJkList = new List<JissekiKakou>();
                    JissekiKakou jkData = new JissekiKakou();
                    if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                    {
                        jkData = JkList.ElementAt(0);
                    }
                    else
                    {
                        selJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                        jkData = selJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                    }
                    var conn = Properties.Settings.Default.connectionString;
                    if (!GetZumenInfo(conn, jkData))
                    {
                        MessageBox.Show("関連図番を正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        return;
                    }
                }
                else if (selTab.TabIndex == KOUTEIJOUHOU_TAB)
                {
                    if ((JkList.Count == 0 || JkList == null) || (KumiList != null || CldList != null)
                        || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
                    {
                        return;
                    }
                    List<JissekiKakou> selJkList = new List<JissekiKakou>();
                    JissekiKakou jkData = new JissekiKakou();
                    if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                    {
                        jkData = JkList.ElementAt(0);
                    }
                    else
                    {
                        selJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                        jkData = selJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                    }
                    var conn = Properties.Settings.Default.connectionString;
                    List<KouteiHyoInfo> KouteiDataList = GetKouteiInfo(conn, jkData.OrderNo, jkData.Group, int.Parse(jkData.Renno));
                    kouteiInfoList.Rows.Clear();
                    foreach (var data in KouteiDataList)
                    {
                        // データの追加
                        kouteiInfoList.Rows.Add(data.KouteiJun, data.Kakosaki, data.SagyoKoutei, data.YoteiDate, data.Biko);
                    }
                }
                else if (selTab.TabIndex == SAMEZUMENINFO_TAB)
                {
                    if ((JkList.Count == 0 || JkList == null) || (KumiList != null || CldList != null)
                        || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
                    {
                        return;
                    }
                    List<JissekiKakou> selJkList = new List<JissekiKakou>();
                    JissekiKakou jkData = new JissekiKakou();
                    if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                    {
                        jkData = JkList.ElementAt(0);
                    }
                    else
                    {
                        selJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                        jkData = selJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                    }
                    var conn = Properties.Settings.Default.connectionString;
                    if (!GetSameZumenInfo(conn, jkData))
                    {
                        MessageBox.Show("同図番一覧を正しく取得できませんでした。",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                        return;
                    }
                }
                else if (selTab.TabIndex == ZUMENQUERY_TAB)
                {
                    var zumenPath = "";
                    var zumenBan = "";
                    var zumenFile = "";
                    var orderNo = "";
                    if ((JkList.Count == 0 || JkList == null) && (KumiList == null || CldList == null)
                        || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
                    {
                        return;
                    }
                    else if (JkList.Count != 0 && JkList != null && (KumiList == null || CldList == null))
                    {
                        List<JissekiKakou> selJkList = new List<JissekiKakou>();
                        JissekiKakou jkData = new JissekiKakou();
                        if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                        {
                            jkData = JkList.ElementAt(0);
                        }
                        else
                        {
                            selJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                            jkData = selJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                        }
                        zumenPath = jkData.ZumenPath;
                        zumenBan = jkData.ZumenBan;
                        zumenFile = jkData.FileName;
                        orderNo = jkData.OrderNo;
                    }
                    else if (KumiList != null && CldList != null)
                    {
                        List<PartsList> selPtList = new List<PartsList>();
                        PartsList ptData = new PartsList();
                        if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                        {
                            ptData = KumiList.ElementAt(0);
                        }
                        else
                        {
                            var tag = CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Split('_');
                            if (tag.Length != 4)
                            {
                                return;
                            }
                            if (tag[3] == "k")
                            {
                                selPtList = KumiList.FindAll(x => x.Plid == tag[0]);
                                ptData = selPtList.Find(x => x.Rownum == tag[1]);
                            }
                            else
                            {
                                selPtList = CldList.FindAll(x => x.Plid == tag[0]);
                                ptData = selPtList.Find(x => x.Rownum == tag[1]);
                            }
                        }
                        zumenPath = ptData.ZumenPath;
                        zumenBan = ptData.ZumenBan;
                        zumenFile = ptData.FileName;
                        orderNo = ptData.OrderNo;
                    }


                    QueryDataList = new List<RirekiData>();
                    FullRirekiDataList = new List<RirekiData>();
                    var originalData = new RirekiData();
                    // 元データを追加
                    originalData.OrderNo = orderNo;
                    originalData.ZumenBan = zumenBan;
                    originalData.ZumenPath = zumenPath;
                    originalData.FileName = zumenFile;
                    originalData.UpdateDater = "";
                    originalData.UpdateDateTime = "";
                    FullRirekiDataList.Add(originalData);
                    // 実績加工履歴テーブルより履歴データを取得
                    var conn = Properties.Settings.Default.connectionString;
                    var queryDataList = new List<RirekiData>();
                    queryDataList = CommonModule.GetZumenRirekiInfo(conn, zumenBan);
                    // 履歴データを追加
                    foreach (var query in queryDataList)
                    {
                        if (!query.ZumenPath.Contains("問い合わせデータ"))
                        {
                            continue;
                        }
                        FullRirekiDataList.Add(query);
                    }
                    if (chkIncOtherOrder.Checked)
                    {
                        QueryDataList = FullRirekiDataList;
                    }
                    else
                    {
                        QueryDataList = FullRirekiDataList.FindAll(x => x.OrderNo == orderNo || x.OrderNo == "9999999");
                    }
                    // 問い合わせリストにデータ追加
                    queryList.Items.Clear();
                    foreach (var item in QueryDataList)
                    {
                        var updttime = "";
                        if (item.UpdateDateTime != "")
                        {
                            updttime = "(" + item.UpdateDateTime + ")";
                        }
                        queryList.Items.Add(item.ZumenBan + updttime);
                    }

                    if (queryList.Items.Count > 0 && queryList.SelectedIndex == -1)
                    {
                        queryList.SelectedIndex = 0;
                    }
                    // 図面表示
                    DisplayQueryZumen();
                }
                else if (selTab.TabIndex == ZUMENWORKINGREGISTER_TAB)
                {
                    this.BarTextBox.Focus();
                    this.userCd.Text = Properties.Settings.Default.LoginCd;

                    // 作業者コード取得
                    DataTable dt = GetShainMst();
                    dt.Rows.InsertAt(dt.NewRow(), 0);
                    this.wokerComboBox.DataSource = dt;
                    // 表示用の列を設定
                    this.wokerComboBox.DisplayMember = "作業者名";
                    // データ用の列を設定
                    this.wokerComboBox.ValueMember = "作業者コード";
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        private void wokerComboBox_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(this.wokerComboBox.Text))
            {
                this.NameTextBox.Text = this.wokerComboBox.SelectedValue.ToString().PadLeft(3, '0');
            }
            else
            {
                this.NameTextBox.Text = string.Empty;
            }
        }

        /// <summary>
        /// 作業者を取得します
        /// </summary>
        /// <returns></returns>
        private DataTable GetShainMst()
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                var cmd = new NpgsqlCommand();
                DataTable dt = new DataTable();

                con.Open();
                StringBuilder query = new StringBuilder();

                query.AppendLine("SELECT ");
                query.AppendLine("    作業者コード ");
                query.AppendLine("   ,作業者名 ");
                query.AppendLine("FROM ");
                query.AppendLine("    作業者マスタ ");
                query.AppendLine("WHERE ");
                query.AppendLine("    作業者コード< 155 ");
                query.AppendLine("AND 作業者コード NOT IN (1,2,3,4,5,6,10,14,18,20,23,25,28,30, ");
                query.AppendLine("31,33,34,35,36,41,46,47,49,50,52,53,54,55,56,57");
                query.AppendLine(",59,60,61,62,63,64,65,66,67,72,73,74,75,76,77,79,81,82");
                query.AppendLine(",83,84,87,91,92,93,94,95,98,99, 100,101,108,109,110,111,112,118");
                query.AppendLine(",119,120,121,122,123,124,125,126,127,128,129,130,135,136,137,138,139,140,142 ) ");
                query.AppendLine("ORDER BY ");
                query.AppendLine("    作業者コード ");

                cmd = new NpgsqlCommand(query.ToString(), con);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                // 接続を閉じる
                con.Close();
                con.Dispose();

                return dt;
            }
        }

        /// <summary>
        /// 全画面表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fullDispBtn_Click(object sender, EventArgs e)
        {
            try
            {
                // 図面が存在しない場合リターン
                if (toolStripStatusLabel1.Text.Equals("ファイルが登録されていません。"))
                {
                    return;
                }

                FullDisp.ZumenUrlLeft = KumiZumen.Text;
                FullDisp.ZumenUrlRight = axDeskCtrlZumen.src;
                FullDisp.isSingleZumen = false;
                if (((JkList.Count == 0 || JkList == null) && (KumiList == null || CldList == null))
                     || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
                {
                    return;
                }
                else if (JkList != null && JkList.Count != 0 && !this.KumiFlg)
                {
                    List<JissekiKakou> selJkList = new List<JissekiKakou>();
                    JissekiKakou jkData = new JissekiKakou();
                    if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                    {
                        // 選択されていない場合、先頭の1図面のみ表示
                        FullDisp.ZumenUrlLeft = "";
                    }
                    else
                    {
                        selJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                        jkData = selJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                        FullDisp.ZumenUrlRight = ZUMEN_URL + jkData.FileName + ".xdw";
                        // スキャン、図面検索の場合、選択図面のみ表示
                        FullDisp.ZumenUrlLeft = "";

                        FullDisp.rightIndex = zumenList.SelectedNode.Index;
                        if ((FullDisp.FullDispList == null || FullDisp.FullDispList.Count == 0)
                            && ((FullDisp.FullDispLeftList == null && FullDisp.FullDispRightList == null)
                            || (FullDisp.FullDispLeftList.Count == 0 && FullDisp.FullDispRightList.Count == 0)))
                        {
                            if (JkList.Count > 1 && !scanFlg)
                            {
                                FullDisp.FullDispList = JkList;
                            }
                        }
                        else
                        {
                            FullDisp.isSingleZumen = true;
                        }
                    }
                }
                else if (KumiList != null && CldList != null)
                {
                    if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                    {
                        // 選択されていない場合、先頭の1図面のみ表示
                        FullDisp.ZumenUrlLeft = "";
                    }
                    else
                    {
                        var tag = CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Split('_');
                        if (tag.Length != 4)
                        {
                            return;
                        }
                        FullDisp.selLeftBlockNo = tag[2] + "_k";
                        FullDisp.selRightBlockNo = tag[2] + "_b";
                        // 組立図検索
                        if (((FullDisp.FullDispLeftList == null && FullDisp.FullDispRightList == null)
                            || (FullDisp.FullDispLeftList.Count == 0 && FullDisp.FullDispRightList.Count == 0))
                            && (FullDisp.FullDispList == null || FullDisp.FullDispList.Count == 0))
                        {
                            FullDisp.FullDispLeftList = KumiList;
                            FullDisp.FullDispRightList = CldList;
                            FullDisp.SubDispLeftList = KumiList.FindAll(x => x.BlockNo == tag[2]);
                            FullDisp.SubDispRightList = CldList.FindAll(x => x.BlockNo == tag[2]);
                            if (tag[3] == "k")
                            {
                                List<PartsList> selPtList = new List<PartsList>();
                                PartsList ptData = new PartsList();
                                selPtList = KumiList.FindAll(x => x.Plid == tag[0]);
                                ptData = selPtList.Find(x => x.Rownum == tag[1]);
                                // 選択時
                                FullDisp.ZumenUrlLeft = zumenPt(ptData);
                                if (zumenList.SelectedNode.Parent != null)
                                {
                                    FullDisp.leftIndex = zumenList.SelectedNode.Index;
                                }
                                FullDisp.rightIndex = 0;
                                if (FullDisp.SubDispRightList.Count > 0)
                                {
                                    FullDisp.ZumenUrlRight = zumenPt(FullDisp.SubDispRightList.ElementAt(0));
                                }
                                else
                                {
                                    FullDisp.ZumenUrlRight = ERROR_FILE;
                                }
                            }
                            else
                            {
                                // 部品図選択時
                                FullDisp.leftIndex = 0;
                                FullDisp.rightIndex = zumenList.SelectedNode.Index;
                                FullDisp.ZumenUrlLeft = zumenPt(FullDisp.SubDispLeftList.ElementAt(0));
                                FullDisp.ZumenUrlRight = zumenPt(FullDisp.SubDispRightList.ElementAt(zumenList.SelectedNode.Index));
                            }
                        }
                        else
                        {
                            // 単図面を開くようにする
                            FullDisp.ZumenUrlLeft = "";
                            FullDisp.isSingleZumen = true;
                        }
                    }
                }

                // 全画面表示ダイアログ表示
                var a = new FullDisp();
                var s = System.Windows.Forms.Screen.AllScreens;
                // デュアルディスプレイの場合のみメイン画面を最小化
                if (s.Length > 1)
                {
                    this.WindowState = FormWindowState.Minimized;
                }
                // 複数図面を表示できるようにする
                a.Show();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        /// <summary>
        /// 図面ファイルの履歴を実績加工履歴データ(DB)に登録する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <param name="newRenno"></param>
        /// <param name="zumenPath"></param>
        /// <returns></returns>
        public static bool RirekiInsert(string connectionString, string zumenBan, int newRenno, string zumenPath, string orderNo)
        {
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    var cmd = new NpgsqlCommand("insert into 実績加工履歴データ " +
                                                "  (図面番号" +
                                                "  ,連番" +
                                                "  ,更新者 " +
                                                "  ,更新日時 " +
                                                "  ,図面パス " +
                                                "  ,\"オーダーNO\")" +
                                                "  values " +
                                                "  (:zumenBan " +
                                                "  ,:newRenno " +
                                                "  ,:updater " +
                                                "  ,now() " +
                                                "  ,:zumenPath " +
                                                "  ,:orderNo) ", con);
                    cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = zumenBan });
                    cmd.Parameters.Add(new NpgsqlParameter("newRenno", DbType.Int32) { Value = newRenno });
                    cmd.Parameters.Add(new NpgsqlParameter("updater", DbType.String) { Value = Environment.MachineName });
                    cmd.Parameters.Add(new NpgsqlParameter("zumenPath", DbType.String) { Value = zumenPath });
                    cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                    try
                    {
                        var result = cmd.ExecuteNonQuery();
                        Debug.WriteLine($"[result]{result}");
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine($"[err]{e.Message}");
                        tran.Rollback();
                        return false;
                    }
                    tran.Commit();
                }
            }
            return true;
        }

        /// <summary>
        /// 図面ファイルの履歴を実績加工履歴データ(DB)に新規登録する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <param name="orderNo"></param>
        /// <param name="zumenEditor"></param>
        /// <returns></returns>
        public static void RirekiNewInsert(string connectionString, string zumenBan, string orderNo, string zumenEditor)
        {
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    var cmd = new NpgsqlCommand("insert into 実績加工履歴データ " +
                                                "  (図面番号" +
                                                "  ,連番" +
                                                "  ,更新日時" +
                                                "  ,\"オーダーNO\"" +
                                                "  ,図面編集者)" +
                                                "  values " +
                                                "  (:zumenBan " +
                                                "  ,0 " +
                                                "  ,now() " +
                                                "  ,:orderNo " +
                                                "  ,:zumenEditor) ", con);
                    cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = zumenBan });
                    cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });
                    cmd.Parameters.Add(new NpgsqlParameter("zumenEditor", DbType.String) { Value = zumenEditor });

                    try
                    {
                        var result = cmd.ExecuteNonQuery();
                        Debug.WriteLine($"[result]{result}");
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine($"[err]{e.Message}");
                        tran.Rollback();
                        throw e;
                    }
                    tran.Commit();
                }
            }
        }

        /// <summary>
        /// 実績加工履歴データ(DB)の編集者に端末名を登録
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <param name="orderNo"></param>
        /// <param name="zumenEditor"></param>
        /// <returns></returns>
        public static void RirekiUpdate(string connectionString, string zumenBan, string orderNo, string zumenEditor)
        {
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    var sqlCmdTxt = " update 実績加工履歴データ ";
                    if (!String.IsNullOrEmpty(zumenEditor))
                    {
                        sqlCmdTxt += " set 図面編集者 = :zumenEditor ";
                    }
                    else
                    {
                        sqlCmdTxt += " set 図面編集者 = NULL ";
                    }
                    sqlCmdTxt += " where 図面番号 = :zumenBan " +
                                 "  and \"オーダーNO\" = UPPER(:orderNo) ";
                    var cmd = new NpgsqlCommand(sqlCmdTxt, con);
                    cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = zumenBan });
                    cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });
                    cmd.Parameters.Add(new NpgsqlParameter("zumenEditor", DbType.String) { Value = zumenEditor });

                    try
                    {
                        var result = cmd.ExecuteNonQuery();
                        Debug.WriteLine($"[result]{result}");
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine($"[err]{e.Message}");
                        tran.Rollback();
                        throw e;
                    }
                    tran.Commit();
                }
            }
        }

        /// <summary>
        /// 実績加工履歴データ(DB)の端末名を削除
        /// </summary>
        /// <param name="zumenEditor"></param>
        /// <returns></returns>
        public static void RirekiUpdate(string orderNo, string zumenEditor)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();
                using (var tran = con.BeginTransaction())
                {
                    var sqlCmdTxt = " update 実績加工履歴データ ";
                    sqlCmdTxt += " set 図面編集者 = NULL ";
                    sqlCmdTxt += " where 図面編集者 = :zumenEditor ";
                    sqlCmdTxt += "   and \"オーダーNO\" = UPPER(:orderNo) ";

                    var cmd = new NpgsqlCommand(sqlCmdTxt, con);
                    cmd.Parameters.Add(new NpgsqlParameter("zumenEditor", DbType.String) { Value = zumenEditor });
                    cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });

                    try
                    {
                        var result = cmd.ExecuteNonQuery();
                        Debug.WriteLine($"[result]{result}");
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine($"[err]{e.Message}");
                        tran.Rollback();
                        throw e;
                    }
                    tran.Commit();
                }
            }
        }

        /// <summary>
        /// 図面の編集者を取得する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public static string GetZumenEditor(string connectionString, string zumenBan, string orderNo)
        {
            string zumenEditor = "新規";

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select 図面編集者 " +
                                " from 実績加工履歴データ " +
                                " where 図面番号 = '" + zumenBan + "'" +
                                "  and \"オーダーNO\" = '" + orderNo + "'" +
                                "  and 連番 = 0";

                var cmd = new NpgsqlCommand(sqlCmdTxt, con);

                var result = cmd.ExecuteReader();
                if (result.Read())
                {
                    zumenEditor = CommonModule.ConvertToString(result["図面編集者"]);
                }
            }
            return zumenEditor;
        }

        /// <summary>
        /// 実績加工履歴テーブルの連番の最大を抽出する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="zumenBan"></param>
        /// <returns></returns>
        public static int GetMaxRenno(string connectionString, string zumenBan)
        {
            var maxRenno = 0;

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var cmd = new NpgsqlCommand("select" +
                                            "  max(連番) as 最大連番" +
                                            " from 実績加工履歴データ" +
                                            " where 図面番号 = :zumenBan", con);
                cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = zumenBan });

                try
                {
                    var result = cmd.ExecuteReader();
                    if (result.Read())
                    {
                        maxRenno = CommonModule.ConvertToInt(result["最大連番"]);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return -1;
                }
            }
            return maxRenno;
        }

        /// <summary>
        /// 図面表示
        /// </summary>
        /// <returns></returns>
        private void DisplayZumen()
        {
            if (rirekiList.Items.Count > 0)
            {
                var filePath = "";
                var fileName = "";
                if (rirekiList.SelectedIndex != -1)
                {
                    filePath = RirekiDataList.ElementAt(rirekiList.SelectedIndex).ZumenPath;
                    fileName = RirekiDataList.ElementAt(rirekiList.SelectedIndex).FileName;
                    if (File.Exists(filePath) && filePath.Contains("図面データ"))
                    {
                        axDeskCtrlRireki.LoadFile(ZUMEN_URL + fileName.Replace("#", "%23") + ".xdw");
                        toolStripStatusLabel1.Text = "";
                    }
                    else if (File.Exists(filePath) && filePath.Contains("履歴データ"))
                    {
                        axDeskCtrlRireki.LoadFile(RIREKI_URL + fileName.Replace("#", "%23") + ".xdw");
                        toolStripStatusLabel1.Text = "";
                    }
                    else
                    {
                        // 図面ファイルが存在しなければ表示しない
                        axDeskCtrlRireki.LoadFile(MainDisp.FILE_URL + "図面データ/NoImarge.xdw");
                        toolStripStatusLabel1.Text = "ファイルが登録されていません。";
                    }
                }
                else
                {
                    // 履歴データを選択していない場合、先頭の履歴を表示させる
                    filePath = RirekiDataList.ElementAt(0).ZumenPath;
                    fileName = RirekiDataList.ElementAt(0).FileName;
                    if (File.Exists(RirekiDataList.ElementAt(0).ZumenPath))
                    {
                        axDeskCtrlRireki.LoadFile(ZUMEN_URL + fileName.Replace("#", "%23") + ".xdw");
                        toolStripStatusLabel1.Text = "";
                    }
                    else
                    {
                        // 図面ファイルが存在しなければ表示しない
                        axDeskCtrlRireki.LoadFile(MainDisp.FILE_URL + "図面データ/NoImarge.xdw");
                        toolStripStatusLabel1.Text = "ファイルが登録されていません。";
                    }
                }
                SetPropertyDeskCtr();
            }
        }

        /// <summary>
        /// 図面表示(問い合わせ図面)
        /// </summary>
        /// <returns></returns>
        private void DisplayQueryZumen()
        {
            if (queryList.Items.Count > 0)
            {
                var filePath = "";
                var fileName = "";
                if (queryList.SelectedIndex != -1)
                {
                    filePath = QueryDataList.ElementAt(queryList.SelectedIndex).ZumenPath;
                    fileName = QueryDataList.ElementAt(queryList.SelectedIndex).FileName;
                    if (File.Exists(filePath) && filePath.Contains("図面データ"))
                    {
                        axDeskCtrlQuery.LoadFile(ZUMEN_URL + fileName.Replace("#", "%23") + ".xdw");
                        toolStripStatusLabel1.Text = "";
                    }
                    else if (File.Exists(filePath) && filePath.Contains("問い合わせデータ"))
                    {
                        axDeskCtrlQuery.LoadFile(QUERY_URL + fileName.Replace("#", "%23") + ".xdw");
                        toolStripStatusLabel1.Text = "";
                    }
                    else
                    {
                        // 図面ファイルが存在しなければ表示しない
                        axDeskCtrlQuery.LoadFile(MainDisp.FILE_URL + "図面データ/NoImarge.xdw");
                        toolStripStatusLabel1.Text = "ファイルが登録されていません。";
                    }
                }
                else
                {
                    // 履歴データを選択していない場合、先頭の履歴を表示させる
                    filePath = QueryDataList.ElementAt(0).ZumenPath;
                    fileName = QueryDataList.ElementAt(0).FileName;
                    if (File.Exists(QueryDataList.ElementAt(0).ZumenPath))
                    {
                        axDeskCtrlQuery.LoadFile(ZUMEN_URL + fileName.Replace("#", "%23") + ".xdw");
                        toolStripStatusLabel1.Text = "";
                    }
                    else
                    {
                        // 図面ファイルが存在しなければ表示しない
                        axDeskCtrlQuery.LoadFile(MainDisp.FILE_URL + "図面データ/NoImarge.xdw");
                        toolStripStatusLabel1.Text = "ファイルが登録されていません。";
                    }
                }
                SetPropertyDeskCtr();
            }
        }

        /// <summary>
        /// 図面情報表示
        /// </summary>
        /// <returns></returns>
        private void DisplayZumenInfo()
        {
            if (JkList == null)
            {
                return;
            }
            JissekiKakou jkData = new JissekiKakou();
            if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
            {
                jkData = JkList.ElementAt(0);
            }
            else
            {
                List<JissekiKakou> selJkList = new List<JissekiKakou>();
                selJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                jkData = selJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
            }
            txtOrderNo.Text = txtOrderNo2.Text = txtOrderNo3.Text = txtOrderNo4.Text = jkData.OrderNo;
            //「組立工数参照」ボタン活性
            if (!String.IsNullOrWhiteSpace(jkData.OrderNo))
            {
                btnRefKumiJob.Enabled = true;
            }
            else
            {
                btnRefKumiJob.Enabled = false;
            }
            txtZumenBan.Text = txtZumenBan2.Text = txtZumenBan3.Text = txtZumenBan4.Text = jkData.ZumenBan;
            txtProductName.Text = txtProductName2.Text = txtProductName3.Text = txtProductName4.Text = jkData.ProductName;
            txtKakoQty.Text = txtKakoQty2.Text = jkData.KakoQty;
            txtKakosaki.Text = txtKakosaki2.Text = jkData.Kakosaki;
            txtNouki.Text = txtNouki2.Text = jkData.Nouki;
            txtKyakuNouki.Text = txtKyakuNouki2.Text = jkData.KyakuNouki;
            txtSinchoku.Text = txtSinchoku2.Text = jkData.Sinchoku;
            txtZaiNyuka.Text = txtZaiNyuka2.Text = jkData.ZaiNyuka;
            txtZairyo.Text = txtZairyo2.Text = jkData.Zairyo;
            txtZaiSize.Text = txtZaiSize2.Text = jkData.ZaiSize;
            txtZaiQty.Text = txtZaiQty2.Text = jkData.ZaiQty;
            txtNyukaDate.Text = txtNyukaDate2.Text = jkData.NyukaDate;
            txtBlockNo.Text = txtBlockNo2.Text = jkData.BlockNo;
            txtTantou.Text = txtTantou2.Text = jkData.Tantou;
            txtKakouTantou.Text = jkData.KakouTantou;
            txtKouteihyouNo.Text = jkData.KouteihyouNo;
            selTantouCd = jkData.TantouCd;
            announceList.Clear();
            if (jkData.isAdvance == 1)
            {
                announceList.Items.Insert(0, "先行手配");
            }
            if (jkData.isKumitate == 1)
            {
                announceList.Items.Insert(0, "組立有");
            }
        }

        /// <summary>
        /// 組立図面情報表示
        /// </summary>
        /// <returns></returns>
        private void DisplayKumiZumenInfo()
        {
            this.KumiFlg = true;
            if (KumiList == null || CldList == null)
            {
                this.KumiFlg = false;
                return;
            }
            PartsList ptData = new PartsList();
            // 未選択の場合は先頭の図面を表示
            if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
            {
                ptData = KumiList.ElementAt(0);
                // ツリービューの先頭ノードを取得する
                TreeNode objTopNode = zumenList.TopNode;
                // 先頭ノードを選択状態に変更する
                zumenList.SelectedNode = objTopNode;
            }
            else
            {
                var tag = CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Split('_');
                if (tag.Length != 4)
                {
                    return;
                }
                if (tag[3] == "k")
                {
                    // 組立図の場合
                    List<PartsList> selKumiList = KumiList.FindAll(x => x.Plid == tag[0]);
                    ptData = selKumiList.Find(x => x.Rownum == tag[1]);
                }
                else
                {
                    // 部品図の場合
                    List<PartsList> selCldList = CldList.FindAll(x => x.Plid == tag[0]);
                    ptData = selCldList.Find(x => x.Rownum == tag[1]);
                }
            }
            // 図面情報記載
            txtOrderNo.Text = txtOrderNo2.Text = txtOrderNo4.Text = ptData.OrderNo;
            //「組立工数参照」ボタン活性
            if (!String.IsNullOrWhiteSpace(ptData.OrderNo))
            {
                btnRefKumiJob.Enabled = true;
            }
            else
            {
                btnRefKumiJob.Enabled = false;
            }
            txtZumenBan.Text = txtZumenBan2.Text = txtZumenBan4.Text = ptData.ZumenBan;
            txtProductName.Text = txtProductName2.Text = txtProductName4.Text = ptData.ProductName;
            txtKakoQty.Text = ptData.Qty;
            txtKakosaki.Text = ptData.kakoSaki;
        }

        /// <summary>
        /// 履歴情報表示
        /// </summary>
        /// <returns></returns>
        private void DisplayRirekiInfo(int selIndex)
        {
            txtOrderNo2.Text = RirekiDataList.ElementAt(selIndex).OrderNo;
            txtZumenBan2.Text = RirekiDataList.ElementAt(selIndex).ZumenBan;
            txtUpdater.Text = RirekiDataList.ElementAt(selIndex).UpdateDater;
            txtUpdateDatetime.Text = RirekiDataList.ElementAt(selIndex).UpdateDateTime.ToString();
        }

        /// <summary>
        /// 問い合わせ情報表示
        /// </summary>
        /// <returns></returns>
        private void DisplayQueryInfo(int selIndex)
        {
            txtOrderNo4.Text = QueryDataList.ElementAt(selIndex).OrderNo;
            txtZumenBan4.Text = QueryDataList.ElementAt(selIndex).ZumenBan;
            txtUpdater2.Text = QueryDataList.ElementAt(selIndex).UpdateDater;
            txtUpdateDatetime2.Text = QueryDataList.ElementAt(selIndex).UpdateDateTime.ToString();
        }

        /// <summary>
        /// TreeViewデータ選択時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zumenList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (zumenList.Nodes.Count == 0 || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
            {
                return;
            }
            if (JkList != null && JkList.Count != 0 && !this.KumiFlg)
            {
                var filePath = "";
                var fileName = "";
                List<JissekiKakou> selJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                JissekiKakou jkData = selJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                if (zumenList.SelectedNode.Index != -1)
                {
                    filePath = jkData.ZumenPath;
                    fileName = jkData.FileName;
                    if (jkData.isMultiZumen)
                    {
                        //TabControlをオーナードローする
                        tabControl.DrawMode = TabDrawMode.OwnerDrawFixed;
                        //DrawItemイベントハンドラを追加
                        tabControl.DrawItem += new DrawItemEventHandler(TabControl1_DrawItem);
                    }
                    else
                    {
                        //TabControlをノーマルにする
                        tabControl.DrawMode = TabDrawMode.Normal;
                    }
                    // 「表面処理完了」の場合は「追加工中」にするかどうかのダイアログ表示(追加工対応)
                    if (jkData.Sinchoku == "表面処理完了")
                    {
                        DialogResult dr = MessageBox.Show("作業状況を「追加工中」にしますか？", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                        if (dr == DialogResult.Yes)
                        {
                            // 作業状況を「追加工中」に設定
                            SetTuiKakouChu(jkData);
                            JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7)).Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8)).Sinchoku = "追加工中";
                        }
                    }
                    // 図面情報表示
                    DisplayZumenInfo();
                }
                if (filePath != null && filePath.Length > 0)
                {
                    if (!this.ZumenDispFlg)
                    {
                        // 図面表示
                        axDeskCtrlZumen.LoadFile(ZUMEN_URL + fileName.Replace("#", "%23") + ".xdw");
                    }
                    // hidden図面
                    KumiZumen.Text = zumenJk(jkData);
                    toolStripStatusLabel1.Text = "";
                }
                else
                {
                    // ファイルが存在しなければNOIMAGE画像表示
                    axDeskCtrlZumen.LoadFile(MainDisp.FILE_URL + "図面データ/NoImarge.xdw");
                    toolStripStatusLabel1.Text = "ファイルが登録されていません。";
                }
            }
            else if ((KumiList != null && KumiList.Count != 0) || (CldList != null && CldList.Count != 0))
            {
                var filePath = "";
                var fileName = "";
                var tag = CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Split('_');
                if (tag.Length != 4)
                {
                    return;
                }
                PartsList selData = new PartsList();
                if (tag[3] == "k")
                {
                    // 組立図
                    List<PartsList> selKumiList = KumiList.FindAll(x => x.Plid == tag[0]);
                    selData = selKumiList.Find(x => x.Rownum == tag[1]);
                }
                else
                {
                    // 部品図
                    List<PartsList> selCldList = CldList.FindAll(x => x.Plid == tag[0]);
                    selData = selCldList.Find(x => x.Rownum == tag[1]);
                }
                if (zumenList.SelectedNode.Index != -1)
                {
                    filePath = selData.ZumenPath;
                    fileName = selData.FileName;
                    // 図面情報表示
                    DisplayKumiZumenInfo();
                }
                if (selData.ZumenPath != null && selData.ZumenPath.Length > 0)
                {
                    if (!this.ZumenDispFlg)
                    {
                        // 図面表示
                        axDeskCtrlZumen.LoadFile(ZUMEN_URL + fileName.Replace("#", "%23") + ".xdw");
                    }
                    // hidden図面
                    KumiZumen.Text = zumenPt(selData);
                    toolStripStatusLabel1.Text = "";
                }
                else
                {
                    // ファイルが存在しなければNOIMAGE画像表示
                    axDeskCtrlZumen.LoadFile(MainDisp.FILE_URL + "図面データ/NoImarge.xdw");
                    toolStripStatusLabel1.Text = "ファイルが登録されていません。";
                }
            }

            SetPropertyDeskCtr();
        }

        /// <summary>
        /// 作業状況を「追加工中」に設定
        /// </summary>
        /// <returns></returns>
        private void SetTuiKakouChu(JissekiKakou jkData)
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                con.Open();

                var cmd = new NpgsqlCommand();

                var sqlCmdTxt = "UPDATE 検査実績データ  SET 作業状況 = 8 WHERE \"オーダーNO\" = '" + jkData.OrderNo + "' AND グループ = '" + jkData.Group + "' AND 連番 = " + jkData.Renno;

                cmd = new NpgsqlCommand(sqlCmdTxt, con);

                var result = cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// 組立図検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KumiSearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                scanFlg = false;
                // 検索ダイアログ表示
                var a = new SearchEntryKumi();
                a.ShowDialog();

                if (KumiList != null && CldList != null && isKumiSearch)
                {
                    isKumiSearch = false;

                    // 図面情報,工程情報クリア
                    ClearZumenKouteiInfo();

                    // 図面リスト初期化
                    //JkList = null;

                    // 図面リスト作成
                    zumenList.Nodes.Clear();
                    BlockList = new List<string>();
                    var blNo = "";
                    var i = 0;
                    foreach (var item in KumiList)
                    {
                        // 引合NOが変わるごとに組立図、部品図の項目追加
                        if (blNo != item.BlockNo)
                        {
                            // ブロック番号追加(組立図)
                            TreeNode prTn = new TreeNode();
                            prTn.Text = item.BlockNo + "(組立図)";
                            prTn.Tag = item.Plid + "_" + item.Rownum + "_" + item.BlockNo + "_k";
                            zumenList.Nodes.Add(prTn);
                            BlockList.Add(prTn.Text + "_" + item.BlockNo);
                            // ブロック番号追加(部品図)
                            TreeNode clTn = new TreeNode();
                            clTn.Text = "  " + "(部品図)";
                            clTn.Tag = item.Plid + "_" + item.Rownum + "_" + item.BlockNo + "_k";
                            zumenList.Nodes.Add(clTn);
                            BlockList.Add(clTn.Text + "_" + item.BlockNo);
                            if (blNo != "")
                            {
                                i = i + 2;
                            }
                            blNo = item.BlockNo;

                            List<PartsList> selCldList = CldList.FindAll(x => x.BlockNo == blNo);
                            foreach (var subItem in selCldList)
                            {
                                TreeNode zubanSubTn = new TreeNode();
                                zubanSubTn.Text = subItem.ZumenBan;
                                zubanSubTn.Tag = subItem.Plid + "_" + subItem.Rownum + "_" + subItem.BlockNo + "_b";
                                //組立図に紐づく部品図を追加
                                zumenList.Nodes[i + 1].Nodes.Add(zubanSubTn);
                            }
                        }
                        TreeNode zubanTn = new TreeNode();
                        zubanTn.Text = item.ZumenBan;
                        zubanTn.Tag = item.Plid + "_" + item.Rownum + "_" + item.BlockNo + "_k";
                        //組立図追加
                        zumenList.Nodes[i].Nodes.Add(zubanTn);
                    }

                    if (File.Exists(KumiList.ElementAt(0).ZumenPath))
                    {
                        if (!this.ZumenDispFlg)
                        {
                            axDeskCtrlZumen.LoadFile(ZUMEN_URL + KumiList.ElementAt(0).FileName.Replace("#", "%23") + ".xdw");
                        }
                        toolStripStatusLabel1.Text = "";
                    }
                    SetPropertyDeskCtr();
                    // 図面情報表示
                    DisplayKumiZumenInfo();
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        /// <summary>
        /// 図面工程情報クリア
        /// </summary>
        /// <returns></returns>
        private void ClearZumenKouteiInfo()
        {
            //図面情報クリア
            txtSinchoku.Clear();
            txtNouki.Clear();
            txtKyakuNouki.Clear();
            txtZaiNyuka.Clear();
            txtZairyo.Clear();
            txtZaiSize.Clear();
            txtZaiQty.Clear();
            txtNyukaDate.Clear();
            txtBlockNo.Clear();
            txtTantou.Clear();
            txtKakouTantou.Clear();

            txtOrderNo3.Clear();
            txtProductName3.Clear();
            txtZumenBan3.Clear();
            txtNouki2.Clear();
            txtKyakuNouki2.Clear();
            txtKakosaki2.Clear();
            txtSinchoku2.Clear();
            txtKakoQty2.Clear();
            txtZaiNyuka2.Clear();
            txtZairyo2.Clear();
            txtZaiSize2.Clear();
            txtZaiQty2.Clear();
            txtNyukaDate2.Clear();
            txtBlockNo2.Clear();
            txtTantou2.Clear();

            announceList.Clear();

            //関連図番クリア
            relatedZumenInfoList.Rows.Clear();

            //工程情報クリア
            kouteiInfoList.Rows.Clear();

            //同図面一覧クリア
            sameZumenList.Rows.Clear();
        }

        /// <summary>
        /// PDF化ボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pdfBtn_Click(object sender, EventArgs e)
        {
            var zumenPath = "";
            var zumenFile = "";
            if (((JkList.Count == 0 || JkList == null) && (KumiList == null || CldList == null))
                || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
            {
                return;
            }
            else if (JkList != null && JkList.Count != 0 && !this.KumiFlg)
            {
                List<JissekiKakou> selJkList = new List<JissekiKakou>();
                JissekiKakou jkData = new JissekiKakou();
                if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                {
                    jkData = JkList.ElementAt(0);
                }
                else
                {
                    selJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                    jkData = selJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                }
                zumenPath = jkData.ZumenPath;
                zumenFile = jkData.FileName;
            }
            else if (KumiList != null && CldList != null)
            {
                List<PartsList> selPtList = new List<PartsList>();
                PartsList ptData = new PartsList();
                if (zumenList.SelectedNode == null || zumenList.SelectedNode.Index == -1)
                {
                    ptData = KumiList.ElementAt(0);
                }
                else
                {
                    var tag = CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Split('_');
                    if (tag.Length != 4)
                    {
                        return;
                    }
                    if (tag[3] == "k")
                    {
                        // 組立図
                        selPtList = KumiList.FindAll(x => x.Plid == tag[0]);
                        ptData = selPtList.Find(x => x.Rownum == tag[1]);
                    }
                    else
                    {
                        // 部品図
                        selPtList = CldList.FindAll(x => x.Plid == tag[0]);
                        ptData = selPtList.Find(x => x.Rownum == tag[1]);
                    }
                }
                zumenPath = ptData.ZumenPath;
                zumenFile = ptData.FileName;
            }

            try
            {
                Xdwapi.XDW_DOCUMENT_HANDLE handle = new Xdwapi.XDW_DOCUMENT_HANDLE();
                Xdwapi.XDW_OPEN_MODE_EX mode = new Xdwapi.XDW_OPEN_MODE_EX();
                mode.Option = Xdwapi.XDW_OPEN_READONLY;
                mode.AuthMode = Xdwapi.XDW_AUTH_NODIALOGUE;
                int api_result = 0;
                string localFilePath = "";
                localFilePath = Path.ChangeExtension(Path.GetTempFileName(), ".xdw");

                if (!File.Exists(zumenPath))
                {
                    MessageBox.Show("登録されていないファイルはPDF化できません。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                // PDFファイル出力先をデスクトップに設定する
                string outputFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\";
                // 既にファイルが存在する場合は削除
                System.IO.File.Delete(outputFilePath + zumenFile + ".xdw");
                // 共有フォルダ上の図面データをINIファイルで指定したパスにコピー
                System.IO.File.Copy(zumenPath, outputFilePath + zumenFile + ".xdw");
                api_result = Xdwapi.XDW_OpenDocumentHandle(outputFilePath + zumenFile + ".xdw", ref handle, mode);
                // エラー時、メッセージ表示
                if (api_result < 0)
                {
                    print_error(api_result);
                    return;
                }
                Xdwapi.XDW_DOCUMENT_INFO info = new Xdwapi.XDW_DOCUMENT_INFO();
                // Docuworksファイルの総ページ数取得
                Xdwapi.XDW_GetDocumentInformation(handle, ref info);
                // 既にファイルが存在する場合は削除
                System.IO.File.Delete(outputFilePath + zumenFile + ".pdf");
                Xdwapi.XDW_IMAGE_OPTION_PDF pdf = new Xdwapi.XDW_IMAGE_OPTION_PDF();
                // Docuworksファイルの総ページ分をPDF化
                pdf.EndOfMultiPages = info.Pages;
                Xdwapi.XDW_IMAGE_OPTION_EX ex = new Xdwapi.XDW_IMAGE_OPTION_EX();
                ex.Dpi = 200;
                ex.Color = Xdwapi.XDW_IMAGE_COLOR;
                ex.ImageType = Xdwapi.XDW_IMAGE_PDF;
                ex.DetailOption = pdf;
                // ドキュワークスファイルをPDF化
                api_result = Xdwapi.XDW_ConvertPageToImageFile(handle, 1, outputFilePath + zumenFile + ".pdf", ex);
                // エラー時、メッセージ表示
                if (api_result < 0)
                {
                    print_error(api_result);
                    return;
                }
                Xdwapi.XDW_CloseDocumentHandle(handle);
                MessageBox.Show("PDF出力しました。", "確認", MessageBoxButtons.OK, MessageBoxIcon.None);
                System.IO.File.Delete(outputFilePath + zumenFile + ".xdw");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString(), "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// PDF化エラー時表示するメッセージ
        /// </summary>
        /// <param name="code"></param>
        static void print_error(int code)
        {
            TextWriter errorWriter = Console.Error;
            string errMsg = "";
            switch (code)
            {
                case Xdwapi.XDW_E_NOT_INSTALLED:
                    errMsg = "DocuWorksがインストールされていません。";
                    break;
                case Xdwapi.XDW_E_FILE_NOT_FOUND:
                    errMsg = "指定されたファイルが見つかりません。";
                    break;
                case Xdwapi.XDW_E_FILE_EXISTS:
                    errMsg = "指定されたファイルはすでに存在します。";
                    break;
                case Xdwapi.XDW_E_ACCESSDENIED:
                case Xdwapi.XDW_E_INVALID_NAME:
                case Xdwapi.XDW_E_BAD_NETPATH:
                    errMsg = "指定されたファイルを開くことができません。";
                    break;
                case Xdwapi.XDW_E_BAD_FORMAT:
                    errMsg = "指定されたファイルは正しいフォーマットではありません。";
                    break;
                case Xdwapi.XDW_E_INVALID_ACCESS:
                    errMsg = "指定された操作をする権利がありません。";
                    break;
                default:
                    errMsg = "エラーが発生しました。";
                    break;
            }

            MessageBox.Show(errMsg, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// 図面リスト作成
        /// </summary>
        private void makeZumenList()
        {
            zumenList.Nodes.Clear();
            TreeNode lastTn = new TreeNode();

            TreeNode rootNode1 = new TreeNode("スキャン");
            TreeNode rootNode2 = new TreeNode("図面検索");

            if (this.KumiFlg)
            {
                // 図面リスト作成
                zumenList.Nodes.Clear();
                this.KumiFlg = false;
            }

            foreach (var item in JkList)
            {
                TreeNode tn = new TreeNode();
                tn.Text = item.ZumenBan;
                tn.Tag = item.OrderNo + "_" + item.Renno;
                if (item.SearchMethod == 1)
                {
                    rootNode1.Nodes.Add(tn);
                }
                else
                {
                    rootNode2.Nodes.Add(tn);
                }
                lastTn = tn;
            }

            zumenList.Nodes.Add(rootNode1);
            zumenList.Nodes.Add(rootNode2);

            zumenList.SelectedNode = lastTn;
            // デフォルトで最後の図面を表示する
            if (File.Exists(JkList.ElementAt(JkList.Count - 1).ZumenPath))
            {
                if (!this.ZumenDispFlg)
                {
                    axDeskCtrlZumen.LoadFile(ZUMEN_URL + JkList.ElementAt(JkList.Count - 1).FileName.Replace("#", "%23") + ".xdw");
                }
                SetPropertyDeskCtr();
                toolStripStatusLabel1.Text = "";
            }

            if (JkList.ElementAt(JkList.Count - 1).isMultiZumen)
            {
                //TabControlをオーナードローする
                tabControl.DrawMode = TabDrawMode.OwnerDrawFixed;
                //DrawItemイベントハンドラを追加
                tabControl.DrawItem += new DrawItemEventHandler(TabControl1_DrawItem);
            }
            else
            {
                //TabControlをノーマルにする
                tabControl.DrawMode = TabDrawMode.Normal;
            }
        }

        /// <summary>
        /// 図面ファイルの存在チェック(実績加工データ)
        /// </summary>
        /// <param name="jk"></param>
        /// <returns></returns>
        private string zumenJk(JissekiKakou jk)
        {
            if (jk.ZumenPath != null && jk.ZumenPath.Length > 0)
            {
                return ZUMEN_URL + jk.FileName + ".xdw";
            }
            else
            {
                return ERROR_FILE;
            }
        }

        /// <summary>
        /// 図面ファイルの存在チェック(パーツリスト)
        /// </summary>
        /// <param name="pt"></param>
        /// <returns></returns>
        private string zumenPt(PartsList pt)
        {
            if (pt.ZumenPath != null && pt.ZumenPath.Length > 0)
            {
                return ZUMEN_URL + pt.FileName + ".xdw";
            }
            else
            {
                return ERROR_FILE;
            }
        }

        /// <summary>
        /// 組立メニュー押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnKumiMenu_Click(object sender, EventArgs e)
        {
            try
            {
                CalcWorkTime.orderNo = txtOrderNo.Text;
                PartsListDisp.orderNo = txtOrderNo.Text;
                if (zumenList == null || zumenList.Nodes.Count == 0 || zumenList.SelectedNode == null)
                {
                    CalcWorkTime.blockNo = "";
                }
                else
                {
                    var tag = CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Split('_');
                    if (tag.Length != 4)
                    {
                        CalcWorkTime.blockNo = "";
                    }
                    else
                    {
                        CalcWorkTime.blockNo = tag[2];
                    }
                }

                // 組立メニューダイアログ表示
                var a = new KumiMenu();
                a.Show();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        /// <summary>
        /// 関連図番一覧を抽出する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="jkData"></param>
        /// <returns></returns>
        private bool GetZumenInfo(string connectionString, JissekiKakou jkData)
        {
            var cmd = new NpgsqlCommand();

            var i = 0;
            var selIndex = 0;
            var relatedIndexList = new List<Int32>();
            relatedZumenInfoList.Rows.Clear();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  jkkd.\"オーダーNO\"" +
                                "  ,jkkd.連番" +
                                "  ,jkkd.グループ" +
                                "  ,jkkd.図面番号" +
                                "  ,jkkd.品名" +
                                "  ,jkkd.数量 as 加工数量" +
                                "  ,jkkd.納期" +
                                "  ,ssm.仕入先略名 as 加工先" +
                                "  ,sjm.作業状況名" +
                                " from 実績加工データ jkkd" +
                                " left join 仕入先マスタ ssm" +
                                "   on jkkd.加工先コード = ssm.仕入先コード" +
                                " left join 検査実績データ kjd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 作業状況マスタ sjm" +
                                "   on sjm.作業状況コード = kjd.作業状況" +
                                " where jkkd.\"オーダーNO\" = UPPER(:orderNo)" +
                                " order by jkkd.連番";
                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = txtOrderNo.Text });

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        var orderNo = CommonModule.ConvertToString(result["オーダーNO"]);
                        var group = CommonModule.ConvertToString(result["グループ"]);
                        var renno = CommonModule.ConvertToString(result["連番"]);
                        var zumenBan = CommonModule.ConvertToString(result["図面番号"]);
                        var productName = CommonModule.ConvertToString(result["品名"]);
                        var kakoQty = CommonModule.ConvertToString(result["加工数量"]);
                        var kakosaki = CommonModule.ConvertToString(result["加工先"]);
                        var nouki = CommonModule.ConvertToDateString(result["納期"]);
                        var sinchoku = CommonModule.ConvertToString(result["作業状況名"]);

                        // データの追加
                        relatedZumenInfoList.Rows.Add(orderNo, productName, zumenBan, kakoQty,
                            nouki, kakosaki, sinchoku);
                        if (jkData.OrderNo == orderNo && jkData.Group == group && jkData.Renno == renno)
                        {
                            selIndex = i;
                        }
                        if (jkData.ZumenBan.IndexOf("#") == -1)
                        {
                            if (zumenBan.Contains(jkData.ZumenBan + "#"))
                            {
                                relatedIndexList.Add(i);
                            }
                        }
                        else
                        {
                            var zubanStr = jkData.ZumenBan.Split('#');
                            if (zumenBan.Contains(zubanStr[0] + "#") || zumenBan == zubanStr[0])
                            {
                                relatedIndexList.Add(i);
                            }
                        }
                        i++;
                    }
                    // 該当データ行は選択済みにして背景色を変える
                    relatedZumenInfoList.Rows[selIndex].Selected = true;
                    relatedZumenInfoList.Rows[selIndex].DefaultCellStyle.BackColor = Color.Aquamarine;
                    relatedZumenInfoList.CurrentCell = relatedZumenInfoList[0, selIndex];
                    foreach (var j in relatedIndexList)
                    {
                        relatedZumenInfoList.Rows[j].DefaultCellStyle.BackColor = Color.Aquamarine;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 工程情報一覧を抽出する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="orderNo"></param>
        /// <param name="group"></param>
        /// <param name="renno"></param>
        /// <returns></returns>
        private List<KouteiHyoInfo> GetKouteiInfo(string connectionString, string orderNo, string group, int renno)
        {
            var KouteiList = new List<KouteiHyoInfo>();
            var cmd = new NpgsqlCommand();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  工程順" +
                                "  ,仕入先略名" +
                                "  ,工程名" +
                                "  ,発注予定日" +
                                "  ,備考" +
                                " from 工程表工程順ビュー" +
                                " where \"オーダーNO\" = UPPER(:orderNo)" +
                                "   and グループ = :group" +
                                "   and 連番 = :renno" +
                                " order by 工程順";
                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("orderNo", DbType.String) { Value = orderNo });
                cmd.Parameters.Add(new NpgsqlParameter("group", DbType.String) { Value = group });
                cmd.Parameters.Add(new NpgsqlParameter("renno", DbType.Int32) { Value = renno });

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        var KouteiData = new KouteiHyoInfo();
                        KouteiData.KouteiJun = CommonModule.ConvertToString(result["工程順"]);
                        KouteiData.Kakosaki = CommonModule.ConvertToString(result["仕入先略名"]);
                        KouteiData.SagyoKoutei = CommonModule.ConvertToString(result["工程名"]);
                        KouteiData.YoteiDate = CommonModule.ConvertToDateString(result["発注予定日"]);
                        KouteiData.Biko = CommonModule.ConvertToString(result["備考"]);
                        KouteiList.Add(KouteiData);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    KouteiList = new List<KouteiHyoInfo>();
                    return KouteiList;
                }
            }
            return KouteiList;
        }

        /// <summary>
        /// 同図面一覧を抽出する
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="jkData"></param>
        /// <returns></returns>
        private bool GetSameZumenInfo(string connectionString, JissekiKakou jkData)
        {
            var cmd = new NpgsqlCommand();

            var i = 0;
            var selIndex = -1;
            var miKakouList = new List<Int32>();
            sameZumenList.Rows.Clear();

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();

                var sqlCmdTxt = "select" +
                                "  jkkd.\"オーダーNO\"" +
                                "  ,jkkd.連番" +
                                "  ,jkkd.グループ" +
                                "  ,jkkd.図面番号" +
                                "  ,jkkd.品名" +
                                "  ,jkkd.数量 as 加工数量" +
                                "  ,jkkd.納期" +
                                "  ,sgm.作業者名 as 前回作業者" +
                                "  ,ssm.仕入先略名 as 加工先" +
                                "  ,sjm.作業状況名" +
                                " from 実績加工データ jkkd" +
                                " left join 仕入先マスタ ssm" +
                                "   on jkkd.加工先コード = ssm.仕入先コード" +
                                " left join 検査実績データ kjd" +
                                "   using(\"オーダーNO\", グループ, 連番)" +
                                " left join 作業状況マスタ sjm" +
                                "   on sjm.作業状況コード = kjd.作業状況" +
                                " left join 図面作成データ zsd" +
                                "   on jkkd.図面番号 = zsd.図面番号" +
                                " left join 作業者マスタ sgm" +
                                "   on zsd.作業者 = sgm.作業者コード" +
                                " where jkkd.\"図面番号\" = UPPER(:zumenBan)" +
                                "   and jkkd.納期 >= now() + '-1 year'" +
                                " order by jkkd.連番";
                cmd = new NpgsqlCommand(sqlCmdTxt, con);
                cmd.Parameters.Add(new NpgsqlParameter("zumenBan", DbType.String) { Value = txtZumenBan.Text });

                try
                {
                    var result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        var orderNo = CommonModule.ConvertToString(result["オーダーNO"]);
                        var group = CommonModule.ConvertToString(result["グループ"]);
                        var renno = CommonModule.ConvertToString(result["連番"]);
                        var zumenBan = CommonModule.ConvertToString(result["図面番号"]);
                        var productName = CommonModule.ConvertToString(result["品名"]);
                        var kakoQty = CommonModule.ConvertToString(result["加工数量"]);
                        var kakosaki = CommonModule.ConvertToString(result["加工先"]);
                        var nouki = CommonModule.ConvertToDateString(result["納期"]);
                        var lastWorker = CommonModule.ConvertToString(result["前回作業者"]);
                        var sinchoku = CommonModule.ConvertToString(result["作業状況名"]);
                        // データの追加
                        sameZumenList.Rows.Add(orderNo, productName, zumenBan, kakoQty,
                            nouki, lastWorker, kakosaki, sinchoku);
                        if (jkData.OrderNo == orderNo && jkData.Group == group && jkData.Renno == renno)
                        {
                            selIndex = i;
                        }
                        i++;
                    }
                    if (selIndex != -1)
                    {
                        // 該当データ行は選択済みにして背景色を変える
                        sameZumenList.Rows[selIndex].Selected = true;
                        sameZumenList.Rows[selIndex].DefaultCellStyle.BackColor = Color.Aquamarine;
                        sameZumenList.CurrentCell = sameZumenList[0, selIndex];
                    }
                    // 未加工のデータを赤にする
                    foreach (var j in miKakouList)
                    {
                        sameZumenList[7, j].Style.BackColor = Color.Red;
                        sameZumenList[7, j].Style.ForeColor = Color.White;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"[err]{e.Message}");
                    return false;
                }
            }
            return true;
        }

        // リストから削除ボタン押下時処理
        private void btnDel_Click(object sender, EventArgs e)
        {
            if (JkList.Count == 0 || JkList == null || zumenList.SelectedNode.Text.Equals("スキャン") || zumenList.SelectedNode.Text.Equals("図面検索"))
            {
                return;
            }
            // 組立図検索後は無効
            if (KumiList != null && KumiList.Count != 0 && CldList != null && CldList.Count != 0)
            {
                return;
            }

            // スキャンデータが存在するか
            List<JissekiKakou> list = new List<JissekiKakou>();
            foreach (var item in JkList)
            {
                if (item.SearchMethod == 1)
                {
                    list.Add(item);
                }
            }

            // 選択中のデータ対象。リストの数を0にできない
            if (zumenList.SelectedNode != null && zumenList.SelectedNode.Index >= 0 && (!zumenList.SelectedNode.Parent.Text.Equals("図面検索") && list.Count > 1))
            {

                DialogResult dr = MessageBox.Show(zumenList.SelectedNode.Text + "をリストから削除してもよろしいですか？", "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                if (dr == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
                List<JissekiKakou> delJkList = JkList.FindAll(x => x.OrderNo == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(0, 7));
                JissekiKakou jkDelData = delJkList.Find(x => x.Renno == CommonModule.ConvertToString(zumenList.SelectedNode.Tag).Substring(8));
                TreeNode tn = new TreeNode();
                tn.Text = zumenList.SelectedNode.Text;
                tn.Tag = zumenList.SelectedNode.Tag;
                // データリストから削除
                JkList.Remove(jkDelData);
                // 表示リストから削除
                zumenList.Nodes.Remove(zumenList.SelectedNode);
            }
        }

        // 「図面問い合わせ」ボタン押下時処理
        private void btnQueryMail_Click(object sender, EventArgs e)
        {
            if (queryList.Items.Count == 0)
            {
                return;
            }
            if (queryList.SelectedIndex > -1)
            {
                SendQueryMail.orderNo = txtOrderNo4.Text;
                SendQueryMail.zumenBan = QueryDataList.ElementAt(queryList.SelectedIndex).ZumenBan;
                SendQueryMail.zumenPath = QueryDataList.ElementAt(queryList.SelectedIndex).ZumenPath;
            }
            else
            {
                SendQueryMail.orderNo = txtOrderNo4.Text;
                SendQueryMail.zumenBan = QueryDataList.ElementAt(0).ZumenBan;
                SendQueryMail.zumenPath = QueryDataList.ElementAt(0).ZumenPath;
            }
            // 図面ファイルが存在しない場合、エラーとする
            if (!File.Exists(SendQueryMail.zumenPath))
            {
                MessageBox.Show("図面ファイルがありません。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }
            // 図面問い合わせメール送信ダイアログ表示
            var a = new SendQueryMail();
            a.ShowDialog();
        }

        //伝達事項ボタン押下時処理
        private void btnTransmission_Click(object sender, EventArgs e)
        {
            try
            {
                // 事務所伝達事項メール送信ダイアログ表示
                var a = new SendTransmissionMail();
                a.ShowDialog();
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        /// <summary>
        /// 図面ビューアーを閉じるときに処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainDisp_FormClosing(object sender, FormClosingEventArgs e)
        {
            //ローカルコンピュータ上で実行されている"dwviewer"という名前の
            //すべてのプロセスを取得
            System.Diagnostics.Process[] ps =
                System.Diagnostics.Process.GetProcessesByName("dwviewer");
            // Docuworksファイルが開かれているかチェック
            bool isOpnDocu = false;
            foreach (System.Diagnostics.Process p in ps)
            {
                isOpnDocu = true;
            }
            if (isOpnDocu)
            {
                //メッセージボックスを表示する
                MessageBox.Show("編集中のDocuworksファイルを閉じてください。",
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                e.Cancel = true;
                return;
            }

            // ダイアログを表示する
            DialogResult result = MessageBox.Show("図面ビューアーアプリを終了しますか？", "アプリの終了", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
            {
                // はいボタンをクリックしたときはウィンドウを閉じる
                e.Cancel = true;
            }
            else
            {
                // 図面リストが存在する場合履歴を残すか確認する
                if (JkList != null && JkList.Count != 0)
                {
                    // スキャンデータが存在するか
                    bool flg = false;
                    foreach (var item in JkList)
                    {
                        if (item.SearchMethod == 1)
                        {
                            flg = true;
                        }
                    }

                    if (flg)
                    {
                        result = MessageBox.Show("図面履歴を保存しますか？", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            // 工程Noを保存します。
                            SaveKouteiNo();
                        }
                        else
                        {
                            // 工程Noをクリアします。
                            ClearKouteiNo();
                        }
                    }
                }
            }
        }

        // 工程Noを保存します。
        private void SaveKouteiNo()
        {
            var count = 0;
            foreach (var dto in JkList)
            {
                if (dto.SearchMethod == 1)
                {
                    if (count == 0)
                    {
                        Properties.Settings.Default.KouteiNo = dto.KouteihyouNo;
                    }
                    else if (count < 30)
                    {
                        Properties.Settings.Default.KouteiNo = Properties.Settings.Default.KouteiNo + "/" + dto.KouteihyouNo;
                    }
                    count++;
                }
            }
            Properties.Settings.Default.Save();
        }

        // 工程Noをクリアします。
        private void ClearKouteiNo()
        {
            Properties.Settings.Default.KouteiNo = string.Empty;
            Properties.Settings.Default.Save();
        }

        #region 図面作業者登録

        /// <summary>
        /// 図面作業者検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                // 入力チェック
                if (ValidateCheck())
                {
                    // 図面作業者データ取得
                    List<WorkingRegister> list = GetWorkingRegister();
                    this.dataGridView1.DataSource = list;
                    this.label18.Text = list.Count + "件検索しました";

                    Properties.Settings.Default.LoginCd = userCd.Text;
                    Properties.Settings.Default.Save();
                }

            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Debug.WriteLine($"[err]{ex.Message}");
            }
        }

        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateCheck()
        {

            // 入力チェック
            bool validation = true;
            int i;
            if (!int.TryParse(this.userCd.Text, out i) && !String.IsNullOrWhiteSpace(this.userCd.Text))
            {
                this.errorProvider1.SetError(this.userCd, "数字を入力してください");
                this.userCd.Focus();
                validation = false;
                return validation;
            }

            if (validation)
            {
                Properties.Settings.Default.LoginCd = NameTextBox.Text;

                Properties.Settings.Default.Save();
                this.errorProvider1.Clear();
            }

            return validation;

        }

        /// <summary>
        /// 図面作業者データを取得します。
        /// </summary>
        private List<WorkingRegister> GetWorkingRegister()
        {

            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                var cmd = new NpgsqlCommand();

                con.Open();

                var strSelectSQL = "SELECT 図面番号,作業者,作業者名,作業登録日,図面作成データ.工程表番号 FROM 図面作成データ " +
                                "LEFT JOIN 作業者マスタ ON 図面作成データ.作業者 = 作業者マスタ.作業者コード ";

                var strWhereSQL = "WHERE 作業者 IS NOT NULL ";

                if (!String.IsNullOrWhiteSpace(this.zumenn_no.Text))
                {
                    strWhereSQL += " AND 図面番号 LIKE '" + this.zumenn_no.Text + "%'";
                }

                if (!String.IsNullOrWhiteSpace(this.userCd.Text))
                {
                    strWhereSQL += " AND 作業者 = '" + this.userCd.Text + "'";
                }

                var sqlCmdTxt = strSelectSQL + strWhereSQL + "ORDER BY 作業登録日 DESC";

                cmd = new NpgsqlCommand(sqlCmdTxt, con);

                var result = cmd.ExecuteReader();

                var count = 1;
                List<WorkingRegister> list = new List<WorkingRegister>();
                while (result.Read())
                {
                    WorkingRegister workingRegister = new WorkingRegister();
                    workingRegister.No = count;
                    workingRegister.DrawingNumber = CommonModule.ConvertToString(result["図面番号"]);
                    workingRegister.Worker = CommonModule.ConvertToString(result["作業者名"]);
                    workingRegister.WorkRegistrationDate = Convert.ToDateTime(result["作業登録日"]).ToString("yyyy/MM/dd");
                    workingRegister.KouteihyouNo = CommonModule.ConvertToString(result["工程表番号"]);
                    list.Add(workingRegister);
                    count++;
                }

                return list;

            }
        }

        /// <summary>
        /// 図面番号検索処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BarTextBox_TextChanged(object sender, EventArgs e)
        {
            if (this.BarTextBox.TextLength == 8)
            {
                try
                {
                    var drawingNumber = GetdrawingNumber();
                    if (String.IsNullOrWhiteSpace(drawingNumber))
                    {
                        this.BarLabel.Text = "図面番号が登録されていません";
                        this.BarLabel.ForeColor = Color.Red;
                        this.registerBtn.Enabled = InputBtnEnableCheck();
                    }
                    else
                    {
                        this.BarLabel.Text = drawingNumber;
                        this.BarLabel.ForeColor = Color.Blue;
                        if (InputBtnEnableCheck())
                        {
                            this.registerBtn.Enabled = true;
                            RegisterBtn_Click(sender, e);
                        }
                    }

                }
                catch (Exception ex)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show(ex.Message,
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    Debug.WriteLine($"[err]{ex.Message}");
                }
            }
            else
            {
                this.BarLabel.Text = "バーコードが違います";
                this.BarLabel.ForeColor = Color.Red;
                InputBtnEnableCheck();
                //BarTextBox.Focus();
            }
        }

        /// <summary>
        /// 登録ボタン使用可否判断
        /// </summary>
        private bool InputBtnEnableCheck()
        {

            if (this.BarLabel.ForeColor == Color.Blue && this.NameLabel.ForeColor == Color.Blue)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 図面番号取得
        /// </summary>
        /// <returns></returns>
        private string GetdrawingNumber()
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                var cmd = new NpgsqlCommand();

                con.Open();

                var sqlCmdTxt = "SELECT * FROM 実績加工データ as w WHERE 工程表番号 in( '" + BarTextBox.Text + "',UPPER('" + BarTextBox.Text + "'))";

                cmd = new NpgsqlCommand(sqlCmdTxt, con);

                var result = cmd.ExecuteReader();

                var drawingNumber = string.Empty;
                while (result.Read())
                {
                    drawingNumber = CommonModule.ConvertToString(result["図面番号"]);
                }

                return drawingNumber;

            }
        }

        /// <summary>
        /// 工程表スキャン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KouteiScanBtn_Click(object sender, EventArgs e)
        {
            this.BarTextBox.Text = ScanBarcode();
        }

        /// <summary>
        /// 名前スキャン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NameScanBtn_Click(object sender, EventArgs e)
        {
            this.NameTextBox.Text = ScanBarcode();
        }

        /// <summary>
        /// バーコードスキャン処理
        /// </summary>
        private string ScanBarcode()
        {
            // バーコード読み取り
            var barcodeReader = new BarcodeReader();
            barcodeReader.ShowDialog();

            return barcodeReader.Barcode;

        }

        /// <summary>
        /// 社員名取得処理。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (this.NameTextBox.TextLength == 3)
            {
                try
                {
                    var workingName = GetWorkingName();
                    if (String.IsNullOrWhiteSpace(workingName))
                    {
                        this.NameLabel.Text = "作業者未登録";
                        this.NameLabel.ForeColor = Color.Red;
                        this.registerBtn.Enabled = InputBtnEnableCheck();
                    }
                    else
                    {
                        this.NameLabel.Text = workingName;
                        this.NameLabel.ForeColor = Color.Blue;
                        this.BarTextBox.Focus();
                        Properties.Settings.Default.LoginCd = NameTextBox.Text;

                        Properties.Settings.Default.Save();

                        this.registerBtn.Enabled = InputBtnEnableCheck();
                    }

                }
                catch (Exception ex)
                {
                    //メッセージボックスを表示する
                    MessageBox.Show(ex.Message,
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    Debug.WriteLine($"[err]{ex.Message}");
                }
            }
            else
            {
                this.NameLabel.Text = "コードが違います";
                this.NameLabel.ForeColor = Color.Red;
                this.registerBtn.Enabled = InputBtnEnableCheck();
                this.NameTextBox.Focus();
            }
        }

        /// <summary>
        /// 社員名を取得します。
        /// </summary>
        /// <returns></returns>
        private string GetWorkingName()
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                var cmd = new NpgsqlCommand();

                con.Open();

                var sqlCmdTxt = "SELECT * FROM 作業者マスタ WHERE 作業者コード = '" + this.NameTextBox.Text + "'";

                cmd = new NpgsqlCommand(sqlCmdTxt, con);

                var result = cmd.ExecuteReader();

                var workingName = string.Empty;
                while (result.Read())
                {
                    workingName = CommonModule.ConvertToString(result["作業者名"]);
                }

                return workingName;

            }
        }

        /// <summary>
        /// 作業者を登録します。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegisterBtn_Click(object sender, EventArgs e)
        {
            var messageStr = "";
            if (this.NameTextBox.Text.Length == 3 && this.BarTextBox.Text.Length == 8)
            {
                //　図面データの取得
                var workingName = GetDrawingCreationData();

                using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
                {
                    con.Open();
                    // トランザクション開始
                    using (var tran = con.BeginTransaction())
                    {
                        try
                        {
                            if (String.IsNullOrWhiteSpace(workingName))
                            {
                                // 図面作成データを作成します。
                                InsertDrawingCreationData(con);
                                this.NameLabel.ForeColor = Color.Blue;
                                messageStr = this.BarLabel.Text + "が登録されました。";
                            }
                            else
                            {
                                // 図面作成データを更新します。
                                UpdateDrawingCreationData(con);
                                this.NameLabel.ForeColor = Color.Blue;
                                messageStr = this.BarLabel.Text + "が更新されました。";
                            }
                            // 図面作業者履歴を削除します。
                            DeleteDrawingWorkerHistory(con);
                            // 図面作業者履歴を作成します。
                            InsertDrawingWorkerHistory(con);
                            this.messageLabel.ForeColor = Color.FromKnownColor(KnownColor.ControlText);

                            // 登録内容を一覧に表示
                            WorkingRegister dto = new WorkingRegister();
                            no++;
                            dto.DrawingNumber = this.BarLabel.Text;
                            dto.Worker = this.NameLabel.Text;
                            dto.WorkRegistrationDate = DateTime.Now.ToString("yyyy/MM/dd");
                            dto.No = no;
                            dto.KouteihyouNo = this.BarTextBox.Text;
                            WorkingRegisterList.Insert(0, dto);

                            this.dataGridView1.DataSource = WorkingRegisterList;

                            this.BarTextBox.Text = String.Empty;
                            this.BarLabel.Text = String.Empty;

                            this.BarTextBox.Focus();
                            // コミット
                            tran.Commit();
                        }
                        catch (Exception ex)
                        {
                            // ロールバック
                            tran.Rollback();
                            messageStr = this.BarLabel.Text + "が登録・更新できませんでした。";
                            this.messageLabel.ForeColor = Color.Red;
                            throw ex;
                        }
                        finally
                        {
                            this.messageLabel.Text = messageStr;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 図面作業者履歴を削除します。
        /// </summary>
        private void DeleteDrawingWorkerHistory(NpgsqlConnection con)
        {
            var cmd = new NpgsqlCommand();

            var sqlCmdTxt = "DELETE FROM 図面作業者履歴 WHERE (\"オーダーNO\",グループ,連番) " +
                        " IN (SELECT \"オーダーNO\",グループ,連番 FROM 実績加工データ WHERE 工程表番号 " +
                        " IN ('" + this.BarTextBox.Text + "',UPPER('" + this.BarTextBox.Text + "')))";
            cmd = new NpgsqlCommand(sqlCmdTxt, con);

            var result = cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 図面作業者履歴を作成します。
        /// </summary>
        private void InsertDrawingWorkerHistory(NpgsqlConnection con)
        {

            var cmd = new NpgsqlCommand();

            var sqlCmdTxt = "INSERT INTO 図面作業者履歴 (\"オーダーNO\",グループ,連番,図面番号,担当者コード,登録日時)  " +
                            " SELECT \"オーダーNO\",グループ,連番,図面番号, '" + this.NameTextBox.Text + "',now() " +
                            " FROM 実績加工データ WHERE 工程表番号 IN('" + this.BarTextBox.Text + "',UPPER('" + this.BarTextBox.Text + "'))";
            cmd = new NpgsqlCommand(sqlCmdTxt, con);

            var result = cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 図面作成データを作成します。
        /// </summary>
        private void InsertDrawingCreationData(NpgsqlConnection con)
        {
            var cmd = new NpgsqlCommand();

            var sqlCmdTxt = "INSERT INTO 図面作成データ(図面番号,作業者,作業登録日,工程表番号) VALUES ('" + this.BarLabel.Text + "','" + this.NameTextBox.Text + "', now(),'" + BarTextBox.Text + "')";

            cmd = new NpgsqlCommand(sqlCmdTxt, con);

            var result = cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 図面作成データを更新します。
        /// </summary>
        private void UpdateDrawingCreationData(NpgsqlConnection con)
        {
            var cmd = new NpgsqlCommand();

            var sqlCmdTxt = "UPDATE 図面作成データ  SET 作業者 ='" + this.NameTextBox.Text + "' , 作業登録日= now() , 工程表番号 ='" + this.BarTextBox.Text + "' WHERE 図面番号 ='" + this.BarLabel.Text + "'";

            cmd = new NpgsqlCommand(sqlCmdTxt, con);

            var result = cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 図面作成データを取得します。
        /// </summary>
        /// <returns></returns>
        private string GetDrawingCreationData()
        {
            using (var con = new NpgsqlConnection(Properties.Settings.Default.connectionString))
            {
                var cmd = new NpgsqlCommand();

                con.Open();

                var sqlCmdTxt = "SELECT * FROM 図面作成データ WHERE 図面番号= '" + this.BarLabel.Text + "'";

                cmd = new NpgsqlCommand(sqlCmdTxt, con);

                var result = cmd.ExecuteReader();

                var workingName = string.Empty;
                while (result.Read())
                {
                    workingName = CommonModule.ConvertToString(result["作業者"]);
                }

                return workingName;

            }
        }

        /// <summary>
        /// 図面取得
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                DialogResult result = MessageBox.Show("図面・オーダー検索を行いますか?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No)
                {
                    return;
                }

                var kouteihyouNo = CommonModule.ConvertToString(dataGridView1[4, e.RowIndex].Value);
                if (!String.IsNullOrEmpty(kouteihyouNo))
                {
                    ReadZumenInfo(kouteihyouNo);
                    tabControl.SelectedIndex = ZUME_ORDER_SEARCH;
                }
                else
                {
                    //メッセージボックスを表示する
                    MessageBox.Show("工程表番号が登録されていないデータです",
                        "エラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        #endregion

        /// <summary>
        /// 組立工数Excelを開きます。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefKumiJob_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(txtOrderNo.Text))
                {
                    return;
                }
                // ダイアログを表示する
                DialogResult result = MessageBox.Show("組立工数ファイルを検索しますか？", "組立工数ファイルの検索", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No)
                {
                    return;
                }
                MessageBox.Show("検索中です。お待ちください・・・。", "確認", MessageBoxButtons.OK, MessageBoxIcon.None);

                // 端末にEverythingがインストールされているか確認
                Microsoft.Win32.RegistryKey rkeyEvt = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Everything");
                Microsoft.Win32.RegistryKey rkeyEvt2 = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Everything");
                if (rkeyEvt != null || rkeyEvt2 != null)
                {
                    List<string> partsList = new List<string>();
                    UInt32 i;
                    const int bufsize = 260;
                    StringBuilder buf = new StringBuilder(bufsize);
                    // LANDISKJIMUSYOの指定したオーダーNOのフォルダ内組立Excelを検索
                    Everything_SetSearch("\\\\LANDISKJIMUSYO\\disk\\G\\パーツリスト \\*" + txtOrderNo.Text + "* *組立*.xls*");
                    Everything_SetRequestFlags(EVERYTHING_REQUEST_FILE_NAME | EVERYTHING_REQUEST_PATH);
                    Everything_Query(true);
                    for (i = 0; i < Everything_GetNumResults(); i++)
                    {
                        Everything_GetResultFullPathName(i, buf, bufsize);
                        partsList.Add(buf.ToString());
                    }
                    RefKumiJob.files = partsList.ToArray();
                }
                else
                {
                    //オーダーNOの先頭1文字から検索するフォルダ名を取得
                    List<string> folderNameList = GetFolderName(txtOrderNo.Text.Substring(0, 1));
                    //特定のフォルダ内を検索
                    if (folderNameList.Count > 0)
                    {
                        List<string> partsList = new List<string>();
                        foreach (var folderName in folderNameList)
                        {
                            //富山村田はファイル数が多いためオーダーNOのついている直下のフォルダを検索
                            if (folderName == "富山村田")
                            {
                                string[] tmp = Directory.GetDirectories(@"\\landiskjimusyo\disk\G\パーツリスト\" + folderName, "*" + txtOrderNo.Text + "*", SearchOption.TopDirectoryOnly);
                                if (tmp.Length > 0)
                                {
                                    foreach (var dir in tmp)
                                    {
                                        partsList.AddRange(Directory.GetFiles(dir, "*組立*.xls*", SearchOption.AllDirectories).ToList());
                                    }
                                }
                                else
                                {
                                    partsList.AddRange(Directory.GetFiles(@"\\landiskjimusyo\disk\G\パーツリスト\" + folderName, "*組立*.xls*", SearchOption.AllDirectories).ToList());
                                }
                            }
                            else
                            {
                                partsList.AddRange(Directory.GetFiles(@"\\landiskjimusyo\disk\G\パーツリスト\" + folderName, "*組立*.xls*", SearchOption.AllDirectories).ToList());
                            }
                        }
                        RefKumiJob.files = partsList.ToArray();
                    }
                    //組立工数ファイル全体を検索
                    else
                    {
                        RefKumiJob.files = Directory.GetFiles(@"\\landiskjimusyo\disk\G\パーツリスト", "*組立*.xls*", SearchOption.AllDirectories);
                    }
                }

                if (RefKumiJob.files.Length == 1)
                {
                    //組立工数ファイルを起動する
                    System.Diagnostics.Process p =
                        System.Diagnostics.Process.Start(CommonModule.ConvertToString(RefKumiJob.files[0]));
                }
                else if (RefKumiJob.files.Length > 1)
                {
                    // 組立工数参照ダイアログ表示
                    var a = new RefKumiJob();
                    a.ShowDialog();
                }
                else
                {
                    MessageBox.Show("該当する組立工数ファイルがありません。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// オーダーNOの先頭1文字から検索するフォルダ名を取得
        /// </summary>
        /// <param name="iniChar"></param>
        /// <returns></returns>
        private List<string> GetFolderName(string iniChar)
        {
            List<string> folderNameList = new List<string>();
            switch (iniChar)
            {
                case "T":
                    folderNameList.Add("富山村田");
                    break;
                case "M":
                    folderNameList.Add("野洲");
                    folderNameList.Add("岡山村田");
                    break;
                case "K":
                    folderNameList.Add("金沢村田");
                    break;
                case "U":
                    folderNameList.Add("金津村田");
                    folderNameList.Add("小松村田");
                    folderNameList.Add("ワクラ村田");
                    break;
                case "F":
                    folderNameList.Add("鯖江村田");
                    break;
                case "N":
                    folderNameList.Add("ハクイ村田");
                    break;
                case "H":
                    folderNameList.Add("氷見村田");
                    break;
                case "I":
                    folderNameList.Add("石川サンケン");
                    break;
                case "V":
                    folderNameList.Add("コーセル");
                    break;
                case "D":
                    folderNameList.Add("東京電波");
                    break;
                case "B":
                    folderNameList.Add("社内向け設備");
                    break;
            }
            return folderNameList;
        }

        /// <summary>
        /// 履歴検索ボタン押下時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rirekiSearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                // 履歴検索ダイアログ表示
                var a = new SearchRireki();
                a.ShowDialog();

                // 検索ボタンが押下されたときに実行
                if (isRirekiSearch)
                {
                    isRirekiSearch = false;

                    var orderNo = RirekiDataList.ElementAt(0).OrderNo;
                    if (chkIncOtherOrder.Checked)
                    {
                        // 他オーダーNOも含めて履歴表示
                        RirekiDataList = FullRirekiDataList;
                    }
                    else
                    {
                        RirekiDataList = FullRirekiDataList.FindAll(x => x.OrderNo == orderNo || x.OrderNo == "9999999");
                    }
                    // 最後に編集した図面の履歴のみ表示にチェックあり
                    if (SearchRireki.JisKak.chkOnlyRecentRireki)
                    {
                        List<RirekiData> rirekiDataList = new List<RirekiData>();
                        // 元図面取得
                        rirekiDataList.Add(RirekiDataList.Find(x => x.ZumenPath.Contains("図面データ")));
                        // 最後に更新した図面を取得
                        rirekiDataList.Add(RirekiDataList.OrderByDescending(x => x.UpdateDateTime).FirstOrDefault());
                        RirekiDataList = rirekiDataList;
                    }
                    // 履歴リストにデータ追加
                    rirekiList.Items.Clear();
                    // 品名は固定
                    txtProductName2.Text = MainDisp.RirekiDataList.ElementAt(0).ProductName;
                    foreach (var item in MainDisp.RirekiDataList)
                    {
                        var updttime = "";
                        if (item.UpdateDateTime != "")
                        {
                            updttime = "(" + item.UpdateDateTime + ")";
                        }
                        rirekiList.Items.Add(item.ZumenBan + updttime);
                    }

                    if (rirekiList.Items.Count > 0 && rirekiList.SelectedIndex == -1)
                    {
                        rirekiList.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                //メッセージボックスを表示する
                MessageBox.Show(ex.Message,
                    "エラー",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 配線図面ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickBtnWiring(object sender, EventArgs e)
        {
            try
            {
                if(!LoginInfo.所属.Equals("システム課") && !LoginInfo.所属.Equals("配線課"))
                {
                    MessageBox.Show("配線課用の機能です。");
                    return;
                }

                if (this.View == null || this.View.IsDisposed)
                {
                    View = new WiringDrawing
                    {
                        WindowState = FormWindowState.Maximized
                    };
                    View.Show();
                    View.Activate();
                }
                else
                {
                    MessageBox.Show("配線図面画面は、すでに起動しています");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
            }
        }

        /// <summary>
        /// DeskCtrのプロパティを設定します
        /// </summary>
        private void SetPropertyDeskCtr()
        {
            List<AxDeskCtrl> axDeskCtrl = GetAllAxDeskCtrl(this);

            foreach (AxDeskCtrl ctrl in axDeskCtrl)
            {
                ctrl.fullscreen = "yes";
                ctrl.SetShowAnnotation(true);
                ctrl.SetSheafViewMode(true);
                ctrl.SetCharDrawAdvance(true);
                ctrl.SetZoomWithPage();
                ctrl.ShowStandardToolBar(false);
                ctrl.ShowSheafBar(false);
                ctrl.ShowStatusBar(false);
            }
        }

        /// <summary>
        /// AxDeskCtrlを取得します
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected List<AxDeskCtrl> GetAllAxDeskCtrl(Control parent)
        {
            List<AxDeskCtrl> controls = new List<AxDeskCtrl>();

            foreach (Control child in parent.Controls)
            {
                controls.AddRange(GetAllAxDeskCtrl(child));
            }

            if (parent is AxDeskCtrl)
            {
                controls.Add((AxDeskCtrl)parent);
            }

            return controls;
        }

        /// <summary>
        /// 他オーダーNOも含めて履歴表示チェックボックス押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkIncOtherOrder_CheckedChanged(object sender, EventArgs e)
        {
            if (RirekiDataList == null || RirekiDataList.Count == 0 || rirekiList.SelectedIndex == -1)
            {
                return;
            }
            var orderNo = RirekiDataList.ElementAt(0).OrderNo;
            // チェックありの場合、他オーダーNOの履歴も表示
            if (chkIncOtherOrder.Checked)
            {
                // 他オーダーNOも含めて履歴表示
                RirekiDataList = FullRirekiDataList;
            }
            else
            {
                RirekiDataList = FullRirekiDataList.FindAll(x => x.OrderNo == orderNo || x.OrderNo == "9999999");
            }
            // 最後に編集した図面の履歴のみ表示にチェックあり
            if (SearchRireki.JisKak.chkOnlyRecentRireki)
            {
                List<RirekiData> rirekiDataList = new List<RirekiData>();
                // 元図面取得
                rirekiDataList.Add(RirekiDataList.Find(x => x.ZumenPath.Contains("図面データ")));
                // 最後に更新した図面を取得
                rirekiDataList.Add(RirekiDataList.OrderByDescending(x => x.UpdateDateTime).FirstOrDefault());
                RirekiDataList = rirekiDataList;
            }
            // 履歴リストにデータ追加
            rirekiList.Items.Clear();
            foreach (var item in RirekiDataList)
            {
                var updttime = "";
                if (item.UpdateDateTime != "")
                {
                    updttime = "(" + item.UpdateDateTime + ")";
                }
                rirekiList.Items.Add(item.ZumenBan + updttime);
            }

            if (rirekiList.Items.Count > 0 && rirekiList.SelectedIndex == -1)
            {
                rirekiList.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 加工指示書ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnKakouIndication(object sender, EventArgs e)
        {
            try
            {
                if (this.View2 == null || this.View2.IsDisposed)
                {
                    View2 = new KakouIndication()
                    {
                        StartPosition = FormStartPosition.CenterParent,
                        KouteiNo = this.txtKouteihyouNo.Text,
                        WindowState = FormWindowState.Maximized
                    };
                    View2.Show();
                    View2.Activate();
                }
                else
                {
                    MessageBox.Show("加工指示書画面は、すでに起動しています");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
            }
        }

        /// <summary>
        /// 加工指示書一覧ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickBtnKakouIndicationList(object sender, EventArgs e)
        {
            try
            {
                BaseView.log.Info("0:" + DateTime.Now);
                if (this.View3 == null || this.View3.IsDisposed)
                {
                    View3 = new KakouIndicationList();
                    View3.Show();
                    View3.Activate();
                }
                else
                {
                    MessageBox.Show("加工指示書一覧画面は、すでに起動しています");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
            }
        }


    }
}
